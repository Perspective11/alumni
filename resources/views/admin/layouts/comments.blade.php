@if($commentsCount)
    <div class="row">
        <div class="col-md-offset-1 col-md-10">
            <div class="box box-warning">
                <div class="box-header with-border">
                    <h3 class="box-title">Comments</h3>
                    <div class="box-tx-ls pull-right">
                        <!-- Buttons, labels, and many other things can be placed here! -->
                        <!-- Here is a label for example -->
                        <span class="label label-warning"><i class="fa fa-comments"></i> {{ $commentsCount }}</span>
                    </div><!-- /.box-tools -->
                </div><!-- /.box-header -->
                <div class="box-body">
                    <div class="media-list">
                        @foreach($comments as $comment)
                            <div class="media" id="commment-{{ $comment->id }}">
                                <div class="media-left">
                                    <img src="{{ $comment->user->getPicture()}}" style="height: 50px" class="media-object img-rounded">
                                </div>
                                <div class="media-body">
                                    <h4 class="media-heading">{{ $comment->user->childOrUserName() }} <small><i>Posted {{ $comment->created_at->diffForHumans() }}</i></small></h4>
                                    <p>{{ $comment->body }}</p>

                                    <!-- Nested media object -->
                                    @foreach($comment->replies as $reply)
                                        <div class="media" id="commment-{{ $comment->id }}">
                                            <div class="media-left">
                                                <img src="{{ $reply->user->getPicture() }}"  style="height: 50px"  class="media-object img-rounded ">
                                            </div>
                                            <div class="media-body">
                                                <h4 class="media-heading">{{ $reply->user->childOrUserName() }} <small><i>Posted on {{ $reply->created_at->diffForHumans() }}</i></small></h4>
                                                <p>{{ $reply->body }}</p>
                                            </div>
                                        </div>
                                    @endforeach

                                </div>
                            </div>
                        @endforeach
                    </div>
                </div><!-- /.box-body -->
                <div class="box-footer">
                    {{ $comments->links() }}
                </div><!-- box-footer -->
            </div><!-- /.box -->
        </div>
    </div>
@endif