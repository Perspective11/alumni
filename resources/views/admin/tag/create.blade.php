@extends('adminlte::page')
@section('content')
    <div class="row">
        <form class="col-md-10 col-md-offset-1" action="{{ url('admin/tags') }}" method="post" accept-charset="UTF-8"
              enctype="multipart/form-data">
            {{ csrf_field() }}
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">Create Tags</h3>
                </div><!-- /.box-header -->
                <div id="tagsBox" class="box-body">
                    @php
                        $tagsCount = max(count(old('tags')) , 1);
                    @endphp
                    @for($i = 1; $i <= $tagsCount; $i++)
                        <div class="form-group col-md-3 {{  $errors->has('tags.' . $i) ? ' has-error' : ''  }}">
                            <label for="tags">Tag {{ $i }}:</label>
                            <input type="text" name="tags[{{ $i }}]" class="form-control" required
                                   value="{{ old('tags.' . $i) }}">
                        </div>
                    @endfor
                </div><!-- /.box-body -->
                <div class="box-footer">
                    <button type="button" id="addTag" class="btn btn-warning btn-flat"><i class="fa fa-tag"></i> Add Tag
                    </button>
                    <button type="submit" class="btn btn-success pull-right">Submit Tags</button>
                </div><!-- box-footer -->
            </div><!-- /.box -->
        </form>
    </div>
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            @include('errors.errors')
        </div>
    </div>
@endsection
@section('footer-ex')
    <script>
        var tagsCount = {{ $tagsCount }} ;
        $('#addTag').click(function (e) {
            tagsCount++;

            var htmlstring = "<div class='form-group col-md-3'>" +
                "               <label for='tags'>Tag " + tagsCount + ":</label>" +
                "               <input  name='tags[" + tagsCount + "]' class='form-control' required>" +
                "             </div>";

            $('#tagsBox').append(htmlstring);
        })
    </script>

@endsection
