@extends('adminlte::page')
@section('top-ex')
    <script src="/plugins/bootstrap-confirmation.min.js"></script>
@endsection
@section('content')
    <section class="FAB">
        <div class="FAB__action-button">
            <a href="{{ url('/admin/tags/create') }}"><i class="action-button__icon fa fa-plus"></i></a>
            <p class="action-button__text--hide">Create Tags</p>
        </div>
    </section>
    <table class="table table-condensed jdatatable display" id="tags-table"  style="background-color: #fff;">
        <thead>
        <tr>
            <th>Name</th>
            <th>Posts</th>
            <th>Events</th>
            <th>News</th>
        </tr>
        </thead>
        <tbody>
        </tbody>
    </table>
@endsection
@section('footer-ex')
    <script>
        $(function () {
            $('.FAB__action-button').hover(function(){
                $(this).find('.action-button__text--hide').attr('class', 'action-button__text--show');
                $('.mini-action-button--hide').attr('class', 'mini-action-button--show');
            }, function(){
                $(this).find('.action-button__text--show').attr('class', 'action-button__text--hide');
            });

            $('#tags-table tbody').on('click','.model-link', function (e) {
                e.stopPropagation()
            })

            var $tagsTable = $('#tags-table').DataTable({
                pageLength: 100,
                processing: true,
                serverSide: true,
                ajax: {
                    url: '{{url("/admin/tags/getData")}}',
                    data: function (d) {
                    }
                },

                columns: [
                    {data: 'name', name: 'name'},
                    {data: 'posts', name: 'posts', sortable: 'no', orderable: 'no', searchable: 'no'},
                    {data: 'events', name: 'events', sortable: 'no', orderable: 'no', searchable: 'no'},
                    {data: 'news', name: 'news', sortable: 'no', orderable: 'no', searchable: 'no'},
                ]
            });


            var $modal = $('.action-modal');
            $('#tags-table tbody').on('click', 'tr', function () {

                var data = $tagsTable.row(this).data();
                console.log(data);
                $('.action-modal').modal();
                $modal.find('.modal-tag-name').text(data['name']);
                $modal.find('.modal-tag-posts').html(data['posts']);
                $modal.find('.modal-tag-events').html(data['events']);
                $modal.find('.modal-tag-news').html(data['news']);
                $modal.find('.view-tag-posts').attr('href', '/admin/posts?tag=' + data['name']);
                $modal.find('.view-tag-events').attr('href', '/admin/events?tag=' + data['name']);
                $modal.find('.view-tag-news').attr('href', '/admin/news?tag=' + data['name']);
                $modal.find('.edit-tag').attr('href', '/admin/tags/' + data['id'] + '/edit');
                $modal.find('.delete-tag').attr('action' , '/admin/tags/' + data['id']);

            });

            $('[data-toggle=confirmation]').confirmation({
                rootSelector: '[data-toggle=confirmation]',
                title: 'Are you sure you want to delete this tag?',
                btnOkClass: 'btn btn-xs btn-danger',
                btnCancelClass: 'btn btn-xs btn-default',
                btnOkIcon: 'fa fa-trash',
                btnCancelIcon: 'fa fa-ban'
            });

        });
    </script>
@endsection


@section('modals')
    <div class="modal fade action-modal" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Action</h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="modal-field">
                                <strong>Name:</strong> <span class="modal-tag-name"></span> <br>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="modal-field">
                                <strong>Posts:</strong> <span class="modal-tag-posts"></span> <br>
                            </div>
                            <div class="modal-field">
                                <strong>Events:</strong> <span class="modal-tag-events"></span> <br>
                            </div>
                            <div class="modal-field">
                                <strong>News:</strong> <span class="modal-tag-news"></span> <br>
                            </div>
                        </div>
                    </div>
                    <hr>
                    <div class="list-group">
                        <a href="#" class="list-group-item view-tag-posts">Posts with this tag</a>
                        <a href="#" class="list-group-item view-tag-events">Events with this tag</a>
                        <a href="#" class="list-group-item view-tag-news">News with this tag</a>
                        <a href="#" class="list-group-item edit-tag">Edit tag</a>
                        <form class="list-group-item list-group-item-danger delete-tag" method="post" action="">
                            {{ csrf_field() }}
                            {{ method_field('DELETE') }}
                            <button type="submit" class="btn-link text-red" data-toggle="confirmation" data-popout="true"> Delete Tag</button>
                        </form>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->

@endsection