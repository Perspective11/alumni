@extends('adminlte::page')
@section('content')
    <div class="row">
        <form class="col-md-10 col-md-offset-1" action="{{ $tag->path(true) }}" method="post" accept-charset="UTF-8"
              enctype="multipart/form-data">
            {{ csrf_field() }}
            {{ method_field('PATCH') }}
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">Edit Tag</h3>
                </div><!-- /.box-header -->
                <div id="tagsBox" class="box-body">
                    <div class="form-group {{  $errors->has('name') ? ' has-error' : ''  }}">
                        <label for="tags">Tag:</label>
                        <input type="text" name="name" class="form-control" required
                               value="{{ old('name') ?? $tag->name }}">
                    </div>
                </div><!-- /.box-body -->
                <div class="box-footer">
                    <button type="submit" class="btn btn-success pull-right">Update Tag</button>
                </div><!-- box-footer -->
            </div><!-- /.box -->
        </form>
    </div>
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            @include('errors.errors')
        </div>
    </div>
@endsection
