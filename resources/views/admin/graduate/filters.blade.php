<form id="user-filter" action="" method="get">
    <div class="box box-primary">
        <div class="box-header with-border">
            <h3 class="box-title">Graduate Filters</h3>
            <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i
                            class="fa fa-minus"></i>
                </button>
                <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i>
                </button>
            </div>
        </div><!-- /.box-header -->
        <div class="box-body">
            <div class="col-md-2">
                <h5><strong>Graduate Status:</strong></h5>
                @php
                    $comboStatus = '';
                    $statusArray =  \App\Graduate::pluck('graduate_status')->unique()->toArray();
                    if (request()->has('status')){
                        if (in_array(request('status'), $statusArray))
                            $comboStatus = request('status');
                    }
                @endphp
                <select name="status" class="form-control" id="status">
                    <option {{ $comboStatus== ''? 'selected': '' }} value="">All</option>
                    @foreach($statusArray as $status)
                        <option {{ $comboStatus== $status? 'selected': '' }} value="{{ $status }}">{{ $status }}</option>
                    @endforeach
                </select>
            </div>
            <div class="col-md-2">
                @php
                    $comboMajor = '';
                    $majorsArray =  \App\Major::has('graduates')->pluck('name')->toArray();
                    if (request()->has('major')){
                        if (in_array(request('major'), $majorsArray))
                            $comboMajor = request('major');
                    }
                @endphp
                <h5><strong>Major:</strong></h5>
                <select name="major" class="form-control" id="major">
                    <option {{ $comboMajor== ''? 'selected': '' }} value="">All</option>
                    @foreach($majorsArray as $major)
                        <option {{ $comboMajor== $major? 'selected': '' }} value="{{ $major }}">{{ $major }}</option>
                    @endforeach
                </select>
            </div>
            <div class="col-md-2">
                <h5><strong>User:</strong></h5>
                <div class="form-group">
                    <input type="text" class="form-control" name="user" value="{{ request('user')? request('user') : '' }}">
                </div>
            </div>
            <div class="col-md-2">
                @php
                    $comboYear = '';
                    $yearsArray =  \App\Graduate::pluck('graduation_year')->unique()->sort()->reverse()->toArray();
                    if (request()->has('year')){
                        if (in_array(request('year'), $yearsArray))
                            $comboYear = request('year');
                    }
                @endphp
                <h5><strong>Year:</strong></h5>
                <select name="year" class="form-control" id="year">
                    <option {{ $comboYear== ''? 'selected': '' }} value="">All</option>
                    @foreach($yearsArray as $year)
                        <option {{ $comboYear== $year? 'selected': '' }} value="{{ $year }}">{{ $year }}</option>
                    @endforeach
                </select>
            </div>
            <div class="col-md-2">
                @php
                    $comboGender = '';
                    $genderArray = ['male','female'] ;
                    if (request()->has('gender')){
                        if (in_array(request('gender'), $genderArray))
                            $comboGender = request('gender');
                    }
                @endphp
                <h5><strong>Gender:</strong></h5>
                <select name="gender" class="form-control" id="gender">
                    <option {{ $comboGender== ''? 'selected': '' }} value="">All</option>
                    <option {{ $comboGender== 'male'? 'selected': '' }} value="male">Male</option>
                    <option {{ $comboGender== 'female'? 'selected': '' }} value="female">Female</option>
                </select>
            </div>
            <div class="col-md-2">
                @php
                    $comboHireable = '';
                    $hireableArray = ['yes','no'] ;
                    if (request()->has('hireable')){
                        if (in_array(request('hireable'), $hireableArray))
                            $comboHireable = request('hireable');
                    }
                @endphp
                <h5><strong>Is Hireable:</strong></h5>
                <select name="hireable" class="form-control" id="hireable">
                    <option {{ $comboHireable== ''? 'selected': '' }} value="">All</option>
                    <option {{ $comboHireable== 'yes'? 'selected': '' }} value="yes">Yes</option>
                    <option {{ $comboHireable== 'no'? 'selected': '' }} value="no">No</option>
                </select>
            </div>
        </div><!-- /.box-body -->
        <div class="box-footer">
            <button type="submit" id="search-submit" class="btn btn-primary">Apply Filters</button>
            <button type="reset" id="clear-fields" class="btn btn-default">Clear</button>
        </div><!-- box-footer -->
    </div><!-- /.box -->
</form>
<script>
    $('#clear-fields').on('click', function () {
        var strippedUrl = stripQuery(window.location.href);
        window.location.replace(strippedUrl);
    })
    function stripQuery(url) {
        return url.split("?")[0].split("#")[0];
    }
</script>

