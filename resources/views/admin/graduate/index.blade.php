@extends('adminlte::page')
@section('top-ex')
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/css/select2.min.css" rel="stylesheet"/>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js"></script>
    <script src="/plugins/bootstrap-confirmation.min.js"></script>

@endsection
@section('content')
    <section class="FAB">
        <div class="FAB__action-button">
            <a href="{{ url('/admin/users/create') }}"><i class="action-button__icon fa fa-user-plus"></i></a>
            <p class="action-button__text--hide">Add User</p>
        </div>
    </section>
    <div class="container-fluid">

        @include('admin.graduate.filters')
        <table class="table table-condensed table-responsive jdatatable display cell-border smallest" id="graduates-table"
               style="background-color: #fff;">
            <thead>
            <tr>
                <th>LIU ID</th>
                <th>Full Name</th>
                <th>Email</th>
                <th>Year</th>
                <th>Major</th>
                <th>Gender</th>
                <th>Age</th>
                <th>Phone</th>
                <th>Address</th>
                <th>Status</th>
                <th>GPA</th>
                <th>Hireable</th>
                <th>Links</th>
            </tr>
            </thead>
            <tbody>

            </tbody>
        </table>
    </div>
@endsection
@section('footer-ex')
    <script>
        $(function () {
            $('.FAB__action-button').hover(function () {
                $(this).find('.action-button__text--hide').attr('class', 'action-button__text--show');
                $('.mini-action-button--hide').attr('class', 'mini-action-button--show');
            }, function () {
                $(this).find('.action-button__text--show').attr('class', 'action-button__text--hide');
            });

            var $graduatesTable = $('#graduates-table').DataTable({
                pageLength: 100,
                processing: true,
                serverSide: true,
                ajax: {
                    url: '{{url("/admin/graduates/getData")}}',
                    data: function (d) {
                        d.status = '{{ request('status')? request('status') : '' }}'
                        d.gender = '{{ request('gender')? request('gender') : '' }}'

                        d.major = '{{ request('major')? request('major') : ''}}'
                        d.year = '{{ request('year')? request('year') : ''}}'
                        d.hireable = '{{ request('hireable')? request('hireable') : ''}}'

                        d.user = '{{ request('user')? request('user') : ''}}'

                    }
                },

                columns: [
                    {data: 'liu_id', name: 'liu_id'},
                    {data: 'full_name', name: 'full_name'},
                    {data: 'email', name: 'email', sortable: 'no', orderable: 'no', searchable: 'no'},
                    {data: 'graduation_year', name: 'graduation_year'},
                    {data: 'major', name: 'major', sortable: 'no', orderable: 'no', searchable: 'no'},
                    {data: 'gender', name: 'gender', sortable: 'no', orderable: 'no'},
                    {data: 'age', name: 'age', sortable: 'no', orderable: 'no', searchable: 'no'},
                    {data: 'phone', name: 'phone'},
                    {data: 'curr_address', name: 'curr_address'},
                    {data: 'graduate_status', name: 'graduate_status'},
                    {data: 'gpa', name: 'gpa'},
                    {data: 'is_hireable', name: 'is_hireable', searchable: 'no'},
                    {data: 'links', name: 'links', sortable: 'no', orderable: 'no', searchable: 'no'}
                ]
            });

            $('#graduates-table tbody').on('click', '.link-icon', function (e) {
                e.stopPropagation()
            })

            var $modal = $('.action-modal');
            $('#graduates-table tbody').on('click', 'tr', function () {

                var data = $graduatesTable.row(this).data();
                console.log(data);
                $('.action-modal').modal();
                $modal.find('.modal-graduate-id').text(data['liu_id']);
                $modal.find('.modal-graduate-name').text(data['full_name']);
                $modal.find('.modal-graduate-email').text(data['email']);
                $modal.find('.modal-graduate-year').html(data['graduation_year']);
                $modal.find('.modal-graduate-major').text(data['major']);
                $modal.find('.modal-graduate-gender').html(data['gender']);
                $modal.find('.modal-graduate-age').html(data['age']);
                $modal.find('.modal-graduate-phone').text(data['phone']);
                $modal.find('.modal-graduate-address').text(data['address']);
                $modal.find('.modal-graduate-status').html(data['graduate_status']);
                $modal.find('.modal-graduate-gpa').html(data['gpa']);
                $modal.find('.modal-graduate-links').html(data['links']);
                $modal.find('.view-graduate-posts').attr('href', '/admin/posts?user=' + data['liu_id']);
                $modal.find('.view-graduate-events').attr('href', '/admin/events?user=' + data['liu_id']);
                $modal.find('.view-graduate-details').attr('href', '/admin/users/' + data['user_id']);
                $modal.find('.edit-graduate-details').attr('href', '/admin/users/' + data['user_id'] + '/edit');
                $modal.find('.change-user-role').attr('href', '/admin/users/' + data['user_id'] + '/changeRole');
                $modal.find('.delete-user').attr('action' , '/admin/users/' + data['user_id']);
                $modal.find('.upload-cv').attr('href' , '/admin/users/' + data['user_id'] + '/uploadResumeShow');
                $modal.find('.download-cv').attr('action' , '/admin/users/' + data['user_id'] + '/downloadResume');
                data['file_path'] ? $modal.find('.download-cv').show() : $modal.find('.download-cv').hide();


            });

            $('[data-toggle=confirmation]').confirmation({
                rootSelector: '[data-toggle=confirmation]',
                title: 'Are you sure you want to delete this user?',
                btnOkClass: 'btn btn-xs btn-danger',
                btnCancelClass: 'btn btn-xs btn-default',
                btnOkIcon: 'fa fa-trash',
                btnCancelIcon: 'fa fa-ban'
            });

        });
    </script>
@endsection

@section('modals')
    <div class="modal fade action-modal" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Action</h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="modal-field">
                                <strong>Student Id:</strong> <span class="modal-graduate-id"></span> <br>
                            </div>
                            <div class="modal-field">
                                <strong>Full Name:</strong> <span class="modal-graduate-name"></span> <br>
                            </div>
                            <div class="modal-field">
                                <strong>Graduation Year:</strong> <span class="modal-graduate-year"></span> <br>
                            </div>
                            <div class="modal-field">
                                <strong>Major:</strong> <span class="modal-graduate-major"></span> <br>
                            </div>
                            <div class="modal-field">
                                <strong>Gender:</strong> <span class="modal-graduate-gender"></span> <br>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="modal-field">
                                <strong>Age:</strong> <span class="modal-graduate-age"></span> <br>
                            </div>
                            <div class="modal-field">
                                <strong>Phone:</strong> <span class="modal-graduate-phone"></span> <br>
                            </div>
                            <div class="modal-field">
                                <strong>Address:</strong> <span class="modal-graduate-address"></span> <br>
                            </div>
                            <div class="modal-field">
                                <strong>Status:</strong> <span class="modal-graduate-status"></span> <br>
                            </div>
                            <div class="modal-field">
                                <strong>Gpa:</strong> <span class="modal-graduate-gpa"></span> <br>
                            </div>
                            <div class="modal-field">
                                <strong>Links:</strong> <span class="modal-graduate-links"></span> <br>
                            </div>
                        </div>
                    </div>

                    <hr>
                    <div class="list-group">
                        <a href="#" class="list-group-item view-graduate-posts">View graduate posts</a>
                        <a href="#" class="list-group-item view-graduate-events">View graduate events</a>
                        <a href="#" class="list-group-item view-graduate-details">View graduate details</a>
                        <a href="#" class="list-group-item edit-graduate-details">Edit graduate details</a>
                        <a href="#" class="list-group-item change-user-role">Change user role</a>
                        <a href="#" class="list-group-item upload-cv">Upload CV</a>
                        <form class="list-group-item download-cv" method="post" target="_blank">
                            {{ csrf_field() }}
                            <button type="submit" class="btn-link">Download CV</button>
                        </form>
                        <form class="list-group-item list-group-item-danger delete-user" method="post" action="">
                            {{ csrf_field() }}
                            {{ method_field('DELETE') }}
                            <button type="submit" class="btn-link text-red" data-toggle="confirmation" data-popout="true"> Delete User</button>
                        </form>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->

@endsection