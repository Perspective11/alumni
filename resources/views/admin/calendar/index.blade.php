@extends('adminlte::page')

@section('content')
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="box box-default">
                <div class="box-header with-border">
                    <h4>Events Calendar</h4>
                    @if (! count($events))
                        <p>No Events Available</p>
                    @endif
                    <div class="box-tools pull-right">
                        <button type="button" class="btn btn-box-tool" data-widget="collapse"><i
                                    class="fa fa-minus"></i>
                        </button>
                        <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i>
                        </button>
                    </div>
                </div>
                <div class="box-body">
                    @include('admin.calendar.calendar')
                </div>
                <div class="box-footer">
                    <a href="{{ url('/admin/events/create') }}" class="btn btn-default pull-right">Create an Event</a>
                </div>
            </div>

        </div>
    </div>
@endsection

