@section('top-ex')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.18.1/moment.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/fullcalendar/3.4.0/fullcalendar.min.js"></script>
    <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/fullcalendar/3.4.0/fullcalendar.min.css">
    <script src="/plugins/sweetalert/sweetalert.min.js"></script>
    <link rel="stylesheet" type="text/css" href="/plugins/sweetalert/sweetalert.css">
@endsection


<div id='calendar'></div>


@section('footer-ex')
    <script>
        $('#calendar').fullCalendar({
            header: {
                left: 'prev,next today',
                center: 'title',
                right: 'month,agendaWeek,agendaDay,listMonth'
            },
            eventSources: [
                {
                    url: '/admin/calendar/getEvents',
                }
            ],
            editable: true,
            eventDrop: function (event, delta, revertFunc) {
                $.ajax({
                    type: "POST",
                    url: '/admin/calendar/updateEvent',
                    data: {
                        "_token": "{{ csrf_token() }}",
                        'id': event.id,
                        'start': event.start.format(),
                        'end': event.end ? event.end.format() : null
                    },
                    success: function (result) {
                        var mstart = moment(result.date + ' ' + result.start);
                        var mend = moment(result.date + ' ' + result.end);
                        swal("Update Successful!",
                            "Event '" + event.title + "'\nHave been rescheduled to:\n" + mstart.format("dddd, MMM Do YYYY, h:mm a") + ".",
                            "success")
                    },
                    error: function (ex) {
                        swal({
                            title: "Error!",
                            text: ex.msg,
                            type: "error",
                            confirmButtonText: "Cool"
                        });
                        revertFunc();
                    },
                }); // end ajax
            }, // end event drop


            timeFormat: 'hh:mm a'

        })
    </script>
@endsection