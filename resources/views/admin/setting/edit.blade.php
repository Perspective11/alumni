@extends('adminlte::page')
@section('top-ex')
    <script src="https://cdn.ckeditor.com/4.7.1/standard/ckeditor.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/dropzone/5.1.1/min/dropzone.min.js"></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/dropzone/5.1.1/min/dropzone.min.css">
    <script src="/plugins/sweetalert/sweetalert.min.js"></script>
    <link rel="stylesheet" type="text/css" href="/plugins/sweetalert/sweetalert.css">
@endsection
@section('content')
    <a href="{{ url('/admin/settings/') }}" class="btn btn-primary"><i class="fa fa-arrow-left"></i> Back</a>
    <form action="{{ url('/admin/settings/') }}" method="post">
        {{ csrf_field() }}
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title">Social Links</h3>
                        <div class="box-tools pull-right">
                            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i
                                        class="fa fa-minus"></i>
                            </button>
                            <button type="button" class="btn btn-box-tool" data-widget="remove"><i
                                        class="fa fa-times"></i>
                            </button>
                        </div>
                    </div><!-- /.box-header -->
                    <div class="box-body">
                        <div class="form-links">
                            <div class="form-links">
                                <h5><strong>Links:</strong></h5>
                                <div class="input-group link-input {{ $errors->has('facebook_link') ? ' has-error' : '' }}">
                                    <span class="input-group-addon"><i style="color: #3b5998;"
                                                                       class="fa fa-facebook-official icon"></i></span>
                                    <input type="text" class="form-control" placeholder="Facebook URL"
                                           name="facebook_link"
                                           value="{{ old('facebook_link') ?: (!empty($socialLinks['facebook']['link'])) ? $socialLinks['facebook']['link'] : '' }}">
                                </div>
                                <div class="input-group link-input {{ $errors->has('twitter_link') ? ' has-error' : '' }}">
                                    <span class="input-group-addon"><i style="color: #1da1f2;"
                                                                       class="fa fa-twitter icon"></i></span>
                                    <input type="text" class="form-control" placeholder="Twitter URL"
                                           name="twitter_link"
                                           value="{{ old('twitter_link') ?: (!empty($socialLinks['twitter']['link'])) ? $socialLinks['twitter']['link'] : '' }}">
                                </div>
                                <div class="input-group link-input {{ $errors->has('youtube_link') ? ' has-error' : '' }}">
                                    <span class="input-group-addon"><i style="color: #cd201f;"
                                                                       class="fa fa-youtube-play icon"></i></span>
                                    <input type="text" class="form-control" placeholder="Youtube URL"
                                           name="youtube_link"
                                           value="{{ old('youtube_link') ?: (!empty($socialLinks['youtube']['link'])) ? $socialLinks['youtube']['link'] : '' }}">
                                </div>
                            </div>
                        </div>
                    </div><!-- /.box-body -->
                </div><!-- /.box -->
            </div>
        </div>
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                <div class="box box-success">
                    <div class="box-header with-border">
                        <h3 class="box-title">Contact Page</h3>
                        <div class="box-tools pull-right">
                            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i
                                        class="fa fa-minus"></i>
                            </button>
                            <button type="button" class="btn btn-box-tool" data-widget="remove"><i
                                        class="fa fa-times"></i>
                            </button>
                        </div>
                    </div><!-- /.box-header -->
                    <div class="box-body">
                        <div class="form-group {{ $errors->has('contact.header') ? ' has-error' : '' }}">
                            <h4><label for="ckHeader">Header</label></h4>
                            <textarea name="ckHeader" id="ckHeader">
                                {!! old('contact.header') ?: $contact['header']  !!}
                            </textarea>
                        </div>
                        <div class="form-group {{ $errors->has('contact.info') ? ' has-error' : '' }}">
                            <h4><label for="ckInfo">Info</label></h4>
                            <textarea name="ckInfo" id="ckInfo">
                                {!! old('contact.info') ?: $contact['info']  !!}
                            </textarea>
                        </div>
                    </div><!-- /.box-body -->
                </div><!-- /.box -->
            </div>
        </div>
        <section class="FAB">
            <div class="FAB__action-button">
                <button class="btn-link"><i class="action-button__icon fa fa-edit"></i></button>
                <p class="action-button__text--hide">Submit Settings</p>
            </div>
        </section>
    </form>
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="box box-danger">
                <div class="box-header with-border">
                    <h3 class="box-title">Home Page</h3>
                    <div class="box-tools pull-right">
                        <button type="button" class="btn btn-box-tool" data-widget="collapse"><i
                                    class="fa fa-minus"></i>
                        </button>
                        <button type="button" class="btn btn-box-tool" data-widget="remove"><i
                                    class="fa fa-times"></i>
                        </button>
                    </div>
                </div><!-- /.box-header -->
                <div class="box-body">
                    <h4><label for="">Slider Images</label></h4>
                    <div id="my-awesome-dropzone" class="dropzone">
                        <div class="dropzone-previews"></div> <!-- this is were the previews should be shown. -->
                    </div>
                    <br>
                    <button type="button" id="submitImages" class="btn btn-success pull-right">Submit images</button>
                </div><!-- /.box-body -->
            </div><!-- /.box -->
        </div>
    </div>
    {{--</form>--}}
@endsection
@section('footer-ex')

    <script>
        $('.FAB__action-button').hover(function () {
            $(this).find('.action-button__text--hide').attr('class', 'action-button__text--show');
            $('.mini-action-button--hide').attr('class', 'mini-action-button--show');
        }, function () {
            $(this).find('.action-button__text--show').attr('class', 'action-button__text--hide');
        });

        CKEDITOR.replace('ckHeader', {
            removePlugins: 'image'
        });
        CKEDITOR.replace('ckInfo', {
            removePlugins: 'image'
        });
        //        var myDropzone = new Dropzone("#my-awesome-dropzone", { url: "/file/post"});
        Dropzone.autoDiscover = false;
        $("#my-awesome-dropzone").dropzone({
            url: "/admin/settings/storeImages",
            paramName: 'images',
            headers: {
                "X-CSRF-Token": '{{ csrf_token() }}'
            },
            uploadMultiple: true,
            acceptedFiles: 'image/jpg,image/png,image/jpeg',
            addRemoveLinks: true,
            autoProcessQueue: false,
            parallelUploads: 5,
            maxFiles: 5,
            thumbnailWidth: 500,
            timeout: 60000,
            removedfile: function(file) {
                var name = file.name;
                $.ajax({
                    type: 'DELETE',
                    url: '/admin/settings/deleteImage',
                    data: {
                        '_token' : '{{ csrf_token() }}',
                        'name' : name
                    },
                    dataType: 'html'
                });
                var _ref;
                this.options.maxFiles = this.options.maxFiles + 1;
                return (_ref = file.previewElement) != null ? _ref.parentNode.removeChild(file.previewElement) : void 0;
            },
            thumbnailHeight: 500,

            init: function () {
                var myDropzone = this;

                // First change the button to actually tell Dropzone to process the queue.
                document.querySelector("#submitImages").addEventListener("click", function (e) {
                    // Make sure that the form isn't actually being sent.
                    e.preventDefault();
                    e.stopPropagation();
                    myDropzone.processQueue();
                });

                // Listen to the sendingmultiple event. In this case, it's the sendingmultiple event instead
                // of the sending event because uploadMultiple is set to true.
                this.on("sendingmultiple", function () {
                    //
                    // Gets triggered when the form is actually being sent.
                    // Hide the success button or the complete form.
                });
                this.on("successmultiple", function (files, response) {
                    // Gets triggered when the files have successfully been sent.
                    // Redirect user or notify of success.
                });
                this.on("errormultiple", function (files, response) {
                    // Gets triggered when there was an error sending the files.
                    // Maybe show form again, and notify user of error
                });

                this.on("maxfilesexceeded", function (file) {
                    swal({
                        title: "Sorry!",
                        text: "You are not allowed to upload more than 5 images!",
                        type: "error",
                        confirmButtonText: "Cool"
                    });
                    this.removeFile(file);

                });


                this.on("addedfile", function (file) { // to prevent duplicates
                    if (this.files.length) {
                        var _i, _len;
                        for (_i = 0, _len = this.files.length; _i < _len - 1; _i++) // -1 to exclude current file
                        {
                            if (this.files[_i].name === file.name && this.files[_i].size === file.size && this.files[_i].lastModifiedDate.toString() === file.lastModifiedDate.toString()) {
                                this.removeFile(file);
                            }
                        }
                    }

                });

                this.on("thumbnail", function(file, dataUrl) {
                    $('.dz-image').find('img').attr({width: '100%', height: 'auto'});
                }),
                    this.on("success", function(file) {
                        $('.dz-image').css({"width":"100%", "height":"auto"});
                    })
                @php
                    $counter = 0;
                @endphp
                @foreach($sliderImages as $path)
                // Create the mock file:
                var imgFile{{ $counter }} = {name: "/{{ $path }}".replace(/^.*[\\\/]/, ''), size: 12345};// get real name of image

                // Call the default addedfile event handler
                this.emit("addedfile", imgFile{{ $counter }});

                // And optionally show the thumbnail of the file:
                this.emit("thumbnail", imgFile{{ $counter }}, "/{{ $path }}");
                // Or if the file on your server is not yet in the right
                // size, you can let Dropzone download and resize it
                // callback and crossOrigin are optional.
                this.createThumbnailFromUrl(imgFile{{ $counter }}, "/{{ $path }}");

                // Make sure that there is no progress bar, etc...
                this.emit("complete", imgFile{{ $counter }});

                @php
                    $counter ++;
                @endphp
                @endforeach
                // If you use the maxFiles option, make sure you adjust it to the
                // correct amount:
                var existingFileCount = {{ count($sliderImages) }}; // The number of files already uploaded
                this.options.maxFiles = this.options.maxFiles - existingFileCount;
            }
        });


    </script>

@endsection
