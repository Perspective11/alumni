@extends('adminlte::page')
@section('content_header')
    <h3>Settings and Application Variables</h3>
@endsection
@section('content')
    <section class="FAB">
        <div class="FAB__action-button">
            <a href="{{ url('/admin/settings/edit') }}"><i class="action-button__icon bg-yellow fa fa-wrench"></i></a>
            <p class="action-button__text--hide">Edit Settings</p>
        </div>
    </section>
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">Social Links</h3>
                    <div class="box-tools pull-right">
                        <button type="button" class="btn btn-box-tool" data-widget="collapse"><i
                                    class="fa fa-minus"></i>
                        </button>
                        <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i>
                        </button>
                    </div>
                </div><!-- /.box-header -->
                <div class="box-body">
                    <div class="form-links">
                        @foreach ($socialLinks as $link)
                            <div class="input-group link-input">
                                <a href="{{ $link['link'] }}">
                                    <p>
                                        <i style="color: {{ $link['color'] }};"
                                           class="fa fa-{{ $link['icon'] }} icon"></i>
                                        {{ $link['link'] }}
                                    </p>
                                </a>
                            </div>
                        @endforeach
                    </div>
                </div><!-- /.box-body -->
            </div><!-- /.box -->
        </div>
    </div>
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="box box-success">
                <div class="box-header with-border">
                    <h3 class="box-title">Contact Page</h3>
                    <div class="box-tools pull-right">
                        <button type="button" class="btn btn-box-tool" data-widget="collapse"><i
                                    class="fa fa-minus"></i>
                        </button>
                        <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i>
                        </button>
                    </div>
                </div><!-- /.box-header -->
                <div class="box-body">
                    @foreach ($contact as $key => $value)
                        <h4><label for="">{{ $key }}</label></h4>
                        <div class="input-group link-input">
                                <p>
                                    {!! $value !!}
                                </p>
                        </div>

                    @endforeach
                </div><!-- /.box-body -->
            </div><!-- /.box -->
        </div>
    </div>
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="box box-danger">
                <div class="box-header with-border">
                    <h3 class="box-title">Home Page</h3>
                    <div class="box-tools pull-right">
                        <button type="button" class="btn btn-box-tool" data-widget="collapse"><i
                                    class="fa fa-minus"></i>
                        </button>
                        <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i>
                        </button>
                    </div>
                </div><!-- /.box-header -->
                <div class="box-body">
                    <h4><label for="">Slider Images</label></h4>
                    @foreach ($sliderImages as $path)
                        <div class="row">
                            <div class="col-xs-12">
                                <img src="/{{ $path }}" alt="slider_image" class="img-responsive img-rounded">
                            </div>
                        </div> <br>
                    @endforeach
                </div><!-- /.box-body -->
            </div><!-- /.box -->
        </div>
    </div>
@endsection
@section('footer-ex')
    <script>
        $('.FAB__action-button').hover(function(){
            $(this).find('.action-button__text--hide').attr('class', 'action-button__text--show');
            $('.mini-action-button--hide').attr('class', 'mini-action-button--show');
        }, function(){
            $(this).find('.action-button__text--show').attr('class', 'action-button__text--hide');
        });
    </script>
@endsection
