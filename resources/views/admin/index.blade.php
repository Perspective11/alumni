{{-- resources/views/admin/dashboard.blade.php --}}

@extends('adminlte::page')
@section('top-ex')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.6.0/Chart.bundle.min.js"></script>
@endsection
@section('title', 'Dashboard')

@section('content_header')
    <h1>Dashboard</h1>
@stop

@section('content')
    <div class="row">
        <div class="col-md-3 col-sm-6 col-xs-12">
            <div class="info-box bg-aqua">
                <!-- Apply any bg-* class to to the icon to color it -->
                <span class="info-box-icon "><i class="fa fa-graduation-cap"></i></span>
                <div class="info-box-content">
                    <span class="info-box-text">Graduates</span>
                    <span class="info-box-number">{{ number_format(\App\Graduate::count()) }}</span>
                    <div class="progress">
                        <div class="progress-bar" style="width: {{ \App\Graduate::increasePercentage() }}%"></div>
                    </div>
                    <span class="progress-description">
                        {{ \App\Graduate::increasePercentage() }}% Increase in 30 Days
                    </span>
                </div><!-- /.info-box-content -->
            </div><!-- /.info-box -->
        </div>
        <div class="col-md-3 col-sm-6 col-xs-12">
            <div class="info-box bg-purple">
                <!-- Apply any bg-* class to to the icon to color it -->
                <span class="info-box-icon"><i class="fa fa-briefcase"></i></span>
                <div class="info-box-content">
                    <span class="info-box-text">Companies</span>
                    <span class="info-box-number">{{ number_format(\App\Company::count()) }}</span>
                    <div class="progress">
                        <div class="progress-bar" style="width: {{ \App\Company::increasePercentage() }}%"></div>
                    </div>
                    <span class="progress-description">
                        {{ \App\Company::increasePercentage() }}% Increase in 30 Days
                    </span>
                </div><!-- /.info-box-content -->
            </div><!-- /.info-box -->
        </div>
        <div class="col-md-3 col-sm-6 col-xs-12">
            <div class="info-box bg-green">
                <!-- Apply any bg-* class to to the icon to color it -->
                <span class="info-box-icon"><i class="fa fa-users"></i></span>
                <div class="info-box-content">
                    <span class="info-box-text">Total Users</span>
                    <span class="info-box-number">{{ number_format(\App\User::count()) }}</span>
                    <div class="progress">
                        <div class="progress-bar" style="width: {{ \App\User::increasePercentage() }}%"></div>
                    </div>
                    <span class="progress-description">
                        {{ \App\User::increasePercentage() }}% Increase in 30 Days
                    </span>
                </div><!-- /.info-box-content -->
            </div><!-- /.info-box -->
        </div>
        <div class="col-md-3 col-sm-6 col-xs-12">
            <div class="info-box bg-yellow">
                <!-- Apply any bg-* class to to the icon to color it -->
                <span class="info-box-icon"><i class="fa fa-rss"></i></span>
                <div class="info-box-content">
                    <span class="info-box-text">Posts</span>
                    <span class="info-box-number">{{ number_format(\App\Post::count()) }}</span>
                    <div class="progress">
                        <div class="progress-bar" style="width: {{ \App\Post::increasePercentage() }}%"></div>
                    </div>
                    <span class="progress-description">
                        {{ \App\Post::increasePercentage() }}% Increase in 30 Days
                    </span>
                </div><!-- /.info-box-content -->
            </div><!-- /.info-box -->
        </div>
        <div class="col-md-3 col-sm-6 col-xs-12">
            <div class="info-box bg-red">
                <!-- Apply any bg-* class to to the icon to color it -->
                <span class="info-box-icon"><i class="fa fa-calendar"></i></span>
                <div class="info-box-content">
                    <span class="info-box-text">Events</span>
                    <span class="info-box-number">{{ number_format(\App\Event::count()) }}</span>
                    <div class="progress">
                        <div class="progress-bar" style="width: {{ \App\Event::increasePercentage() }}%"></div>
                    </div>
                    <span class="progress-description">
                        {{ \App\Event::increasePercentage() }}% Increase in 30 Days
                    </span>
                </div><!-- /.info-box-content -->
            </div><!-- /.info-box -->
        </div>
        <div class="col-md-3 col-sm-6 col-xs-12">
            <div class="info-box bg-teal">
                <!-- Apply any bg-* class to to the icon to color it -->
                <span class="info-box-icon"><i class="fa fa-newspaper-o"></i></span>
                <div class="info-box-content">
                    <span class="info-box-text">News</span>
                    <span class="info-box-number">{{ number_format(\App\News::count()) }}</span>
                    <div class="progress">
                        <div class="progress-bar" style="width: {{ \App\News::increasePercentage() }}%"></div>
                    </div>
                    <span class="progress-description">
                        {{ \App\News::increasePercentage() }}% Increase in 30 Days
                    </span>
                </div><!-- /.info-box-content -->
            </div><!-- /.info-box -->
        </div>
        <div class="col-md-3 col-sm-6 col-xs-12">
            <div class="info-box bg-gray-light">
                <!-- Apply any bg-* class to to the icon to color it -->
                <span class="info-box-icon"><i class="fa fa-user-times"></i></span>
                <div class="info-box-content">
                    <span class="info-box-text">Pending Users</span>
                    <span class="info-box-number">{{ number_format(\App\User::where('user_status', 0)->count()) }}</span>
                    <div class="progress">
                        <div class="progress-bar" style="width: {{ \App\News::increasePercentage() }}%"></div>
                    </div>
                    <span class="progress-description">
                        <a href="{{ url('/admin/users') }}?status=inactive&role=&user=">View Pending Users</a>
                    </span>
                </div><!-- /.info-box-content -->
            </div><!-- /.info-box -->
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h4>Site Activity Overview</h4>
                    <div class="box-tools pull-right">
                        <button type="button" class="btn btn-box-tool" data-widget="collapse"><i
                                    class="fa fa-minus"></i>
                        </button>
                        <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i>
                        </button>
                    </div>
                </div>
                <div class="box-body">
                    <div class="row">
                        <div class="col-md-8">
                            <canvas id="siteActivityChart" class="border-right"></canvas>
                        </div>
                        <div class="col-md-4">
                            <p class="text-center">
                                <strong>Status Within the Past Month</strong>
                            </p>

                            <div class="progress-group">
                                @php
                                    $graduatesStatus = \App\Graduate::activatedPercentage();
                                @endphp
                                <span class="progress-text">Activated Graduates</span>
                                <span class="progress-number"><b>{{ $graduatesStatus['activatedCount'] }}</b>/{{ $graduatesStatus['count'] }}</span>

                                <div class="progress sm">
                                    <div class="progress-bar progress-bar-aqua"
                                         style="width: {{ $graduatesStatus['count']? $graduatesStatus['activatedCount'] /  $graduatesStatus['count'] * 100 : 0}}%"></div>
                                </div>
                            </div>
                            <!-- /.progress-group -->
                            <div class="progress-group">
                                @php
                                    $companiesStatus = \App\Company::activatedPercentage();
                                @endphp
                                <span class="progress-text">Activated Companies</span>
                                <span class="progress-number"><b>{{ $companiesStatus['activatedCount'] }}</b>/{{ $companiesStatus['count'] }}</span>

                                <div class="progress sm">
                                    <div class="progress-bar progress-bar-purple"
                                         style="width: {{ $companiesStatus['count']? $companiesStatus['activatedCount'] /  $companiesStatus['count'] * 100 : 0}}%"></div>
                                </div>
                            </div>
                            <!-- /.progress-group -->
                            <div class="progress-group">
                                @php
                                    $postsStatus = \App\Post::publishedPercentage();
                                @endphp
                                <span class="progress-text">Published Posts</span>
                                <span class="progress-number"><b>{{ $postsStatus['publishedCount'] }}</b>/{{ $postsStatus['count'] }}</span>

                                <div class="progress sm">
                                    <div class="progress-bar progress-bar-yellow"
                                         style="width: {{ $postsStatus['count']? $postsStatus['publishedCount'] /  $postsStatus['count'] * 100 : 0}}%"></div>
                                </div>
                            </div>
                            <!-- /.progress-group -->
                            <div class="progress-group">
                                @php
                                    $eventsStatus = \App\Event::publishedPercentage();
                                @endphp
                                <span class="progress-text">Published Events</span>
                                <span class="progress-number"><b>{{ $eventsStatus['publishedCount'] }}</b>/{{ $eventsStatus['count'] }}</span>

                                <div class="progress sm">
                                    <div class="progress-bar progress-bar-red"
                                         style="width: {{ $eventsStatus['count']? $eventsStatus['publishedCount'] /  $eventsStatus['count'] * 100 : 0}}%"></div>
                                </div>
                            </div>
                            <!-- /.progress-group -->
                            <div class="progress-group">
                                @php
                                    $newsStatus = \App\News::publishedPercentage();
                                @endphp
                                <span class="progress-text">Published News Posts</span>
                                <span class="progress-number"><b>{{ $newsStatus['publishedCount'] }}</b>/{{ $newsStatus['count'] }}</span>

                                <div class="progress sm">
                                    <div class="progress-bar progress-bar-teal bg-teal"
                                         style="width: {{ $newsStatus['count']? $newsStatus['publishedCount'] /  $newsStatus['count'] * 100 : 0}}%"></div>
                                </div>
                            </div>
                            <!-- /.progress-group -->

                        </div>
                    </div>
                </div>
                <!-- /.col -->
                {{--<div class="box-footer">--}}
                    {{--<div class="row">--}}
                        {{--<div class="col-sm-3 col-xs-6">--}}
                            {{--<div class="description-block border-right">--}}
                                {{--<span class="description-percentage text-green"><i--}}
                                            {{--class="fa fa-caret-up"></i> 17%</span>--}}
                                {{--<h5 class="description-header">$35,210.43</h5>--}}
                                {{--<span class="description-text">TOTAL REVENUE</span>--}}
                            {{--</div>--}}
                            {{--<!-- /.description-block -->--}}
                        {{--</div>--}}
                        {{--<!-- /.col -->--}}
                        {{--<div class="col-sm-3 col-xs-6">--}}
                            {{--<div class="description-block border-right">--}}
                                {{--<span class="description-percentage text-yellow"><i--}}
                                            {{--class="fa fa-caret-left"></i> 0%</span>--}}
                                {{--<h5 class="description-header">$10,390.90</h5>--}}
                                {{--<span class="description-text">TOTAL COST</span>--}}
                            {{--</div>--}}
                            {{--<!-- /.description-block -->--}}
                        {{--</div>--}}
                        {{--<!-- /.col -->--}}
                        {{--<div class="col-sm-3 col-xs-6">--}}
                            {{--<div class="description-block border-right">--}}
                                {{--<span class="description-percentage text-green"><i--}}
                                            {{--class="fa fa-caret-up"></i> 20%</span>--}}
                                {{--<h5 class="description-header">$24,813.53</h5>--}}
                                {{--<span class="description-text">TOTAL PROFIT</span>--}}
                            {{--</div>--}}
                            {{--<!-- /.description-block -->--}}
                        {{--</div>--}}
                        {{--<!-- /.col -->--}}
                        {{--<div class="col-sm-3 col-xs-6">--}}
                            {{--<div class="description-block">--}}
                                {{--<span class="description-percentage text-red"><i--}}
                                            {{--class="fa fa-caret-down"></i> 18%</span>--}}
                                {{--<h5 class="description-header">1200</h5>--}}
                                {{--<span class="description-text">GOAL COMPLETIONS</span>--}}
                            {{--</div>--}}
                            {{--<!-- /.description-block -->--}}
                        {{--</div>--}}
                    {{--</div>--}}
                    {{--<!-- /.row -->--}}
                {{--</div>--}}
                <!-- /.box-footer -->
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-6">
            <!-- USERS LIST -->
            <div class="box box-info">
                <div class="box-header with-border">
                    <h3 class="box-title">Latest Graduate Members</h3>

                    <div class="box-tools pull-right">
                        <button type="button" class="btn btn-box-tool" data-widget="collapse"><i
                                    class="fa fa-minus"></i>
                        </button>
                        <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i>
                        </button>
                    </div>
                </div>
                <!-- /.box-header -->
                <div class="box-body no-padding">
                    <ul class="users-list clearfix">
                        @forelse ($recentGraduates as $graduate)
                            <li>
                                <img src="{{ $graduate->user->getPicture() }}" style="height: 70px;" alt="User Image">
                                <a class="users-list-name"
                                   href="{{ $graduate->user->path(true) }}">{{ str_limit($graduate->full_name, 20)  }}</a>
                                <span class="users-list-date">{{ $graduate->created_at->diffForHumans() }}</span>
                            </li>

                        @empty
                            <p>No Graduates Available</p>
                        @endforelse
                    </ul>
                    <!-- /.users-list -->
                </div>
                <!-- /.box-body -->
                <div class="box-footer text-center">
                    <a href="{{ url('/admin/graduates') }}" class="uppercase">View All Graduates</a>
                </div>
                <!-- /.box-footer -->
            </div>
            <!--/.box -->
        </div>
        <!-- /.col -->
        <div class="col-md-6">
            <!-- USERS LIST -->
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">Latest Companies</h3>

                    <div class="box-tools pull-right">
                        <button type="button" class="btn btn-box-tool" data-widget="collapse"><i
                                    class="fa fa-minus"></i>
                        </button>
                        <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i>
                        </button>
                    </div>
                </div>
                <!-- /.box-header -->
                <div class="box-body no-padding">
                    <ul class="users-list clearfix">
                        @forelse ($recentCompanies as $company)
                            <li>
                                <img src="{{ $company->user->getPicture() }}" style="height: 70px;" alt="User Image">
                                <a class="users-list-name"
                                   href="{{ $company->user->path(true) }}">{{ str_limit($company->company_name, 20)  }}</a>
                                <span class="users-list-date">{{ $company->created_at->diffForHumans() }}</span>
                            </li>
                        @empty
                            <p>No Copmanies Available</p>
                        @endforelse
                    </ul>
                    <!-- /.users-list -->
                </div>
                <!-- /.box-body -->
                <div class="box-footer text-center">
                    <a href="{{ url('/admin/companies') }}" class="uppercase">View All Companies</a>
                </div>
                <!-- /.box-footer -->
            </div>
            <!--/.box -->
        </div>
        <!-- /.col -->
    </div>
    <div class="row">
        <div class="col-md-4 col-sm-6 col-xs-12">
            <div class="box box-warning">
                <div class="box-header with-border">
                    <h3 class="box-title">Recent Posts</h3>
                    <div class="box-tools pull-right">
                        <button type="button" class="btn btn-box-tool" data-widget="collapse"><i
                                    class="fa fa-minus"></i>
                        </button>
                        <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i>
                        </button>
                    </div>
                </div>
                <div class="box-body">
                    <ul class="products-list product-list-in-box">
                        @forelse ($recentPosts as $post)
                            <li class="item">
                                <div class="product-img">
                                    <img src="{{ $post->getPicture() }}" alt="Product Image">
                                </div>
                                <div class="product-info">
                                    <a href="{{ $post->path(true) }}"
                                       class="product-title">{{ str_limit($post->title , 30) }}
                                        <span class="label label-info pull-right">
                                            {{ $post->comments()->count() . " " . str_plural("comment", $post->comments()->count())}}
                                        </span>
                                    </a>
                                    <span class="product-description">
                                        {{ str_limit($post->body, 50) }}
                                    </span>
                                </div>
                            </li>
                        @empty
                            <p>No Posts Available</p>
                        @endforelse
                    </ul>
                </div>
                <div class="box-footer text-center">
                    <a href="{{ url('/admin/posts') }}" class="uppercase">View All Posts</a>
                </div>
            </div>
        </div>
        <div class="col-md-4 col-sm-6 col-xs-12">
            <div class="box box-danger">
                <div class="box-header with-border">
                    <h3 class="box-title">Upcoming Events</h3>
                    <div class="box-tools pull-right">
                        <button type="button" class="btn btn-box-tool" data-widget="collapse"><i
                                    class="fa fa-minus"></i>
                        </button>
                        <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i>
                        </button>
                    </div>
                </div>
                <div class="box-body">
                    <ul class="products-list product-list-in-box">
                        @forelse ($recentEvents as $event)
                            <li class="item">
                                <div class="product-img">
                                    <img src="{{ $event->getPicture() }}" alt="Product Image">
                                </div>
                                <div class="product-info">
                                    <a href="{{ $event->path(true) }}"
                                       class="product-title">{{ str_limit($event->title , 30) }}
                                        <span class="label label-danger pull-right">
                                            {{ $event->date->format('M jS')}}
                                        </span>
                                    </a>
                                    <span class="product-description">
                                        {{ str_limit($event->body, 50) }}
                                    </span>
                                </div>
                            </li>
                        @empty
                            <p>No Events Available</p>
                        @endforelse
                    </ul>
                </div>
                <div class="box-footer text-center">
                    <a href="{{ url('/admin/events') }}" class="uppercase">View All Events</a>
                </div>
            </div>
        </div>
        <div class="col-md-4 col-sm-6 col-xs-12">
            <div class="box box-success">
                <div class="box-header with-border">
                    <h3 class="box-title">Recent News Posts</h3>
                    <div class="box-tools pull-right">
                        <button type="button" class="btn btn-box-tool" data-widget="collapse"><i
                                    class="fa fa-minus"></i>
                        </button>
                        <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i>
                        </button>
                    </div>
                </div>
                <div class="box-body">
                    <ul class="products-list product-list-in-box">
                        @forelse ($recentNews as $news)
                            <li class="item">
                                <div class="product-img">
                                    <img src="{{ $news->getPicture() }}" alt="Product Image">
                                </div>
                                <div class="product-info">
                                    <a href="{{ $news->path(true) }}"
                                       class="product-title">{{ str_limit($news->title , 30) }}
                                        <span class="label label-warning pull-right">
                                            {{ $news->created_at->diffForHumans() }}
                                        </span>
                                    </a>
                                    <span class="product-description">
                                        {{ str_limit($news->body, 50) }}
                                    </span>
                                </div>
                        @empty
                            <p>No News Available</p>
                        @endforelse
                    </ul>
                </div>
                <div class="box-footer text-center">
                    <a href="{{ url('/admin/news') }}" class="uppercase">View All News</a>
                </div>
            </div>
        </div>
    </div>
@stop

@section('css')
    <link rel="stylesheet" href="/css/admin_custom.css">
@stop

@section('js')
    <script>
        var ctx = document.getElementById("siteActivityChart").getContext("2d");

        var MONTHS = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];
        var myLineChart = new Chart(ctx, {
            type: 'line',
            //TODO: Convert data to JSON
            data: {
                labels: MONTHS,
                datasets: [{
                    label: "Posts",
                    backgroundColor: 'orange',
                    borderColor: 'orange',
                    fill: false,
                    data: [
                        {{ \App\Post::increaseDuringMonth(1) }},
                        {{ \App\Post::increaseDuringMonth(2) }},
                        {{ \App\Post::increaseDuringMonth(3) }},
                        {{ \App\Post::increaseDuringMonth(4) }},
                        {{ \App\Post::increaseDuringMonth(5) }},
                        {{ \App\Post::increaseDuringMonth(6) }},
                        {{ \App\Post::increaseDuringMonth(7) }},
                        {{ \App\Post::increaseDuringMonth(8) }},
                        {{ \App\Post::increaseDuringMonth(9) }},
                        {{ \App\Post::increaseDuringMonth(10) }},
                        {{ \App\Post::increaseDuringMonth(11) }},
                        {{ \App\Post::increaseDuringMonth(12) }}
                    ],
                }, {
                    label: "Events",
                    fill: false,
                    backgroundColor: 'red',
                    borderColor: 'red',
                    data: [
                        {{ \App\Event::increaseDuringMonth(1) }},
                        {{ \App\Event::increaseDuringMonth(2) }},
                        {{ \App\Event::increaseDuringMonth(3) }},
                        {{ \App\Event::increaseDuringMonth(4) }},
                        {{ \App\Event::increaseDuringMonth(5) }},
                        {{ \App\Event::increaseDuringMonth(6) }},
                        {{ \App\Event::increaseDuringMonth(7) }},
                        {{ \App\Event::increaseDuringMonth(8) }},
                        {{ \App\Event::increaseDuringMonth(9) }},
                        {{ \App\Event::increaseDuringMonth(10) }},
                        {{ \App\Event::increaseDuringMonth(11) }},
                        {{ \App\Event::increaseDuringMonth(12) }}
                    ],
                }, {
                    label: "News",
                    fill: false,
                    backgroundColor: 'teal',
                    borderColor: 'teal',
                    data: [
                        {{ \App\News::increaseDuringMonth(1) }},
                        {{ \App\News::increaseDuringMonth(2) }},
                        {{ \App\News::increaseDuringMonth(3) }},
                        {{ \App\News::increaseDuringMonth(4) }},
                        {{ \App\News::increaseDuringMonth(5) }},
                        {{ \App\News::increaseDuringMonth(6) }},
                        {{ \App\News::increaseDuringMonth(7) }},
                        {{ \App\News::increaseDuringMonth(8) }},
                        {{ \App\News::increaseDuringMonth(9) }},
                        {{ \App\News::increaseDuringMonth(10) }},
                        {{ \App\News::increaseDuringMonth(11) }},
                        {{ \App\News::increaseDuringMonth(12) }}
                    ],
                }, {
                    label: "Graduates",
                    fill: false,
                    backgroundColor: 'aqua',
                    borderColor: 'aqua',
                    data: [
                        {{ \App\Graduate::increaseDuringMonth(1) }},
                        {{ \App\Graduate::increaseDuringMonth(2) }},
                        {{ \App\Graduate::increaseDuringMonth(3) }},
                        {{ \App\Graduate::increaseDuringMonth(4) }},
                        {{ \App\Graduate::increaseDuringMonth(5) }},
                        {{ \App\Graduate::increaseDuringMonth(6) }},
                        {{ \App\Graduate::increaseDuringMonth(7) }},
                        {{ \App\Graduate::increaseDuringMonth(8) }},
                        {{ \App\Graduate::increaseDuringMonth(9) }},
                        {{ \App\Graduate::increaseDuringMonth(10) }},
                        {{ \App\Graduate::increaseDuringMonth(11) }},
                        {{ \App\Graduate::increaseDuringMonth(12) }}
                    ],
                }, {
                    label: "Companies",
                    fill: false,
                    backgroundColor: 'purple',
                    borderColor: 'purple',
                    data: [
                        {{ \App\Company::increaseDuringMonth(1) }},
                        {{ \App\Company::increaseDuringMonth(2) }},
                        {{ \App\Company::increaseDuringMonth(3) }},
                        {{ \App\Company::increaseDuringMonth(4) }},
                        {{ \App\Company::increaseDuringMonth(5) }},
                        {{ \App\Company::increaseDuringMonth(6) }},
                        {{ \App\Company::increaseDuringMonth(7) }},
                        {{ \App\Company::increaseDuringMonth(8) }},
                        {{ \App\Company::increaseDuringMonth(9) }},
                        {{ \App\Company::increaseDuringMonth(10) }},
                        {{ \App\Company::increaseDuringMonth(11) }},
                        {{ \App\Company::increaseDuringMonth(12) }}
                    ],
                }
                ]
            },
            options: {
                title: {
                    display: true,
                    text: 'Site Activity ({{ \Carbon\Carbon::now()->year -1 . ' - ' . \Carbon\Carbon::now()->year }})'
                }
            }
        });


    </script>
@stop