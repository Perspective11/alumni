@extends('adminlte::page')
@section('top-ex')
    <script src="/plugins/bootstrap-confirmation.min.js"></script>
@endsection
@section('content')
    <section class="FAB">
        <div class="FAB__action-button">
            <a href="{{ url('/admin/posts/create') }}"><i class="action-button__icon fa fa-plus"></i></a>
            <p class="action-button__text--hide">Add Post</p>
        </div>
    </section>
    @include('admin.post.filters')
    <table class="table table-condensed jdatatable display small" id="posts-table"  style="background-color: #fff;">


        <thead>
        <tr>
            <th>Title</th>
            <th>Body</th>
            <th>Status</th>
            <th>Posted_at</th>
            <th>Category</th>
            <th>Major</th>
            <th>User</th>
            <th>Tags</th>
            <th>Comments</th>
        </tr>
        </thead>
        <tbody>
        </tbody>
    </table>
@endsection
@section('footer-ex')
    <script>
        $(function () {
            $('.FAB__action-button').hover(function(){
                $(this).find('.action-button__text--hide').attr('class', 'action-button__text--show');
                $('.mini-action-button--hide').attr('class', 'mini-action-button--show');
            }, function(){
                $(this).find('.action-button__text--show').attr('class', 'action-button__text--hide');
            });

            var $postsTable = $('#posts-table').DataTable({
                "order": [[ 3, 'desc' ]],
                pageLength: 100,
                processing: true,
                serverSide: true,
                ajax: {
                    url: '{{url("/admin/posts/getData")}}',
                    data: function (d) {
//                        d.status = $('input[name=status]:checked').val();
                        d.status = '{{ request('status')? request('status') : '' }}'
//                        d.role = $('#role').find('option:selected').val();
                        d.major = '{{ request('major')? request('major') : ''}}'

                        d.category = '{{ request('category')? request('category') : ''}}'

                        d.user = '{{ request('user')? request('user') : ''}}'

                        d.tag = '{{ request('tag')? request('tag') : ''}}'
                    }
                },

                columns: [
                    {data: 'title', name: 'title'},
                    {data: 'body', name: 'body'},
                    {data: 'status', name: 'status', sortable: 'no', orderable: 'no', searchable: 'no'},
                    {data: 'created_at', name: 'created_at'},
                    {data: 'category', name: 'category', sortable: 'no', orderable: 'no', searchable: 'no'},
                    {data: 'major', name: 'major', sortable: 'no', orderable: 'no', searchable: 'no'},
                    {data: 'user', name: 'user', sortable: 'no', orderable: 'no', searchable: 'no'},
                    {data: 'tags', name: 'tags', sortable: 'no', orderable: 'no', searchable: 'no'},
                    {data: 'comments', name: 'comments', sortable: 'no', orderable: 'no', searchable: 'no'}
                ]
            });

            $('#posts-table tbody').on('click','.post-status', function (e) {
                e.stopPropagation()
            })

            $('#posts-table tbody').on('click','.post-comments', function (e) {
                e.stopPropagation()
            })

            var $modal = $('.action-modal');
            $('#posts-table tbody').on('click', 'tr', function () {

                var data = $postsTable.row(this).data();
                console.log(data);
                $('.action-modal').modal();
                $modal.find('.modal-post-title').text(data['title']);
                $modal.find('.modal-post-body').text(data['body']);
                $modal.find('.modal-post-category').html(data['category']);
                $modal.find('.modal-post-major').text(data['major']);
                $modal.find('.modal-post-user').text(data['user']);
                $modal.find('.modal-post-status').html(data['status']);
                $modal.find('.modal-post-postedAt').text(data['created_at']);
                $modal.find('.modal-post-tags').text(data['tags']);
                $modal.find('.modal-post-comments').html(data['comments']);
                $modal.find('.posts-by-user').attr('href' , '/admin/posts?user=' + data['user']);
                $modal.find('.view-post-details').attr('href' , '/admin/posts/' + data['id']);
                $modal.find('.edit-post-details').attr('href' , '/admin/posts/' + data['id'] + '/edit');
                $modal.find('.delete-post').attr('action' , '/admin/posts/' + data['id']);
            });

            $('[data-toggle=confirmation]').confirmation({
                rootSelector: '[data-toggle=confirmation]',
                title: 'Are you sure you want to delete this post and all it\'s comments?',
                btnOkClass: 'btn btn-xs btn-danger',
                btnCancelClass: 'btn btn-xs btn-default',
                btnOkIcon: 'fa fa-trash',
                btnCancelIcon: 'fa fa-ban'
            });

        });
    </script>
@endsection


@section('modals')
    <div class="modal fade action-modal" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Action</h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="modal-field">
                                <strong>Title:</strong> <span class="modal-post-title"></span> <br>
                            </div>
                            <div class="modal-field">
                                <strong>Body:</strong> <span class="modal-post-body"></span> <br>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="modal-field">
                                <strong>Category:</strong> <span class="modal-post-category"></span> <br>
                            </div>
                            <div class="modal-field">
                                <strong>Major:</strong> <span class="modal-post-major"></span> <br>
                            </div>
                            <div class="modal-field">
                                <strong>User:</strong> <span class="modal-post-user"></span> <br>
                            </div>
                            <div class="modal-field">
                                <strong>Status:</strong> <span class="modal-post-status"></span> <br>
                            </div>
                            <div class="modal-field">
                                <strong>Posted At:</strong> <span class="modal-post-postedAt"></span> <br>
                            </div>
                            <div class="modal-field">
                                <strong>Tags:</strong> <span class="modal-post-tags"></span> <br>
                            </div>
                            <div class="modal-field">
                                <strong>Comments:</strong> <span class="modal-post-comments"></span> <br>
                            </div>
                        </div>
                    </div>
                    <hr>
                    <div class="list-group">
                        <a href="#" class="list-group-item posts-by-user">Posts by the same user</a>
                        <a href="#" class="list-group-item view-post-details">View post details</a>
                        <a href="#" class="list-group-item edit-post-details">Edit post details</a>
                        <form class="list-group-item list-group-item-danger delete-post" method="post" action="">
                            {{ csrf_field() }}
                            {{ method_field('DELETE') }}
                            <button type="submit" class="btn-link text-red" data-toggle="confirmation" data-popout="true"> Delete Post</button>
                        </form>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->

@endsection