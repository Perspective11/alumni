@section('top-ex')
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/css/select2.min.css" rel="stylesheet"/>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js"></script>
@endsection

<div class="row">
    <div class="col-md-12">
        <div class="form-group {{  $errors->has('title') ? ' has-error' : ''  }}">
            <label for="title">Title:</label> <span style="color:orangered">*</span>
            <input type="text" name="title" class="form-control" id="title" value="{{ old('title') ?? $post->title }}" required>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <div class="form-group {{  $errors->has('body') ? ' has-error' : ''  }}">
            <label for="body">Body:</label> <span style="color:orangered">*</span>
            <textarea class="form-control" name="body" id="body" rows="5" required>{{ old('body') ?? $post->body }}</textarea>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-6">
        <div class="form-group {{  $errors->has('category') ? ' has-error' : ''  }}">
            <label for="category">Category</label>
            <select name="category" class="form-control select-category" id="category">
                <option value="">All</option>
                @foreach($categories as $category)
                    @php
                        $selected = '';
                        if (old('category')){
                            if (old('category') == $category->id)
                            $selected = true;
                        }
                        elseif ($post->category && $post->category->id == $category->id)
                            $selected = true;
                        else $selected = false;
                    @endphp
                    <option {{ $selected? 'selected' : '' }}
                            value="{{ $category->id }}">
                        {{ $category->name }}
                    </option>
                @endforeach
            </select>
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group {{  $errors->has('major') ? ' has-error' : ''  }}">
            <label for="major">Major</label>
            <select name="major" class="form-control select-major" id="major">
                <option value="" selected>All</option>
                @foreach($majors as $major)
                    @php
                        $selected = '';
                        if (old('major')){
                            if (old('major') == $major->id)
                            $selected = true;
                        }
                        elseif ($post->major && $post->major->id == $major->id)
                            $selected = true;
                        else $selected = false;
                    @endphp
                    <option {{ $selected? 'selected' : '' }}
                            value="{{ $major->id }}">
                        {{ $major->name }}
                    </option>
                @endforeach
            </select>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-6">

        <div class="form-group{{ $errors->has('image') ? ' has-error' : '' }}">
            <label for="image" class="control-label">Image</label>
            @if($post->pictures->first())
                <div class="alert">
                    <p>This post already contains an image</p>
                    <a class="btn btn-xs btn-warning change-image">Change Image</a>
                </div>
            @else
                <input type="file" name="picture" id="picture">
                {{--TODO: add a drag and drop plugin--}}
            @endif
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group{{ $errors->has('tags') ? ' has-error' : '' }}">
            <label for="tags">Tags</label>
            <select class="select-tags form-control" name="tags[]" multiple="multiple">
                @foreach($tags as $tag)
                    @php
                        $selected = '';
                        if (old('tags')){
                            if (in_array($tag->id, old('tags')))
                            $selected = true;
                        }
                        elseif ($post->tags->pluck('id')->contains($tag->id))
                            $selected = true;
                        else $selected = false;
                    @endphp
                    <option {{ $selected ? 'selected': ''}} value="{{ $tag->id }}">{{ $tag->name }} </option>
                    {{-- TODO: create tags on the fly --}}
                @endforeach
            </select>
        </div>
    </div>

</div>
@section('footer-ex')
    <script type="text/javascript">
        $(".select-tags").select2({
            placeholder: "Select tags",
            allowClear: true,
            maximumSelectionLength: 5
        });
        $(".select-category").select2();
        $(".select-major").select2();

        $('.change-image').on('click', function (e) {
            e.preventDefault();

            $(this).parent().replaceWith('<input type="file" name="picture" id="picture">');
        })

    </script>
@endsection