@extends("adminlte::page")
@section('top-ex')
    <script src="/plugins/bootstrap-confirmation.min.js"></script>
@endsection
@section("content_header")
    <h3>Post Details</h3>
    <div class="">
        <a href="{{ $post->path(true) . '/edit' }}" class="btn btn-warning btn-sm"><i class="fa fa-pencil"></i> Edit</a>
        <form class="form-inline inline" method="post" action="{{ $post->path(true) }}">
            {{ csrf_field() }}
            {{ method_field('DELETE') }}
            <button type="submit" class="btn btn-danger btn-sm" data-toggle="confirmation" data-popout="true"><i
                        class="fa fa-times"></i> Delete
            </button>
        </form>
    </div>
@endsection


@section("content")
    <div class="row">
        @if($post->pictures()->count())
            <div class="col-md-3">
                <div class="form-group">
                    <label for="image">Image:</label>
                    <img src="{{$post->pictures()->count()? $post->pictures()->first()->path :''}}"
                         class="img-responsive img-rounded" alt="">
                </div>
            </div>
        @endif
        <div class="col-md-6">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">Post</h3>
                </div><!-- /.box-header -->
                <div class="box-body">
                    <div class="form-group">
                        <label class="center-block" for="user">User:</label>
                        @if(($post->user->child() !== null) && $post->user->child()->pictures()->count())
                            <img src="{{ $post->user->child()->pictures()->first()->path }}" alt=""
                                 style="max-height:50px" class="inline img-responsive img-rounded">
                        @endif
                        <span class="inline" id="user">{{$post->user->childOrUserName()}}
                            ( {{$post->user->roles()->first()->name}} )</span>
                    </div>
                    <div class="form-group">
                        <label for="title">Title:</label>
                        <p id="title">{{$post->title}}</p>
                    </div>
                    <div class="form-group">
                        <label for="body">Body:</label>
                        <p id="body">{{$post->body}}</p>
                    </div>
                </div><!-- /.box-body -->
            </div><!-- /.box -->

        </div>
        <div class="col-md-3">
            <div class="box box-success">
                <div class="box-header with-border">
                    <h3 class="box-title">Info</h3>
                </div><!-- /.box-header -->
                <div class="box-body">
                    <div class="form-group">
                        <label for="category">Category:</label>
                        @if($post->category)
                            <p id="category">
                                <i style="color: {{$post->category->color}}"
                                   class="fa {{ $post->category->icon }}"></i> {{$post->category->name}}
                            </p>
                        @endif
                    </div>
                    <div class="form-group">
                        <label for="major">Major:</label>
                        @if($post->major)
                            <p id="major">
                                <i style="color: {{$post->major->color}}"
                                   class="fa {{ $post->major->icon }}"></i> {{$post->major->code . ' ' . $post->major->name}}
                            </p>
                        @endif
                    </div>
                    <div class="form-group">
                        <label for="tags">Tags:</label>
                        @if($post->tags)
                            <p id="tags">{{ $post->tagsString() }}</p>
                        @endif
                    </div>
                    <div class="form-group">
                        <label for="created_at">Posted At:</label>
                        <p id="created_at">{{$post->created_at->diffForHumans()}}</p>
                    </div>
                    <div class="form-group">
                        <label for="updated_at">Updated At:</label>
                        <p id="updated_at">{{$post->updated_at->diffForHumans()}}</p>
                    </div>
                    <div class="form-group">
                        @php
                            $class = $post->status ? 'success' : 'danger';
                            $status = $post->status ? 'Unpublish' : 'Publish';
                        @endphp
                        <label for="status">Status:</label><br>
                        <a href="{{ $post->path(true) . '/activation' }}"
                           class="btn btn-xs post-status btn-{{ $class}}">{{ $status }}</a>
                    </div>
                </div><!-- /.box-body -->
            </div><!-- /.box -->
        </div>
    </div>
    <hr>
    @include('admin.layouts.comments')

@endsection

@section('footer-ex')
    <script>
        $('[data-toggle=confirmation]').confirmation({
            rootSelector: '[data-toggle=confirmation]',
            title: 'Are you sure you want to delete this post and all it\'s comments?',
            btnOkClass: 'btn btn-xs btn-danger',
            btnCancelClass: 'btn btn-xs btn-default',
            btnOkIcon: 'fa fa-trash',
            btnCancelIcon: 'fa fa-ban'
        });
    </script>
@endsection