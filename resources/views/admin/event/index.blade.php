@extends('adminlte::page')
@section('top-ex')
    <script type="text/javascript" src="//cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>

    <script type="text/javascript" src="//cdn.jsdelivr.net/bootstrap.daterangepicker/2/daterangepicker.js"></script>
    <link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/bootstrap.daterangepicker/2/daterangepicker.css" />
    <script src="/plugins/bootstrap-confirmation.min.js"></script>

@endsection
@section('content')
    <section class="FAB">
        <div class="FAB__action-button">
            <a href="{{ url('/admin/events/create') }}"><i class="action-button__icon fa fa-calendar-plus-o"></i></a>
            <p class="action-button__text--hide">Add Event</p>
        </div>
    </section>
    @include('admin.event.filters')
    <table class="table table-condensed jdatatable display smaller" id="events-table"  style="background-color: #fff;">
        <thead>
        <tr>
            <th>Title</th>
            <th>Body</th>
            <th>Location</th>
            <th>Scheduled</th>
            <th>Status</th>
            <th>Posted_at</th>
            <th>Category</th>
            <th>Major</th>
            <th>User</th>
            <th>Tags</th>
            <th>Comments</th>
        </tr>
        </thead>
        <tbody>
        </tbody>
    </table>
@endsection
@section('footer-ex')
    <script>
        $(function () {
            $('.FAB__action-button').hover(function(){
                $(this).find('.action-button__text--hide').attr('class', 'action-button__text--show');
                $('.mini-action-button--hide').attr('class', 'mini-action-button--show');
            }, function(){
                $(this).find('.action-button__text--show').attr('class', 'action-button__text--hide');
            });

            var $eventsTable = $('#events-table').DataTable({
                "order": [[ 5, 'desc' ]],
                pageLength: 100,
                processing: true,
                serverSide: true,
                ajax: {
                    url: '{{url("/admin/events/getData")}}',
                    data: function (d) {
//                        d.status = $('input[name=status]:checked').val();
                        d.status = '{{ request('status')? request('status') : '' }}'
//                        d.role = $('#role').find('option:selected').val();
                        d.major = '{{ request('major')? request('major') : ''}}'

                        d.category = '{{ request('category')? request('category') : ''}}'

                        d.user = '{{ request('user')? request('user') : ''}}'

                        d.tag = '{{ request('tag')? request('tag') : ''}}'

                        d.daterange = '{{ request('daterange')? request('daterange') : ''}}'
                    }
                },

                columns: [
                    {data: 'title', name: 'title'},
                    {data: 'body', name: 'body'},
                    {data: 'location', name: 'location'},
                    {data: 'datetime', name: 'datetime', sortable: 'no', orderable: 'no', searchable: 'no'},
                    {data: 'status', name: 'status', sortable: 'no', orderable: 'no', searchable: 'no'},
                    {data: 'created_at', name: 'created_at'},
                    {data: 'category', name: 'category', sortable: 'no', orderable: 'no', searchable: 'no'},
                    {data: 'major', name: 'major', sortable: 'no', orderable: 'no', searchable: 'no'},
                    {data: 'user', name: 'user', sortable: 'no', orderable: 'no', searchable: 'no'},
                    {data: 'tags', name: 'tags', sortable: 'no', orderable: 'no', searchable: 'no'},
                    {data: 'comments', name: 'comments', sortable: 'no', orderable: 'no', searchable: 'no'}

                ]
            });

            $('#events-table tbody').on('click','.event-status', function (e) {
                e.stopPropagation()
            });
            $('#events-table tbody').on('click','.event-comments', function (e) {
                e.stopPropagation()
            });

            var $modal = $('.action-modal');
            $('#events-table tbody').on('click', 'tr', function () {

                var data = $eventsTable.row(this).data();
                console.log(data);
                $('.action-modal').modal();
                $modal.find('.modal-event-title').text(data['title']);
                $modal.find('.modal-event-body').text(data['body']);
                $modal.find('.modal-event-location').text(data['location']);
                $modal.find('.modal-event-datetime').html(data['datetime']);
                $modal.find('.modal-event-category').html(data['category']);
                $modal.find('.modal-event-major').html(data['major']);
                $modal.find('.modal-event-user').text(data['user']);
                $modal.find('.modal-event-status').html(data['status']);
                $modal.find('.modal-event-postedAt').text(data['created_at']);
                $modal.find('.modal-event-tags').text(data['tags']);
                $modal.find('.modal-event-comments').html(data['comments']);
                $modal.find('.events-by-user').attr('href' , '/admin/events?user=' + data['user']);
                $modal.find('.view-event-details').attr('href' , '/admin/events/' + data['id']);
                $modal.find('.edit-event-details').attr('href' , '/admin/events/' + data['id'] + '/edit');
                $modal.find('.delete-event').attr('action' , '/admin/events/' + data['id']);


            });

            $('[data-toggle=confirmation]').confirmation({
                rootSelector: '[data-toggle=confirmation]',
                title: 'Are you sure you want to delete this event and all it\'s comments?',
                btnOkClass: 'btn btn-xs btn-danger',
                btnCancelClass: 'btn btn-xs btn-default',
                btnOkIcon: 'fa fa-trash',
                btnCancelIcon: 'fa fa-ban'
            });

        });
    </script>
@endsection


@section('modals')
    <div class="modal fade action-modal" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Action</h4>
                </div>
                <div class="modal-body">
                    <div class="row modal-att">
                        <div class="col-md-8">
                            <div class="modal-field">
                                <strong>Title:</strong> <span class="modal-event-title"></span> <br>
                            </div>
                            <div class="modal-field">
                                <strong>Body:</strong> <span class="modal-event-body"></span> <br>
                            </div>
                            <div class="modal-field">
                                <strong>Location:</strong> <span class="modal-event-location"></span> <br>
                            </div>
                            <div class="modal-field">
                                <strong>Scheduled:</strong> <span class="modal-event-datetime"></span> <br>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="modal-field">
                                <strong>Category:</strong> <span class="modal-event-category"></span> <br>
                            </div>
                            <div class="modal-field">
                                <strong>Major:</strong> <span class="modal-event-major"></span> <br>
                            </div>
                            <div class="modal-field">
                                <strong>User:</strong> <span class="modal-event-user"></span> <br>
                            </div>
                            <div class="modal-field">
                                <strong>Status:</strong> <span class="modal-event-status"></span> <br>
                            </div>
                            <div class="modal-field">
                                <strong>Posted At:</strong> <span class="modal-event-postedAt"></span> <br>
                            </div>
                            <div class="modal-field">
                                <strong>Tags:</strong> <span class="modal-event-tags"></span> <br>
                            </div>
                            <div class="modal-field">
                                <strong>Comments:</strong> <span class="modal-event-comments"></span> <br>
                            </div>
                        </div>
                    </div>
                    <hr>
                    <div class="list-group">
                        <a href="#" class="list-group-item events-by-user">Events by the same user</a>
                        <a href="#" class="list-group-item view-event-details">View event details</a>
                        <a href="#" class="list-group-item edit-event-details">Edit event details</a>
                        <form class="list-group-item list-group-item-danger delete-event" method="post" action="">
                            {{ csrf_field() }}
                            {{ method_field('DELETE') }}
                            <button type="submit" class="btn-link text-red" data-toggle="confirmation" data-popout="true"> Delete Event</button>
                        </form>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->

@endsection