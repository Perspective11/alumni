@section('top-ex')
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/css/select2.min.css" rel="stylesheet" />
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js"></script>
    <link rel="stylesheet" href="/vendor/adminlte/plugins/datepicker/datepicker3.css">
    <link rel="stylesheet" href="/vendor/adminlte/plugins/timepicker/bootstrap-timepicker.css">
@endsection

<div class="row">
    <div class="col-md-12">
        <div class="form-group {{  $errors->has('title') ? ' has-error' : ''  }}">
            <label for="title">Title:</label> <span style="color:orangered">*</span>
            <input type="text" name="title" class="form-control" id="title" value="{{ old('title') ?? $event->title }}">
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <div class="form-group {{  $errors->has('body') ? ' has-error' : ''  }}">
            <label for="body">Body:</label> <span style="color:orangered">*</span>
            <textarea class="form-control" name="body" id="body" rows="5">{{ old('body') ?? $event->body }}</textarea>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <div class="form-group {{  $errors->has('location') ? ' has-error' : ''  }}">
            <label for="location">Location:</label>
            <input type="text" name="location" class="form-control" id="location"
                   value="{{ old('location') ?? $event->location }}">
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-6">
        <div class="form-group {{  $errors->has('date') ? ' has-error' : ''  }}">
            <label for="date">Date:</label> <span style="color:orangered">*</span>
            <div class="input-group date">
                <input placeholder="Select Date"
                       type="text"
                       value="{{ old('date') ?? ($event->date? $event->date->format('Y-m-d') : '') }}"
                       name="date"
                       id="date"
                       class="form-control">
                <div class="input-group-addon">
                    <span class="fa fa-calendar"></span>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-3">
        <div class="form-group {{  $errors->has('start') ? ' has-error' : ''  }}">
            <label for="date">Start Time:</label> <span style="color:orangered">*</span>
            <div class="input-group bootstrap-timepicker timepicker">
                <input placeholder="Select Time"
                       id="start"
                       type="text"
                       value="{{ old('start')?? ($event->start? \Carbon\Carbon::parse($event->start)->format('h:i A') : '') }}"
                       name="start"
                       class="form-control input-small">
                <span class="input-group-addon"><i class="glyphicon glyphicon-time"></i></span>
            </div>
        </div>
    </div>
    <div class="col-md-3">
        <div class="form-group {{  $errors->has('end') ? ' has-error' : ''  }}">
            <label for="end">End Time:</label> <span style="color:orangered">*</span>
            <div class="input-group bootstrap-timepicker timepicker">
                <input placeholder="Select Time"
                       id="end"
                       type="text"
                       value="{{ old('end')?? ($event->end? \Carbon\Carbon::parse($event->end)->format('h:i A') : '') }}"
                       name="end"
                       class="form-control input-small">
                <span class="input-group-addon"><i class="glyphicon glyphicon-time"></i></span>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            <label for="category">Category</label>
            <select name="category" class="form-control select-category" id="category">
                <option value="">All</option>
                @foreach($categories as $category)
                    @php
                        $selected = '';
                        if (old('category')){
                            if (old('category') == $category->id)
                            $selected = true;
                        }
                        elseif ($event->category && $event->category->id == $category->id)
                            $selected = true;
                        else $selected = false;
                    @endphp
                    <option {{ $selected? 'selected' : '' }}
                            value="{{ $category->id }}">
                        {{ $category->name }}
                    </option>
                @endforeach
            </select>
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            <label for="major">Major</label>
            <select name="major" class="form-control select-major" id="major">
                <option value="">All</option>
                @foreach($majors as $major)
                    @php
                        $selected = '';
                        if (old('major')){
                            if (old('major') == $major->id)
                            $selected = true;
                        }
                        elseif ($event->major && $event->major->id == $major->id)
                            $selected = true;
                        else $selected = false;
                    @endphp
                    <option {{ $selected? 'selected' : '' }}
                            value="{{ $major->id }}">
                        {{ $major->name }}
                    </option>
                @endforeach
            </select>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-6">
        <div class="form-group{{ $errors->has('image') ? ' has-error' : '' }}">
            <label for="image" class="control-label">Image</label>
            @if($event->pictures->first())
                <div class="alert">
                    <p>This event already contains an image</p>
                    <a class="btn btn-xs btn-warning change-image">Change Image</a>
                </div>
            @else
                <input type="file" name="picture" id="picture">
                {{--TODO: add a drag and drop plugin--}}
            @endif
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group{{ $errors->has('tags') ? ' has-error' : '' }}">
            <label for="tags">Tags</label>
            <select class="select-tags form-control" name="tags[]" multiple="multiple">
                @foreach($tags as $tag)
                    @php
                        $selected = '';
                        if (old('tags')){
                            if (in_array($tag->id, old('tags')))
                            $selected = true;
                        }
                        elseif ($event->tags->pluck('id')->contains($tag->id))
                            $selected = true;
                        else $selected = false;
                    @endphp
                    <option {{ $selected ? 'selected': ''}} value="{{ $tag->id }}">{{ $tag->name }} </option>
                    {{-- TODO: create tags on the fly --}}
                @endforeach
            </select>
        </div>
    </div>

</div>
@section('footer-ex')
    <script type="text/javascript" src="/vendor/adminlte/plugins/datepicker/bootstrap-datepicker.js"></script>
    <script type="text/javascript" src="/vendor/adminlte/plugins/timepicker/bootstrap-timepicker.js"></script>
    <script type="text/javascript">

        $('#date').datepicker({
            startDate: '+1d',
            format: 'yyyy-mm-dd',
        });

        $('#start').timepicker({
            defaultTime: false
        });

        $('#end').timepicker({
            defaultTime: false
        });

        $(".select-tags").select2({
            placeholder: "Select tags",
            allowClear: true,
            maximumSelectionLength: 5
        });
        $(".select-category").select2();
        $(".select-major").select2();

        $('.change-image').on('click', function (e) {
            e.preventDefault();

            $(this).parent().replaceWith('<input type="file" name="picture" id="picture">');
        })

    </script>
@endsection