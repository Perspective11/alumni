@extends("adminlte::page")
@section('top-ex')
    <script src="/plugins/bootstrap-confirmation.min.js"></script>
@endsection
@section("content_header")
    <h3>Event Details</h3>
    <div class="">
        <a href="{{ $event->path(true) . '/edit' }}" class="btn btn-warning btn-sm"><i class="fa fa-pencil"></i> Edit</a>
        <form class="form-inline inline" method="post" action="{{ $event->path(true) }}">
            {{ csrf_field() }}
            {{ method_field('DELETE') }}
            <button type="submit" class="btn btn-danger btn-sm" data-toggle="confirmation" data-popout="true"><i class="fa fa-times"></i> Delete</button>
        </form>
    </div>
@endsection


@section("content")
    <div class="row">
        @if($event->pictures()->count())
            <div class="col-md-3">
                <div class="form-group">
                    <label for="image">Image:</label>
                    <img src="{{$event->pictures()->count()? $event->pictures()->first()->path :''}}"
                         class="img-responsive img-rounded" alt="">
                </div>
            </div>
        @endif
        <div class="col-md-6">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">Event</h3>
                </div><!-- /.box-header -->
                <div class="box-body">
                    <div class="form-group">
                        <label class="center-block" for="user">User:</label>
                        @if(($event->user->child() !== null) && $event->user->child()->pictures()->count())
                            <img src="{{ $event->user->child()->pictures()->first()->path }}" alt="" style="max-height:50px" class="inline img-responsive img-rounded">
                        @endif
                        <span class="inline" id="user">{{$event->user->childOrUserName()}} ( {{$event->user->roles()->first()->name}} )</span>
                    </div>
                    <div class="form-group">
                        <label for="title">Title:</label>
                        <p id="title">{{$event->title}}</p>
                    </div>
                    <div class="form-group">
                        <label for="body">Body:</label>
                        <p id="body">{{$event->body}}</p>
                    </div>
                    <div class="form-group">
                        <label for="body">Location:</label>
                        <p id="location">{{$event->location}}</p>
                    </div>

                </div><!-- /.box-body -->
            </div><!-- /.box -->

        </div>
        <div class="col-md-3">
            <div class="box box-success">
                <div class="box-header with-border">
                    <h3 class="box-title">Info</h3>
                </div><!-- /.box-header -->
                <div class="box-body">
                    <div class="form-group">
                        @php
                            $date = $event->date->toFormattedDateString();
                            $start =  date("g:i a", strtotime( $event->start));
                            $end = date("g:i a", strtotime( $event->end));
                        @endphp
                        <label for="datetime">Datetime:</label><br>
                        <div class="label label-sm label-info">{{ $date }}</div>
                        <br><br><div class="label label-sm label-warning">{{ $start . ' - ' . $end }}</div>

                    </div>
                    <div class="form-group">
                        <label for="category">Category:</label>
                        @if($event->category)
                            <p id="category">
                                <i style="color: {{$event->category->color}}"
                                   class="fa {{ $event->category->icon }}"></i> {{$event->category->name}}
                            </p>
                        @endif
                    </div>
                    <div class="form-group">
                        <label for="major">Major:</label>
                        @if($event->major)
                            <p id="major">
                                <i style="color: {{$event->major->color}}"
                                   class="fa {{ $event->major->icon }}"></i> {{$event->major->code . ' ' . $event->major->name}}
                            </p>
                        @endif
                    </div>
                    <div class="form-group">
                        <label for="tags">Tags:</label>
                        @if($event->tags)
                            <p id="tags">{{ $event->tagsString() }}</p>
                        @endif
                    </div>
                    <div class="form-group">
                        <label for="created_at">Posted At:</label>
                        <p id="created_at">{{$event->created_at->diffForHumans()}}</p>
                    </div>
                    <div class="form-group">
                        <label for="updated_at">Updated At:</label>
                        <p id="updated_at">{{$event->updated_at->diffForHumans()}}</p>
                    </div>
                    <div class="form-group">
                        @php
                            $class = $event->status ? 'success' : 'danger';
                            $status = $event->status ? 'Unpublish' : 'Publish';
                        @endphp
                        <label for="status">Status:</label><br>
                        <a href="{{ $event->path(true) . '/activation' }}" class="btn btn-xs event-status btn-{{ $class}}">{{ $status }}</a>
                    </div>
                </div><!-- /.box-body -->
            </div><!-- /.box -->
        </div>
    </div>
    <hr>
    @include('admin.layouts.comments')

@endsection

@section('footer-ex')
    <script>
        $('[data-toggle=confirmation]').confirmation({
            rootSelector: '[data-toggle=confirmation]',
            title: 'Are you sure you want to delete this event and all it\'s comments?',
            btnOkClass: 'btn btn-xs btn-danger',
            btnCancelClass: 'btn btn-xs btn-default',
            btnOkIcon: 'fa fa-trash',
            btnCancelIcon: 'fa fa-ban'
        });
    </script>
@endsection