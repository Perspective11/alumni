@extends('adminlte::page')
@section('top-ex')
    <script src="/plugins/bootstrap-confirmation.min.js"></script>
@endsection
@section('content')
    <section class="FAB">
        <div class="FAB__action-button">
            <a href="{{ url('/admin/majors/create') }}"><i class="action-button__icon fa fa-plus"></i></a>
            <p class="action-button__text--hide">Add Major</p>
        </div>
    </section>
    <table class="table table-condensed jdatatable display" id="majors-table"  style="background-color: #fff;">
        <thead>
        <tr>
            <th>Name</th>
            <th>Code</th>
            <th>Icon</th>
            <th>Graduates</th>
            <th>Posts</th>
            <th>Events</th>
            <th>News</th>
        </tr>
        </thead>
        <tbody>
        </tbody>
    </table>
@endsection
@section('footer-ex')
    <script>
        $(function () {
            $('.FAB__action-button').hover(function(){
                $(this).find('.action-button__text--hide').attr('class', 'action-button__text--show');
                $('.mini-action-button--hide').attr('class', 'mini-action-button--show');
            }, function(){
                $(this).find('.action-button__text--show').attr('class', 'action-button__text--hide');
            });

            var $majorsTable = $('#majors-table').DataTable({
                "order": [[ 0, 'desc' ]],
                pageLength: 100,
                processing: true,
                serverSide: true,
                ajax: {
                    url: '{{url("/admin/majors/getData")}}',
                    data: function (d) {
                    }
                },

                columns: [
                    {data: 'name', name: 'name'},
                    {data: 'code', name: 'code'},
                    {data: 'icon', name: 'icon', sortable: 'no', orderable: 'no', searchable: 'no'},
                    {data: 'graduates', name: 'graduates', sortable: 'no', orderable: 'no', searchable: 'no'},
                    {data: 'posts', name: 'posts', sortable: 'no', orderable: 'no', searchable: 'no'},
                    {data: 'events', name: 'events', sortable: 'no', orderable: 'no', searchable: 'no'},
                    {data: 'news', name: 'news', sortable: 'no', orderable: 'no', searchable: 'no'},
                ]
            });

            var $modal = $('.action-modal');
            $('#majors-table tbody').on('click', 'tr', function () {

                var data = $majorsTable.row(this).data();
                console.log(data);
                $('.action-modal').modal();
                $modal.find('.modal-major-name').text(data['name']);
                $modal.find('.modal-major-code').text(data['code']);
                $modal.find('.modal-major-icon').html(data['icon']);
                $modal.find('.modal-major-graduates').html(data['graduates']);
                $modal.find('.modal-major-posts').html(data['posts']);
                $modal.find('.modal-major-news').html(data['news']);
                $modal.find('.modal-major-events').html(data['events']);
                $modal.find('.view-major-graduates').attr('href', '/admin/graduates?major=' + data['name']);
                $modal.find('.view-major-posts').attr('href', '/admin/posts?major=' + data['name']);
                $modal.find('.view-major-events').attr('href', '/admin/events?major=' + data['name']);
                $modal.find('.view-major-news').attr('href', '/admin/news?major=' + data['name']);
                $modal.find('.edit-major').attr('href', '/admin/majors/' + data['id'] + '/edit');
                $modal.find('.delete-major').attr('action' , '/admin/majors/' + data['id']);

            });

            $('[data-toggle=confirmation]').confirmation({
                rootSelector: '[data-toggle=confirmation]',
                title: 'Are you sure you want to delete this major?',
                btnOkClass: 'btn btn-xs btn-danger',
                btnCancelClass: 'btn btn-xs btn-default',
                btnOkIcon: 'fa fa-trash',
                btnCancelIcon: 'fa fa-ban'
            });

        });
    </script>
@endsection


@section('modals')
    <div class="modal fade action-modal" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Action</h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="modal-field">
                                <strong>Name:</strong> <span class="modal-major-name"></span> <br>
                            </div>
                            <div class="modal-field">
                                <strong>Code:</strong> <span class="modal-major-code"></span> <br>
                            </div>
                            <div class="modal-field">
                                <strong>Icon:</strong> <span class="modal-major-icon"></span> <br>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="modal-field">
                                <strong>Graduates:</strong> <span class="modal-major-graduates"></span> <br>
                            </div>
                            <div class="modal-field">
                                <strong>Posts:</strong> <span class="modal-major-posts"></span> <br>
                            </div>
                            <div class="modal-field">
                                <strong>Events:</strong> <span class="modal-major-events"></span> <br>
                            </div>
                            <div class="modal-field">
                                <strong>News:</strong> <span class="modal-major-news"></span> <br>
                            </div>
                        </div>
                    </div>
                    <hr>
                    <div class="list-group">
                        <a href="#" class="list-group-item view-major-graduates">Graduates with this major</a>
                        <a href="#" class="list-group-item  view-major-posts">Posts with this major</a>
                        <a href="#" class="list-group-item  view-major-events">Events with this major</a>
                        <a href="#" class="list-group-item  view-major-news">News with this major</a>
                        <a href="#" class="list-group-item edit-major">Edit major</a>
                        <form class="list-group-item list-group-item-danger delete-major" method="post" action="">
                            {{ csrf_field() }}
                            {{ method_field('DELETE') }}
                            <button type="submit" class="btn-link text-red" data-toggle="confirmation" data-popout="true"> Delete Major</button>
                        </form>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->

@endsection
