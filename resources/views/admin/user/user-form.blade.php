<input type="hidden" name="id" value="{{ $user->id }}">
<div class="row">
    <div class="col-md-6">
        <div class="form-group {{  $errors->has('name') ? ' has-error' : ''  }}">
            <label for="name">Username:</label> <span style="color:orangered">*</span>
            <input type="text" name="name" class="form-control" id="name" value="{{ old('name') ?? $user->name }}"
                   required>
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group {{  $errors->has('email') ? ' has-error' : ''  }}">
            <label for="email">Email Address:</label> <span style="color:orangered">*</span>
            <input type="email" name="email" class="form-control" id="email" value="{{ old('email') ?? $user->email }}"
                   required>
        </div>
    </div>
</div>
<div class="row">
        <div class="col-md-6">
            <div class="form-group {{  $errors->has('password') ? ' has-error' : ''  }}">
                <label for="password">Password:</label> <span style="color:orangered">*</span>
                <input type="password" name="password" class="form-control" id="password" placeholder="{{ $user->password? '(Unchanged)' : '' }}" >
            </div>
        </div>
        <div class="col-md-6">
            <div class="form-group {{  $errors->has('password_confirmation') ? ' has-error' : ''  }}">
                <label for="password_confirmation">Confirm Password:</label> <span style="color:orangered">*</span>
                <input type="password" name="password_confirmation" class="form-control" id="password_confirmation" placeholder="{{ $user->password? '(Unchanged)' : '' }}">
            </div>
        </div>
    </div>
<div class="row">
    <div class="col-md-6">
        <div class="center-block form-group {{ $errors->has('user-type') ? ' has-error' : '' }}">
            @php
                $checked = '';
                $disabled = '';
                if (old('user_type')){
                    $checked = old('user_type');
                }
                elseif (count($user->roles))
                    $checked = $user->roles->first()->id;

                if(count($user->roles)){
                    $disabled = 'disabled';
                }
            @endphp
            <label for="user-type">Type:</label> <span style="color:orangered">*</span>
            @if(count($user->roles))
                <input type="hidden" name="user_type" value="{{ $user->roles()->first()->id}}">
                <p>{{ $user->roles()->first()->name }}</p>
            @else
                <div class="">
                    <input {{ $disabled }} type="radio" name="user_type" id="user-type-admin" class="user-type"
                           value="1" {{ $checked == '1'? 'checked' : ''  }}>
                    Admin &nbsp;&nbsp;
                    <input {{ $disabled }} type="radio" name="user_type" id="user-type-moderator" class="user-type"
                           value="2" {{ $checked == '2'? 'checked' : ''  }}>
                    Moderator &nbsp;&nbsp;
                    <input {{ $disabled }} type="radio" name="user_type" id="user-type-graduate" class="user-type"
                           value="3" {{ $checked == '3'? 'checked' : ''  }}>
                    Graduate &nbsp;&nbsp;
                    <input {{ $disabled }} type="radio" name="user_type" id="user-type-company" class="user-type"
                           value="4" {{ $checked == '4'? 'checked' : '' }}>
                    Company
                </div>
            @endif
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group {{ $errors->has('user_status') ? ' has-error' : '' }}">
            @php
                $checked = '0';
                if (old('user_status')){
                    $checked = old('user_status');
                }
                else
                    $checked = $user->user_status ?? '0';
            @endphp
            <label for="user_status">Status:</label>  <span style="color:orangered">*</span>
            <div class="">
                <input type="radio" name="user_status" id="user_status-activated" class="user_status-radio"
                       value="1" {{ $checked === '1'? 'checked' : ''  }}>
                Activated <br>
                <input type="radio" name="user_status" id="user_status-deactivated" class="user_status-radio"
                       value="0" {{ $checked == '0'? 'checked' : ''  }}>
                Deactivated
            </div>
        </div>
    </div>

</div>
