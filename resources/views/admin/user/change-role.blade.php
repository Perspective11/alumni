@extends('adminlte::page')
@section('top-ex')
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/css/select2.min.css" rel="stylesheet"/>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js"></script>
@endsection
@section('content')
    <div class="row">
        <form class="col-md-10 col-md-offset-1" action="{{ $user->path(true) . '/changeRole' }}" method="post"
              accept-charset="UTF-8"
              enctype="multipart/form-data">
            {{ csrf_field() }}
            {{ method_field('PATCH') }}
            <div class="box box-info">
                <div class="box-header with-border">
                    <h3 class="box-title">User</h3>
                </div><!-- /.box-header -->
                <div class="box-body">
                    <div class="form-group col-md-3">
                        <label for="name">Username:</label>
                        <p id="name">{{$user->name}}</p>
                    </div>
                    @if($user->childName())
                        <div class="form-group col-md-3">
                            <label for="childName">Company/Graduate Name:</label>
                            <p id="childName">{{ $user->childName() }}</p>
                        </div>
                    @endif
                    <div class="form-group col-md-3">
                        <label for="email">Email:</label>
                        <p id="email">{{$user->email}}</p>
                    </div>
                    <div class="form-group col-md-3">
                        <label for="role">Role:</label>
                        <p id="role">{{$user->roles->first()->name}}</p>
                    </div>
                </div><!-- /.box-body -->
            </div><!-- /.box -->

            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">Change User Role</h3>
                    <div class="box-tools text-muted">
                        <p>
                            <small>Fields with <span style="color:orangered">*</span> are required</small>
                        </p>
                    </div>
                </div><!-- /.box-header -->
                <div class="box-body">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="center-block form-group {{ $errors->has('user-type') ? ' has-error' : '' }}">
                                @php
                                    $checked = '';
                                    $disabled = '';
                                    if (old('user_type')){
                                        $checked = old('user_type');
                                    }
                                    elseif (count($user->roles))
                                        $checked = $user->roles->first()->id;

                                @endphp
                                <label for="user-type">Type:</label> <span style="color:orangered">*</span>
                                <div class="">
                                    <input type="radio" name="user_type" id="user-type-admin" class="user-type"
                                           value="1" {{ $checked == '1'? 'checked' : ''  }}>
                                    Admin &nbsp;&nbsp;
                                    <input type="radio" name="user_type" id="user-type-moderator" class="user-type"
                                           value="2" {{ $checked == '2'? 'checked' : ''  }}>
                                    Moderator &nbsp;&nbsp;
                                    <input type="radio" name="user_type" id="user-type-graduate" class="user-type"
                                           value="3" {{ $checked == '3'? 'checked' : ''  }}>
                                    Graduate &nbsp;&nbsp;
                                    <input type="radio" name="user_type" id="user-type-company" class="user-type"
                                           value="4" {{ $checked == '4'? 'checked' : '' }}>
                                    Company
                                </div>
                            </div>
                        </div>
                    </div>
                </div><!-- /.box-body -->
            </div><!-- /.box -->
            <div class="box box-success graduate-box" style="display: none;">
                <div class="box-header with-border">
                    <h3 class="box-title">Graduate Info</h3>
                </div><!-- /.box-header -->
                <div class="box-body">
                    <div class="row">
                        <div class="col-md-3">
                            <div class="form-group {{  $errors->has('liu_id') ? ' has-error' : ''  }}">

                                <label for="liu_id">Student ID:</label> <span style="color:orangered">*</span>
                                <input type="text" name="liu_id" class="form-control" id="liu_id"
                                       value="{{ old('liu_id') ?? $graduate->liu_id }}">
                            </div>
                        </div>
                        <div class="col-md-9">
                            <div class="form-group {{  $errors->has('full_name') ? ' has-error' : ''  }}">
                                <label for="full_name">Full Name:</label> <span style="color:orangered">*</span>
                                <input type="text" name="full_name" class="form-control" id="full_name"
                                       value="{{ old('full_name') ?? $graduate->full_name }}">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-3">
                            <div class="form-group {{ $errors->has('gender') ? ' has-error' : '' }}">
                                @php
                                    $checked = '';
                                    if (old('gender')){
                                        $checked = old('gender');
                                    }
                                    else
                                        $checked = $graduate->gender . '';
                                @endphp
                                <label for="gender">Gender:</label>  <span style="color:orangered">*</span>
                                <div class="">
                                    <input type="radio" name="gender" id="gender-male" class="gender-radio"
                                           value="1" {{ $checked === '1'? 'checked' : ''  }}>
                                    Male <br>
                                    <input type="radio" name="gender" id="gender-female" class="gender-radio"
                                           value="0" {{ $checked === '0'? 'checked' : ''  }}>
                                    Female
                                </div>
                            </div>
                        </div>
                        <div class="col-md-5">
                            <div class="form-group {{ $errors->has('major_id') ? ' has-error' : '' }}">
                                <label for="major_id">Major:</label> <span style="color:orangered">*</span>
                                <select name="major_id" style="width:100%;" class="select-major" id="major_id">
                                    @foreach($majors as $major)
                                        @php
                                            $selected = '';
                                            if (old('major_id')){
                                                if (old('major_id') == $major->id)
                                                $selected = true;
                                            }
                                            elseif ($graduate->major && $graduate->major->id == $major->id)
                                                $selected = true;
                                            else $selected = false;
                                        @endphp
                                        <option {{ $selected? 'selected' : '' }}
                                                value="{{ $major->id }}">
                                            {{ $major->name }}
                                        </option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group {{  $errors->has('graduation_year') ? ' has-error' : ''  }}">
                                <label for="graduation_year">Graduation Year:</label> <span style="color:orangered">*</span>
                                <input type="text" name="graduation_year" class="form-control" id="graduation_year"
                                       value="{{ old('graduation_year') ?? $graduate->graduation_year }}">
                            </div>
                        </div>
                    </div>
                </div><!-- /.box-body -->
            </div><!-- /.box -->
            <div class="box box-success company-box" style="display: none;">
                <div class="box-header with-border">
                    <h3 class="box-title">Company Info</h3>
                </div><!-- /.box-header -->
                <div class="box-body">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group {{  $errors->has('company_name') ? ' has-error' : ''  }}">
                                <label for="company_name">Company Name:</label> <span style="color:orangered">*</span>
                                <input type="text" name="company_name" class="form-control" id="company_name"
                                       value="{{ old('company_name') ?? $company->company_name }}">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group {{  $errors->has('industry') ? ' has-error' : ''  }}">
                                <label for="industry">Industry:</label> <span style="color:orangered">*</span>
                                <input type="text" name="industry" class="form-control" id="industry"
                                       value="{{ old('industry') ?? $company->industry }}">
                            </div>
                        </div>
                    </div>
                </div><!-- /.box-body -->
            </div><!-- /.box -->
            <div class="box box-danger">
                <div class="box-footer">
                    <input type="submit" class="btn btn-danger pull-right" value="Update Role">
                    @if(! old())
                        <div class="clearfix"></div>
                        <br>
                        <div class="alert alert-link alert-dismissible" role="alert">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            <p><b>Be noted</b> that if a user role changes from graduate to any other role,
                                all the graduate records and info will be <b>deleted</b>.
                                Same goes for the companies.</p>
                        </div>
                    @endif
                </div>
            </div>
        </form>
    </div>
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            @include('errors.errors')
        </div>
    </div>
@endsection

@section('footer-ex')
    <script type="text/javascript">
        $(".select-major").select2();

        if ($('#user-type-graduate').is(':checked')) {
            $('.graduate-box').show();
            $('.company-box').hide();
        } else if ($('#user-type-company').is(':checked')) {
            $('.graduate-box').hide();
            $('.company-box').show();
        }

        $('#user-type-graduate').click(function () {
            $('.graduate-box').slideDown();
            $('.company-box').slideUp();
        });

        $('#user-type-company').click(function () {
            $('.company-box').slideDown();
            $('.graduate-box').slideUp();
        });
        $('#user-type-admin').click(function () {
            $('.company-box').slideUp();
            $('.graduate-box').slideUp();
        });
        $('#user-type-moderator').click(function () {
            $('.company-box').slideUp();
            $('.graduate-box').slideUp();
        });

    </script>
@endsection
