@extends('adminlte::page')
@section('content')
    <div class="box box-info">
        <div class="box-header with-border">
            <h3 class="box-title">User</h3>
        </div><!-- /.box-header -->
        <div class="box-body">
            <div class="form-group col-md-3">
                <label for="name">Username:</label>
                <p id="name">{{$user->name}}</p>
            </div>
            @if($user->childName())
                <div class="form-group col-md-3">
                    <label for="childName">Company/Graduate Name:</label>
                    <p id="childName">{{ $user->childName() }}</p>
                </div>
            @endif
            <div class="form-group col-md-3">
                <label for="email">Email:</label>
                <p id="email">{{$user->email}}</p>
            </div>
            <div class="form-group col-md-3">
                <label for="role">Role:</label>
                <p id="role">{{$user->roles->first()->name}}</p>
            </div>
        </div><!-- /.box-body -->
    </div><!-- /.box -->
    <form action="{{ url('/admin/changePassword') }}" method="post" class="box box-primary">
        {{ csrf_field() }}
        <div class="box-header with-border">
            <h3 class="box-title">Change Password</h3>
        </div><!-- /.box-header -->
        <div class="box-body">
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group {{  $errors->has('old_password') ? ' has-error' : ''  }}">
                        <label for="password">Old Password:</label> <span style="color:orangered">*</span>
                        <input type="password" name="old_password" class="form-control" id="old_password">
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group {{  $errors->has('password') ? ' has-error' : ''  }}">
                        <label for="password">New Password:</label> <span style="color:orangered">*</span>
                        <input type="password" name="password" class="form-control" id="password">
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group {{  $errors->has('password_confirmation') ? ' has-error' : ''  }}">
                        <label for="password_confirmation">Confirm Password:</label> <span
                                style="color:orangered">*</span>
                        <input type="password" name="password_confirmation" class="form-control"
                               id="password_confirmation">
                    </div>
                </div>
            </div>
        </div>
        <div class="box-footer">
            <input type="submit" value="Submit New Password" class="btn btn-primary pull-right">
        </div>
    </form>
    @include('errors.errors')
@endsection