<h4>Company Details</h4>
<div class="row">
    <div class="col-md-3">
        <div class="box box-primary">
            <div class="box-header with-border">
                <h3 class="box-title">Company Info</h3>
            </div><!-- /.box-header -->
            <div class="box-body">
                <div class="form-group">
                    <label for="full_name">Company Name:</label>
                    <p id="full_name">{{$company->full_name}}</p>
                </div>
                @if($company->industry)
                    <div class="form-group">
                        <label for="industry">Industry:</label>
                        <p id="industry">{{$company->industry}}</p>
                    </div>
                @endif
                @if($company->type)
                    <div class="form-group">
                        <label for="type">Type:</label>
                        <p id="type">{{$company->type}}</p>
                    </div>
                @endif
                <div class="form-group">
                    <label for="company_status">Company Status:</label><br>
                    <span class="label graduate-status label-warning">{{ $company->company_status }}</span>
                </div>
            </div><!-- /.box-body -->
        </div><!-- /.box -->

    </div>
    <div class="col-md-6">
        <div class="box box-primary">
            <div class="box-header with-border">
                <h3 class="box-title">More Info</h3>
            </div><!-- /.box-header -->
            <div class="box-body">
                @if($company->headquarters)
                    <div class="form-group">
                        <label for="headquarters">Headquarters:</label>
                        <p id="headquarters">{{$company->headquarters}}</p>
                    </div>
                @endif
                @if($company->founded_at)
                    <div class="form-group">
                        <label for="founded_at">Founded At:</label>
                        <p id="founded_at">{{$company->founded_at}}</p>
                    </div>
                @endif
                @if($company->size)
                    <div class="form-group">
                        <label for="size">Company Size:</label>
                        <p id="size">{{$company->size}}</p>
                    </div>
                @endif
                @if($company->short_description)
                    <div class="form-group">
                        <label for="short_description">Short Description:</label>
                        <p id="short_description">{{$company->short_description}}</p>
                    </div>
                @endif
                @if($company->long_description)
                    <div class="form-group">
                        <label for="long_description">Long Description:</label>
                        <p id="long_description">{{$company->long_description}}</p>
                    </div>
                @endif
            </div><!-- /.box-body -->
        </div><!-- /.box -->

    </div>
    <div class="col-md-3">
        <div class="box box-primary">
            <div class="box-header with-border">
                <h3 class="box-title">Activity</h3>
            </div><!-- /.box-header -->
            <div class="box-body">
                <div class="list-group">
                    <a href="{{ url('/admin/posts?user=' . $company->company_name) }}" class="list-group-item">
                        <span class="badge">{{ $user->posts()->count() }}</span>
                        Posts by this user
                    </a>
                    <a href="{{ url('/admin/events?user=' . $company->company_name) }}" class="list-group-item">
                        <span class="badge">{{ $user->events()->count() }}</span>
                        Events by this user
                    </a>
                    <a href="{{ url('/admin/news?user=' . $company->company_name) }}" class="list-group-item">
                        <span class="badge">{{ $user->news()->count() }}</span>
                        News by this user
                    </a>
                    <a href="#" class="list-group-item">
                        <span class="badge">{{ $user->comments()->count() }}</span>
                        Comments by this user
                    </a>
                </div>
            </div><!-- /.box-body -->
        </div><!-- /.box -->

    </div>
</div>
<div class="row">
    <div class="col-md-offset-3 col-md-6">
        <div class="box box-primary">
            <div class="box-header with-border">
                <h3 class="box-title">Addresses</h3>
            </div><!-- /.box-header -->
            <div class="box-body">
                @if($company->address_head)
                    <div class="form-group">
                        <label for="address_head">Main Branch:</label>
                        <p id="address_head">{{$company->address_head}}</p>
                    </div>
                @endif
                @if($company->address_1)
                    <div class="form-group">
                        <label for="address_1">Branch #1:</label>
                        <p id="address_1">{{$company->address_1}}</p>
                    </div>
                @endif
                 @if($company->address_2)
                    <div class="form-group">
                        <label for="address_2">Branch #2:</label>
                        <p id="address_2">{{$company->address_2}}</p>
                    </div>
                @endif
                 @if($company->address_3)
                    <div class="form-group">
                        <label for="address_3">Branch #3:</label>
                        <p id="address_3">{{$company->address_3}}</p>
                    </div>
                @endif
                 @if($company->address_4)
                    <div class="form-group">
                        <label for="address_4">Branch #4:</label>
                        <p id="address_4">{{$company->address_4}}</p>
                    </div>
                @endif


            </div><!-- /.box-body -->
        </div><!-- /.box -->

    </div>
</div>
