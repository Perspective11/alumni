<div class="row">
    <div class="col-md-4">
        <div class="form-group {{  $errors->has('company_name') ? ' has-error' : ''  }}">
            <label for="company_name">Company Name:</label> <span style="color:orangered">*</span>
            <input type="text" name="company_name" class="form-control" id="company_name"
                   value="{{ old('company_name') ?? $company->company_name }}">
        </div>
    </div>
    <div class="col-md-4">
        <div class="form-group {{  $errors->has('industry') ? ' has-error' : ''  }}">
            <label for="industry">Industry:</label> <span style="color:orangered">*</span>
            <input type="text" name="industry" class="form-control" id="industry"
                   value="{{ old('industry') ?? $company->industry }}">
        </div>
    </div>
    <div class="col-md-4">
        <div class="form-group{{ $errors->has('image') ? ' has-error' : '' }}">
            <label for="image" class="control-label">Company Picture</label>
            @if($company->pictures->first())
                <div class="alert">
                    <p>This user already has an image</p>
                    <a class="btn btn-xs btn-warning change-image">Change Image</a>
                </div>
            @else
                <input type="file" name="picture" id="picture">
                {{--TODO: add a drag and drop plugin--}}
            @endif
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-5">
        <div class="form-group {{  $errors->has('short_description') ? ' has-error' : ''  }}">


            <label for="short_description">Short Description:</label>
            <textarea name="short_description" class="form-control" id="short_description"
                      rows="4">{{ old('short_description') ?? $company->short_description }}</textarea>
        </div>
    </div>
    <div class="col-md-7">
        <div class="form-group {{  $errors->has('full_description') ? ' has-error' : ''  }}">
            <label for="full_description">Full Description:</label>
            <textarea name="full_description" class="form-control" id="full_description"
                      rows="6">{{ old('full_description') ?? $company->full_description }}</textarea>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-3">
        <div class="form-group {{  $errors->has('headquarters') ? ' has-error' : ''  }}">


            <label for="headquarters">Headquarters:</label>
            <input type="text" name="headquarters" class="form-control" id="headquarters"
                   value="{{ old('headquarters') ?? $company->headquarters }}">
        </div>
    </div>
    <div class="col-md-3">
        <div class="form-group {{ $errors->has('size') ? ' has-error' : '' }}">
            @php
                $selected = '';
                if (old('size')){
                    $selected = old('size');
                }
                elseif ($company->size)
                    $selected = $company->size;
            @endphp
            <label for="size">Company Size:</label>
            <select style="width: 100%;" class="form-control select-size" id="size" name="size">
                <option></option>
                @foreach(Config::get("enums.company_sizes") as $key => $value)
                    <option
                            {{ $selected == $value? 'selected' : ''}}
                            value="{{ $value }}">{{ $key . ': '. $value }}
                    </option>
                @endforeach
            </select>
        </div>
    </div>
    <div class="col-md-3">
        <div class="form-group {{  $errors->has('founded_at') ? ' has-error' : ''  }}">
            <label for="founded_at">Founded At:</label>
            <input type="text" name="founded_at" class="form-control" id="founded_at"
                   value="{{ old('founded_at') ?? $company->founded_at }}">
        </div>
    </div>
    <div class="col-md-3">
        <div class="form-group {{  $errors->has('type') ? ' has-error' : ''  }}">
            <label for="type">Company Type:</label>
            <input type="text" name="type" class="form-control" id="type" value="{{ old('type') ?? $company->type }}">
        </div>
    </div>
</div>
<div class="form-group {{  $errors->has('address_head') ? ' has-error' : ''  }}">
    <label for="address_head">Main Branch:</label>
    <input type="text" name="address_head" class="form-control" id="address_head"
           value="{{ old('address_head') ?? $company->address_head }}">
</div>
<div class="form-group {{  $errors->has('address_1') ? ' has-error' : ''  }}">
    <label for="address_1">Branch #1:</label>
    <input type="text" name="address_1" class="form-control" id="address_1"
           value="{{ old('address_1') ?? $company->address_1 }}">
</div>
<div class="form-group {{  $errors->has('address_2') ? ' has-error' : ''  }}">
    <label for="address_1">Branch #2:</label>
    <input type="text" name="address_2" class="form-control" id="address_2"
           value="{{ old('address_2') ?? $company->address_2 }}">
</div>
<div class="form-group {{  $errors->has('address_3') ? ' has-error' : ''  }}">
    <label for="address_1">Branch #3:</label>
    <input type="text" name="address_3" class="form-control" id="address_3"
           value="{{ old('address_3') ?? $company->address_3 }}">
</div>
<div class="form-group {{  $errors->has('address_4') ? ' has-error' : ''  }}">
    <label for="address_4">Branch #4:</label>
    <input type="text" name="address_4" class="form-control" id="address_4"
           value="{{ old('address_4') ?? $company->address_4 }}">
</div>
<div class="form-links">
    <h5><strong>Links:</strong></h5>
    <div class="input-group link-input {{ $errors->has('website_link') ? ' has-error' : '' }}">
        <span class="input-group-addon"><i style="color: #34bf49;" class="fa fa-edge icon"></i></span>
        <input type="text" class="form-control" placeholder="Website URL"
               name="website_link" value="{{ old('website_link') ?? $company->website_link }}">
    </div>
    <div class="input-group link-input {{ $errors->has('facebook_link') ? ' has-error' : '' }}">
        <span class="input-group-addon"><i style="color: #3b5998;" class="fa fa-facebook-official icon"></i></span>
        <input type="text" class="form-control" placeholder="Facebook URL"
               name="facebook_link" value="{{ old('facebook_link') ?? $company->facebook_link }}">
    </div>
    <div class="input-group link-input {{ $errors->has('twitter_link') ? ' has-error' : '' }}">
        <span class="input-group-addon"><i style="color: #1da1f2;" class="fa fa-twitter icon"></i></span>
        <input type="text" class="form-control" placeholder="Twitter URL"
               name="twitter_link" value="{{ old('twitter_link') ?? $company->twitter_link }}">
    </div>
    <div class="input-group link-input {{ $errors->has('linkedin_link') ? ' has-error' : '' }}">
        <span class="input-group-addon"><i style="color: #0077b5;" class="fa fa-linkedin icon"></i></span>
        <input type="text" class="form-control" placeholder="LinkedIn URL"
               name="linkedin_link" value="{{ old('linkedin_link') ?? $company->linkedin_link }}">
    </div>
</div>

