@extends('adminlte::page')
@section('top-ex')
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/css/select2.min.css" rel="stylesheet"/>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js"></script>
    <script src="/plugins/bootstrap-confirmation.min.js"></script>

@endsection
@section('content')
    <section class="FAB">
        <div class="FAB__action-button">
            <a href="{{ url('/admin/users/create') }}"><i class="action-button__icon fa fa-user-plus"></i></a>
            <p class="action-button__text--hide">Add User</p>
        </div>
    </section>
    <div class="container-fluid">

        @include('admin.user.filters')
        <table class="table table-condensed jdatatable display" id="users-table" style="background-color: #fff;">
            <thead>
            <tr>
                <th>Id</th>
                <th>Username</th>
                <th>Company/Graduate Name</th>
                <th>Email</th>
                <th>Status</th>
                <th>Joined</th>
                <th>Role</th>
            </tr>
            </thead>
            <tbody>

            </tbody>
        </table>
    </div>
@endsection
@section('footer-ex')
    <script>
        $(function () {
            $('.FAB__action-button').hover(function () {
                $(this).find('.action-button__text--hide').attr('class', 'action-button__text--show');
                $('.mini-action-button--hide').attr('class', 'mini-action-button--show');
            }, function () {
                $(this).find('.action-button__text--show').attr('class', 'action-button__text--hide');
            });

            var $usersTable = $('#users-table').DataTable({
                "order": [[ 5, 'desc' ]],
                pageLength: 100,
                processing: true,
                serverSide: true,
                ajax: {
                    url: '{{url("/admin/users/getData")}}',
                    data: function (d) {

                        d.status = '{{ request('status')? request('status') : '' }}'

                        d.role = '{{ request('role')? request('role') : ''}}'

                        d.user = '{{ request('user')? request('user') : ''}}'

                    }
                },

                columns: [
                    {data: 'id', name: 'id'},
                    {data: 'name', name: 'name'},
                    {data: 'child_name', name: 'child_name', sortable: 'no', orderable: 'no', searchable: 'no'},
                    {data: 'email', name: 'email'},
                    {data: 'user_status', name: 'user_status'},
                    {data: 'created_at', name: 'created_at'},
                    {data: 'role', name: 'role', sortable: 'no', orderable: 'no', searchable: 'no'}
                ]
            });

            $('#users-table tbody').on('click', '.user-status', function (e) {
                e.stopPropagation()
            })

            var $modal = $('.action-modal');
            $('#users-table tbody').on('click', 'tr', function () {

                var data = $usersTable.row(this).data();
                console.log(data);
                $('.action-modal').modal();
                $modal.find('.modal-user-name').text(data['name']);
                $modal.find('.modal-user-childname').text(data['child_name']);
                $modal.find('.modal-user-email').text(data['email']);
                $modal.find('.modal-user-status').html(data['user_status']);
                $modal.find('.modal-user-created').text(data['created_at']);
                $modal.find('.modal-user-role').text(data['role']);
                $modal.find('.view-user-posts').attr('href', '/admin/posts?user=' + data['name']);
                $modal.find('.view-user-events').attr('href', '/admin/events?user=' + data['name']);
                $modal.find('.view-user-details').attr('href', '/admin/users/' + data['id']);
                $modal.find('.edit-user-details').attr('href', '/admin/users/' + data['id'] + '/edit');
                $modal.find('.change-user-role').attr('href', '/admin/users/' + data['id'] + '/changeRole');
                if (data['role'] !== 'admin'){
                    $modal.find('.delete-user').attr('action' , '/admin/users/' + data['id']);
                }else{
                    $modal.find('.delete-user').attr('style' , 'display:none');
                }

            });

            $('[data-toggle=confirmation]').confirmation({
                rootSelector: '[data-toggle=confirmation]',
                title: 'Are you sure you want to delete this category?',
                btnOkClass: 'btn btn-xs btn-danger',
                btnCancelClass: 'btn btn-xs btn-default',
                btnOkIcon: 'fa fa-trash',
                btnCancelIcon: 'fa fa-ban'
            });

        });
    </script>
@endsection

@section('modals')
    <div class="modal fade action-modal" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Action</h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-8">
                            <div class="modal-field">
                                <strong>Name:</strong> <span class="modal-user-name"></span> <br>
                            </div>
                            <div class="modal-field">
                                <strong>Company/Graduate Name:</strong> <span class="modal-user-childname"></span> <br>
                            </div>
                            <div class="modal-field">
                                <strong>Email:</strong> <span class="modal-user-email"></span> <br>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="modal-field">
                                <strong>Status:</strong> <span class="modal-user-status"></span> <br>
                            </div>
                            <div class="modal-field">
                                <strong>Joined:</strong> <span class="modal-user-created"></span> <br>
                            </div>
                            <div class="modal-field">
                                <strong>Role:</strong> <span class="modal-user-role"></span> <br>
                            </div>
                        </div>
                    </div>
                    <hr>
                    <div class="list-group">
                        <a href="#" class="list-group-item view-user-posts">View user posts</a>
                        <a href="#" class="list-group-item view-user-events ">View user events</a>
                        <a href="#" class="list-group-item view-user-details">View user details</a>
                        <a href="#" class="list-group-item edit-user-details">Edit user details</a>
                        <a href="#" class="list-group-item change-user-role">Change user role</a>
                        <form class="list-group-item list-group-item-danger delete-user" method="post" action="">
                            {{ csrf_field() }}
                            {{ method_field('DELETE') }}
                            <button type="submit" class="btn-link text-red" data-toggle="confirmation" data-popout="true"> Delete User</button>
                        </form>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->

@endsection