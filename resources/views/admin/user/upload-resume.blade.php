@extends('adminlte::page')
@section('top-ex')
    <script src="/plugins/bootstrap-confirmation.min.js"></script>
    <link href="/plugins/fine-uploader/fine-uploader-new.min.css" rel="stylesheet">
    <script src="/plugins/fine-uploader/fine-uploader.min.js"></script>
    <script src="/plugins/sweetalert/sweetalert.min.js"></script>
    <link rel="stylesheet" type="text/css" href="/plugins/sweetalert/sweetalert.css">
    @include('admin.user.fineuploader-template')
@endsection
@section('content')
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="box box-info">
                <div class="box-header with-border">
                    <h3 class="box-title">Graduate</h3>
                </div><!-- /.box-header -->
                <div class="box-body">
                    <div class="form-group col-md-3">
                        <label for="name">Student ID:</label>
                        <p id="name">{{$graduate->liu_id}}</p>
                    </div>
                    @if($user->childName())
                        <div class="form-group col-md-3">
                            <label for="childName">Full Name:</label>
                            <p id="childName">{{ $user->childName() }}</p>
                        </div>
                    @endif
                    <div class="form-group col-md-3">
                        <label for="email">Email:</label>
                        <p id="email">{{$user->email}}</p>
                    </div>
                    <div class="form-group col-md-3">
                        <label for="role">Major:</label>
                        <p id="major">{{$graduate->major->name}}</p>
                    </div>
                </div><!-- /.box-body -->
            </div><!-- /.box -->

            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">Upload CV</h3>
                </div><!-- /.box-header -->
                <div id="" class="box-body">
                    <label for="uploader">Resume PDF file</label>
                    @if ($graduate->file_path)
                        <p>A Resume PDF file already exists for this user. You can replace it by uploading another file</p>
                    @endif
                    <div id="uploader"></div>
                </div><!-- /.box-body -->
                <div class="box-footer">
                    @if ($graduate->file_path)
                        <form class="form-inline inline" method="post" action="{{ url($user->path(true) . '/deleteResume') }}">
                            {{ csrf_field() }}
                            {{ method_field('DELETE') }}
                            <button type="submit" class="btn btn-danger" data-toggle="confirmation" data-popout="true"><i class="fa fa-times"></i> Delete Existing CV</button>
                        </form>
                    @endif
                    <a href="{{ url($user->path(true)) }}" class="btn btn-success pull-right">Go Back</a>
                </div><!-- box-footer -->
            </div><!-- /.box -->
        </div>
    </div>
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            @include('errors.errors')
        </div>
    </div>
@endsection
@section('footer-ex')
    <script type="text/javascript">
        var uploader = new qq.FineUploader({
            element: document.getElementById('uploader'),
            debug: true,
            request: {
                inputName: 'resume',
                endpoint: '{{ url($user->path(true) . '/uploadResumeStore') }}',
                customHeaders: {
                    'X-CSRF-TOKEN': '{{ csrf_token() }}'
                },
            },
            multiple: false,
            showMessage: function(message) { swal(message); },
            validation: {
                acceptFiles: 'application/pdf',
                sizeLimit: 2000000,
                allowedExtensions: ['pdf']
            },

            callbacks: {
                onError: function(id, name, errorReason, response) {
                    var obj = JSON.parse(response['response']);
                    var errorList = obj['resume'];
                    var errorString = qq.format("Error on file {}.\nReasons: ", name);
                    for(var i in errorList)
                    {
                        errorString += "\n\n" + errorList[i];
                    }
                    responseText = qq.format("Error on file {}.\n" , name);
                    swal("Failed!", errorString , "error");
                },
                onComplete: function(id, name, responseJSON, xhr){
                    if(responseJSON['success'])
                        swal("File Uploaded Successfully!", '', "success");
                }
            }

        });

        $('[data-toggle=confirmation]').confirmation({
            rootSelector: '[data-toggle=confirmation]',
            title: 'Are you sure you want to delete your resume?',
            btnOkClass: 'btn btn-xs btn-danger',
            btnCancelClass: 'btn btn-xs btn-default',
            btnOkIcon: 'fa fa-trash',
            btnCancelIcon: 'fa fa-ban'
        });


    </script>
@endsection
