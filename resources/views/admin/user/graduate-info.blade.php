<h4>Graduate Details</h4>
<div class="row">
    <div class="col-md-3">
        <div class="box box-primary">
            <div class="box-header with-border">
                <h3 class="box-title">Graduate Info</h3>
            </div><!-- /.box-header -->
            <div class="box-body">
                <div class="form-group">
                    <label for="liu_id">Student_id:</label>
                    <p id="liu_id">{{$graduate->liu_id}}</p>
                </div>
                <div class="form-group">
                    <label for="full_name">Full Name:</label>
                    <p id="full_name">{{$graduate->full_name}}</p>
                </div>
                @if ($graduate->major)
                    <div class="form-group">
                        <label for="major">Major:</label>
                        <p id="major"><i style="color: {{$graduate->major->color}}"
                                         class="fa {{ $graduate->major->icon }}"></i> {{$graduate->major->code . ' ' . $graduate->major->name}}
                        </p>
                    </div>
                @endif
                <div class="form-group">
                    <label for="graduation_year">Graduation Year:</label>
                    <p id="graduation_year">
                        <span class="label label-info">{{ $graduate->graduation_year }}</span>
                    </p>
                </div>
                <div class="form-group">
                    <label for="user_status">Graduate Status:</label><br>
                    <span class="label graduate-status label-warning">{{ $graduate->graduate_status }}</span>
                </div>
            </div><!-- /.box-body -->
        </div><!-- /.box -->

    </div>
    <div class="col-md-6">
        <div class="box box-primary">
            <div class="box-header with-border">
                <h3 class="box-title">Personal Info</h3>
            </div><!-- /.box-header -->
            <div class="box-body">
                @if($graduate->phone)
                    <div class="form-group">
                        <label for="phone">Mobile No:</label>
                        <p id="phone">{{$graduate->phone}}</p>
                    </div>
                @endif
                @if($graduate->perm_address)
                    <div class="form-group">
                        <label for="perm_address">Permanent Address:</label>
                        <p id="perm_address">{{$graduate->perm_address}}</p>
                    </div>
                @endif
                @if($graduate->curr_address)
                    <div class="form-group">
                        <label for="curr_address">Current Address:</label>
                        <p id="curr_address">{{$graduate->curr_address}}</p>
                    </div>
                @endif
                @if($graduate->origin)
                    <div class="form-group">
                        <label for="origin">Place of Origin:</label>
                        <p id="origin">{{$graduate->origin}}</p>
                    </div>
                @endif
                @if($graduate->dob)
                    <div class="form-group">
                        <label for="age">Age:</label>
                        <p id="age">
                            <span class='label label-warning label-lg'>{{$graduate->age()}}</span>
                        </p>
                    </div>
                @endif
                @if($graduate->gender !== null)
                    <div class="form-group">
                        @php
                            $gender = $graduate->gender ? 'male': 'female';
                            $color = $graduate->gender ? 'lightblue': 'pink';
                        @endphp
                        <label for="gender">Gender:</label>
                        <p id="gender">
                            <span style="color:{{$color}}" class="fa-2x fa fa-{{$gender}}"></span>
                        </p>
                    </div>
                @endif
            </div><!-- /.box-body -->
        </div><!-- /.box -->

    </div>
    <div class="col-md-3">
        <div class="box box-primary">
            <div class="box-header with-border">
                <h3 class="box-title">Activity</h3>
            </div><!-- /.box-header -->
            <div class="box-body">
                <div class="list-group">
                    <a href="{{ url('/admin/posts?user=' . $graduate->liu_id) }}" class="list-group-item">
                        <span class="badge">{{ $user->posts()->count() }}</span>
                        Posts by this user
                    </a>
                    <a href="{{ url('/admin/events?user=' . $graduate->liu_id) }}" class="list-group-item">
                        <span class="badge">{{ $user->events()->count() }}</span>
                        Events by this user
                    </a>
                    <a href="{{ url('/admin/news?user=' . $graduate->liu_id) }}" class="list-group-item">
                        <span class="badge">{{ $user->news()->count() }}</span>
                        News by this user
                    </a>
                    <a href="#" class="list-group-item">
                        <span class="badge">{{ $user->comments()->count() }}</span>
                        Comments by this user
                    </a>
                </div>
            </div><!-- /.box-body -->
        </div><!-- /.box -->

    </div>
</div>
<div class="row">
    <div class="col-md-3">
        <div class="box box-primary">
            <div class="box-header with-border">
                <h3 class="box-title">Education</h3>
            </div><!-- /.box-header -->
            <div class="box-body">
                @if($graduate->is_studying !== null)
                    <div class="form-group">
                        @php
                            $check = '';
                            $studying = $graduate->is_studying;
                            if($studying === 0)
                                $check = 'times';
                            elseif($studying === 1)
                                $check = 'check';
                            $color = $studying ? 'lightgreen': 'lightsalmon';
                        @endphp
                        <label for="is_studying">Is Studying:</label>
                        <p id="is_studying">
                            <span style="color:{{$color}}" class="fa-2x fa fa-{{$check}}"></span>
                        </p>
                    </div>
                @endif
                @if($graduate->field_of_specialty)
                    <div class="form-group">
                        <label for="field_of_specialty">Field of Specialty:</label>
                        <p id="field_of_specialty">{{$graduate->field_of_specialty}}</p>
                    </div>
                @endif
                @if($graduate->edu_level)
                    <div class="form-group">
                        <label for="edu_level">Educational Level</label>
                        <p id="edu_level">{{$graduate->edu_level}}</p>
                    </div>
                @endif
                @if($graduate->gpa)
                    <div class="form-group">
                        <label for="gpa">GPA:</label>
                        <p id="gpa">
                            <span class="label label-warning">{{$graduate->gpa}}</span>
                        </p>
                    </div>
                @endif
                @if($graduate->english_pro)
                    <div class="form-group">
                        <label for="english_pro">English Proficiency:</label>
                        <p id="english_pro">{{$graduate->english_pro}}</p>
                    </div>
                @endif
                @if($graduate->languages)
                    <div class="form-group">
                        <label for="languages">Languages:</label>
                        <p id="languages">{{$graduate->languages}}</p>
                    </div>
                @endif
            </div><!-- /.box-body -->
        </div><!-- /.box -->

    </div>
    <div class="col-md-6">
        <div class="box box-primary">
            <div class="box-header with-border">
                <h3 class="box-title">More Info</h3>
            </div><!-- /.box-header -->
            <div class="box-body">
                @if($graduate->description)
                    <div class="form-group">
                        <label for="description">Description:</label>
                        <p id="description">{{$graduate->description}}</p>
                    </div>
                @endif
                @if($graduate->interests)

                    <div class="form-group">
                        <label for="interests">Interests:</label>
                        <p id="interests">{{$graduate->interests}}</p>
                    </div>
                @endif
                @if($graduate->achievements)

                    <div class="form-group">
                        <label for="achievements">Achievements:</label>
                        <p id="achievements">{{$graduate->achievements}}</p>
                    </div>
                @endif
                @if($graduate->volunteer)

                    <div class="form-group">
                        <label for="volunteer">Volunteer:</label>
                        <p id="volunteer">{{$graduate->volunteer}}</p>
                    </div>
                @endif
                @if($graduate->goal)

                    <div class="form-group">
                        <label for="goal">Goal:</label>
                        <p id="goal">{{$graduate->goal}}</p>
                    </div>
                @endif
            </div><!-- /.box-body -->
        </div><!-- /.box -->

    </div>
    <div class="col-md-3">
        <div class="box box-primary">
            <div class="box-header with-border">
                <h3 class="box-title">Employment</h3>
            </div><!-- /.box-header -->
            <div class="box-body">
                @if($graduate->is_employed !== null)
                    <div class="form-group">
                        @php
                            $check = '';
                            $employed = $graduate->is_employed;
                            if($employed === 0)
                                $check = 'times';
                            elseif($employed === 1)
                                $check = 'check';
                            $color = $employed ? 'lightgreen': 'lightsalmon';
                        @endphp
                        <label for="is_employed">Is Employed:</label>
                        <p id="is_employed">
                            <span style="color:{{$color}}" class="fa-2x fa fa-{{$check}}"></span>
                        </p>
                    </div>
                @endif
                @if($graduate->job_title)
                    <div class="form-group">
                        <label for="job_title">Job Title:</label>
                        <p id="job_title">{{$graduate->job_title}}</p>
                    </div>
                @endif
                @if($graduate->field_of_work)
                    <div class="form-group">
                        <label for="field_of_work">Filed of Work:</label>
                        <p id="field_of_work">{{$graduate->field_of_work}}</p>
                    </div>
                @endif
                @if($graduate->is_hirable !== null)
                    <div class="form-group">
                        @php
                            $check = '';
                            $hireable = $graduate->is_hireable;
                            if($hireable === 0)
                                $check = 'times';
                            elseif($hireable === 1)
                                $check = 'check';
                            $color = $hireable ? 'lightgreen': 'lightsalmon';
                        @endphp
                        <label for="is_hireable">Is Hireable:</label>
                        <p id="is_hireable">
                            <span style="color:{{$color}}" class="fa-2x fa fa-{{$check}}"></span>
                        </p>
                    </div>
                @endif
            </div><!-- /.box-body -->
        </div><!-- /.box -->

    </div>
</div>
