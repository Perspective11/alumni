@extends('adminlte::page')
@section('top-ex')
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/css/select2.min.css" rel="stylesheet" />
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js"></script>
    <link rel="stylesheet" href="/vendor/adminlte/plugins/datepicker/datepicker3.css">
@endsection

@section('content')
    <div class="row">
        <form class="col-md-10 col-md-offset-1" action="{{ $user->path(true)  }}" method="post" accept-charset="UTF-8"
              enctype="multipart/form-data">
            {{ csrf_field() }}
            {{ method_field('PATCH') }}
            <section class="FAB">
                <div class="FAB__action-button">
                    <button class="btn-link " type="submit"><i class="action-button__icon bg-orange-active fa fa-floppy-o"></i></button>
                    <p class="action-button__text--hide">Save Changes</p>
                </div>
            </section>
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">Edit User</h3>
                    <div class="box-tools text-muted">
                        <p><small>Fields with <span style="color:orangered">*</span> are required</small></p>
                    </div>
                </div><!-- /.box-header -->
                <div class="box-body">
                    @include('admin.user.user-form')
                </div><!-- /.box-body -->
            </div><!-- /.box -->
            @if($user->isGraduate())
            <div class="box box-success graduate-box">
                <div class="box-header with-border">
                    <h3 class="box-title">Graduate Info</h3>
                </div><!-- /.box-header -->
                <div class="box-body">
                    @include('admin.user.graduate-form')
                </div><!-- /.box-body -->
            </div><!-- /.box -->
            @endif
            @if($user->isCompany())
            <div class="box box-success company-box">
                <div class="box-header with-border">
                    <h3 class="box-title">Company Info</h3>
                </div><!-- /.box-header -->
                <div class="box-body">
                    @include('admin.user.company-form')
                </div><!-- /.box-body -->
            </div><!-- /.box -->
            @endif
        </form>
    </div>
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            @include('errors.errors')
        </div>
    </div>
@endsection
@section('footer-ex')
    <script type="text/javascript" src="/vendor/adminlte/plugins/datepicker/bootstrap-datepicker.js"></script>
    <script type="text/javascript">
        $('.FAB__action-button').hover(function () {
            $(this).find('.action-button__text--hide').attr('class', 'action-button__text--show');
            $('.mini-action-button--hide').attr('class', 'mini-action-button--show');
        }, function () {
            $(this).find('.action-button__text--show').attr('class', 'action-button__text--hide');
        });

        $(".select-size").select2({
            placeholder: "Select Size"
        });

        $(".select-major").select2();

        $('.change-image').on('click', function (e) {
            e.preventDefault();

            $(this).parent().replaceWith('<input type="file" name="picture" id="picture">');
        })

    </script>
@endsection
