    <div class="row">
    <div class="col-md-6">
        <div class="form-group {{  $errors->has('liu_id') ? ' has-error' : ''  }}">

            <label for="liu_id">Student ID:</label> <span style="color:orangered">*</span>
            <input type="text" name="liu_id" class="form-control" id="liu_id"
                   value="{{ old('liu_id') ?? $graduate->liu_id }}">
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group {{  $errors->has('full_name') ? ' has-error' : ''  }}">
            <label for="full_name">Full Name:</label> <span style="color:orangered">*</span>
            <input type="text" name="full_name" class="form-control" id="full_name"
                   value="{{ old('full_name') ?? $graduate->full_name }}">
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-6">
        <div class="form-group{{ $errors->has('image') ? ' has-error' : '' }}">


            <label for="image" class="control-label">Graduate Picture</label>
            @if($graduate->pictures->first())
                <div class="alert">
                    <p>This user already has an image</p>
                    <a class="btn btn-xs btn-warning change-image">Change Image</a>
                </div>
            @else
                <input type="file" name="picture" id="picture">
                {{--TODO: add a drag and drop plugin--}}
            @endif
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group {{ $errors->has('gender') ? ' has-error' : '' }}">
            @php
                $checked = '';
                if (old('gender')){
                    $checked = old('gender');
                }
                else
                    $checked = $graduate->gender . '';
            @endphp
            <label for="gender">Gender:</label>  <span style="color:orangered">*</span>
            <div class="">
                <input type="radio" name="gender" id="gender-male" class="gender-radio"
                       value="1" {{ $checked === '1'? 'checked' : ''  }}>
                Male <br>
                <input type="radio" name="gender" id="gender-female" class="gender-radio"
                       value="0" {{ $checked === '0'? 'checked' : ''  }}>
                Female
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-6">
        <div class="form-group {{ $errors->has('major_id') ? ' has-error' : '' }}">
            <label for="major_id">Major:</label> <span style="color:orangered">*</span>
            <select name="major_id" style="width:100%;" class="select-major" id="major_id">
                @foreach($majors as $major)
                    @php
                        $selected = '';
                        if (old('major_id')){
                            if (old('major_id') == $major->id)
                            $selected = true;
                        }
                        elseif ($graduate->major && $graduate->major->id == $major->id)
                            $selected = true;
                        else $selected = false;
                    @endphp
                    <option {{ $selected? 'selected' : '' }}
                            value="{{ $major->id }}">
                        {{ $major->name }}
                    </option>
                @endforeach
            </select>
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group {{  $errors->has('graduation_year') ? ' has-error' : ''  }}">
            <label for="graduation_year">Graduation Year:</label> <span style="color:orangered">*</span>
            <input type="text" name="graduation_year" class="form-control" id="graduation_year"
                   value="{{ old('graduation_year') ?? $graduate->graduation_year }}">
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-4">
        <div class="form-group {{  $errors->has('dob') ? ' has-error' : ''  }}">


            <label for="dob">Date of Birth:</label>
            <div class="input-group date">
                <input placeholder="Select Date"
                       type="text"
                       value="{{ old('dob') ?? ($graduate->dob? $graduate->dob->format('Y-m-d') : '') }}"
                       name="dob"
                       id="dob"
                       class="form-control">
                <div class="input-group-addon">
                    <span class="fa fa-calendar"></span>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-4">
        <div class="form-group {{  $errors->has('phone') ? ' has-error' : ''  }}">
            <label for="phone">Mobile Number:</label>
            <input type="text" name="phone" class="form-control" id="phone"
                   value="{{ old('phone') ?? $graduate->phone }}">
        </div>
    </div>
    <div class="col-md-4">
        <div class="form-group {{  $errors->has('origin') ? ' has-error' : ''  }}">


            <label for="origin">Place of Origin:</label>
            <input type="text" name="origin" class="form-control" id="origin"
                   value="{{ old('origin') ?? $graduate->origin }}">
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-6">
        <div class="form-group {{  $errors->has('perm_address') ? ' has-error' : ''  }}">
            <label for="perm_address">Permanent Address:</label>
            <input type="text" name="perm_address" class="form-control" id="perm_address"
                   value="{{ old('perm_address') ?? $graduate->perm_address }}">
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group {{  $errors->has('curr_address') ? ' has-error' : ''  }}">
            <label for="curr_address">Current Address:</label>
            <input type="text" name="curr_address" class="form-control" id="curr_address"
                   value="{{ old('curr_address') ?? $graduate->curr_address }}">
        </div>
    </div>
</div>
<hr>
<h4>Education</h4>
<hr>
<div class="row">
    <div class="col-md-4">
        <div class="form-group {{ $errors->has('is_studying') ? ' has-error' : '' }}">


            @php
                $checked = '';
                if (old('is_studying')){
                    $checked = old('is_studying');
                }
                else
                    $checked = $graduate->is_studying . '';
            @endphp
            <label for="is_studying">Currently Studying:</label>
            <div class="">
                <input type="radio" name="is_studying" id="is_studying-1" class="is_studying-radio"
                       value="1" {{ $checked === '1'? 'checked' : ''  }}>
                Yes <br>
                <input type="radio" name="is_studying" id="is_studying-0" class="is_studying-radio"
                       value="0" {{ $checked === '0'? 'checked' : ''  }}>
                No
            </div>
        </div>
    </div>
    <div class="col-md-4">
        <div class="form-group {{  $errors->has('field_of_specialty') ? ' has-error' : ''  }}">
            <label for="field_of_specialty">Field of Specialty:</label>
            <input type="text" name="field_of_specialty" class="form-control" id="field_of_specialty"
                   value="{{ old('field_of_specialty') ?? $graduate->field_of_specialty }}">
        </div>
    </div>
    <div class="col-md-4">
        <div class="form-group {{  $errors->has('edu_level') ? ' has-error' : ''  }}">
            <label for="edu_level">Level of Education:</label>
            <input type="text" name="edu_level" class="form-control" id="edu_level"
                   value="{{ old('edu_level') ?? $graduate->edu_level }}">
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-4">
        <div class="form-group {{  $errors->has('gpa') ? ' has-error' : ''  }}">


            <label for="gpa">GPA:</label>
            <input type="text" name="gpa" class="form-control" id="gpa" value="{{ old('gpa') ?? $graduate->gpa }}">
        </div>
    </div>
    <div class="col-md-4">
        <div class="form-group {{  $errors->has('english_pro') ? ' has-error' : ''  }}">
            <label for="english_pro">English Proficiency:</label>
            <select name="english_pro" id="english_pro" class="form-control">
                <option value="" selected disabled>Select from the options</option>
                <option value="mother langauge" {{ $graduate->english_pro == 'mother language' ?'selected':'' }}>Mother Langauge</option>
                <option value="excellent" {{ $graduate->english_pro == 'excellent' ?'selected':'' }}>Excellent</option>
                <option value="good" {{ $graduate->english_pro == 'good' ?'selected':'' }}>Good</option>
                <option value="weak" {{ $graduate->english_pro == 'weak' ?'selected':'' }}>Weak</option>
            </select>
        </div>
    </div>
    <div class="col-md-4">
        <div class="form-group {{  $errors->has('languages') ? ' has-error' : ''  }}">
            <label for="languages">Languages Spoken:</label>
            <input type="text" name="languages" class="form-control" id="languages"
                   value="{{ old('languages') ?? $graduate->languages }}">
        </div>
    </div>
</div>
<hr>
<h4>Employment</h4>
<hr>
<div class="row">
    <div class="col-md-4">
        <div class="form-group {{ $errors->has('is_employed') ? ' has-error' : '' }}">
            @php
                $checked = '';
                if (old('is_employed')){
                    $checked = old('is_employed');
                }
                else
                    $checked = $graduate->is_employed . '';
            @endphp
            <label for="is_employed">Currently Employed:</label>
            <div class="">
                <input type="radio" name="is_employed" id="is_employed-1" class="is_employed-radio"
                       value="1" {{ $checked === '1'? 'checked' : ''  }}>
                Yes <br>
                <input type="radio" name="is_employed" id="is_employed-0" class="is_employed-radio"
                       value="0" {{ $checked === '0'? 'checked' : ''  }}>
                No
            </div>
        </div>
    </div>
    <div class="col-md-4">
        <div class="form-group {{ $errors->has('is_hireable') ? ' has-error' : '' }}">
            @php
                $checked = '';
                if (old('is_hireable')){
                    $checked = old('is_hireable');
                }
                else
                    $checked = $graduate->is_hireable . '';
            @endphp
            <label for="is_hireable">Available for Hire</label>
            <div class="">
                <input type="radio" name="is_hireable" id="is_hireable-1" class="is_hireable-radio"
                       value="1" {{ $checked === '1'? 'checked' : ''  }}>
                Yes <br>
                <input type="radio" name="is_hireable" id="is_hireable-0" class="is_hireable-radio"
                       value="0" {{ $checked === '0'? 'checked' : ''  }}>
                No
            </div>
        </div>
    </div>
    <div class="col-md-4">
        <div class="form-group {{  $errors->has('years_of_experience') ? ' has-error' : ''  }}">
            <label for="years_of_experience">Years of Experience:</label>
            <input type="text" name="years_of_experience" class="form-control" id="years_of_experience"
                   value="{{ old('years_of_experience') ?? $graduate->years_of_experience }}">
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-4">
    <div class="form-group {{  $errors->has('job_title') ? ' has-error' : ''  }}">



            <label for="job_title">Job Title:</label>
            <input type="text" name="job_title" class="form-control" id="job_title"
                   value="{{ old('job_title') ?? $graduate->job_title }}">
        </div>
    </div>
    <div class="col-md-4">
        <div class="form-group {{  $errors->has('field_of_work') ? ' has-error' : ''  }}">
            <label for="field_of_work">Field of Work:</label>
            <input type="text" name="field_of_work" class="form-control" id="field_of_work"
                   value="{{ old('field_of_work') ?? $graduate->field_of_work }}">
        </div>
    </div>
    <div class="col-md-4">
        <div class="form-group {{  $errors->has('emp_place') ? ' has-error' : ''  }}">
            <label for="emp_place">Place of Employment:</label>
            <input type="text" name="emp_place" class="form-control" id="emp_place"
                   value="{{ old('emp_place') ?? $graduate->emp_place }}">
        </div>
    </div>
</div>
<hr>
<h4>More info</h4>
<hr>
<div class="form-group {{  $errors->has('description') ? ' has-error' : ''  }}">
    <label for="description">Description:</label>
    <textarea name="description" class="form-control" id="description"
              rows="5">{{ old('description') ?? $graduate->description }}</textarea>
</div>
<div class="form-group {{  $errors->has('interests') ? ' has-error' : ''  }}">
    <label for="interests">Interests:</label>
    <textarea name="interests" class="form-control" id="interests"
              rows="5">{{ old('interests') ?? $graduate->interests }}</textarea>
</div>
<div class="form-group {{  $errors->has('achievements') ? ' has-error' : ''  }}">
    <label for="achievements">Achievements:</label>
    <textarea name="achievements" class="form-control" id="achievements"
              rows="5">{{ old('achievements') ?? $graduate->achievements }}</textarea>
</div>
<div class="form-group {{  $errors->has('volunteer') ? ' has-error' : ''  }}">
    <label for="volunteer">Volunteer:</label>
    <textarea name="volunteer" class="form-control" id="volunteer"
              rows="5">{{ old('volunteer') ?? $graduate->volunteer }}</textarea>
</div>
<div class="form-group {{  $errors->has('goal') ? ' has-error' : ''  }}">
    <label for="goal">Goal:</label>
    <textarea name="goal" class="form-control" id="goal" rows="5">{{ old('goal') ?? $graduate->goal }}</textarea>
</div>
<div class="form-links">
    <h5><strong>Links:</strong></h5>
    <div class="input-group link-input {{ $errors->has('website_link') ? ' has-error' : '' }}">
        <span class="input-group-addon"><i style="color: #34bf49;" class="fa fa-edge icon"></i></span>
        <input type="text" class="form-control" placeholder="Website URL"
               name="website_link" value="{{ old('website_link') ?? $graduate->website_link }}">
    </div>
    <div class="input-group link-input {{ $errors->has('facebook_link') ? ' has-error' : '' }}">
        <span class="input-group-addon"><i style="color: #3b5998;" class="fa fa-facebook-official icon"></i></span>
        <input type="text" class="form-control" placeholder="Facebook URL"
               name="facebook_link" value="{{ old('facebook_link') ?? $graduate->facebook_link }}">
    </div>
    <div class="input-group link-input {{ $errors->has('twitter_link') ? ' has-error' : '' }}">
        <span class="input-group-addon"><i style="color: #1da1f2;" class="fa fa-twitter icon"></i></span>
        <input type="text" class="form-control" placeholder="Twitter URL"
               name="twitter_link" value="{{ old('twitter_link') ?? $graduate->twitter_link }}">
    </div>
    <div class="input-group link-input {{ $errors->has('instagram_link') ? ' has-error' : '' }}">
        <span class="input-group-addon"><i style="color: #c13584;" class="fa fa-instagram icon"></i></span>
        <input type="text" class="form-control" placeholder="Instagram URL"
               name="instagram_link" value="{{ old('instagram_link') ?? $graduate->instagram_link }}">
    </div>
    <div class="input-group link-input {{ $errors->has('github_link') ? ' has-error' : '' }}">
        <span class="input-group-addon"><i style="color: #333;" class="fa fa-github icon"></i></span>
        <input type="text" class="form-control" placeholder="Github URL"
               name="github_link" value="{{ old('github_link') ?? $graduate->github_link }}">
    </div>
    <div class="input-group link-input {{ $errors->has('linkedin_link') ? ' has-error' : '' }}">
        <span class="input-group-addon"><i style="color: #0077b5;" class="fa fa-linkedin icon"></i></span>
        <input type="text" class="form-control" placeholder="LinkedIn URL"
               name="linkedin_link" value="{{ old('linkedin_link') ?? $graduate->linkedin_link }}">
    </div>
    <div class="input-group link-input {{ $errors->has('dribble_link') ? ' has-error' : '' }}">
        <span class="input-group-addon"><i style="color: #ea4c89;" class="fa fa-dribbble icon"></i></span>
        <input type="text" class="form-control" placeholder="Dribble URL"
               name="dribble_link" value="{{ old('dribble_link') ?? $graduate->dribble_link }}">
    </div>
</div>




