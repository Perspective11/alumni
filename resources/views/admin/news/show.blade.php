@extends("adminlte::page")
@section('top-ex')
    <script src="/plugins/bootstrap-confirmation.min.js"></script>
@endsection
@section("content_header")
    <h3>News Post Details</h3>
    <div class="">
        <a href="{{ $news->path(true) . '/edit' }}" class="btn btn-warning btn-sm"><i class="fa fa-pencil"></i> Edit</a>
        <form class="form-inline inline" method="post" action="{{ $news->path(true) }}">
            {{ csrf_field() }}
            {{ method_field('DELETE') }}
            <button type="submit" class="btn btn-danger btn-sm" data-toggle="confirmation" data-popout="true"><i class="fa fa-times"></i> Delete</button>
        </form>
    </div>
@endsection


@section("content")
    <div class="row">
        @if($news->pictures()->count())
            <div class="col-md-3">
                <div class="form-group">
                    <label for="image">Image:</label>
                    <img src="{{$news->pictures()->count()? $news->pictures()->first()->path :''}}"
                         class="img-responsive img-rounded" alt="">
                </div>
            </div>
        @endif
        <div class="col-md-6">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">News Post</h3>
                </div><!-- /.box-header -->
                <div class="box-body">
                    <div class="form-group">
                        <label class="center-block" for="user">User:</label>
                        @if(($news->user->child() !== null) && $news->user->child()->pictures()->count())
                            <img src="{{ $news->user->child()->pictures()->first()->path }}" alt="" style="max-height:50px" class="inline img-responsive img-rounded">
                        @endif
                        <span class="inline" id="user">{{$news->user->childOrUserName()}} ( {{$news->user->roles()->first()->name}} )</span>
                    </div>
                    <div class="form-group">
                        <label for="title">Title:</label>
                        <p id="title">{{$news->title}}</p>
                    </div>
                    <div class="form-group">
                        <label for="body">Body:</label>
                        <p id="body">{{$news->body}}</p>
                    </div>
                </div><!-- /.box-body -->
            </div><!-- /.box -->

        </div>
        <div class="col-md-3">
            <div class="box box-success">
                <div class="box-header with-border">
                    <h3 class="box-title">Info</h3>
                </div><!-- /.box-header -->
                <div class="box-body">
                    <div class="form-group">
                        <label for="category">Category:</label>
                        @if($news->category)
                            <p id="category">
                                <i style="color: {{$news->category->color}}"
                                   class="fa {{ $news->category->icon }}"></i> {{$news->category->name}}
                            </p>
                        @endif
                    </div>
                    <div class="form-group">
                        <label for="major">Major:</label>
                        @if($news->major)
                            <p id="major">
                                <i style="color: {{$news->major->color}}"
                                   class="fa {{ $news->major->icon }}"></i> {{$news->major->code . ' ' . $news->major->name}}
                            </p>
                        @endif
                    </div>
                    <div class="form-group">
                        <label for="tags">Tags:</label>
                        @if($news->tags)
                            <p id="tags">{{ $news->tagsString() }}</p>
                        @endif
                    </div>
                    <div class="form-group">
                        <label for="created_at">Posted At:</label>
                        <p id="created_at">{{$news->created_at->diffForHumans()}}</p>
                    </div>
                    <div class="form-group">
                        <label for="updated_at">Updated At:</label>
                        <p id="updated_at">{{$news->updated_at->diffForHumans()}}</p>
                    </div>
                    <div class="form-group">
                        @php
                            $class = $news->status ? 'success' : 'danger';
                            $status = $news->status ? 'Unpublish' : 'Publish';
                        @endphp
                        <label for="status">Status:</label><br>
                        <a href="{{ $news->path(true) . '/activation' }}" class="btn btn-xs news-status btn-{{ $class}}">{{ $status }}</a>
                    </div>
                </div><!-- /.box-body -->
            </div><!-- /.box -->
        </div>
    </div>
    <hr>
    @include('admin.layouts.comments')

@endsection

@section('footer-ex')
    <script>
        $('[data-toggle=confirmation]').confirmation({
            rootSelector: '[data-toggle=confirmation]',
            title: 'Are you sure you want to delete this news post and all it\'s comments?',
            btnOkClass: 'btn btn-xs btn-danger',
            btnCancelClass: 'btn btn-xs btn-default',
            btnOkIcon: 'fa fa-trash',
            btnCancelIcon: 'fa fa-ban'
        });
    </script>
@endsection