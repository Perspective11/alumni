@extends('adminlte::page')
@section('content')
    <form class="col-md-10 col-md-offset-1" action="{{ $news->path(true) }}" method="post" accept-charset="UTF-8" enctype="multipart/form-data">
        {{ csrf_field() }}
        {{ method_field('PATCH') }}
        <div class="box box-primary">
            <div class="box-header with-border">
                <h3 class="box-title">Edit News Post</h3>
                <div class="box-tools text-muted">
                    <p><small>Fields with <span style="color:orangered">*</span> are required</small></p>
                </div>
            </div><!-- /.box-header -->
            <div class="box-body">
                @include('admin.news.form')
            </div><!-- /.box-body -->
            <div class="box-footer">
                <button type="submit" class="btn btn-info pull-right">Edit News Post</button>
            </div><!-- box-footer -->
        </div><!-- /.box -->
    </form>
    <div class="col-md-10 col-md-offset-1">
        @include('errors.errors')
    </div>
@endsection
