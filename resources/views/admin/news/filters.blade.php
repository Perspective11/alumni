<form id="user-filter" action="" method="get">
    <div class="box box-primary">
        <div class="box-header with-border">
            <h3 class="box-title">News Filters</h3>
            <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i
                            class="fa fa-minus"></i>
                </button>
                <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i>
                </button>
            </div>
        </div><!-- /.box-header -->
        <div class="box-body">
            <div class="col-md-2">
                <h5><strong>News Status:</strong></h5>
                @php
                    $radioStatus = '';
                    if (request()->has('status')){
                        if (request('status') == 'published')
                            $radioStatus = request('status');
                        else if (request('status') == 'unpublished')
                            $radioStatus = request('status');
                    }
                @endphp
                <div class="radio">
                    <label>
                        <input type="radio" name="status" id="radioActive"
                               value="published" {{ $radioStatus == 'published'? 'checked': '' }}>
                        Published
                    </label>
                </div>
                <div class="radio">
                    <label>
                        <input type="radio" name="status" id="radioInactive"
                               value="unpublished" {{ $radioStatus == 'unpublished'? 'checked': ''  }}>
                        Unpublished
                    </label>
                </div>
                <div class="radio">
                    <label>
                        <input type="radio" name="status" id="radioAll"
                               value="" {{ $radioStatus== ''? 'checked': '' }}>
                        All
                    </label>
                </div>
            </div>
            <div class="col-md-2">
                @php
                    $comboMajor = '';
                    $majorsArray =  \App\Major::has('news')->pluck('name')->toArray();
                    if (request()->has('major')){
                        if (in_array(request('major'), $majorsArray))
                            $comboMajor = request('major');
                    }
                @endphp
                <h5><strong>Major:</strong></h5>
                <select name="major" class="form-control" id="major">
                    <option {{ $comboMajor== ''? 'selected': '' }} value="">All</option>
                    @foreach($majorsArray as $major)
                        <option {{ $comboMajor== $major? 'selected': '' }} value="{{ $major }}">{{ $major }}</option>
                    @endforeach
                </select>
            </div>
            <div class="col-md-2">
                @php
                    $comboCategory = '';
                    $categoriesArray =  \App\Category::has('news')->pluck('name')->toArray();
                    if (request()->has('category')){
                        if (in_array(request('category'), $categoriesArray))
                            $comboCategory = request('category');
                    }
                @endphp
                <h5><strong>Category:</strong></h5>
                <select name="category" class="form-control" id="category">
                    <option {{ $comboCategory== ''? 'selected': '' }} value="">All</option>
                    @foreach($categoriesArray as $category)
                        <option {{ $comboCategory== $category? 'selected': '' }} value="{{ $category }}">{{ $category }}</option>
                    @endforeach
                </select>
            </div>
            <div class="col-md-2">
                <h5><strong>User:</strong></h5>
                <div class="form-group">
                    <input type="text" class="form-control" name="user" value="{{ request('user')? request('user') : '' }}">
                </div>
            </div>
            <div class="col-md-2">
                @php
                    $comboTag = '';
                    $tagsArray =  \App\Tag::has('news')->pluck('name')->toArray();
                    if (request()->has('tag')){
                        if (in_array(request('tag'), $tagsArray))
                            $comboTag = request('tag');
                    }
                @endphp
                <h5><strong>Tag:</strong></h5>
                <select name="tag" class="form-control" id="tag">
                    <option {{ $comboTag== ''? 'selected': '' }} value="">All</option>
                    @foreach($tagsArray as $tag)
                        <option {{ $comboTag== $tag? 'selected': '' }} value="{{ $tag }}">{{ $tag }}</option>
                    @endforeach
                </select>
            </div>
        </div><!-- /.box-body -->
        <div class="box-footer">
            <button type="submit" id="search-submit" class="btn btn-primary">Apply Filters</button>
            <button type="reset" id="clear-fields" class="btn btn-default">Clear</button>
        </div><!-- box-footer -->
    </div><!-- /.box -->
</form>
<script>
    $('#clear-fields').on('click', function () {
        var strippedUrl = stripQuery(window.location.href);
        window.location.replace(strippedUrl);
    })
    function stripQuery(url) {
        return url.split("?")[0].split("#")[0];
    }
</script>