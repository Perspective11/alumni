@extends('adminlte::page')
@section('top-ex')
    <script src="/plugins/bootstrap-confirmation.min.js"></script>
@endsection
@section('content')
    <section class="FAB">
        <div class="FAB__action-button">
            <a href="{{ url('/admin/news/create') }}"><i class="action-button__icon fa fa-plus"></i></a>
            <p class="action-button__text--hide">Add News Post</p>
        </div>
    </section>
    @include('admin.news.filters')
    <table class="table table-condensed jdatatable display small" id="news-table"  style="background-color: #fff;">
        <thead>
        <tr>
            <th>Title</th>
            <th>Body</th>
            <th>Status</th>
            <th>Posted_at</th>
            <th>Category</th>
            <th>Major</th>
            <th>User</th>
            <th>Tags</th>
            <th>Comments</th>
        </tr>
        </thead>
        <tbody>
        </tbody>
    </table>
@endsection
@section('footer-ex')
    <script>
        $(function () {
            $('.FAB__action-button').hover(function(){
                $(this).find('.action-button__text--hide').attr('class', 'action-button__text--show');
                $('.mini-action-button--hide').attr('class', 'mini-action-button--show');
            }, function(){
                $(this).find('.action-button__text--show').attr('class', 'action-button__text--hide');
            });

            var $newsTable = $('#news-table').DataTable({
                "order": [[ 3, 'desc' ]],
                pageLength: 100,
                processing: true,
                serverSide: true,
                ajax: {
                    url: '{{url("/admin/news/getData")}}',
                    data: function (d) {
//                        d.status = $('input[name=status]:checked').val();
                        d.status = '{{ request('status')? request('status') : '' }}'
//                        d.role = $('#role').find('option:selected').val();
                        d.major = '{{ request('major')? request('major') : ''}}'

                        d.category = '{{ request('category')? request('category') : ''}}'

                        d.user = '{{ request('user')? request('user') : ''}}'

                        d.tag = '{{ request('tag')? request('tag') : ''}}'
                    }
                },

                columns: [
                    {data: 'title', name: 'title'},
                    {data: 'body', name: 'body'},
                    {data: 'status', name: 'status', sortable: 'no', orderable: 'no', searchable: 'no'},
                    {data: 'created_at', name: 'created_at'},
                    {data: 'category', name: 'category', sortable: 'no', orderable: 'no', searchable: 'no'},
                    {data: 'major', name: 'major', sortable: 'no', orderable: 'no', searchable: 'no'},
                    {data: 'user', name: 'user', sortable: 'no', orderable: 'no', searchable: 'no'},
                    {data: 'tags', name: 'tags', sortable: 'no', orderable: 'no', searchable: 'no'},
                    {data: 'comments', name: 'comments', sortable: 'no', orderable: 'no', searchable: 'no'}
                ]
            });

            $('#news-table tbody').on('click','.news-status', function (e) {
                e.stopPropagation()
            })
            $('#news-table tbody').on('click','.news-comments', function (e) {
                e.stopPropagation()
            })

            var $modal = $('.action-modal');
            $('#news-table tbody').on('click', 'tr', function () {

                var data = $newsTable.row(this).data();
                console.log(data);
                $('.action-modal').modal();
                $modal.find('.modal-news-title').text(data['title']);
                $modal.find('.modal-news-body').text(data['body']);
                $modal.find('.modal-news-category').html(data['category']);
                $modal.find('.modal-news-major').text(data['major']);
                $modal.find('.modal-news-user').text(data['user']);
                $modal.find('.modal-news-status').html(data['status']);
                $modal.find('.modal-news-postedAt').text(data['created_at']);
                $modal.find('.modal-news-tags').text(data['tags']);
                $modal.find('.modal-news-comments').html(data['comments']);
                $modal.find('.news-by-user').attr('href' , '/admin/news?user=' + data['user']);
                $modal.find('.view-news-details').attr('href' , '/admin/news/' + data['id']);
                $modal.find('.edit-news-details').attr('href' , '/admin/news/' + data['id'] + '/edit');
                $modal.find('.delete-news').attr('action' , '/admin/news/' + data['id']);

            });
            $('[data-toggle=confirmation]').confirmation({
                rootSelector: '[data-toggle=confirmation]',
                title: 'Are you sure you want to delete this news post with all it\'s comments?',
                btnOkClass: 'btn btn-xs btn-danger',
                btnCancelClass: 'btn btn-xs btn-default',
                btnOkIcon: 'fa fa-trash',
                btnCancelIcon: 'fa fa-ban'
            });


        });
    </script>
@endsection


@section('modals')
    <div class="modal fade action-modal" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Action</h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="modal-field">
                                <strong>Title:</strong> <span class="modal-news-title"></span> <br>
                            </div>
                            <div class="modal-field">
                                <strong>Body:</strong> <span class="modal-news-body"></span> <br>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="modal-field">
                                <strong>Category:</strong> <span class="modal-news-category"></span> <br>
                            </div>
                            <div class="modal-field">
                                <strong>Major:</strong> <span class="modal-news-major"></span> <br>
                            </div>
                            <div class="modal-field">
                                <strong>User:</strong> <span class="modal-news-user"></span> <br>
                            </div>
                            <div class="modal-field">
                                <strong>Status:</strong> <span class="modal-news-status"></span> <br>
                            </div>
                            <div class="modal-field">
                                <strong>Posted At:</strong> <span class="modal-news-postedAt"></span> <br>
                            </div>
                            <div class="modal-field">
                                <strong>Tags:</strong> <span class="modal-news-tags"></span> <br>
                            </div>
                            <div class="modal-field">
                                <strong>Comments:</strong> <span class="modal-news-comments"></span> <br>
                            </div>

                        </div>
                    </div>
                    <hr>
                    <div class="list-group">
                        <a href="#" class="list-group-item news-by-user">News by the same user</a>
                        <a href="#" class="list-group-item view-news-details">View news post details</a>
                        <a href="#" class="list-group-item edit-news-details">Edit news post details</a>
                        <form class="list-group-item list-group-item-danger delete-news" method="post" action="">
                            {{ csrf_field() }}
                            {{ method_field('DELETE') }}
                            <button type="submit" class="btn-link text-red" data-toggle="confirmation" data-popout="true"> Delete News Post</button>
                        </form>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->

@endsection