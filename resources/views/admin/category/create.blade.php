@extends('adminlte::page')

@section('content')
    <div class="row">
        <form class="col-md-10 col-md-offset-1" action="{{ url('/admin/categories') }}" method="post" accept-charset="UTF-8"
              enctype="multipart/form-data">
            {{ csrf_field() }}
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">Category</h3>
                </div><!-- /.box-header -->
                <div id="categoryBox" class="box-body">
                    @include('admin.category.form',[
                    'category' => new App\Category
                    ])
                </div><!-- /.box-body -->
                <div class="box-footer">
                    <button type="submit" class="btn btn-success pull-right">Create Category</button>
                </div><!-- box-footer -->
            </div><!-- /.box -->
        </form>
    </div>
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            @include('errors.errors')
        </div>
    </div>
@endsection
