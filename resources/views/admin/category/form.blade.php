@section('top-ex')
    <link rel="stylesheet" href="/plugins/bootstrap-iconpicker/css/bootstrap-iconpicker.min.css"/>
    <link rel="stylesheet" href="/plugins/bootstrap-colorpicker/css/bootstrap-colorpicker.min.css"/>
    <script type="text/javascript"
            src="/plugins/bootstrap-iconpicker/js/iconset/iconset-fontawesome-4.7.0.min.js"></script>
    <script type="text/javascript" src="/plugins/bootstrap-iconpicker/js/bootstrap-iconpicker.min.js"></script>
    <script type="text/javascript" src="/plugins/bootstrap-colorpicker/js/bootstrap-colorpicker.min.js"></script>
@endsection
<input type="hidden" name="id" value="{{ $category->id }}">
<div class="row">
    <div class="col-md-3">
        <div class="form-group {{  $errors->has('name') ? ' has-error' : ''  }}">
            <label for="name">Name:</label>
            <input type="text"
                   name="name"
                   class="form-control"
                   required
                   id="name"
                   value="{{ old('name') ?? $category->name }}">
        </div>
    </div>
    <div class="col-md-2">
        <div class="form-group {{ $errors->has('icon') ? ' has-error' : '' }}">
            <label for="icon">Icon:</label>
            <div>
                <button
                        class="btn btn-default icon-picker"
                        data-iconset="fontawesome"
                        data-icon="{{ old('icon') ?? $category->icon }}"
                        name="icon"
                        role="iconpicker">
                </button>
            </div>
        </div>
    </div>
    <div class="col-md-3">
        <div class="form-group {{  $errors->has('color') ? ' has-error' : ''  }}">
            <label for="color">Color:</label>
            <div class="color-picker input-group colorpicker-component">
                <input type="text"
                       placeholder="Select Color"
                       name="{{ 'color' }}"
                       value="{{ old('color') ?? $category->color }}"
                       class="form-control"/>
                <span class="input-group-addon"><i></i></span>
            </div>
        </div>
    </div>
    <div class="col-md-4">
        <div class="form-group">
            <label for="preview">Preview:</label>
            <div>
                <i class="icon-preview fa {{ old('icon') ?? $category->icon }}" style="color: {{ old('color') ?? $category->color }}"></i>
                <span class="text-preview">{{ old('name') ?? $category->name }}</span>
            </div>
        </div>
    </div>
</div>
@section('footer-ex')
    <script>
        $('#categoryBox').on('input', '#name', function (e) {
            $textPreview = $(this).closest('.row').find('.text-preview');
            $textPreview.text($(this).val());
        });

        $('.icon-picker').iconpicker({
            arrowClass: 'btn-danger',
            cols: 10,
            rows: 5,
            selectedClass: 'btn-success',
            unselectedClass: ''
        });
        $('#categoryBox').on('change', '.icon-picker', function (e) {
            $iconPreview = $(this).closest('.row').find('.icon-preview');
            $iconPreview.removeClass(function (index, className) {
                return (className.match(/(^|\s)fa-\S+/g) || []).join(' ');
            });
//            $iconPreview.addClass('fa-2x ' + e.icon);
            $iconPreview.addClass(e.icon);
        });

        $('.color-picker').colorpicker();
        $('#categoryBox').on('changeColor', '.color-picker', function (e) {
            $iconPreview = $(this).closest('.row').find('.icon-preview');
            $iconPreview.css('color', $(this).colorpicker('getValue', '#456'));
        });

    </script>

@endsection

