@extends('adminlte::page')
@section('top-ex')
    <script src="/plugins/bootstrap-confirmation.min.js"></script>
@endsection
@section('content')
    <section class="FAB">
        <div class="FAB__action-button">
            <a href="{{ url('/admin/categories/create') }}"><i class="action-button__icon fa fa-plus"></i></a>
            <p class="action-button__text--hide">Add Category</p>
        </div>
    </section>
    <table class="table table-condensed jdatatable display" id="categories-table"  style="background-color: #fff;">
        <thead>
        <tr>
            <th>Name</th>
            <th>Icon</th>
            <th>Posts</th>
            <th>Events</th>
            <th>News</th>
        </tr>
        </thead>
        <tbody>
        </tbody>
    </table>
@endsection
@section('footer-ex')
    <script>
        $(function () {
            $('.FAB__action-button').hover(function(){
                $(this).find('.action-button__text--hide').attr('class', 'action-button__text--show');
                $('.mini-action-button--hide').attr('class', 'mini-action-button--show');
            }, function(){
                $(this).find('.action-button__text--show').attr('class', 'action-button__text--hide');
            });

            var $categoriesTable = $('#categories-table').DataTable({
                "order": [[ 0, 'desc' ]],
                pageLength: 100,
                processing: true,
                serverSide: true,
                ajax: {
                    url: '{{url("/admin/categories/getData")}}',
                    data: function (d) {
                    }
                },

                columns: [
                    {data: 'name', name: 'name'},
                    {data: 'icon', name: 'icon', sortable: 'no', orderable: 'no', searchable: 'no'},
                    {data: 'posts', name: 'posts', sortable: 'no', orderable: 'no', searchable: 'no'},
                    {data: 'events', name: 'events', sortable: 'no', orderable: 'no', searchable: 'no'},
                    {data: 'news', name: 'news', sortable: 'no', orderable: 'no', searchable: 'no'},
                ]
            });


            var $modal = $('.action-modal');
            $('#categories-table tbody').on('click', 'tr', function () {

                var data = $categoriesTable.row(this).data();
                console.log(data);
                $('.action-modal').modal();
                $modal.find('.modal-category-name').text(data['name']);
                $modal.find('.modal-category-icon').html(data['icon']);
                $modal.find('.modal-category-posts').html(data['posts']);
                $modal.find('.modal-category-events').html(data['events']);
                $modal.find('.modal-category-news').html(data['news']);
                $modal.find('.view-category-posts').attr('href', '/admin/posts?category=' + data['name']);
                $modal.find('.view-category-events').attr('href', '/admin/events?category=' + data['name']);
                $modal.find('.view-category-news').attr('href', '/admin/news?category=' + data['name']);
                $modal.find('.edit-category').attr('href', '/admin/categories/' + data['id'] + '/edit');
                $modal.find('.delete-category').attr('action' , '/admin/categories/' + data['id']);


            });

            $('[data-toggle=confirmation]').confirmation({
                rootSelector: '[data-toggle=confirmation]',
                title: 'Are you sure you want to delete this category?',
                btnOkClass: 'btn btn-xs btn-danger',
                btnCancelClass: 'btn btn-xs btn-default',
                btnOkIcon: 'fa fa-trash',
                btnCancelIcon: 'fa fa-ban'
            });

        });
    </script>
@endsection


@section('modals')
    <div class="modal fade action-modal" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Action</h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="modal-field">
                                <strong>Name:</strong> <span class="modal-category-name"></span> <br>
                            </div>
                            <div class="modal-field">
                                <strong>Icon:</strong> <span class="modal-category-icon"></span> <br>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="modal-field">
                                <strong>Posts:</strong> <span class="modal-category-posts"></span> <br>
                            </div>
                            <div class="modal-field">
                                <strong>News:</strong> <span class="modal-category-news"></span> <br>
                            </div>
                            <div class="modal-field">
                                <strong>Events:</strong> <span class="modal-category-events"></span> <br>
                            </div>
                        </div>
                    </div>
                    <hr>
                    <div class="list-group">
                        <a href="#" class="list-group-item view-category-posts">Posts with this category</a>
                        <a href="#" class="list-group-item view-category-events">Events with this category</a>
                        <a href="#" class="list-group-item view-category-news">News with this category</a>
                        <a href="#" class="list-group-item edit-category">Edit category</a>
                        <form class="list-group-item list-group-item-danger delete-category" method="post" action="">
                            {{ csrf_field() }}
                            {{ method_field('DELETE') }}
                            <button type="submit" class="btn-link text-red" data-toggle="confirmation" data-popout="true"> Delete Category</button>
                        </form>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->

@endsection