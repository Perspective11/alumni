<form id="user-filter" action="" method="get">
    <div class="box box-primary">
        <div class="box-header with-border">
            <h3 class="box-title">Company Filters</h3>
            <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i
                            class="fa fa-minus"></i>
                </button>
                <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i>
                </button>
            </div>
        </div><!-- /.box-header -->
        <div class="box-body">
            <div class="col-md-2">
                <h5><strong>Company Status:</strong></h5>
                @php
                    $comboStatus = '';
                    $statusArray =  \App\Company::pluck('company_status')->unique()->toArray();
                    if (request()->has('status')){
                        if (in_array(request('status'), $statusArray))
                            $comboStatus = request('status');
                    }
                @endphp
                <select name="status" class="form-control" id="status">
                    <option {{ $comboStatus== ''? 'selected': '' }} value="">All</option>
                    @foreach($statusArray as $status)
                        <option {{ $comboStatus== $status? 'selected': '' }} value="{{ $status }}">{{ $status }}</option>
                    @endforeach
                </select>
            </div>
            <div class="col-md-2">
                <h5><strong>User:</strong></h5>
                <div class="form-group">
                    <input type="text" class="form-control" name="user" value="{{ request('user')? request('user') : '' }}">
                </div>
            </div>
            <div class="col-md-2">
                <h5><strong>Industry:</strong></h5>
                @php
                    $comboIndustry = '';
                    $industryArray =  \App\Company::pluck('industry')->unique()->toArray();
                    if (request()->has('industry')){
                        if (in_array(request('industry'), $industryArray))
                            $comboIndustry = request('industry');
                    }
                @endphp
                <select name="industry" class="form-control" id="industry">
                    <option {{ $comboIndustry== ''? 'selected': '' }} value="">All</option>
                    @foreach($industryArray as $industry)
                        <option {{ $comboIndustry== $industry? 'selected': '' }} value="{{ $industry }}">{{ $industry }}</option>
                    @endforeach
                </select>
            </div>
            <div class="col-md-2">
                <h5><strong>Type:</strong></h5>
                @php
                    $comboType = '';
                    $typeArray = array_filter(\App\Company::pluck('type')->unique()->toArray(), 'strlen');
                    if (request()->has('type')){
                        if (in_array(request('type'), $typeArray))
                            $comboType = request('type');
                    }
                @endphp
                <select name="type" class="form-control" id="type">
                    <option {{ $comboType== ''? 'selected': '' }} value="">All</option>
                    @foreach($typeArray as $type)
                        <option {{ $comboType== $type? 'selected': '' }} value="{{ $type }}">{{ $type }}</option>
                    @endforeach
                </select>
            </div>
            <div class="col-md-2">
                <h5><strong>Size:</strong></h5>
                @php
                    $comboSize = '';
                    $sizeArray =  \App\Company::pluck('size')->unique()->toArray();
                    if (request()->has('size')){
                        if (in_array(request('size'), $sizeArray))
                            $comboSize = request('size');
                    }
                @endphp
                <select name="size" class="form-control" id="size">
                    <option {{ $comboSize== ''? 'selected': '' }} value="">All</option>
                    @foreach($sizeArray as $size)
                        <option {{ $comboSize== $size? 'selected': '' }} value="{{ $size }}">{{ $size }}</option>
                    @endforeach
                </select>
            </div>
        </div><!-- /.box-body -->
        <div class="box-footer">
            <button type="submit" id="search-submit" class="btn btn-primary">Apply Filters</button>
            <button type="reset" id="clear-fields" class="btn btn-default">Clear</button>
        </div><!-- box-footer -->
    </div><!-- /.box -->
</form>
<script>
    $('#clear-fields').on('click', function () {
        var strippedUrl = stripQuery(window.location.href);
        window.location.replace(strippedUrl);
    })
    function stripQuery(url) {
        return url.split("?")[0].split("#")[0];
    }
</script>

