@extends('adminlte::page')
@section('top-ex')
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/css/select2.min.css" rel="stylesheet"/>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js"></script>
    <script src="/plugins/bootstrap-confirmation.min.js"></script>
@endsection
@section('content')
    <section class="FAB">
        <div class="FAB__action-button">
            <a href="{{ url('/admin/users/create') }}"><i class="action-button__icon fa fa-user-plus"></i></a>
            <p class="action-button__text--hide">Add User</p>
        </div>
    </section>
    <div class="container-fluid">
        @include('admin.company.filters')
        <table class="table table-condensed table-responsive jdatatable display cell-border smallest" id="companies-table"
               style="background-color: #fff;">


            <thead>
            <tr>
                <th>Company Name</th>
                <th>Email</th>
                <th>Industry</th>
                <th>Type</th>
                <th>Size</th>
                <th>Main Branch</th>
                <th>Status</th>
                <th>Links</th>
            </tr>
            </thead>
            <tbody>

            </tbody>
        </table>
    </div>
@endsection
@section('footer-ex')
    <script>
        $(function () {
            $('.FAB__action-button').hover(function () {
                $(this).find('.action-button__text--hide').attr('class', 'action-button__text--show');
                $('.mini-action-button--hide').attr('class', 'mini-action-button--show');
            }, function () {
                $(this).find('.action-button__text--show').attr('class', 'action-button__text--hide');
            });

            var $companiesTable = $('#companies-table').DataTable({
                pageLength: 100,
                processing: true,
                serverSide: true,
                ajax: {
                    url: '{{url("/admin/companies/getData")}}',
                    data: function (d) {
                        d.status = '{{ request('status')? request('status') : '' }}'
                        d.user = '{{ request('user')? request('user') : ''}}'
                        d.industry = '{{ request('industry')? request('industry') : ''}}'
                        d.type = '{{ request('type')? request('type') : ''}}'
                        d.size = '{{ request('size')? request('size') : ''}}'

                    }
                },

                columns: [
                    {data: 'company_name', name: 'company_name'},
                    {data: 'email', name: 'email', sortable: 'no', orderable: 'no', searchable: 'no'},
                    {data: 'industry', name: 'industry'},
                    {data: 'type', name: 'type'},
                    {data: 'size', name: 'size'},
                    {data: 'address_head', name: 'address_head'},
                    {data: 'company_status', name: 'company_status'},
                    {data: 'links', name: 'links', sortable: 'no', orderable: 'no', searchable: 'no'}
                ]
            });

            $('#companies-table tbody').on('click', '.link-icon', function (e) {
                e.stopPropagation()
            })

            var $modal = $('.action-modal');
            $('#companies-table tbody').on('click', 'tr', function () {

                var data = $companiesTable.row(this).data();
                console.log(data);
                $('.action-modal').modal();
                $modal.find('.modal-company-name').text(data['company_name']);
                $modal.find('.modal-company-email').text(data['email']);
                $modal.find('.modal-company-industry').text(data['industry']);
                $modal.find('.modal-company-type').text(data['type']);
                $modal.find('.modal-company-size').html(data['size']);
                $modal.find('.modal-company-mainBranch').text(data['address_head']);
                $modal.find('.modal-company-status').html(data['company_status']);
                $modal.find('.modal-company-links').html(data['links']);
                $modal.find('.view-company-posts').attr('href', '/admin/posts?user=' + data['company_name']);
                $modal.find('.view-company-events').attr('href', '/admin/events?user=' + data['company_name']);
                $modal.find('.view-company-details').attr('href', '/admin/users/' + data['user_id']);
                $modal.find('.edit-company-details').attr('href', '/admin/users/' + data['user_id'] + '/edit');
                $modal.find('.change-user-role').attr('href', '/admin/users/' + data['user_id'] + '/changeRole');
                $modal.find('.delete-user').attr('action' , '/admin/users/' + data['user_id']);

            });
            $('[data-toggle=confirmation]').confirmation({
                rootSelector: '[data-toggle=confirmation]',
                title: 'Are you sure you want to delete this user?',
                btnOkClass: 'btn btn-xs btn-danger',
                btnCancelClass: 'btn btn-xs btn-default',
                btnOkIcon: 'fa fa-trash',
                btnCancelIcon: 'fa fa-ban'
            });


        });
    </script>
@endsection

@section('modals')
    <div class="modal fade action-modal" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Action</h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="modal-field">
                                <strong>Company Name:</strong> <span class="modal-company-name"></span> <br>
                            </div>
                            <div class="modal-field">
                                <strong>Email:</strong> <span class="modal-company-email"></span> <br>
                            </div>
                            <div class="modal-field">
                                <strong>Industry:</strong> <span class="modal-company-industry"></span> <br>
                            </div>
                            <div class="modal-field">
                                <strong>Type:</strong> <span class="modal-company-type"></span> <br>
                            </div>
                            <div class="modal-field">
                                <strong>Size:</strong> <span class="modal-company-size"></span> <br>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="modal-field">
                                <strong>Main Branch:</strong> <span class="modal-company-mainBranch"></span> <br>
                            </div>
                            <div class="modal-field">
                                <strong>Status:</strong> <span class="modal-company-status"></span> <br>
                            </div>
                            <div class="modal-field">
                                <strong>Links:</strong> <span class="modal-company-links"></span> <br>
                            </div>
                        </div>
                    </div>

                    <hr>
                    <div class="list-group">
                        <a href="#" class="list-group-item view-company-posts">View company posts</a>
                        <a href="#" class="list-group-item view-company-events">View company events</a>
                        <a href="#" class="list-group-item view-company-details">View company details</a>
                        <a href="#" class="list-group-item edit-company-details ">Edit company details</a>
                        <a href="#" class="list-group-item change-user-role">Change user role</a>
                        <form class="list-group-item list-group-item-danger delete-user" method="post" action="">
                            {{ csrf_field() }}
                            {{ method_field('DELETE') }}
                            <button type="submit" class="btn-link text-red" data-toggle="confirmation" data-popout="true"> Delete User</button>
                        </form>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->

@endsection