@extends('public.layouts.app')
@section('content')
    <!-- === BEGIN CONTENT === -->
    <div id="content">
        <div class="container background-white">
            <div class="row margin-vert-30">
                <div class="col-md-12">
                    <div class="error-404-page text-center">
                        <h2>404!</h2>
                        <h3>The page can not be found</h3>
                        <p><a href="{{ url()->previous() }}" class="btn btn-primary">Go Back</a></p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- === END CONTENT === -->
@endsection