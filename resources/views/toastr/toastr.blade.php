@if (Session::has('toastr'))
    @php
        $toastr = Session::get('toastr');
    @endphp
    <script>
        $(document).ready(function(){
            toastr.options = {
                "closeButton": false,
                "debug": false,
                "newestOnTop": false,
                "progressBar": true,
                "positionClass": "toast-bottom-left",
                "preventDuplicates": false,
                "onclick": null,
                "showDuration": "300",
                "hideDuration": "1000",
                "timeOut": "5000",
                "extendedTimeOut": "1000",
                "showEasing": "swing",
                "hideEasing": "linear",
                "showMethod": "fadeIn",
                "hideMethod": "fadeOut"
            };
            @if(is_array($toastr))
                toastr.{{ isset($toastr[1])? $toastr[1] : 'info' }}('{{ $toastr[0] }}' {!! isset($toastr[2])? ', "'. $toastr[2] . '"' :  '' !!} );
            @else
                toastr.info('{{ $toastr  }}');
            @endif
        });

    </script>
@endif


