@extends('public.layouts.app')
@section('content')
    <!-- === BEGIN CONTENT === -->
    <div id="content">
        <div class="container background-white">
            <div class="row margin-vert-30">
                <!-- Main Column -->
                <div class="col-md-9">
                    <!-- Main Content -->
                    <div class="headline">
                        <h2>Contact Form</h2>
                    </div>
                    <p>{!! $contact_header !!}  </p>
                    <br>
                    <!-- Contact Form -->
                    <form action="{{ route('contact') }}" method="post">
                        {{ csrf_field() }}
                        <label>Name <span class="color-red">{{ $errors->has('name')?$errors->first('name'):'' }}</span></label>
                        <div class="row margin-bottom-20">
                            <div class="col-md-6 col-md-offset-0">
                                <input class="form-control" name="name" type="text">
                            </div>
                        </div>
                        <label>Email
                            <span class="color-red">* {{ $errors->has('email')?$errors->first('email'):'' }}</span>
                        </label>
                        <div class="row margin-bottom-20">
                            <div class="col-md-6 col-md-offset-0">
                                <input class="form-control" name="email" type="email">
                            </div>
                        </div>
                        <label>Message <span
                                    class="color-red">{{ $errors->has('message')?$errors->first('message'):'' }}</span></label>
                        <div class="row margin-bottom-20">
                            <div class="col-md-8 col-md-offset-0">
                                <textarea rows="8" class="form-control" name="message"></textarea>
                            </div>
                        </div>
                        <p>
                            <button type="submit" class="btn btn-primary">Send Message</button>
                        </p>
                    </form>
                    <!-- End Contact Form -->
                    <!-- End Main Content -->
                </div>
                <!-- End Main Column -->
                <!-- Side Column -->
                <div class="col-md-3">
                @if($contact_info)
                    <!-- Recent Posts -->
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h3 class="panel-title">Contact Info</h3>
                            </div>
                            <div class="panel-body">
                                {!! $contact_info !!}
                            </div>
                        </div>
                        <!-- End recent Posts -->
                    @endif
                </div>
                <!-- End Side Column -->
            </div>
        </div>
    </div>
    <!-- === END CONTENT === -->
@endsection