@extends('public.layouts.app')
@section('content')
    <div class="container background-white">
        <div class="row margin-vert-20">
            <div class="col-md-8 col-sm-12">
                <h2>Graduates</h2>
                @if($graduates->count())
                    @foreach($graduates as $graduate)
                        <div class="panel panel-default card">
                            {{--<div class="panel-heading">Login</div>--}}
                            <div class="panel-body">
                                <div class="col-md-4 col-sm-12 center-block">
                                    <img src="{{ count($graduate->pictures)?$graduate->pictures()->first()->path:asset('images/graduates/profile/default.png')}}" alt=""
                                         class="img-responsive img-circle">
                                </div>
                                <div class="col-md-8 col-sm-12">
                                    <h4>{{ str_limit($graduate->full_name, 70)  }}</h4>
                                    <h6>
                                        <a href="{{ route('graduates.major',$graduate->major->id) }}">{{ isset($graduate->major->name)? $graduate->major->name : '' }}</a>
                                        {{ isset($graduate->graduation_year)? ' | '. $graduate->graduation_year : '' }}
                                    </h6>
                                    <p>{{str_limit($graduate->description, 250) }}</p>
                                    <a href="{{ route('profile',$graduate->user->id) }}" class="btn btn-sm btn-primary">View Profile</a>
                                </div>

                            </div>
                        </div>
                    @endforeach
                    {{ $graduates->links() }}
                @else
                    <h3>There are no graduates with this search</h3>
                @endif
            </div>
            <div class="col-md-4 col-sm-12">
                {{--Search company--}}
                <div class="blog-tags">
                    <h3>Search</h3>
                    <p>
                    <form action="" method="get">
                        <input type="text" name="name" class="form-control" placeholder="Graduate name">
                        <input type="number" name="id" class="form-control" placeholder="Graduate id">
                        <button type="submit" class="btn btn-primary margin-vert-10 btn-block"><span class="fa fa-search"></span></button>
                    </form>
                    </p>
                </div>
                {{--end search company--}}
                <!-- Blog Tags -->
                <div class="blog-tags">
                    <h3>Majors</h3>
                    <ul class="blog-tags">
                        @if(count($majors))
                            @foreach($majors as $major)
                                <li>
                                    <a href="{{ route('graduates.major',$major->id) }}" class="blog-tag">{{ $major->name }}</a>
                                </li>
                            @endforeach
                        @else
                            <h4>There are no tags assigned to posts</h4>
                        @endif
                    </ul>
                </div>
                <!-- End Blog Tags -->
            </div>
        </div>
    </div>
@endsection