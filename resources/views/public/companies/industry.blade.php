@extends('public.layouts.app')
@section('content')
    <div class="container background-white">
        <div class="row margin-vert-20">
            <div class="col-md-8 col-sm-12">
                <h2>Companies with "{{ $industry }}" industry</h2>
                @foreach($companies as $company)
                    <div class="panel panel-default card" style="width: 100%;">
                        {{--<div class="panel-heading">Login</div>--}}
                        <div class="panel-body">
                            <div class="col-md-4 col-sm-12 center-block">
                                <img src="{{ count($company->pictures)?$company->pictures()->first()->path:asset('images/companies/profile/default.jpg')}}" alt=""
                                     class="img-responsive img-rounded">
                            </div>
                            <div class="col-md-8 col-sm-12">
                                <h4>{{ str_limit($company->company_name, 70)  }}</h4>
                                <h6>
                                    {{ isset($company->industry)? $company->industry : '' }}
                                    {{ isset($company->headquarters)? ' | '. $company->headquarters : '' }}
                                </h6>
                                <p>{{str_limit($company->short_description, 250) }}</p>
                                <a href="{{ route('profile',$company->user->id) }}" class="btn btn-sm btn-primary">View Profile</a>
                            </div>

                        </div>
                    </div>
                @endforeach
                {{ $companies->links() }}
            </div>
            <div class="col-md-4 col-sm-12">
                {{--Search company--}}
                <div class="blog-tags">
                    <h3>Search</h3>
                    <p>
                    <form action="" method="get" class="input-group">
                        <input type="search" name="search" class="form-control" placeholder="Company name">
                        <span class="input-group-btn">
                             <button type="submit" class="btn btn-primary"><span class="fa fa-search"></span></button>
                         </span>
                    </form>
                    </p>
                </div>
                {{--end search company--}}
            </div>
        </div>
    </div>
@endsection