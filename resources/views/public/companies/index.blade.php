@extends('public.layouts.app')
@section('content')
    <div class="container background-white">
        <div class="row margin-vert-20">
            <div class="col-md-8 col-sm-12">
                <h2>Companies</h2>
                @foreach($companies as $company)
                    <div class="panel panel-default card" style="width: 100%;">
                        {{--<div class="panel-heading">Login</div>--}}
                        <div class="panel-body">
                            <div class="col-md-4 col-sm-12 center-block">
                                <img src="{{ count($company->pictures)?$company->pictures()->first()->path:asset('images/companies/profile/default.jpg')}}" alt=""
                                     class="img-responsive img-rounded">
                            </div>
                            <div class="col-md-8 col-sm-12">
                                <h4>{{ str_limit($company->company_name, 70)  }}</h4>
                                <h6>
                                    {{ isset($company->industry)? $company->industry : '' }}
                                    {{ isset($company->headquarters)? ' | '. $company->headquarters : '' }}
                                </h6>
                                <p>{{str_limit($company->short_description, 250) }}</p>
                                <a href="{{ route('profile',$company->user->id) }}" class="btn btn-sm btn-primary">View Profile</a>
                            </div>

                        </div>
                    </div>
                    @endforeach
                {{ $companies->links() }}
            </div>
            <div class="col-md-4 col-sm-12">
                {{--Search company--}}
                <div class="blog-tags">
                    <h3>Search</h3>
                    <p>
                    <form action="" method="get" class="input-group">
                        <input type="search" name="search" class="form-control" placeholder="Company name">
                        <span class="input-group-btn">
                             <button type="submit" class="btn btn-primary"><span class="fa fa-search"></span></button>
                         </span>
                    </form>
                    </p>
                </div>
                {{--end search company--}}
                <!-- Blog Tags -->
                <div class="blog-tags">
                    <h3>Industries</h3>
                    <ul class="blog-tags">
                        @if(count($industries))
                            @foreach($industries as $industry)
                                <li>
                                    <a href="{{ route('companies.industry',$industry->industry) }}" class="blog-tag">{{ $industry->industry }}</a>
                                </li>
                            @endforeach
                        @else
                            <h4>There are no tags assigned to posts</h4>
                        @endif
                    </ul>
                </div>
                <!-- End Blog Tags -->
                {{--recent Companies--}}
                <div class="recent-posts">
                    <h3>Recent Companies</h3>
                    <ul class="posts-list margin-top-10">
                        @foreach($recentCompanies as $recentCompany)
                            <li>
                                <div class="recent-post">
                                    <a href="{{ $recentCompany->path() }}">
                                        <img class="pull-left img-responsive" style="height: 50px;" src="{{ count($recentCompany->pictures)?$recentCompany->pictures()->first()->path:asset('images/companies/profile/default.jpg')}}" alt="{{ $recentCompany->company_name }}">
                                    </a>
                                    <a href="{{ $recentCompany->path() }}" class="posts-list-title">{{ str_limit($recentCompany->company_name , 40) }}</a>
                                    <br>
                                    <span class="recent-post-date">
                                                {{ $recentCompany->created_at->diffForHumans() }}
                                            </span>
                                </div>
                                <div class="clearfix"></div>
                            </li>
                        @endforeach
                    </ul>
                </div>
                {{--end recent Companies--}}
            </div>
        </div>
    </div>
@endsection