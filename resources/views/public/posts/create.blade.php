@extends('public.layouts.app')
@section('content')
    <div id="content-top-border" class="container">
    </div>
    <!-- === END HEADER === -->
    <!-- === BEGIN CONTENT === -->
    <div id="content">
        <div class="container background-white">
            <div class="row margin-vert-30">
                <!-- Register Box -->
                <div class="col-md-6 col-md-offset-3 col-sm-offset-3">
                    <form class="signup-page" action="{{ route('posts.store') }}" method="post" enctype="multipart/form-data">
                        {{ csrf_field() }}
                        <div class="signup-header">
                            <h2>Create a new Post</h2>
                            <p>The post will published after admin review</p>
                        </div>
                        <label>Title</label>
                        <input class="form-control margin-bottom-20" name="title" type="text" value="{{old('title')}}" autofocus>
                        <label>Body</label>
                        <textarea class="form-control margin-bottom-20" rows="8" name="body">{{ old('body') }}</textarea>
                        <div class="row">
                            <div class="col-sm-6">
                                <label>Major</label>
                                <select class="form-control margin-bottom-20 col-sm-6" id="major" name="major">
                                    @foreach($majors as $major)
                                        <option value="{{ $major->id }}" {{ old('major') == $major->id ?'selected':'' }}>{{ $major->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="col-sm-6">
                                <label>Category</label>
                                <select class="form-control margin-bottom-20 col-sm-6" id="category" name="category">
                                    @foreach($categories as $category)
                                        <option value="{{ $category->id }}" {{ old('category') == $category->id ?'selected':'' }}>{{ $category->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <label class=" margin-top-20">Image</label>
                        <input type="file" name="picture" class="form-control margin-bottom-20">
                        <label>Tags</label>
                        <select name="tags[]" class="form-control margin-bottom-20" id="tags" multiple>
                            @foreach($tags as $tag)
                                <option value="{{ $tag->id }}" {{ old('tags') == $tag->id ?'selected':'' }}>{{ $tag->name }}</option>
                            @endforeach
                        </select>
                        <div class="margin-bottom-20"></div>
                        @if(count($errors))
                        <hr>
                        <div class="form-group">
                            <div class="alert alert-danger">
                                <ul class="list-group">
                                    @foreach($errors->all() as $error)
                                        <li class="list-group-item">&bull; {{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        </div>
                        @endif
                        <hr>
                        <div class="col-lg-4 pull-right">
                            <button class="btn btn-primary" type="submit">Submit</button>
                        </div>
                        <div class="clearfix"></div>
                    </form>
                </div>
                <!-- End Register Box -->
            </div>
        </div>
    </div>
    <!-- === END CONTENT === -->
@endsection
@section('scripts')
    <script>
        $('#major').select2();
        $('#category').select2();
        $('#tags').select2({
            placeholder: "Select tags",
            allowClear: true,
            maximumSelectionLength: 5
        });
    </script>
@endsection