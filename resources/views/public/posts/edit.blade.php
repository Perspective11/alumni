@extends('public.layouts.app')
@section('styles')
    <style>
        label[for="picture"] {
            position: absolute;
            bottom: 0;
            left: 5px;
            background: orange;
            color: white;
            padding: 5px;
            border-radius: 5px;
        }

        label[for="picture"]:hover {
            cursor: pointer;
        }
    </style>
@endsection
@section('content')
    <div id="content-top-border" class="container">
    </div>
    <!-- === END HEADER === -->
    <!-- === BEGIN CONTENT === -->
    <div id="content">
        <div class="container background-white">
            <div class="row margin-vert-30">
                <!-- Register Box -->
                <div class="col-md-6 col-md-offset-3 col-sm-offset-3">
                    <form class="signup-page" action="{{ route('posts.update', $post->id) }}" method="post"
                          enctype="multipart/form-data">
                        {{ csrf_field() }}
                        {{method_field("PUT")}}
                        <div class="signup-header">
                            <h2>Update a Post</h2>
                        </div>
                        <label>Title</label>
                        <input class="form-control margin-bottom-20" name="title" type="text"
                               value="{{old('title')?:$post->title}}" autofocus>
                        <label>Body</label>
                        <textarea class="form-control margin-bottom-20" rows="8"
                                  name="body">{{ old('body')?:$post->body }}</textarea>
                        <div class="row">
                            <div class="col-sm-6">
                                <label>Major</label>
                                <select class="form-control margin-bottom-20 col-sm-6" id="major" name="major">
                                    @foreach($majors as $major)
                                        <option value="{{ $major->id }}" {{ $post->major->id == $major->id?'selected':'' }}>{{ $major->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="col-sm-6">
                                <label>Category</label>
                                <select class="form-control margin-bottom-20 col-sm-6" id="category" name="category">
                                    @foreach($categories as $category)
                                        <option value="{{ $category->id }}" {{ $post->category->id == $category->id ?'selected="selected"':'' }}>{{ $category->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <label class=" margin-top-20">Image</label>
                        <div style="position:relative;margin-bottom: 10px">
                            @if(count($post->pictures))
                                <img src="{{ asset($post->pictures()->first()->path) }}" alt="{{ $post->title }}"
                                     style="display: block;width: 100%;">
                                <label for="picture">{{ count($post->pictures)?'Edit':'Choose' }}</label>
                                <input type="file" style="display: none;" name="picture"
                                       class="form-control margin-bottom-20" id="picture">
                            @else
                                <input type="file" style="display: none;" name="picture"
                                       class="form-control margin-bottom-20" id="picture">

                            @endif

                        </div>
                        <label>Tags</label>
                        <select name="tags[]" class="form-control margin-bottom-20" id="tags" multiple>
                            @foreach($tags as $tag)
                                @php
                                    $selected = '';
                                    if (old('tags')){
                                        if (in_array($tag->id, old('tags')))
                                        $selected = true;
                                    }
                                    elseif ($post->tags->pluck('id')->contains($tag->id))
                                        $selected = true;
                                    else $selected = false;
                                @endphp
                                <option {{ $selected?'selected':'' }} value="{{ $tag->id }}">{{ $tag->name }}</option>
                            @endforeach
                        </select>
                        <div class="margin-bottom-20"></div>
                        @if(count($errors))
                            <hr>
                            <div class="form-group">
                                <div class="alert alert-danger">
                                    <ul class="list-group">
                                        @foreach($errors->all() as $error)
                                            <li class="list-group-item">&bull; {{ $error }}</li>
                                        @endforeach
                                    </ul>
                                </div>
                            </div>
                        @endif
                        <hr>
                        <div class="col-lg-4 pull-right">
                            <button class="btn btn-primary" type="submit">Submit</button>
                        </div>
                        <div class="clearfix"></div>
                    </form>
                </div>
                <!-- End Register Box -->
            </div>
        </div>
    </div>
    <!-- === END CONTENT === -->
@endsection
@section('scripts')
    <script>
        $('#major').select2();
        $('#category').select2();
        $('#tags').select2({
            tags: true,
            multiple: true,
            placeholder: "Select tags",
            maximumSelectionLength: 5,
            tokenSeparators: [',']
        });
    </script>
@endsection