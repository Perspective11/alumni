@extends('public.layouts.app')
@section('content')
    <!-- === BEGIN CONTENT === -->
    <div id="content">
        <div class="container background-white">
            <div class="row margin-vert-30">
                <!-- Main Column -->
                <div class="col-md-9">
                @forelse($posts as $post)
                    <!-- Blog Post -->
                        <div class="blog-post padding-bottom-20">
                            <!-- Blog Item Header -->
                            <div class="blog-item-header">
                                <!-- Title -->
                                <h2>
                                    <a href="{{ route('posts.show', $post->id) }}">{{ $post->title }}</a>
                                </h2>
                                <div class="clearfix"></div>
                                <!-- End Title -->
                            </div>
                            <!-- End Blog Item Header -->
                            <!-- Blog Item Details -->
                            <div class="blog-post-details">
                                <!-- Author Name -->
                                <div class="blog-post-details-item blog-post-details-item-left">
                                    <i class="fa fa-user color-gray-light"></i>
                                    <a href="{{ route('profile', $post->user->id) }}">{{ $post->user->name }}</a>
                                </div>
                                <!-- End Author Name -->
                                <!-- Date -->
                                <div class="blog-post-details-item blog-post-details-item-left">
                                    <i class="fa fa-clock-o color-gray-light"></i>
                                    {{ $post->created_at->diffForHumans() }}
                                </div>
                                <!-- End Date -->
                            @if($post->tags_count)
                                <!-- Tags -->
                                    <div class="blog-post-details-item blog-post-details-item-left blog-post-details-tags">
                                        <i class="fa fa-tag color-gray-light"></i>
                                        {!! $post->tagsWithLinks() !!}
                                    </div>
                                    <!-- End Tags -->
                                @endif
                                @if($post->category_count)
                                <!-- categories -->
                                <div class="blog-post-details-item blog-post-details-item-left blog-post-details-tags">
                                    <i class="fa {{ $post->category->icon }}"
                                       style="color: {{ $post->category->color }}"></i>
                                    <a href="{{ route('posts.category',$post->category->id) }}">{{ $post->category->name }}</a>
                                </div>
                                <!-- End categories -->
                            @endif

                            @if(count($post->major))
                                <!-- categories -->
                                    <div class="blog-post-details-item blog-post-details-item-left blog-post-details-tags">
                                        Major:
                                        <i class="fa {{ $post->major->icon }}"
                                           style="color: {{ $post->major->color }}"></i>
                                        <span>{{ $post->major->name }}</span>
                                    </div>
                                    <!-- End categories -->
                            @endif
                            <!-- # of Comments -->
                                <div class="blog-post-details-item blog-post-details-item-left blog-post-details-item-last">
                                    <a href="{{ route('posts.show', $post->id) }}">
                                        <i class="fa fa-comments color-gray-light"></i>
                                        {{ $post->comments_count }} Comments
                                    </a>
                                </div>
                                <!-- End # of Comments -->
                            </div>
                            <!-- End Blog Item Details -->
                            <!-- Blog Item Body -->
                            <div class="blog">
                                <div class="clearfix"></div>
                                <div class="blog-post-body row margin-top-15">
                                    <div class="col-md-5">
                                        <img class="margin-bottom-20" src="{{ count($post->pictures)?$post->pictures[0]->path:asset('images/posts/default.png') }}" alt="{{ $post->title }}">
                                    </div>
                                    <div class="col-md-7">
                                        <p>
                                            {{ str_limit($post->body,350,'...') }}
                                        </p>
                                        <!-- Read More -->
                                        <a href="{{ route('posts.show', $post->id) }}" class="btn btn-primary">
                                            Read More
                                            <i class="icon-chevron-right readmore-icon"></i>
                                        </a>
                                        @if($post->user->id == auth()->id())
                                            <a href="{{ route('posts.edit', $post->id) }}" class="btn btn-warning">
                                                Edit
                                                <i class="fa fa-edit"></i>
                                            </a>
                                    @endif
                                    <!-- End Read More -->
                                    </div>
                                </div>
                            </div>
                            <!-- End Blog Item Body -->
                        </div>
                        <!-- End Blog Item -->
                @empty
                    <h2 class="text-center"><i class="fa fa-exclamation"></i> There is no Posts ...</h2>
                @endforelse
                <!-- Pagination -->
                {{ $posts->links() }}
                <!-- End Pagination -->
                </div>
                <!-- End Main Column -->
                <!-- Side Column -->
                <div class="col-md-3">
                    {{--Search Post--}}
                    <div class="blog-tags">
                        <h3>Search</h3>
                        <p>
                        <form action="" method="get" class="input-group">
                            <input type="search" name="search" class="form-control" placeholder="post title">
                            <span class="input-group-btn">
                                 <button type="submit" class="btn btn-primary"><span
                                             class="fa fa-search"></span></button>
                                 </span>
                        </form>
                        </p>
                    </div>
                {{--end search post--}}
                <!-- Blog Tags -->
                    <div class="blog-tags">
                        <h3>Tags</h3>
                        <ul class="blog-tags">
                            @if(count($tags))
                                @foreach($tags as $tag)
                                    <li>
                                        <a href="{{ route('posts.tag',$tag->id) }}"
                                           class="blog-tag">{{ $tag->name }}</a>
                                    </li>
                                @endforeach
                            @else
                                <h4>There are no tags assigned to posts</h4>
                            @endif
                        </ul>
                    </div>
                    <!-- End Blog Tags -->
                    <!-- Blog Category -->
                    <div class="blog-tags">
                        <h3>Categories</h3>
                        <ul class="blog-tags">
                            @if(count($categories))
                                @foreach($categories as $category)
                                    <li>
                                        <a href="{{ route('posts.category',$category->id) }}"
                                           class="blog-tag">{{ $category->name }}</a>
                                    </li>
                                @endforeach
                            @else
                                <h4>There are no categories assigned to posts</h4>
                            @endif
                        </ul>
                    </div>
                    <!-- End Blog Category -->
                    <!-- Recent Posts -->
                    <div class="recent-posts">
                        <h3>Recent Posts</h3>
                        <ul class="posts-list margin-top-10">
                            @foreach($recentPosts as $recentPost)
                                <li>
                                    <div class="recent-post">
                                        <a href="{{ $recentPost->path() }}">
                                            <img class="pull-left img-responsive" style="height: 50px;"
                                                 src="{{ count($recentPost->pictures)?$recentPost->pictures()->first()->path:asset('images/posts/default.png')}}"
                                                 alt="thumb1">
                                        </a>
                                        <a href="{{ $recentPost->path() }}"
                                           class="posts-list-title">{{  str_limit($recentPost->title , 40)  }}</a>
                                        <br>
                                        <span class="recent-post-date">
                                                {{ $recentPost->created_at->diffForHumans() }}
                                            </span>
                                    </div>
                                    <div class="clearfix"></div>
                                </li>
                            @endforeach
                        </ul>
                    </div>
                    <!-- End Recent Posts -->
                </div>
                <!-- End Side Column -->
            </div>
        </div>
    </div>
    <!-- === END CONTENT === -->
@endsection