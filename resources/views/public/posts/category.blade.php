@extends('public.layouts.app')
@section('content')
    <!-- === BEGIN CONTENT === -->
    <div id="content">
        <div class="container background-white">
            <div class="row margin-vert-30">
                <!-- Main Column -->
                <div class="col-md-9">
                    <h3>Posts for "{{ $category->name }}" category</h3>
                @foreach($posts as $post)
                    <!-- Blog Post -->
                        <div class="blog-post padding-bottom-20">
                            <!-- Blog Item Header -->
                            <div class="blog-item-header">
                                <!-- Title -->
                                <h2>
                                    <a href="{{ $post->path() }}">{{ $post->title }}</a>
                                </h2>
                                <div class="clearfix"></div>
                                <!-- End Title -->
                            </div>
                            <!-- End Blog Item Header -->
                            <!-- Blog Item Details -->
                            <div class="blog-post-details">
                                <!-- Author Name -->
                                <div class="blog-post-details-item blog-post-details-item-left">
                                    <i class="fa fa-user color-gray-light"></i>
                                    <a href="{{ $post->user->path() }}">{{ $post->user->name }}</a>
                                </div>
                                <!-- End Author Name -->
                                <!-- Date -->
                                <div class="blog-post-details-item blog-post-details-item-left">
                                    <i class="fa fa-calendar color-gray-light"></i>
                                    {{ $post->created_at->format('jS M, Y') }}
                                </div>
                                <!-- End Date -->
                            @if(count($post->tags))
                                <!-- Tags -->
                                    <div class="blog-post-details-item blog-post-details-item-left blog-post-details-tags">
                                        <i class="fa fa-tag color-gray-light"></i>
                                        @foreach($post->tags as $tag)
                                            <a href="{{ route('posts.tag',$tag->id) }}">{{ $tag->name }}</a>,
                                        @endforeach
                                    </div>
                                    <!-- End Tags -->
                            @endif
                            <!-- # of Comments -->
                                <div class="blog-post-details-item blog-post-details-item-left blog-post-details-item-last">
                                    <a href="{{ $post->path() }}">
                                        <i class="fa fa-comments color-gray-light"></i>
                                        {{ count($post->comments) }} Comments
                                    </a>
                                </div>
                                <!-- End # of Comments -->
                            </div>
                            <!-- End Blog Item Details -->
                            <!-- Blog Item Body -->
                            <div class="blog">
                                <div class="clearfix"></div>
                                <div class="blog-post-body row margin-top-15">
                                    <div class="col-md-5">
                                        <img class="margin-bottom-20" src="{{ count($post->pictures)?$post->pictures()->first()->path:asset('images/blog/image1.jpg') }}" alt="thumb1">
                                    </div>
                                    <div class="col-md-7">
                                        <p>
                                            {{ str_limit($post->body,350,'...') }}
                                        </p>
                                        <!-- Read More -->
                                        <a href="{{ $post->path() }}" class="btn btn-primary">
                                            Read More
                                            <i class="icon-chevron-right readmore-icon"></i>
                                        </a>
                                        <!-- End Read More -->
                                    </div>
                                </div>
                            </div>
                            <!-- End Blog Item Body -->
                        </div>
                        <!-- End Blog Item -->
                @endforeach
                <!-- Pagination -->
                {{ $posts->links() }}
                <!-- End Pagination -->
                </div>
                <!-- End Main Column -->
                <!-- Side Column -->
                <div class="col-md-3">
                    {{--Search Post--}}
                    <div class="blog-tags">
                        <h3>Search</h3>
                        <p>
                        <form action="" method="get" class="input-group">
                            <input type="search" name="search" class="form-control" placeholder="post title">
                            <span class="input-group-btn">
                                 <button type="submit" class="btn btn-primary"><span class="fa fa-search"></span></button>
                                 </span>
                        </form>
                        </p>
                    </div>
                {{--end search post--}}
                <!-- Recent Posts -->
                    <div class="recent-posts">
                        <h3>Recent Posts</h3>
                        <ul class="posts-list margin-top-10">
                            @foreach($recentPosts as $recentPost)
                                <li>
                                    <div class="recent-post">
                                        <a href="{{ $recentPost->path() }}">
                                            <img class="pull-left" width="64" src="{{ count($recentPost->pictures)?$recentPost->pictures()->first()->path:asset('images/blog/thumbs/thumb1.jpg')}}" alt="thumb1">
                                        </a>
                                        <a href="{{ $recentPost->path() }}" class="posts-list-title">{{ $recentPost->title }}</a>
                                        <br>
                                        <span class="recent-post-date">
                                                {{ $recentPost->created_at->diffForHumans() }}
                                            </span>
                                    </div>
                                    <div class="clearfix"></div>
                                </li>
                            @endforeach
                        </ul>
                    </div>
                    <!-- End Recent Posts -->
                </div>
                <!-- End Side Column -->
            </div>
        </div>
    </div>
    <!-- === END CONTENT === -->
@endsection