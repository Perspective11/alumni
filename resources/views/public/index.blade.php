@extends('public.layouts.app')
@section('content')
    <!-- === BEGIN CONTENT === -->
    <div id="content">
        @if($photosSlider)
            <div class="container no-padding background-white">
                <div class="row">
                    <!-- Carousel Slideshow -->
                    <div id="carousel-example" class="carousel slide" data-ride="carousel">
                        <!-- Carousel Indicators -->
                        <ol class="carousel-indicators">
                            @for($i=0;$i<count($photosSlider);$i++)
                                <li data-target="#carousel-example"
                                    data-slide-to="{{$i}}" {{ $i == 0?'class=active':'' }}></li>
                            @endfor
                        </ol>
                        <div class="clearfix"></div>
                        <!-- End Carousel Indicators -->
                        <!-- Carousel Images -->
                        <div class="carousel-inner">
                            @for($i=0;$i<count($photosSlider);$i++)
                                <div class="item {{$i == 0?'active':''}}">
                                    <img src="{{ asset($photosSlider[$i]) }}" style="width: 100%;">
                                </div>
                            @endfor
                        </div>
                        <!-- End Carousel Images -->
                        <!-- Carousel Controls -->
                        <a class="left carousel-control" href="#carousel-example" data-slide="prev">
                            <span class="fa fa-arrow-left"></span>
                        </a>
                        <a class="right carousel-control" href="#carousel-example" data-slide="next">
                            <span class="glyphicon glyphicon-chevron-left fa fa-arrow-right"></span>
                        </a>
                        <!-- End Carousel Controls -->
                    </div>
                    <!-- End Carousel Slideshow -->
                </div>
            </div>
        @endif
        <div class="container background-gray-lighter">
            <div class="row margin-vert-40 text-center">
                <div class="col-md-4">
                    <div class="col-md-4">
                        <i class="fa-graduation-cap fa-5x color-primary"></i>
                    </div>
                    <div class="col-md-8">
                        <h2 class="margin-top-5 margin-bottom-0">{{ count($graduates) }}</h2>
                        <p>Graduated Students</p>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="col-md-4">
                        <i class="fa-briefcase fa-5x color-primary"></i>
                    </div>
                    <div class="col-md-8">
                        <h2 class="margin-top-5 margin-bottom-0">{{ count($companies) }}</h2>
                        <p>Associated Companies</p>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="col-md-4">
                        <i class="fa-file-text fa-5x color-primary"></i>
                    </div>
                    <div class="col-md-8">
                        <h2 class="margin-top-5 margin-bottom-0">{{ $posts }}</h2>
                        <p>Number of Posts</p>
                    </div>
                </div>
            </div>
        </div>
        <div class="container background-white">
            <div class="row margin-vert-30">
                <!-- Main Text -->
                <div class="col-md-12">
                    <h2 class="text-center">What is this?</h2>
                    <p class="text-center">
                        Once an LIU student has completed their academic pursuit in this university, he/she had no means
                        of communicating, or sharing their work experiences. But not anymore. Now all LIU graduates can
                        gather in one place and be there for each other. They can share career advice, they can schedule
                        gatherings and events, they can look for jobs posted by companies within this site.
                    </p>
                </div>
                <!-- End Main Text -->
            </div>
        </div>
        <div class="container background-gray-lighter">
            <div class="row padding-vert-20">
                <h2 class="text-center">Trending Posts</h2>
                <div class="col-md-12">
                    <!-- Portfolio -->
                    <div class="portfolio-group row">
                        @foreach($trendingPosts as $trendingPost)
                            <div class="portfolio-item col-sm-6 col-xs-12">
                                <a href="{{ route('posts.show', ['id'=> $trendingPost->id]) }}">
                                    <figure class="animate fadeInLeft">
                                        <img style="width: 100%" alt="image1"
                                             src="{{ $trendingPost->pictures_count?$trendingPost->pictures[0]->path:asset('images/posts/default.png') }}">
                                        <figcaption>
                                            <h3>{{ $trendingPost->title }}</h3>
                                            <span>{{ str_limit($trendingPost->body, 150, '...') }}</span>
                                        </figcaption>
                                    </figure>
                                </a>
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
        <div class="container background-white">
            <div class="row padding-vert-40">
                <div class="col-md-12">
                    <h2 class="text-center">How can I participate?</h2>
                    <h4 class="text-center">Graduate</h4>
                    <p class="text-center">
                        If you are an LIU graduate, you can start now by filling your profile with your personal and
                        employment information. That profile can be seen by fellow alumni as well as hiring companies.
                        You can also share posts, schedule events and have fun.
                    </p>
                    <h4 class="text-center">Hiring Company</h4>
                    <p class="text-center">
                        If you are an interested hiring company, this is exactly the place where you want to be. Here
                        you will find talented fresh LIU graduates ready for employment. But first, you need to create
                        an account and fill your profile with your info, and that info has to be verified by LIU Admins.
                        Your company profile can be seen by Alumni members who are looking for a job.
                    </p>
                    <h4 class="text-center">Visitor</h4>
                    <p class="text-center">
                        If you are not an LIU graduate nor a hiring company, you don't need to create an account. You
                        have the ability to browse the posts, news, and events published by members of the site.
                    </p>
                </div>
            </div>
        </div>
    </div>
    <!-- === END CONTENT === -->
@endsection