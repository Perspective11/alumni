@extends('public.layouts.app')
@section('content')
    <div class="content">
        <div class="container background-white">
            <div class="row">
                <div class="col-sm-3">
                    <div class="row"
                         style="padding:10px;background: #FBB034;border-bottom-left-radius: 10px;border-bottom-right-radius: 10px;">
                        @if($user->isGraduate())
                            @if($user->graduate->facebook_link)
                                <div class="col-xs-4 text-center">
                                    <a href="{{ $user->graduate->facebook_link }}" target="_blank"><i
                                                class="fa-3x fa-facebook-official color-primary"></i></a>
                                </div>
                            @endif
                            @if($user->graduate->twitter_link)
                                <div class="col-xs-4 text-center">
                                    <a href="{{ $user->graduate->twitter_link }}" target="_blank"><i
                                                class="fa-3x fa-twitter color-primary"></i></a>
                                </div>
                            @endif
                            @if($user->graduate->instagram_link)
                                <div class="col-xs-4 text-center">
                                    <a href="{{ $user->graduate->instagram_link }}" target="_blank"><i
                                                class="fa-3x fa-instagram color-primary"></i></a>
                                </div>
                            @endif
                        @elseif($user->isCompany())
                            @if($user->company->facebook_link)
                                <div class="col-xs-4 text-center">
                                    <a href="{{ $user->company->facebook_link }}" target="_blank"><i
                                                class="fa-3x fa-facebook-official color-primary"></i></a>
                                </div>
                            @endif
                            @if($user->company->twitter_link)
                                <div class="col-xs-4 text-center">
                                    <a href="{{ $user->company->twitter_link }}" target="_blank"><i
                                                class="fa-3x fa-twitter color-primary"></i></a>
                                </div>
                            @endif
                            @if($user->company->linkedin_link)
                                <div class="col-xs-4 text-center">
                                    <a href="{{ $user->company->linkedin_link }}" target="_blank"><i
                                                class="fa-3x fa-linkedin color-primary"></i></a>
                                </div>
                            @endif
                        @endif
                    </div>
                    <div class="image margin-top-40">
                        @if($user->isGraduate())
                            <img class="img-circle" style="border: #002a5c 5px solid"
                                 src="{{ count($user->graduate->pictures)?$user->graduate->pictures()->first()->path:asset('images/graduates/profile/default.png') }}"
                                 alt="">
                        @else
                            <img class="img-circle" style="border: #002a5c 5px solid"
                                 src="{{ count($user->company->pictures)?$user->company->pictures()->first()->path:asset('images/companies/profile/default.jpg') }}"
                                 alt="">
                        @endif
                    </div>
                    <div class="info text-center text-uppercase margin-top-20 padding-bottom-60">
                        @if($user->isGraduate())
                            <h2>{{ $user->graduate->full_name }}</h2>
                            <p style="border-top: 2px #002a5c dotted;max-width: 50%;margin: 0 auto">
                                {{ $user->graduate->field_of_specialty }}
                            </p>
                        @elseif($user->isCompany())
                            <h2>{{ $user->company->company_name }}</h2>
                            <p style="border-top: 2px #002a5c dotted;max-width: 50%;margin: 0 auto">
                                {{ $user->company->industry }}
                            </p>
                        @endif
                    </div>
                    <div class="options text-center"
                         style="position:relative;bottom: 0;left: 50%;transform: translateX(-50%);border-bottom: 3px solid #002a5c;border-top: 2px #002a5c dotted;padding-top: 5px">
                        @if($user->isGraduate())
                            <h3>{{ $user->graduate->file_path?'C.V &': ''}} Email</h3>
                        @endif
                        <br>
                        <div class="btn-group">
                            @if($user->isGraduate())
                                @if($user->graduate->file_path)
                                    <a href="{{ route('profile.view.cv', [$user->id, str_replace(' ', '-',$user->graduate->full_name)]) }}"
                                       class="btn btn-primary" title="View CV"><span
                                                class="fa fa-eye color-white"></span></a>
                                    <a href="{{ route('profile.download.cv', [$user->id, $user->graduate->id]) }}"
                                       class="btn btn-primary" action="download" title="Downlaod CV"><span
                                                class="fa fa-download color-white"></span></a>
                                @endif
                            @endif
                            <a href="mailTo:{{ $user->email }}" class="btn btn-primary" title="Send mail"><span
                                        class="fa fa-at color-white"></span></a>
                        </div>
                    </div>
                </div>
                <div class="col-sm-9">
                    <h2 class="text-right margin-top-10">
                        @if(auth()->id() == $user->id)
                            @if(auth()->user()->isGraduate())
                                {!! auth()->user()->graduate->fullProfile() ? '' : '<small class="text-danger">Fill the profile data and upload the CV </small> ' !!}
                            @endif
                            <a href="{{ route('profile.edit',auth()->id()) }}"><span class="fa fa-edit
                                @if(auth()->user()->isGraduate())
                                {{ auth()->user()->graduate->fullProfile() ? '' : 'text-danger' }}
                                @endif
                                        "></span></a>

                        @endif
                        Profile
                    </h2>
                    @if($user->isGraduate())
                        <div style="border-top: 1px solid #FBB034" class="padding-top-5">
                            <div class="panel">
                                <div class="panel-heading">
                                    <h3>LIU Information</h3>
                                </div>
                                <div class="panel-body">
                                    <table style="width: 100%;">
                                        @if (!empty( $user->graduate->liu_id))
                                            <tr>
                                                <th>Student ID</th>
                                                <td>{{ $user->graduate->liu_id }}</td>
                                            </tr>
                                        @endif
                                        @if (!empty( $user->graduate->major->name ))
                                            <tr>
                                                <th>Major</th>
                                                <td>
                                                    <a href="{{ route('graduates.major',$user->graduate->major->id) }}">{{ $user->graduate->major->name }}</a>
                                                </td>
                                            </tr>
                                        @endif
                                        @if (!empty( $user->graduate->graduation_year ))
                                            <tr>
                                                <th>Graduation Year</th>
                                                <td>{{ $user->graduate->graduation_year }}</td>
                                            </tr>
                                        @endif
                                    </table>
                                </div>
                            </div>
                        </div>
                        <div style="border-top: 1px solid #FBB034" class="padding-top-5">
                            <div class="panel">
                                <div class="panel-heading">
                                    <h3>Personal Information</h3>
                                </div>
                                <div class="panel-body">
                                    <table style="width: 100%;">
                                        @if (!empty($user->graduate->gender))
                                            <tr>
                                                <th>Gender</th>
                                                <td>{{ $user->graduate->gender? 'Male' : 'Female' }}</td>
                                            </tr>
                                        @endif
                                        @if (!empty($user->graduate->dob))
                                            <tr>
                                                <th>Age</th>
                                                <td>{{ $user->graduate->dob->age  }}</td>
                                            </tr>
                                        @endif
                                        @if (!empty( $user->graduate->phone ))
                                            <tr>
                                                <th>Mobile No</th>
                                                <td>{{ $user->graduate->phone }}</td>
                                            </tr>
                                        @endif
                                        @if (!empty($user->graduate->perm_address))
                                            <tr>
                                                <th>Permanent Address</th>
                                                <td>{{ $user->graduate->perm_address }}</td>
                                            </tr>
                                        @endif
                                        @if (!empty($user->graduate->curr_address))
                                            <tr>
                                                <th>Current Address</th>
                                                <td>
                                                    {{ $user->graduate->curr_address  }}
                                                </td>
                                            </tr>
                                        @endif
                                        @if (!empty($user->graduate->origin))
                                            <tr>
                                                <th>Place of Origin</th>
                                                <td>{{ $user->graduate->origin }}</td>
                                            </tr>
                                        @endif
                                        @if (!empty($user->graduate->website_link))
                                            <tr>
                                                <th>Personal Website</th>
                                                <td><a href="{{ $user->graduate->website_link }}"
                                                       target="_blank">{{ $user->graduate->website_link }}</a></td>
                                            </tr>
                                        @endif
                                        <tr>
                                            <th>Social Media Links</th>
                                            @if ($user->graduate->facebook_link || $user->graduate->twitter_link || $user->graduate->instagram_link)
                                                <td>
                                                    @if (!empty($user->graduate->facebook_link))
                                                        <a href="{{ $user->graduate->facebook_link }}"
                                                           target="_blank"><i style="color: #3b5998;"
                                                                              class="fa fa-2x fa-facebook icon"></i></a>
                                                    @endif
                                                    @if (!empty($user->graduate->twitter_link))
                                                        <a href="{{ $user->graduate->twitter_link }}" target="_blank"><i
                                                                    style="color: #1da1f2;"
                                                                    class="fa fa-2x fa-twitter icon"></i></a>
                                                    @endif
                                                    @if (!empty($user->graduate->instagram_link))
                                                        <a href="{{ $user->graduate->instagram_link }}" target="_blank"><i
                                                                    style="color: #fcaf45;"
                                                                    class="fa fa-2x fa-instagram icon"></i></a>
                                                    @endif
                                                </td>
                                            @endif
                                        </tr>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <div style="border-top: 1px solid #FBB034" class="padding-top-5">
                            <div class="panel">
                                <div class="panel-heading">
                                    <h3>Academic Information</h3>
                                </div>
                                <div class="panel-body">
                                    <table style="width: 100%;">
                                        @if (!empty($user->graduate->is_studying))
                                            <tr>
                                                <th>Currently Studying</th>
                                                <td>{{ $user->graduate->is_studying? 'Yes' : 'No' }}</td>
                                            </tr>
                                        @endif
                                        @if (!empty($user->graduate->field_of_specialty))
                                            <tr>
                                                <th>Field of Specialty</th>
                                                <td>{{ $user->graduate->field_of_specialty }}</td>
                                            </tr>
                                        @endif
                                        @if (!empty($user->graduate->edu_level))
                                            <tr>
                                                <th>Level of Education</th>
                                                <td>{{ $user->graduate->edu_level }}</td>
                                            </tr>
                                        @endif
                                        @if (!empty($user->graduate->gpa))
                                            <tr>
                                                <th>LIU GPA</th>
                                                <td>
                                                    <span class="label label-success">{{ $user->graduate->gpa }}</span>
                                                </td>
                                            </tr>
                                        @endif
                                        @if (!empty($user->graduate->english_pro))
                                            <tr>
                                                <th>English Proficiency</th>
                                                <td>
                                                    {{ $user->graduate->english_pro }}
                                                </td>
                                            </tr>
                                        @endif
                                        @if (!empty($user->graduate->languages))
                                            <tr>
                                                <th>Languages</th>
                                                <td>{{ $user->graduate->languages }}</td>
                                            </tr>
                                        @endif
                                    </table>
                                </div>
                            </div>
                        </div>
                        <div style="border-top: 1px solid #FBB034" class="padding-top-5">
                            <div class="panel">
                                <div class="panel-heading">
                                    <h3>Employment Information</h3>
                                </div>
                                <div class="panel-body">
                                    <table style="width: 100%;">
                                        @if (!empty($user->graduate->is_employed))
                                            <tr>
                                                <th>Currently Employed</th>
                                                <td>{{ $user->graduate->is_employed? 'Yes' : 'No' }}</td>
                                            </tr>
                                        @endif
                                        @if (!empty($user->graduate->job_title))
                                            <tr>
                                                <th>Current Job Title</th>
                                                <td>{{ $user->graduate->job_title }}</td>
                                            </tr>
                                        @endif
                                        @if (!empty($user->graduate->emp_place))
                                            <tr>
                                                <th>Place of Employment</th>
                                                <td>{{ $user->graduate->emp_place }}</td>
                                            </tr>
                                        @endif
                                        @if (!empty($user->graduate->field_of_work))

                                            <tr>
                                                <th>Field of Work</th>
                                                <td>{{ $user->graduate->field_of_work }}</td>
                                            </tr>
                                        @endif
                                        @if (!empty($user->graduate->years_of_experience))
                                            <tr>
                                                <th>Work Experience</th>
                                                <td>{{ $user->graduate->years_of_experience }}</td>
                                            </tr>
                                        @endif
                                        @if (!empty($user->graduate->github_link) || !empty($user->graduate->linkedin_link) || !empty($user->graduate->dribble_link) )
                                            <tr>
                                                <th>Links</th>
                                                <td>
                                                    @if (!empty($user->graduate->github_link))
                                                        <a href="{{ $user->graduate->github_link }}"><i
                                                                    style="color: #333;"
                                                                    class="fa fa-2x fa-github icon"></i></a>
                                                    @endif
                                                    @if (!empty($user->graduate->linkedin_link))
                                                        <a href="{{ $user->graduate->linkedin_link }}"><i
                                                                    style="color: #0077b5;"
                                                                    class="fa fa-2x fa-linkedin icon"></i></a>
                                                    @endif
                                                    @if (!empty($user->graduate->dribble_link))
                                                        <a href="{{ $user->graduate->dribble_link }}"><i
                                                                    style="color: #ea4c89;"
                                                                    class="fa fa-2x fa-dribbble icon"></i></a>
                                                    @endif
                                                </td>
                                            </tr>
                                        @endif
                                        @if (!empty($user->graduate->is_hireable))
                                            <tr>
                                                <th>Available for Hire</th>
                                                <td>
                                                    <span class="">{{ $user->graduate->is_hireable? 'Yes' : 'No' }}</span>
                                                </td>
                                            </tr>
                                        @endif
                                    </table>
                                </div>
                            </div>
                        </div>
                        @if(!empty($user->graduate->description) && !empty($user->graduate->interests) && !empty($user->graduate->achievements) && !empty($user->graduate->volunteer) && !empty($user->graduate->goal))
                            <div style="border-top: 1px solid #FBB034" class="padding-top-5">
                                <div class="panel">
                                    <div class="panel-heading">
                                        <h3>More Information</h3>
                                    </div>
                                    <div class="panel-body">
                                        <ul class="list-group">
                                            @if (!empty($user->graduate->description))
                                                <li class="list-group-item">
                                                    <h5>Short Description</h5>
                                                    <p>{{ $user->graduate->description }}</p>
                                                </li>
                                            @endif
                                            @if (!empty($user->graduate->interests))

                                                <li class="list-group-item">
                                                    <h5>Hobbies and Interests</h5>
                                                    <p>{{ $user->graduate->interests }}</p>
                                                </li>
                                            @endif
                                            @if (!empty($user->graduate->achievements))

                                                <li class="list-group-item">
                                                    <h5>Achievements</h5>
                                                    <p>{{ $user->graduate->achievements }}</p>
                                                </li>
                                            @endif
                                            @if (!empty($user->graduate->volunteer))
                                                <li class="list-group-item">
                                                    <h5>Volunteer Work</h5>
                                                    <p>{{ $user->graduate->volunteer }}</p>
                                                </li>
                                            @endif
                                            @if (!empty($user->graduate->goal))

                                                <li class="list-group-item">
                                                    <h5>Work Goal</h5>
                                                    <p>{{ $user->graduate->goal }}</p>
                                                </li>
                                            @endif
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        @endif
                    @elseif($user->isCompany())
                        <div style="border-top: 1px solid #FBB034" class="padding-top-5">
                            <div class="panel">
                                <div class="panel-heading">
                                    <h3>About us</h3>
                                </div>
                                <div class="panel-body">
                                    <p>{{ $user->company->full_description }}</p>
                                    @if (!empty($user->company->headquarters))
                                        <div class="col-md-4">
                                            <h5>Headquarters</h5>
                                            <p>{{ $user->company->headquarters }}</p>
                                        </div>
                                    @endif

                                    @if (!empty($user->company->industry))
                                        <div class="col-md-4">
                                            <h5>Industry</h5>
                                            <p>{{ $user->company->industry }}</p>
                                        </div>
                                    @endif

                                    @if (!empty($user->company->size))
                                        <div class="col-md-4">
                                            <h5>Company Size</h5>
                                            <p>{{ $user->company->size }} employees</p>
                                        </div>
                                    @endif

                                    @if (!empty($user->company->founded_at))
                                        <div class="col-md-4">
                                            <h5>Founded At</h5>
                                            <p>{{ $user->company->founded_at }}</p>
                                        </div>
                                    @endif


                                    @if (!empty($user->company->type))
                                        <div class="col-md-4">
                                            <h5>Type</h5>
                                            <p>{{ $user->company->type }}</p>
                                        </div>
                                    @endif
                                    @if (!empty($user->company->website_link))
                                        <div class="col-md-4">
                                            <h5>Webiste</h5>
                                            <p>
                                                <a href="{{ $user->company->website_link }}">{{ $user->company->website_link }}</a>
                                            </p>
                                        </div>
                                    @endif
                                    @if ($user->company->facebook_link || $user->company->twitter_link || $user->company->linkedin_link)
                                        <div class="col-md-4">
                                            <h5>Links</h5>
                                            <p>
                                                @if (!empty($user->company->facebook_link))
                                                    <a href="{{ $user->company->facebook_link }}"><i
                                                                style="color: #3b5998;"
                                                                class="fa fa-2x fa-facebook icon"></i></a>
                                                @endif
                                                @if (!empty($user->company->twitter_link))
                                                    <a href="{{ $user->company->twitter_link }}"><i
                                                                style="color: #1da1f2;"
                                                                class="fa fa-2x fa-twitter icon"></i></a>
                                                @endif
                                                @if (!empty($user->company->linkedin_link))
                                                    <a href="{{ $user->company->linkedin_link }}"><i
                                                                style="color: #0077b5;"
                                                                class="fa fa-2x fa-linkedin icon"></i></a>
                                            </p>
                                            @endif
                                        </div>
                                    @endif
                                </div>
                            </div>
                        </div>
                        <div style="border-top: 1px solid #FBB034" class="padding-top-5">
                            <div class="panel">
                                <div class="panel-heading">
                                    <h3>Addresses</h3>
                                </div>
                                <div class="panel-body">
                                    @if (!empty($user->company->address_head))
                                        <div>
                                            <h5>Main Branch</h5>
                                            <p>{{ $user->company->address_head }}</p>
                                        </div>
                                    @endif
                                    @if (!empty($user->company->address_1))
                                        <div>
                                            <h5>Branch #1</h5>
                                            <p>{{ $user->company->address_1 }}</p>
                                        </div>
                                    @endif
                                    @if (!empty($user->company->address_2))
                                        <div>
                                            <h5>Branch #2</h5>
                                            <p>{{ $user->company->address_2 }}</p>
                                        </div>
                                    @endif
                                    @if (!empty($user->company->address_3))
                                        <div>
                                            <h5>Branch #3</h5>
                                            <p>{{ $user->company->address_3 }}</p>
                                        </div>
                                    @endif
                                    @if (!empty($user->company->address_4))
                                        <div>
                                            <h5>Branch #4</h5>
                                            <p>{{ $user->company->address_4 }}</p>
                                        </div>
                                    @endif
                                </div>
                            </div>
                        </div>
                    @endif
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script>
        $('a[action="download"]').on('click', function (eve) {
            eve.preventDefault();
            $.get("{{ $user->isGraduate()?route('profile.download.cv', [$user->id, $user->graduate->id]):'' }}")
        })
    </script>
@endsection