@extends('public.layouts.app')

@section('links')
    <link rel="stylesheet"
          href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-daterangepicker/2.1.25/daterangepicker.css"/>
@endsection

@section('styles')
    <style>
        li {
            list-style: none;
        }
    </style>
@endsection

@section('content')
    <div class="content">
        <div class="container background-white">
            <div class="row">
                <form action="
                        @if($user->isGraduate())
                {{ route('profile.GUpdate',auth()->id()) }}
                @elseif($user->isCompany())
                {{ route('profile.CUpdate',auth()->id()) }}
                @endif
                        " method="post" enctype="multipart/form-data" id="form">
                    {{csrf_field()}}
                    {{method_field('PUT')}}
                    <div class="col-sm-3">
                        <div class="row"
                             style="height: 50px;padding:10px;background: #FBB034;border-bottom-left-radius: 10px;border-bottom-right-radius: 10px;"></div>
                        <div class="image margin-top-40">
                            @if($user->isGraduate())
                                <img class="img-circle" style="border: #002a5c 5px solid"
                                     src="{{ count($user->graduate->pictures)?$user->graduate->pictures()->first()->path:asset('images/graduates/profile/default.png') }}"
                                     alt="">
                                <div class="text-center margin-vert-10">
                                    <input type="file" accept="image/*" name="picture" style="display: none" id="image"><label
                                            style="color: #fff;background: {{ $errors->has('picture')?'red':'#FBB034' }};opacity: 0.9;border-radius: 5px;padding: 5px 10px;"
                                            for="image"><span class="fa fa-pencil color-white"></span>Change</label>
                                    @if($errors->has('picture'))
                                        <span class="help-block">
                                            <strong class="color-red">{{$errors->first('picture')}}</strong>
                                        </span>
                                    @endif
                                </div>
                            @else
                                <img class="img-circle" style="border: #002a5c 5px solid"
                                     src="{{ count($user->company->pictures)?$user->company->pictures()->first()->path:asset('images/companies/profile/default.jpg') }}"
                                     alt="">
                                <div class="text-center margin-vert-10">
                                    <input type="file" accept="image/*" name="picture" style="display: none" id="image"><label
                                            style="color: #fff;background: {{ $errors->has('picture')?'red':'#FBB034' }};opacity: 0.9;border-radius: 5px;padding: 5px 10px;"
                                            for="image"><span class="fa fa-pencil color-white"></span>Change</label>
                                    @if($errors->has('picture'))
                                        <span class="help-block">
                                            <strong class="color-red">{{$errors->first('picture')}}</strong>
                                        </span>
                                    @endif
                                </div>
                            @endif
                        </div>
                        <div class="info text-center text-uppercase margin-top-20 padding-bottom-60">
                            @if($user->isGraduate())
                                <div class="form-group{{ $errors->has('name')?' has-error':'' }}">
                                    <label>Username:</label>
                                    <input type="text" class="form-control text-center" name="name"
                                           value="{{ old('name')?:$user->name }}" autofocus>
                                    @if($errors->has('name'))
                                        <span class="help-block">
                                            <strong>{{$errors->first('name')}}</strong>
                                        </span>
                                    @endif
                                </div>
                                <div class="form-group{{  $errors->has('full_name')?' has-error':'' }}">
                                    <label>Full name:</label>
                                    <input type="text" class="form-control text-center" name="full_name"
                                           value="{{ old('full_name')?:$user->graduate->full_name }}">
                                    @if($errors->has('full_name'))
                                        <span class="help-block">
                                            <strong>{{$errors->first('full_name')}}</strong>
                                        </span>
                                    @endif
                                </div>
                                <p class="form-group{{ $errors->has('field_of_specialty')?' has-error':'' }}"
                                   style="border-top: 2px #002a5c dotted;min-width: 50%;margin: 0 auto">
                                    <label>Field of specialty:</label>
                                    <input type="text" class="form-control text-center" name="field_of_specialty"
                                           value="{{ old('field_of_specialty')?:$user->graduate->field_of_specialty }}">
                                    @if($errors->has('field_of_specialty'))
                                        <span class="help-block">
                                            <strong>{{$errors->first('field_of_specialty')}}</strong>
                                        </span>
                                    @endif
                                </p>
                            @elseif($user->isCompany())
                                <div class="form-group{{ $errors->has('name')?' has-error':'' }}">
                                    <label>Username:</label>
                                    <input type="text" class="form-control text-center" name="name"
                                           value="{{ old('name')?:$user->name }}" autofocus>
                                    @if($errors->has('name'))
                                        <span class="help-block">
                                            <strong>{{$errors->first('name')}}</strong>
                                        </span>
                                    @endif
                                </div>
                                <div class="form-group{{ $errors->has('company_name')?' has-error':'' }}">
                                    <label>Company name:</label>
                                    <input type="text" class="form-control text-center" name="company_name"
                                           value="{{ old('company_name')?:$user->company->company_name }}" autofocus>
                                    @if($errors->has('company_name'))
                                        <span class="help-block">
                                            <strong>{{$errors->first('company_name')}}</strong>
                                        </span>
                                    @endif
                                </div>
                                <div class="form-group{{ $errors->has('industry')?' has-error':'' }}"
                                     style="border-top: 2px #002a5c dotted;min-width: 50%;margin: 0 auto">
                                    <label>Industry:</label>
                                    <input type="text" class="form-control text-center" name="industry"
                                           value="{{ old('industry')?:$user->company->industry }}" autofocus>
                                    @if($errors->has('industry'))
                                        <span class="help-block">
                                            <strong>{{$errors->first('industry')}}</strong>
                                        </span>
                                    @endif
                                </div>
                            @endif
                        </div>
                        @if($user->isGraduate())
                            <div class="options text-center"
                                 style="position:relative;bottom: 0;left: 50%;transform: translateX(-50%);border-bottom: 3px solid #002a5c">
                                <p id="pdfFileName"></p>
                                <div class="btn-group">
                                    <label for="resume" class="btn btn-primary" style="border-radius: 0"
                                           title="Upload your CV">Upload CV</label>
                                    <input type="file" name="resume" id="resume" accept="application/pdf"
                                           style="display: none">
                                </div>
                            </div>
                            @if($errors->has('resume'))
                                <span class="help-block">
                                <strong class="color-red">{{$errors->first('resume')}}</strong>
                            </span>
                            @endif
                        @endif
                    </div>
                    <div class="col-sm-9">
                        <h2 class="text-right margin-top-10">
                            <button class="btn btn-link color-white"
                                    style="width: 100px;height: 70px;position: fixed;z-index: 10;top: 265px;right: 0;"
                                    title="save"
                                    type="submit" onclick="$('#form').submit()">Save <span class="fa fa-save"></span>
                            </button>
                            <a href="{{ route('profile',auth()->id()) }}" style="text-decoration: underline">Profile</a>
                        </h2>
                        @if($user->isGraduate())
                            <div style="border-top: 1px solid #FBB034" class="padding-top-5">
                                <div class="panel">
                                    <div class="panel-heading">
                                        <h3>LIU Information</h3>
                                    </div>
                                    <div class="panel-body">
                                        <table style="width: 100%;">
                                            <tr>
                                                <th>Student ID</th>
                                                <td>{{ $user->graduate->liu_id }}</td>
                                            </tr>
                                            <tr>
                                                <th>Major</th>
                                                <td>
                                                    <a href="{{ $user->graduate->major->path() }}">{{ $user->graduate->major->name }}</a>
                                                </td>
                                            </tr>
                                            <tr>
                                                <th>Graduation Year</th>
                                                <td>{{ $user->graduate->graduation_year }}</td>
                                            </tr>
                                        </table>
                                    </div>
                                </div>
                            </div>
                            <div style="border-top: 1px solid #FBB034" class="padding-top-5">
                                <div class="panel">
                                    <div class="panel-heading">
                                        <h3>Personal Information</h3>
                                    </div>
                                    <div class="panel-body">
                                        <table style="width: 100%;">
                                            <tr>
                                                <th>Gender</th>
                                                <td>
                                                    <div class="form-group {{ $errors->has('gender') ? ' has-error' : '' }}">
                                                        <input type="radio" name="gender"
                                                               value="1" {{ old('gender') === 1 ? 'checked':'' }} {{ $user->graduate->gender === 1 ? 'checked' : '' }}>
                                                        Male &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                        <input type="radio" name="gender"
                                                               value="0" {{ old('gender') === 0 ? 'checked':'' }} {{ $user->graduate->gender === 0? 'checked' : '' }}>
                                                        Female
                                                        @if($errors->has('gender'))
                                                            <span class="help-block">
                                                                <strong>{{ $errors->first('gender') }}</strong>
                                                            </span>
                                                        @endif
                                                    </div>
                                                </td>
                                            </tr>
                                            <tr>
                                                <th>Date of Birth</th>
                                                <td>
                                                    <div class="form-group {{ $errors->has('dob') ? ' has-error' : '' }}">
                                                        <input type="text" name="dob" id="dob" class="form-control"
                                                               placeholder="31/12/1991">
                                                        @if($errors->has('dob'))
                                                            <span class="help-block">
                                                                  <strong>{{ $errors->first('dob') }}</strong>
                                                              </span>
                                                        @endif
                                                    </div>
                                                </td>
                                            </tr>
                                            <tr>
                                                <th>Permanent Address</th>
                                                <td class="form-group {{ $errors->has('perm_address') ? ' has-error' : '' }}">
                                                    <textarea class="form-control" name="perm_address" class=""
                                                              id="perm_address">{{ old('perm_address')?:$user->graduate->perm_address }}</textarea>
                                                    @if($errors->has('prem_address'))
                                                        <span class="help-block">
                                                                <strong>{{ $errors->first('prem_address') }}</strong>
                                                            </span>
                                                    @endif
                                                </td>
                                            </tr>
                                            <tr>
                                                <th>Current Address</th>
                                                <td class="form-group {{ $errors->has('curr_address') ? ' has-error' : '' }}">
                                                    <textarea class="form-control" name="curr_address"
                                                              id="curr_address">{{ old('curr_address')?:$user->graduate->curr_address }}</textarea>
                                                    @if($errors->has('curr_address'))
                                                        <span class="help-block">
                                                                <strong>{{ $errors->first('curr_address') }}</strong>
                                                            </span>
                                                    @endif
                                                </td>
                                            </tr>
                                            <tr>
                                                <th>Place of Origin</th>
                                                <td class="form-group{{ $errors->has('origin') ? ' has-error' : '' }}">
                                                    <textarea class="form-control " name="origin"
                                                              id="origin">{{ old('origin')?:$user->graduate->origin }}</textarea>
                                                    @if($errors->has('origin'))
                                                        <span class="help-block">
                                                                <strong>{{ $errors->first('origin') }}</strong>
                                                            </span>
                                                    @endif
                                                </td>
                                            </tr>
                                            <tr>
                                                <th>Personal Website</th>
                                                <td class="{{ $errors->has('website_link') ? ' has-error' : '' }}">
                                                    <input class="form-control" type="url" name="website_link"
                                                           id="website_link"
                                                           value="{{ old('website_link')?:$user->graduate->website_link }}">
                                                    @if($errors->has('website_link'))
                                                        <span class="help-block">
                                                            {{ $errors->first('website_link') }}
                                                        </span>
                                                    @endif
                                                </td>
                                            </tr>
                                            <tr>
                                                <th>Social Media Links</th>
                                                <td>
                                                    <ul>
                                                        <li>
                                                            <div class="input-group link-input {{ $errors->has('facebook_link') ? ' has-error' : '' }}">
                                                            <span class="input-group-addon"><i style="color: #3b5998;"
                                                                                               class="fa fa-facebook icon"></i></span>
                                                                <input type="url" class="form-control "
                                                                       placeholder="Facebook URL"
                                                                       name="facebook_link"
                                                                       value="{{ old('facebook_link')?:$user->graduate->facebook_link }}">
                                                                @if($errors->has('facebook_link'))
                                                                    <span class="help-block">
                                                                        {{ $errors->first('facebook_link') }}
                                                                    </span>
                                                                @endif
                                                            </div>
                                                        </li>
                                                        <li>
                                                            <div class="input-group link-input {{ $errors->has('twitter_link') ? ' has-error' : '' }}">
                                                            <span class="input-group-addon"><i style="color: #1da1f2;"
                                                                                               class="fa fa-twitter icon"></i></span>
                                                                <input type="url" class="form-control "
                                                                       placeholder="Twitter URL"
                                                                       name="twitter_link"
                                                                       value="{{ old('twitter_link')?:$user->graduate->twitter_link }}">
                                                                @if($errors->has('twitter_link'))
                                                                    <span class="help-block">
                                                                        {{ $errors->first('twitter_link') }}
                                                                    </span>
                                                                @endif
                                                            </div>
                                                        </li>
                                                        <li>
                                                            <div class="input-group link-input {{ $errors->has('instagram_link') ? ' has-error' : '' }} ">
                                                            <span class="input-group-addon"><i style="color: #fcaf45;"
                                                                                               class="fa fa-instagram icon"></i></span>
                                                                <input type="url" class="form-control "
                                                                       placeholder="Instagram URL"
                                                                       name="instagram_link"
                                                                       value="{{ old('instagram')?:$user->graduate->instagram_link }}">
                                                                @if($errors->has('instagram_link'))
                                                                    <span class="help-block">
                                                                        {{ $errors->first('instagram_link') }}
                                                                    </span>
                                                                @endif
                                                            </div>
                                                        </li>
                                                    </ul>
                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                </div>
                            </div>
                            <div style="border-top: 1px solid #FBB034" class="padding-top-5">
                                <div class="panel">
                                    <div class="panel-heading">
                                        <h3>Academic Information</h3>
                                    </div>
                                    <div class="panel-body">
                                        <table style="width: 100%;">
                                            <tr>
                                                <th>Currently Studying</th>
                                                <td>
                                                    <div class="form-group {{ $errors->has('is_studying') ? ' has-error' : '' }}">
                                                        <input type="radio" name="is_studying"
                                                               value="1" {{ old('is_studying') === 1? 'checked' : '' }} {{ $user->graduate->is_studying === 1? 'checked' : '' }}>
                                                        Yes &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                        <input type="radio" name="is_studying"
                                                               value="0" {{ old('is_studying') === 0? 'checked' : '' }} {{ $user->graduate->is_studying === 0? 'checked' : '' }}>
                                                        No
                                                        @if($errors->has('is_studying'))
                                                            <span class="help-block">
                                                                        {{ $errors->first('is_studying') }}
                                                                    </span>
                                                        @endif
                                                    </div>
                                                </td>
                                            </tr>
                                            <tr>
                                                <th>Level of Education</th>
                                                <td class="{{ $errors->has('edu_level') ? ' has-error' : '' }}">
                                                    <input type="text" name="edu_level" id="edu_level"
                                                           class="form-control"
                                                           value="{{ old('edu_level')?:$user->graduate->edu_level }}">
                                                    @if($errors->has('edu_level'))
                                                        <span class="help-block">
                                                            {{ $errors->first('edu_level') }}
                                                        </span>
                                                    @endif
                                                </td>
                                            </tr>
                                            <tr>
                                                <th>LIU GPA</th>
                                                <td class="{{ $errors->has('gpa') ? ' has-error' : '' }}">
                                                    <input type="text" name="gpa" id="gpa" class="form-control"
                                                           value="{{ old('gpa')?:$user->graduate->gpa }}">
                                                    @if($errors->has('gpa'))
                                                        <span class="help-block">
                                                            {{ $errors->first('gpa') }}
                                                        </span>
                                                    @endif
                                                </td>
                                            </tr>

                                            <tr>
                                                <th>English Proficiency</th>
                                                <td class="{{ $errors->has('english_pro') ? ' has-error' : '' }}">
                                                    <select type="text" name="english_pro" id="english_pro"
                                                            class="form-control">
                                                        <option value="" selected disabled>Select from the options
                                                        </option>
                                                        <option value="mother langauge" {{ $user->graduate->english_pro == 'mother language' ?'selected':'' }}>
                                                            Mother Langauge
                                                        </option>
                                                        <option value="excellent" {{ $user->graduate->english_pro == 'excellent' ?'selected':'' }}>
                                                            Excellent
                                                        </option>
                                                        <option value="good" {{ $user->graduate->english_pro == 'good' ?'selected':'' }}>
                                                            Good
                                                        </option>
                                                        <option value="weak" {{ $user->graduate->english_pro == 'weak' ?'selected':'' }}>
                                                            Weak
                                                        </option>
                                                    </select>
                                                    @if($errors->has('english_pro'))
                                                        <span class="help-block">
                                                            {{ $errors->first('english_pro') }}
                                                        </span>
                                                    @endif
                                                </td>
                                            </tr>
                                            <tr>
                                                <th>Languages</th>
                                                <td class="{{ $errors->has('languages') ? ' has-error' : '' }}">
                                                    <input type="text" name="languages" id="languages"
                                                           class="form-control"
                                                           value="{{ old('languages')?:$user->graduate->languages }}">
                                                    @if($errors->has('languages'))
                                                        <span class="help-block">
                                                            {{ $errors->first('languages') }}
                                                        </span>
                                                    @endif
                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                </div>
                            </div>
                            <div style="border-top: 1px solid #FBB034" class="padding-top-5">
                                <div class="panel">
                                    <div class="panel-heading">
                                        <h3>Employment Information</h3>
                                    </div>
                                    <div class="panel-body">
                                        <table style="width: 100%;">
                                            <tr>
                                                <th>Currently Employed</th>
                                                <td class="{{ $errors->has('is_employed') ? ' has-error' : '' }}">
                                                    <div class="form-group ">
                                                        <input type="radio" name="is_employed"
                                                               value="1" {{ old('is_employed') === 1? 'checked' : '' }} {{ $user->graduate->is_employed === 1? 'checked' : '' }}>
                                                        Yes &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                        <input type="radio" name="is_employed"
                                                               value="0" {{ old('is_employed') === 0? 'checked' : '' }} {{ $user->graduate->is_employed === 0? 'checked' : '' }}>
                                                        No
                                                    </div>
                                                    @if($errors->has('is_employed'))
                                                        <span class="help-block">
                                                                {{ $errors->first('is_employed') }}
                                                            </span>
                                                    @endif
                                                </td>
                                            </tr>
                                            <tr class="currentlyWorking">
                                                <th>Current Job Title</th>
                                                <td class="{{ $errors->has('job_title') ? ' has-error' : '' }}">
                                                    <input type="text" name="job_title" id="job_title"
                                                           class="form-control"
                                                           value="{{ old('job_title')?:$user->graduate->job_title }}">
                                                    @if($errors->has('job_title'))
                                                        <span class="help-block">
                                                                                                        {{ $errors->first('job_title') }}
                                                                                                    </span>
                                                    @endif
                                                </td>
                                            </tr>
                                            <tr class="currentlyWorking">
                                                <th>Place of Employment</th>
                                                <td class="{{ $errors->has('emp_place') ? ' has-error' : '' }}">
                                                    <input type="text" name="emp_place" id="emp_place"
                                                           class="form-control"
                                                           value="{{ old('emp_place')?:$user->graduate->emp_place }}">
                                                    @if($errors->has('emo_place'))
                                                        <span class="help-block">
                                                                                                        {{ $errors->first('emo_place') }}
                                                                                                    </span>
                                                    @endif
                                                </td>
                                            </tr>

                                            <tr class="currentlyWorking">
                                                <th>Field of Work</th>
                                                <td class="{{ $errors->has('field_of_work') ? ' has-error' : '' }}">
                                                    <input type="text" name="field_of_work" id="field_of_work"
                                                           class="form-control"
                                                           value="{{ old('field_of_work')?:$user->graduate->field_of_work }}">
                                                    @if($errors->has('field_of_work'))
                                                        <span class="help-block">
                                                                {{ $errors->first('field_of_work') }}
                                                            </span>
                                                    @endif
                                                </td>
                                            </tr>
                                            <tr class="currentlyWorking">
                                                <th>Work Experience</th>
                                                <td>
                                                    <div class="input-group link-input {{ $errors->has('years_of_experience') ? ' has-error' : '' }}">
                                                        <input type="number" class="form-control"
                                                               placeholder="years of experience"
                                                               name="years_of_experience"
                                                               value="{{ old('years_of_experience')?:$user->graduate->years_of_experience }}">
                                                        <span class="input-group-addon">Years</span>
                                                        @if($errors->has('years_of_experience'))
                                                            <span class="help-block">
                                                                {{ $errors->first('years_of_experience') }}
                                                            </span>
                                                        @endif
                                                    </div>
                                                </td>
                                            </tr>
                                            <tr>
                                                <th>Links</th>
                                                <td>
                                                    <ul>
                                                        <li>
                                                            <div class="input-group link-input {{ $errors->has('github_link') ? ' has-error' : '' }}">
                                                            <span class="input-group-addon"><i style="color: #333;"
                                                                                               class="fa fa-github icon"></i></span>
                                                                <input type="url" class="form-control"
                                                                       placeholder="Github URL"
                                                                       name="github_link"
                                                                       value="{{ old('github_link')?:$user->graduate->github_link }}">
                                                                @if($errors->has('github_link'))
                                                                    <span class="help-block">
                                                                        {{ $errors->first('github_link') }}
                                                                    </span>
                                                                @endif
                                                            </div>
                                                        </li>
                                                        <li>
                                                            <div class="input-group link-input {{ $errors->has('linkedin_link') ? ' has-error' : '' }}">
                                                            <span class="input-group-addon"><i style="color: #0077b5;"
                                                                                               class="fa fa-linkedin icon"></i></span>
                                                                <input type="url" class="form-control"
                                                                       placeholder="LinkedIn URL"
                                                                       name="linkedin_link"
                                                                       value="{{ old('linkedin_link')?:$user->graduate->linkedin_link }}">
                                                                @if($errors->has('linkedin_link'))
                                                                    <span class="help-block">
                                                                        {{ $errors->first('linkedin_link') }}
                                                                    </span>
                                                                @endif
                                                            </div>
                                                        </li>
                                                        <li>
                                                            <div class="input-group link-input {{ $errors->has('dribble_link') ? ' has-error' : '' }}">
                                                            <span class="input-group-addon"><i style="color: #ea4c89;"
                                                                                               class="fa fa-dribbble icon"></i></span>
                                                                <input type="url" class="form-control"
                                                                       placeholder="Dribble URL"
                                                                       name="dribble_link"
                                                                       value="{{ old('dribble_link')?:$user->graduate->dribble_link }}">
                                                                @if($errors->has('dribble_link'))
                                                                    <span class="help-block">
                                                                        {{ $errors->first('dribble_link') }}
                                                                    </span>
                                                                @endif
                                                            </div>
                                                        </li>
                                                    </ul>
                                                </td>
                                            </tr>
                                            <tr>
                                                <th>Available for Hire</th>
                                                <td class="{{ $errors->has('is_hireable') ? ' has-error' : '' }}">
                                                    <div class="form-group ">
                                                        <input type="radio" name="is_hireable"
                                                               value="1" {{ old('is_hireable') === 1? 'checked' : '' }} {{ $user->graduate->is_hireable === 1? 'checked' : '' }}>
                                                        Yes &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                        <input type="radio" name="is_hireable"
                                                               value="0" {{ old('is_hireable') === 0? 'checked' : '' }} {{ $user->graduate->is_hireable === 0? 'checked' : '' }}>
                                                        No
                                                    </div>
                                                    @if($errors->has('is_hireable'))
                                                        <span class="help-block">
                                                                {{ $errors->first('is_hireable') }}
                                                            </span>
                                                    @endif
                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                </div>
                            </div>
                            <div style="border-top: 1px solid #FBB034" class="padding-top-5">
                                <div class="panel">
                                    <div class="panel-heading">
                                        <h3>More Information</h3>
                                    </div>
                                    <div class="panel-body">
                                        <ul class="list-group">
                                            <li class="list-group-item">
                                                <h5>Short Description</h5>
                                                <p class="{{ $errors->has('description') ? ' has-error' : '' }}">
                                                    <textarea class="form-control " name="description"
                                                              id="description">{{ old('description')?:$user->graduate->description }}</textarea>
                                                    @if($errors->has('description'))
                                                        <span class="help-block">
                                                                {{ $errors->first('description') }}
                                                            </span>
                                                    @endif
                                                </p>
                                            </li>

                                            <li class="list-group-item">
                                                <h5>Hobbies and Interests</h5>
                                                <p class="{{ $errors->has('interests') ? ' has-error' : '' }}">
                                                    <textarea class="form-control " name="interests"
                                                              id="interests">{{ old('interests')?:$user->graduate->interests }}</textarea>
                                                    @if($errors->has('interests'))
                                                        <span class="help-block">
                                                                {{ $errors->first('interests') }}
                                                            </span>
                                                    @endif
                                                </p>
                                            </li>

                                            <li class="list-group-item">
                                                <h5>Achievements</h5>
                                                <p class="{{ $errors->has('achievements') ? ' has-error' : '' }}">
                                                    <textarea class="form-control " name="achievements"
                                                              id="achievements">{{ old('achievements')?:$user->graduate->achievements }}</textarea>
                                                    @if($errors->has('achievements'))
                                                        <span class="help-block">
                                                                {{ $errors->first('achievements') }}
                                                            </span>
                                                    @endif
                                                </p>
                                            </li>
                                            <li class="list-group-item">
                                                <h5>Volunteer Work</h5>
                                                <p class="{{ $errors->has('volunteer') ? ' has-error' : '' }}">
                                                    <textarea class="form-control " name="volunteer"
                                                              id="volunteer">{{ old('volunteer')?:$user->graduate->volunteer }}</textarea>
                                                    @if($errors->has('volunteer'))
                                                        <span class="help-block">
                                                                {{ $errors->first('volunteer') }}
                                                            </span>
                                                    @endif
                                                </p>
                                            </li>

                                            <li class="list-group-item">
                                                <h5>Work Goal</h5>
                                                <p class="{{ $errors->has('goal') ? ' has-error' : '' }}">
                                                    <textarea class="form-control " name="goal"
                                                              id="goal">{{ old('goal')?:$user->graduate->goal }}</textarea>
                                                    @if($errors->has('goal'))
                                                        <span class="help-block">
                                                                {{ $errors->first('goal') }}
                                                            </span>
                                                    @endif
                                                </p>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        @elseif($user->isCompany())
                            <div style="border-top: 1px solid #FBB034" class="padding-top-5">
                                <div class="panel">
                                    <div class="panel-heading">
                                        <h3>About us</h3>
                                    </div>
                                    <div class="panel-body">
                                        <div class="form-group {{ $errors->has('short_description') ? ' has-error' : '' }}">
                                            <label for="short_description">Short Description</label>
                                            <textarea class="form-control" name="short_description"
                                                      id="short_description"
                                                      placeholder="">{{ old('short_description')?: $user->company->short_description}}</textarea>
                                            @if($errors->has('short_description'))
                                                <span class="help-block">
                                                    <strong>{{ $errors->first('short_description') }}</strong>
                                                </span>
                                            @endif
                                        </div>

                                        <div class="form-group {{ $errors->has('full_description') ? ' has-error' : '' }}">
                                            <label for="full_description">Full Description</label>
                                            <textarea class="form-control big-textarea" name="full_description"
                                                      id="full_description"
                                                      placeholder="">{{ old('full_description')? old('full_description') : $user->company->full_description}}</textarea>
                                            @if($errors->has('full_description'))
                                                <span class="help-block">
                                                    <strong>{{ $errors->first('full_description') }}</strong>
                                                </span>
                                            @endif
                                        </div>

                                        {{-- TODO: change to dropdown countries and regions--}}
                                        <div class="form-group col-md-6 {{ $errors->has('headquarters') ? ' has-error' : '' }}">
                                            <label for="headquarters">Headquarters</label>
                                            <input type="text" class="form-control" name="headquarters"
                                                   id="headquarters" placeholder=""
                                                   value="{{ old('headquarters')? old('headquarters') : $user->company->headquarters}}">
                                            @if($errors->has('headquarters'))
                                                <span class="help-block">
                                                    <strong>{{ $errors->first('headquarters') }}</strong>
                                                </span>
                                            @endif
                                        </div>

                                        <div class="form-group col-md-6 {{ $errors->has('size') ? ' has-error' : '' }}">
                                            <label for="size" class=" control-label">Company Size</label>
                                            <select class="form-control select-size" id="size" name="size">
                                                @foreach(Config::get("enums.company_sizes") as $key => $value)
                                                    <option></option>
                                                    <option
                                                            {{ old('size') == $value ? 'selected': $user->company->size == $value? 'selected' : ''  }}
                                                            value="{{ $value }}">{{ $key . ': '. $value }}
                                                    </option>
                                                @endforeach
                                            </select>
                                            @if($errors->has('size'))
                                                <span class="help-block">
                                                    <strong>{{ $errors->first('size') }}</strong>
                                                </span>
                                            @endif
                                        </div>


                                        <div class="form-group col-md-6 {{ $errors->has('founded_at') ? ' has-error' : '' }}">
                                            <label for="founded_at">Founded At</label>
                                            <input type="text" class="form-control" name="founded_at" id="founded_at"
                                                   placeholder=""
                                                   value="{{ old('founded_at')?  old('founded_at') : $user->company->founded_at }}">
                                            @if($errors->has('founded_at'))
                                                <span class="help-block">
                                                    <strong>{{ $errors->first('founded_at') }}</strong>
                                                </span>
                                            @endif
                                        </div>

                                        <div class="form-group col-md-6 {{ $errors->has('type') ? ' has-error' : '' }}">
                                            <label for="type">Type</label>
                                            <input type="text" class="form-control" name="type" id="type" placeholder=""
                                                   value="{{ old('type')?  old('type') : $user->company->type}}">
                                            @if($errors->has('type'))
                                                <span class="help-block">
                                                    <strong>{{ $errors->first('type') }}</strong>
                                                </span>
                                            @endif
                                        </div>
                                        <div class="form-group col-md-6{{ $errors->has('website_link') ? ' has-error' : '' }}">
                                            <label for="website_link">Website</label>
                                            <input type="url" class="form-control" name="website_link"
                                                   id="website_link" placeholder=""
                                                   value="{{ old('website_link')?  old('website_link') : $user->company->website_link}}">
                                            @if($errors->has('website_link'))
                                                <span class="help-block">
                                                    <strong>{{ $errors->first('website_link') }}</strong>
                                                </span>
                                            @endif
                                        </div>
                                        <div class="clearfix"></div>

                                        <div class="form-group">
                                            <label for="social_links">Links</label>
                                            <ul id="social_links">
                                                <li>
                                                    <div class="input-group link-input {{ $errors->has('facebook_link') ? ' has-error' : '' }}">
                                                        <span class="input-group-addon"><i style="color: #3b5998;"
                                                                                           class="fa fa-facebook icon"></i></span>
                                                        <input type="url" class="form-control"
                                                               placeholder="Facebook URL"
                                                               name="facebook_link"
                                                               value="{{  old('facebook_link')? old('facebook_link') : $user->company->facebook_link }}">
                                                        @if($errors->has('facebook_link'))
                                                            <span class="help-block">
                                                                <strong>{{ $errors->first('facebook_link') }}</strong>
                                                            </span>
                                                        @endif
                                                    </div>
                                                </li>
                                                <li>
                                                    <div class="input-group link-input {{ $errors->has('twitter_link') ? ' has-error' : '' }}">
                                                        <span class="input-group-addon"><i style="color: #1da1f2;"
                                                                                           class="fa fa-twitter icon"></i></span>
                                                        <input type="url" class="form-control"
                                                               placeholder="Twitter URL"
                                                               name="twitter_link"
                                                               value="{{ old('twitter_link')? old('twitter_link') : $user->company->twitter_link }}">
                                                        @if($errors->has('twitter_link'))
                                                            <span class="help-block">
                                                                <strong>{{ $errors->first('twitter_link') }}</strong>
                                                            </span>
                                                        @endif
                                                    </div>
                                                </li>

                                                <li>
                                                    <div class="input-group link-input {{ $errors->has('linkedin_link') ? ' has-error' : '' }}">
                                                        <span class="input-group-addon"><i style="color: #0077b5;"
                                                                                           class="fa fa-linkedin icon"></i></span>
                                                        <input type="url" class="form-control"
                                                               placeholder="LinkedIn URL"
                                                               name="linkedin_link"
                                                               value="{{ old('linkedin_link')? old('linkedin_link') : $user->company->linkedin_link }}">
                                                        @if($errors->has('linkedin_link'))
                                                            <span class="help-block">
                                                                <strong>{{ $errors->first('linkedin_link') }}</strong>
                                                            </span>
                                                        @endif
                                                    </div>
                                                </li>

                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div style="border-top: 1px solid #FBB034" class="padding-top-5">
                                <div class="panel">
                                    <div class="panel-heading">
                                        <h3>Addresses</h3>
                                    </div>
                                    <div class="panel-body">
                                        <div class="form-group {{ $errors->has('address_head') ? ' has-error' : '' }}">
                                            <label for="address_head">Main Branch</label>
                                            <textarea class="form-control" id="address_head" name="address_head"
                                                      placeholder=""
                                            >{{ old('address_head')?  old('address_head') : $user->company->address_head}}</textarea>
                                            @if($errors->has('address_head'))
                                                <span class="help-block">
                                                    <strong>{{ $errors->first('address_head') }}</strong>
                                                </span>
                                            @endif
                                        </div>
                                        <div class="form-group {{ $errors->has('address_1') ? ' has-error' : '' }}">
                                            <label for="address_1">Branch #1</label>
                                            <textarea class="form-control" id="address_1" name="address_1"
                                                      placeholder=""
                                            >{{ old('address_1')?  old('address_1') : $user->company->address_1}}</textarea>
                                            @if($errors->has('address_1'))
                                                <span class="help-block">
                                                    <strong>{{ $errors->first('address_1') }}</strong>
                                                </span>
                                            @endif
                                        </div>
                                        <div class="form-group {{ $errors->has('address_2') ? ' has-error' : '' }}">
                                            <label for="address_2">Branch #2</label>
                                            <textarea class="form-control" id="address_2" name="address_2"
                                                      placeholder=""
                                            >{{ old('address_2')?  old('address_2') : $user->company->address_2}}</textarea>
                                            @if($errors->has('address_2'))
                                                <span class="help-block">
                                                    <strong>{{ $errors->first('address_2') }}</strong>
                                                </span>
                                            @endif
                                        </div>
                                        <div class="form-group {{ $errors->has('address_3') ? ' has-error' : '' }}">
                                            <label for="address_3">Branch #3</label>
                                            <textarea class="form-control" id="address_3" name="address_3"
                                                      placeholder=""
                                            >{{ old('address_3')?  old('address_3') : $user->company->address_3}}</textarea>
                                            @if($errors->has('address_3'))
                                                <span class="help-block">
                                                    <strong>{{ $errors->first('address_3') }}</strong>
                                                </span>
                                            @endif
                                        </div>
                                        <div class="form-group {{ $errors->has('address_4') ? ' has-error' : '' }}">
                                            <label for="address_4">Branch #4</label>
                                            <textarea class="form-control" id="address_4" name="address_4"
                                                      placeholder=""
                                            >{{ old('address_4')?  old('address_4') : $user->company->address_4}}</textarea>
                                            @if($errors->has('address_4'))
                                                <span class="help-block">
                                                    <strong>{{ $errors->first('address_4') }}</strong>
                                                </span>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                            </div>
                        @endif
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
@section('scripts')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.18.1/moment.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-daterangepicker/2.1.25/daterangepicker.js"></script>
    @if($user->isGraduate())
        <script type="text/javascript">
            $('input[type="file"]').on('change', function (eve) {
                eve.preventDefault();
                var data = $(this).val();
                data = data.split('\\');
                $('#pdfFileName').text(data[2]);
            })

            $('#dob').daterangepicker({
                locale: {
                    format: 'YYYY-MM-DD'
                },
                startDate: '@php
                    if(old('dob')){
                        echo old('dob');
                    }elseif ($user->graduate->dob){
                        echo $user->graduate->dob->format('Y-m-d');
                    }else{
                        echo \Carbon\Carbon::now()->format('Y-m-d');
                    }
                @endphp',
                singleDatePicker: true,
                showDropdowns: true
            });

            $(".select-month").select2({
                placeholder: "Select month"
            });
            $(".select-day").select2({
                placeholder: "Select day"
            });
            $(".select-year").select2({
                placeholder: "Select year"
            });

            $('input[name=is_employed]').on('change', function (eve) {
                if ($(this).val() == 0) {
                    $('.currentlyWorking').slideUp();
                } else {
                    $('.currentlyWorking').slideDown();
                }
            })

            if ($('input[name=is_employed]:checked').val() == 0) {
                $('.currentlyWorking').slideUp();
            } else {
                $('.currentlyWorking').slideDown();
            }

        </script>
    @endif
@endsection