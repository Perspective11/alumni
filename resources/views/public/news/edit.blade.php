@extends("layouts.master")
@section('top-ext')
    {{--    <link href="{{ asset("css/select2.min.css") }}" />--}}
    {{--    <script src="{{ asset("js/select2.min.js") }}"></script>--}}
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/css/select2.min.css" rel="stylesheet" />
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js"></script>


@endsection
@section("content")

    <div class="panel panel-default">
        <div class="panel-heading">Submit a new news post</div>
        <div class="panel-body">
            <form class="form-horizontal" role="form" method="post" action="{{ url('/news/' . $new->id) }} " enctype="multipart/form-data" >
                {{ csrf_field() }}
                {{ method_field("PUT") }}
                <input type="hidden" name="MAX_FILE_SIZE" value="20971520">
                @php

                @endphp
                <div class="form-group{{ $errors->has('title') ? ' has-error' : '' }}">
                    <label for="title" class="col-md-2 control-label">Title</label>

                    <div class="col-md-8">
                        <input id="title" type="text" class="form-control" name="title" value="{{ isset($new->title)? $new->title : old('title') }}" required autofocus>
                    </div>
                </div>

                <div class="form-group{{ $errors->has('body') ? ' has-error' : '' }}">
                    <label for="body" class="col-md-2 control-label">Body</label>

                    <div class="col-md-8">
                        <textarea rows="10" id="body" class="form-control" name="body" required>{{ isset($new->body)? $new->body : old('body') }}</textarea>
                    </div>
                </div>

                <div class="form-group{{ $errors->has('category') ? ' has-error' : '' }}">
                    <label for="category" class="col-md-2 control-label">Category</label>

                    <div class="col-md-8">
                        <select class="select-category" name="category">
                            @foreach($categories as $category)
                                <option {{ $category->name == $new->category->name? 'selected': ''}} value="{{ $category->id }}">{{ $category->name }}  </option>
                            @endforeach
                        </select>
                    </div>
                </div>

                <div class="form-group{{ $errors->has('major') ? ' has-error' : '' }}">
                    <label for="major" class="col-md-2 control-label">Major</label>

                    <div class="col-md-8">
                        <select class="select-major" name="major">
                            @foreach($majors as $major)
                                <option {{ $major->name == $new->major->name? 'selected': ''}} value="{{ $major->id }}">{{ $major->name }} </option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <br>

                <div class="form-group{{ $errors->has('image') ? ' has-error' : '' }}">
                    <label for="image" class="col-md-2 control-label">Image</label>

                    <div class="col-md-8">
                        @if($new->pictures->first())
                            <div class="alert">
                                <p>This News already contains an image</p>
                                <a class="btn btn-xs btn-warning change-image">Change Image</a>
                            </div>
                        @else
                            <input type="file" name="picture" id="picture">
                            {{--TODO: add a drag and drop plugin--}}
                        @endif

                    </div>
                </div>

                <div class="form-group{{ $errors->has('tags') ? ' has-error' : '' }}">
                    <label for="tags" class="col-md-2 control-label">Tags</label>

                    <div class="col-md-8">
                        <select class="select-tags" name="tags[]" multiple="multiple">
                            @foreach($tags as $tag)
                                <option {{ $new->tags->pluck('name')->contains($tag->name) ? 'selected': ''}} value="{{ $tag->id }}">{{ $tag->name }} </option>
                                {{-- TODO: create tags on the fly --}}
                            @endforeach
                        </select>


                    </div>
                </div>



                <div class="form-group">
                    <div class="col-md-10 col-md-offset-2">
                        <button type="submit" class="btn btn-primary">
                            Submit
                        </button>
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-md-8 col-md-offset-2">
                        @include('errors.errors')
                    </div>
                </div>
            </form>

        </div>
    </div>


@endsection
@section('bottom-ext')
    <script type="text/javascript">
        $(".select-tags").select2({
            placeholder: "Select tags",
            allowClear: true,
            maximumSelectionLength: 5
        });
        $(".select-category").select2();
        $(".select-major").select2();

        $('.change-image').on('click', function (e) {
            e.preventDefault();

            $(this).parent().replaceWith('<input type="file" name="picture" id="picture">');
        })

    </script>
@endsection