@extends('public.layouts.app')
@section('content')
    <!-- === BEGIN CONTENT === -->
    <div id="content">
        <div class="container background-white">
            <div class="row margin-vert-30">
                <!-- Main Column -->
                <div class="col-md-9">
                    <h3>News for "{{ $tag->name }}" tag</h3>
                @foreach($news as $new)
                    <!-- Blog Post -->
                        <div class="blog-post padding-bottom-20">
                            <!-- Blog Item Header -->
                            <div class="blog-item-header">
                                <!-- Title -->
                                <h2>
                                    <a href="{{ $new->path() }}">{{ $new->title }}</a>
                                </h2>
                                <div class="clearfix"></div>
                                <!-- End Title -->
                            </div>
                            <!-- End Blog Item Header -->
                            <!-- Blog Item Details -->
                            <div class="blog-post-details">
                                <!-- Date -->
                                <div class="blog-post-details-item blog-post-details-item-left">
                                    <i class="fa fa-calendar color-gray-light"></i>
                                    {{ $new->created_at->format('jS M, Y') }}
                                </div>
                                <!-- End Date -->
                            @if(count($new->tags))
                                <!-- Tags -->
                                    <div class="blog-post-details-item blog-post-details-item-left blog-post-details-tags">
                                        <i class="fa fa-tag color-gray-light"></i>
                                        @foreach($new->tags as $tag)
                                            <a href="{{ route('news.tag',$tag->id) }}">{{ $tag->name }}</a>,
                                        @endforeach
                                    </div>
                                    <!-- End Tags -->
                            @endif
                            @if(count($new->category))
                                <!-- categories -->
                                    <div class="blog-post-details-item blog-post-details-item-left blog-post-details-tags">
                                        <i class="fa fa-bookmark color-gray-light"></i>
                                        <a href="{{ route('news.category',$new->category->id) }}">{{ $new->category->name }}</a>,
                                    </div>
                                    <!-- End categories -->
                            @endif
                                <!-- # of Comments -->
                                <div class="blog-post-details-item blog-post-details-item-left blog-post-details-item-last">
                                    <a href="{{ $new->path() }}">
                                        <i class="fa fa-comments color-gray-light"></i>
                                        {{ count($new->comments) }} Comments
                                    </a>
                                </div>
                                <!-- End # of Comments -->
                            </div>
                            <!-- End Blog Item Details -->
                            <!-- Blog Item Body -->
                            <div class="blog">
                                <div class="clearfix"></div>
                                <div class="blog-post-body row margin-top-15">
                                    <div class="col-md-5">
                                        <img class="margin-bottom-20" src="{{ count($new->pictures)?$new->pictures()->first()->path:asset('images/blog/image1.jpg') }}" alt="thumb1">
                                    </div>
                                    <div class="col-md-7">
                                        <p>
                                            {{ str_limit($new->body,350,'...') }}
                                        </p>
                                        <!-- Read More -->
                                        <a href="{{ $new->path() }}" class="btn btn-primary">
                                            Read More
                                            <i class="icon-chevron-right readmore-icon"></i>
                                        </a>
                                        <!-- End Read More -->
                                    </div>
                                </div>
                            </div>
                            <!-- End Blog Item Body -->
                        </div>
                        <!-- End Blog Item -->
                @endforeach
                <!-- Pagination -->
                {{ $news->links() }}
                <!-- End Pagination -->
                </div>
                <!-- End Main Column -->
                <!-- Side Column -->
                <div class="col-md-3">
                    {{--Search Post--}}
                    <div class="blog-tags">
                        <h3>Search</h3>
                        <p>
                        <form action="" method="get" class="input-group">
                            <input type="search" name="search" class="form-control" placeholder="news title">
                            <span class="input-group-btn">
                                 <button type="submit" class="btn btn-primary"><span class="fa fa-search"></span></button>
                                 </span>
                        </form>
                        </p>
                    </div>
                {{--end search post--}}
                    <!-- Recent news -->
                    <div class="recent-posts">
                        <h3>Recent news</h3>
                        <ul class="posts-list margin-top-10">
                            @foreach($recentNews as $recentNew)
                                <li>
                                    <div class="recent-post">
                                        <a href="{{ $recentNew->path() }}">
                                            <img class="pull-left" width="64" src="{{ count($recentNew->pictures)?$recentNew->pictures()->first()->path:asset('images/blog/thumbs/thumb1.jpg')}}" alt="thumb1">
                                        </a>
                                        <a href="{{ $recentNew->path() }}" class="posts-list-title">{{ $recentNew->title }}</a>
                                        <br>
                                        <span class="recent-post-date">
                                                {{ $recentNew->created_at->diffForHumans() }}
                                            </span>
                                    </div>
                                    <div class="clearfix"></div>
                                </li>
                            @endforeach
                        </ul>
                    </div>
                    <!-- End Recent news -->
                </div>
                <!-- End Side Column -->
            </div>
        </div>
    </div>
    <!-- === END CONTENT === -->
    <!-- === BEGIN FOOTER === -->
@endsection