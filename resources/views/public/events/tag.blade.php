@extends('public.layouts.app')
@section('content')
    <!-- === BEGIN CONTENT === -->
    <div id="content">
        <div class="container background-white">
            <div class="row margin-vert-30">
                <!-- Main Column -->
                <div class="col-md-9">
                    <h3>Events for "{{ $tag->name }}" tag</h3>
                @foreach($events as $event)
                    <!-- Blog Post -->
                        <div class="blog-post padding-bottom-20">
                            <!-- Blog Item Header -->
                            <div class="blog-item-header">
                                <!-- Title -->
                                <h2>
                                    <a href="{{ $event->path() }}">{{ $event->title }}</a>
                                </h2>
                                <div class="clearfix"></div>
                                <!-- End Title -->
                            </div>
                            <!-- End Blog Item Header -->
                            <!-- Blog Item Details -->
                            <div class="blog-post-details">
                                <!-- Date -->
                                <div class="blog-post-details-item blog-post-details-item-left">
                                    <i class="fa fa-calendar color-gray-light"></i>
                                    {{ $event->created_at->format('jS M, Y') }}
                                </div>
                                <!-- End Date -->
                            @if(count($event->tags))
                                <!-- Tags -->
                                    <div class="blog-post-details-item blog-post-details-item-left blog-post-details-tags">
                                        <i class="fa fa-tag color-gray-light"></i>
                                        @foreach($event->tags as $tag)
                                            <a href="{{ route('posts.tag',$tag->id) }}">{{ $tag->name }}</a>,
                                        @endforeach
                                    </div>
                                    <!-- End Tags -->
                            @endif
                            @if(count($event->category))
                                <!-- categories -->
                                    <div class="blog-post-details-item blog-post-details-item-left blog-post-details-tags">
                                        <i class="fa fa-bookmark color-gray-light"></i>
                                        <a href="{{ route('events.category',$event->category->id) }}">{{ $event->category->name }}</a>,
                                    </div>
                                    <!-- End categories -->
                            @endif
                                <!-- # of Comments -->
                                <div class="blog-post-details-item blog-post-details-item-left blog-post-details-item-last">
                                    <a href="{{ $event->path() }}">
                                        <i class="fa fa-comments color-gray-light"></i>
                                        {{ count($event->comments) }} Comments
                                    </a>
                                </div>
                                <!-- End # of Comments -->
                                <!-- Start date -->
                                <div class="blog-post-details-item blog-post-details-item-left">
                                    <i class="fa fa-calendar color-gray-light"></i>
                                    {{ $event->date->diffForHumans() }}
                                </div>
                                <!-- End Start date -->
                                <!-- time -->
                                <div class="blog-post-details-item blog-post-details-item-left">
                                    <i class="fa fa-clock-o color-gray-light"></i>
                                    {{ date("g:i a", strtotime( $event->start)) }} -
                                    {{ date("g:i a", strtotime( $event->end)) }}
                                </div>
                                <!-- End time -->
                            </div>
                            <!-- End Blog Item Details -->
                            <!-- Blog Item Body -->
                            <div class="blog">
                                <div class="clearfix"></div>
                                <div class="blog-post-body row margin-top-15">
                                    <div class="col-md-5">
                                        <img class="margin-bottom-20" src="{{ count($event->pictures)?$event->pictures()->first()->path:asset('images/blog/image1.jpg') }}" alt="thumb1">
                                    </div>
                                    <div class="col-md-7">
                                        <p>
                                            {{ str_limit($event->body,350,'...') }}
                                        </p>
                                        <!-- Read More -->
                                        <a href="{{ $event->path() }}" class="btn btn-primary">
                                            Read More
                                            <i class="icon-chevron-right readmore-icon"></i>
                                        </a>
                                        <!-- End Read More -->
                                    </div>
                                </div>
                            </div>
                            <!-- End Blog Item Body -->
                        </div>
                        <!-- End Blog Item -->
                @endforeach
                <!-- Pagination -->
                {{ $events->links() }}
                <!-- End Pagination -->
                </div>
                <!-- End Main Column -->
                <!-- Side Column -->
                <div class="col-md-3">
                    {{--Search Post--}}
                    <div class="blog-tags">
                        <h3>Search</h3>
                        <p>
                        <form action="" method="get" class="input-group">
                            <input type="search" name="search" class="form-control" placeholder="Events title">
                            <span class="input-group-btn">
                                 <button type="submit" class="btn btn-primary"><span class="fa fa-search"></span></button>
                                 </span>
                        </form>
                        </p>
                    </div>
                {{--end search post--}}
                    <!-- Recent news -->
                    <div class="recent-posts">
                        <h3>Recent Events</h3>
                        <ul class="posts-list margin-top-10">
                            @foreach($recentEvents as $recentEvent)
                                <li>
                                    <div class="recent-post">
                                        <a href="{{ $recentEvent->path() }}">
                                            <img class="pull-left" width="64" src="{{ count($recentEvent->pictures)?$recentEvent->pictures()->first()->path:asset('images/blog/thumbs/thumb1.jpg')}}" alt="thumb1">
                                        </a>
                                        <a href="{{ $recentEvent->path() }}" class="posts-list-title">{{ $recentEvent->title }}</a>
                                        <br>
                                        <span class="recent-post-date">
                                                {{ $recentEvent->created_at->diffForHumans() }}
                                            </span>
                                    </div>
                                    <div class="clearfix"></div>
                                </li>
                            @endforeach
                        </ul>
                    </div>
                    <!-- End Recent news -->
                </div>
                <!-- End Side Column -->
            </div>
        </div>
    </div>
    <!-- === END CONTENT === -->
    <!-- === BEGIN FOOTER === -->
@endsection