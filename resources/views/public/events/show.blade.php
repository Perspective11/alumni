@extends('public.layouts.app')
@section('content')
    <!-- === BEGIN CONTENT === -->
    <div id="content">
        <div class="container background-white">
            <div class="row margin-vert-30">
                <!-- Main Column -->
                <div class="col-md-9">
                    <div class="blog-post">
                        <div class="blog-item-header">
                            <h2 class="pull-left">
                                {{ $event->title }}
                            </h2>
                            <div class="pull-right">
                                @if(Auth::check())
                                    @if(Auth::user()->isAdmin())
                                        <a href="{{ route('admin.events.show', $event->id) }}" class="btn btn-primary">Admin</a>
                                    @endif
                                @endif
                            </div>
                            <div class="clearfix"></div>
                        </div>
                        <div class="blog-post-details">
                            <!-- Date -->
                            <div class="blog-post-details-item blog-post-details-item-left">
                                Created
                                {{ $event->created_at->diffForHumans() }}
                            </div>
                            <!-- End Date -->
                        @if(count($event->tags))
                            <!-- Tags -->
                                <div class="blog-post-details-item blog-post-details-item-left blog-post-details-tags">
                                    <i class="fa fa-tag color-gray-light"></i>
                                    {!! $event->tagsWithLinks() !!}
                                </div>
                                <!-- End Tags -->
                        @endif
                        @if(count($event->category))
                            <!-- categories -->
                                <div class="blog-post-details-item blog-post-details-item-left blog-post-details-tags">
                                    <i class="fa {{ $event->category->icon }}"
                                       style="color: {{ $event->category->color }}"></i>
                                    <a href="{{ route('events.category',$event->category->id) }}">{{ $event->category->name }}</a>
                                </div>
                                <!-- End categories -->
                        @endif
                        @if(count($event->major))
                            <!-- categories -->
                                <div class="blog-post-details-item blog-post-details-item-left blog-post-details-tags">
                                    Major:
                                    <i class="fa {{ $event->major->icon }}"
                                       style="color: {{ $event->major->color }}"></i>
                                    <a href="">{{ $event->major->name }}</a>
                                </div>
                                <!-- End categories -->
                        @endif
                        <!-- # of Comments -->
                            <div class="blog-post-details-item blog-post-details-item-left blog-post-details-item-last">
                                <i class="fa fa-comments color-gray-light"></i>
                                {{ count($event->comments) }} Comments
                            </div>
                            <!-- End # of Comments -->
                            <!-- Start date -->
                            <div class="blog-post-details-item blog-post-details-item-left">
                                <i class="fa fa-calendar color-gray-light"></i>
                                {{ $event->date->format('jS M, Y') }}
                            </div>
                            <!-- End Start date -->
                            <!-- time -->
                            <div class="blog-post-details-item blog-post-details-item-left">
                                <i class="fa fa-clock-o color-gray-light"></i>
                                {{ date("g:i a", strtotime( $event->start)) }} -
                                {{ date("g:i a", strtotime( $event->end)) }}
                            </div>
                            <!-- End time -->
                        </div>
                        <div class="blog-item">
                            <div class="clearfix"></div>
                            <div class="blog-post-body margin-top-15">
                                <div>
                                    <img class="margin-bottom-20"
                                         src="{{ count($event->pictures)?$event->pictures[0]->path:asset('images/events/default.png') }}"
                                         alt="{{ count($event->pictures)?$event->pictures()->first()->path:$event->title }}">
                                </div>
                                <p>{{ $event->body }}</p>
                            </div>
                            <div class="blog-item-footer">
                                <!-- Comments -->
                                <div class="blog-recent-comments panel panel-default margin-bottom-30">
                                    <div class="panel-heading">
                                        <h3>Comments</h3>
                                    </div>
                                    <ul class="list-group">
                                        @foreach($event->comments as $comment)
                                            <li class="list-group-item">
                                                <div class="row">
                                                    <div class="col-md-2 profile-thumb" style="margin-top: 25px">
                                                        <a href="{{ route('profile',$comment->user->id) }}">
                                                            @if($comment->user->isGraduate())
                                                                <img class="media-object"
                                                                     src="{{ count($comment->user->graduate->pictures)?$comment->user->graduate->pictures[0]->path:'images/graduates/profile/default.png' }}"
                                                                     alt="{{ $comment->user->name }}">
                                                            @endif
                                                        </a>
                                                    </div>
                                                    <div class="col-md-10">
                                                        <h5>
                                                            <a href="{{ route('profile',$comment->user->id) }}">{{ $comment->user->name }}</a>
                                                        </h5>
                                                        <p id="c-{{ $comment->id }}">
                                                            {{ $comment->body }}
                                                        </p>
                                                        @if(auth()->id() == $comment->user->id)
                                                            <form action="{{ route('comments.update',$comment->id) }}"
                                                                  method="post" style="display: none"
                                                                  id="comment-{{ $comment->id }}">
                                                                {{ csrf_field() }}
                                                                {{ method_field('PUT') }}
                                                                <textarea name="body" class="form-control"
                                                                          rows="4">{{ $comment->body }}</textarea>
                                                                <input type="submit"
                                                                       class="btn btn-primary pull-right margin-vert-10"
                                                                       value="Change">
                                                                <div class="clearfix"></div>
                                                            </form>
                                                        @endif
                                                        @if(auth()->id() == $comment->user->id)
                                                            <form action="{{ route('comments.delete',$comment->id) }}"
                                                                  method="post" style="display: none"
                                                                  id="dComment-{{ $comment->id }}">
                                                                {{ csrf_field() }}
                                                                {{ method_field('DELETE') }}
                                                            </form>
                                                        @endif
                                                    </div>
                                                    <span class="date" style="position: absolute;right: 5px;top: 0;">
                                                    @if(auth()->id() == $comment->user->id)
                                                            <div class="dropdown text-right">
                                                            <a href="#" class="dropdown-toggle" type="button"
                                                               data-toggle="dropdown">
                                                                <span class="fa fa-ellipsis-v"></span>
                                                            </a>
                                                            <ul class="dropdown-menu dropdown-menu-right">
                                                                <li><a href="#" class="fa-edit editComment"
                                                                       num="{{ $comment->id }}">Edit</a></li>
                                                                <li><a href="#" class="fa-trash-o deleteComment"
                                                                       num="{{ $comment->id }}">Delete</a></li>
                                                            </ul>
                                                        </div>
                                                        @endif
                                                        <i class="fa fa-clock-o color-gray-light"></i> {{ $comment->created_at->diffForHumans() }}
                                                </span>
                                                </div>
                                            </li>
                                        @endforeach
                                        @if(Auth::check())
                                            @if(Auth::user()->isGraduate())
                                            <!-- Comment Form -->
                                                <li class="list-group-item">
                                                    <div class="blog-comment-form">
                                                        <div class="row margin-top-20">
                                                            <div class="col-md-12">
                                                                <div class="pull-left">
                                                                    <h3>Leave a Comment</h3>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="row margin-top-20">
                                                            <div class="col-md-12">
                                                                <form action="{{ route('eventComments.store',$event->id) }}"
                                                                      method="post">
                                                                    {{ csrf_field() }}
                                                                    <label>Message</label>
                                                                    <div class="row margin-bottom-20">
                                                                        <div class="col-md-11 col-md-offset-0">
                                                                            <textarea class="form-control" name="body"
                                                                                      rows="8"></textarea>
                                                                        </div>
                                                                    </div>
                                                                    <p>
                                                                        <button class="btn btn-primary" type="submit">
                                                                            Submit
                                                                        </button>
                                                                    </p>
                                                                </form>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </li>
                                                <!-- End Comment Form -->
                                            @endif
                                        @endif
                                    </ul>
                                </div>
                                <!-- End Comments -->
                            </div>
                        </div>
                    </div>
                    <!-- End Blog Post -->
                </div>
                <!-- End Main Column -->
                <!-- Side Column -->
                <div class="col-md-3">
                    <!-- Recent Events -->
                    <div class="recent-posts">
                        <h3>Recent Events</h3>
                        <ul class="posts-list margin-top-10">
                            @foreach($recentEvents as $recentEvent)
                                <li>
                                    <div class="recent-post">
                                        <a href="{{ route('events.show', $recentEvent->id) }}">
                                            <img class="pull-left" width="64"
                                                 src="{{ count($recentEvent->pictures)?$recentEvent->pictures[0]->path:asset('images/events/default.png') }}"
                                                 alt="thumb1">
                                        </a>
                                        <a href="{{ route('events.show', $recentEvent->id) }}"
                                           class="posts-list-title">{{ $recentEvent->title }}</a>
                                        <br>
                                        <span class="recent-post-date">
                                                {{ $recentEvent->created_at->diffForHumans() }}
                                            </span>
                                    </div>
                                    <div class="clearfix"></div>
                                </li>
                            @endforeach
                        </ul>
                    </div>
                    <!-- End Recent Events -->
                    <!-- End Side Column -->
                </div>
            </div>
        </div>
    </div>
    <!-- === END CONTENT === -->
@endsection

@section('scripts')
    <script>
        $('.editComment').on('click', function (e) {
//            alert($(this).attr('num'))
            $('#c-' + $(this).attr('num')).slideUp();
            $('form#comment-' + $(this).attr('num')).slideDown(function () {
                $(this).focus();
            });

            e.preventDefault();

        });

        $('.deleteComment').on('click', function (e) {
            if (confirm('Are you sure to delete?')) {
                $('#dComment-' + $(this).attr('num')).submit();
                e.preventDefault();
            }
        })
    </script>
@endsection