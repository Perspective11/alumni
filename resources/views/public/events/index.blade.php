@extends('public.layouts.app')

@section('styles')
    <link rel="stylesheet"
          href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.7.1/css/bootstrap-datepicker.min.css"/>
@endsection

@section('content')
    <!-- === BEGIN CONTENT === -->
    <div id="content">
        <div class="container background-white">
            <div class="row margin-vert-30">
                <!-- Main Column -->
                <div class="col-md-9">
                @forelse($events as $event)
                    <!-- Blog Post -->
                        <div class="blog-post padding-bottom-20">
                            <!-- Blog Item Header -->
                            <div class="blog-item-header">
                                <!-- Title -->
                                <h2>
                                    <a href="{{ route('events.show', $event->id) }}">{{ $event->title }}</a>
                                </h2>
                                <div class="clearfix"></div>
                                <!-- End Title -->
                            </div>
                            <div class="clearfix"></div>

                            <!-- End Blog Item Header -->
                            <!-- Blog Item Details -->
                            <div class="blog-post-details">
                                <!-- Date -->
                                <div class="blog-post-details-item blog-post-details-item-left">
                                    Created
                                    {{ $event->created_at->diffForHumans() }}
                                </div>
                                <!-- End Date -->
                            @if(count($event->tags))
                                <!-- Tags -->
                                    <div class="blog-post-details-item blog-post-details-item-left blog-post-details-tags">
                                        <i class="fa fa-tag color-gray-light"></i>
                                        {!! $event->tagsWithLinks() !!}
                                    </div>
                                    <!-- End Tags -->
                            @endif
                            @if(count($event->category))
                                <!-- categories -->
                                    <div class="blog-post-details-item blog-post-details-item-left blog-post-details-tags">
                                        <i class="fa {{ $event->category->icon }}"
                                           style="color: {{ $event->category->color }}"></i>
                                        <a href="{{ route('events.category',$event->category->id) }}">{{ $event->category->name }}</a>
                                    </div>
                                    <!-- End categories -->
                            @endif
                            @if(count($event->major))
                                <!-- categories -->
                                    <div class="blog-post-details-item blog-post-details-item-left blog-post-details-tags">
                                        Major:
                                        <i class="fa {{ $event->major->icon }}"
                                           style="color: {{ $event->major->color }}"></i>
                                        <a href="">{{ $event->major->name }}</a>
                                    </div>
                                    <!-- End categories -->
                            @endif
                            <!-- # of Comments -->
                                <div class="blog-post-details-item blog-post-details-item-left blog-post-details-item-last">
                                    <a href="{{ route('events.show', $event->id) }}">
                                        <i class="fa fa-comments color-gray-light"></i>
                                        {{ count($event->comments) }} Comments
                                    </a>
                                </div>
                                <!-- End # of Comments -->
                                <!-- Start date -->
                                <div class="blog-post-details-item blog-post-details-item-left">
                                    <i class="fa fa-calendar color-gray-light"></i>
                                    {{ $event->date->format('jS M, Y') }}
                                </div>
                                <!-- End Start date -->
                                <!-- time -->
                                <div class="blog-post-details-item blog-post-details-item-left">
                                    <i class="fa fa-clock-o color-gray-light"></i>
                                    {{ date("g:i a", strtotime( $event->start)) }} -
                                    {{ date("g:i a", strtotime( $event->end)) }}
                                </div>
                                <!-- End time -->
                            </div>
                            <!-- End Blog Item Details -->
                            <!-- Blog Item Body -->
                            <div class="blog">
                                <div class="clearfix"></div>
                                <div class="blog-post-body row margin-top-15">
                                    <div class="col-md-5">
                                        <img class="margin-bottom-20"
                                             src="{{ count($event->pictures)?$event->pictures[0]->path:asset('images/events/default.png') }}"
                                             alt="thumb1">
                                    </div>
                                    <div class="col-md-7">
                                        <p>
                                            {{ str_limit($event->body,350,'...') }}
                                        </p>
                                        <!-- Read More -->
                                        <a href="{{ route('events.show', $event->id) }}" class="btn btn-primary">
                                            Read More
                                            <i class="icon-chevron-right readmore-icon"></i>
                                        </a>
                                        <!-- End Read More -->
                                    </div>
                                </div>
                            </div>
                            <!-- End Blog Item Body -->
                        </div>
                        <!-- End Blog Item -->
                    @empty
                        @if(request('date'))
                            <h2 class="text-center">There is no Events @ <strong>{{ request('date')?:'' }}</strong>
                                <br><a href="{{ route('events.index') }}"><< Go Back</a>
                            </h2>
                        @else
                            <h2 class="text-center"><i class="fa fa-exclamation"></i> There is no Events ...</h2>
                        @endif
                @endforelse
                <!-- Pagination -->
                {{ $events->links() }}
                <!-- End Pagination -->
                </div>
                <!-- End Main Column -->
                <!-- Side Column -->
                <div class="col-md-3">
                    {{--Search Post--}}
                    <div class="blog-tags">
                        <h3>Search</h3>
                        <p>
                        <form action="" method="get" class="input-group">
                            <input type="search" name="search" class="form-control" placeholder="Event title">
                            <span class="input-group-btn">
                                 <button type="submit" class="btn btn-primary"><span
                                             class="fa fa-search"></span></button>
                                 </span>
                        </form>
                        </p>
                        <p>
                        <h3>By date
                            <small class="pull-right">
                                <a href="{{ route('events.index') }}?date={{ \Carbon\Carbon::now()->format('Y-m-d') }}">Today</a>
                            </small>
                        </h3>
                        <div id="datepicker" data-date="{{ request('date')?: \Carbon\Carbon::now() }}"></div>
                        </p>
                    </div>
                {{--end search post--}}
                <!-- Blog Tags -->
                    <div class="blog-tags">
                        <h3>Tags</h3>
                        <ul class="blog-tags">
                            @if(count($tags))
                                @foreach($tags as $tag)
                                    <li>
                                        <a href="{{ route('events.tag',$tag->id) }}"
                                           class="blog-tag">{{ $tag->name }}</a>
                                    </li>
                                @endforeach
                            @else
                                <h4>There are no tags assigned to posts</h4>
                            @endif
                        </ul>
                    </div>
                    <!-- End Blog Tags -->
                    <!-- Blog Category -->
                    <div class="blog-tags">
                        <h3>Categories</h3>
                        <ul class="blog-tags">
                            @if(count($categories))
                                @foreach($categories as $category)
                                    <li>
                                        <a href="{{ route('events.category',$category->id) }}"
                                           class="blog-tag">{{ $category->name }}</a>
                                    </li>
                                @endforeach
                            @else
                                <h4>There are no categories assigned to posts</h4>
                            @endif
                        </ul>
                    </div>
                    <!-- End Blog Category -->
                    <!-- Recent Posts -->
                    <div class="recent-posts">
                        <h3>Recent events</h3>
                        <ul class="posts-list margin-top-10">
                            @foreach($recentEvents as $recentEvent)
                                <li>
                                    <div class="recent-post">
                                        <a href="{{ route('events.show', $recentEvent->id) }}">
                                            <img class="pull-left img-responsive" style="height: 50px;"
                                                 src="{{ count($recentEvent->pictures)?$recentEvent->pictures[0]->path:asset('images/events/default.png')}}"
                                                 alt="thumb1">
                                        </a>
                                        <a href="{{ route('events.show', $recentEvent->id) }}"
                                           class="posts-list-title">{{  str_limit($recentEvent->title , 40)  }}</a>
                                        <br>
                                        <span class="recent-post-date">
                                                {{ $recentEvent->created_at->diffForHumans() }}
                                            </span>
                                    </div>
                                    <div class="clearfix"></div>
                                </li>
                            @endforeach
                        </ul>
                    </div>
                    <!-- End Recent Posts -->
                </div>
                <!-- End Side Column -->
            </div>
        </div>
    </div>
    <!-- === END CONTENT === -->
@endsection

@section('scripts')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.7.1/js/bootstrap-datepicker.min.js"></script>
    <script>
        $('#datepicker').datepicker();
        $('#datepicker').on('changeDate', function () {
            location.assign('{{ route('events.index') }}' + '?date=' + $(this).datepicker('getFormattedDate'))
        });
    </script>
@endsection