@section('links')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.18.1/moment.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/fullcalendar/3.4.0/fullcalendar.min.js"></script>
    <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/fullcalendar/3.4.0/fullcalendar.min.css">
@endsection


<div id='calendar'></div>


@section('scripts')
    <script>
        $(function() {
            $('#calendar').fullCalendar({
                header: {
                    left: 'prev,next today',
                    center: 'title',
                    right: 'month,agendaWeek,agendaDay,listMonth'
                },
                eventSources: [
                    {
                        url: '/calendar/getEvents',
                    }
                ],
                editable: false,
                timeFormat: 'hh:mm a'

            })
        });
    </script>
@endsection