@extends('public.layouts.app')
@section('content')
<!-- === BEGIN CONTENT === -->
<div id="content">
    <div class="container background-white">
        <div class="row margin-vert-30">
            @include('public.calendar.calendar')
        </div>
    </div>

</div>
<!-- === END CONTENT === -->
@endsection