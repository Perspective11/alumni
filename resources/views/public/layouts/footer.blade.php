<!-- === BEGIN FOOTER === -->
<div id="content-bottom-border" class="container">
</div>
<div id="base">
    <div class="container padding-vert-30 margin-top-60">
        <div class="row">
            <!-- Contact Details -->
            <div class="col-md-4">
                <h3 class="margin-bottom-10">Contact Details</h3>
                <p>
                    <span class="fa-phone">Telephone:</span> 01434361
                    <br>
                    <span class="fa-envelope">Email:</span>
                    <a href="mailto:info@liu.edu"> info@liu.edu</a>
                    <br>
                    <span class="fa-link">Website:</span>
                    <a href="http://www.liuyemen.com"> www.liuyemen.com</a>
                    <br>
                    <span class="fa-location-arrow">Location: </span>
                    50th Street, Sana'a, Yemen
                </p>
            </div>
            <!-- End Contact Details -->
            <!-- Sample Menu -->
            <div class="col-md-4">
                <a href="http://www.liuyemen.com" target="_blank"
                   style="width: 60%;position: relative;display: block;margin: 0 auto;">
                    <img src="{{ asset('images/Logo.png') }}" alt="">
                </a>
                <div class="clearfix"></div>
            </div>
            <!-- Disclaimer -->
            <div class="col-md-4">
                <h6 style="color: #fff;">Built By:</h6>
                <p><a href="http://www.facebook.com/aiman.noman" target="_blank">Aiman Noman</a> & <a
                            href="http://www.facebook.com/ahishamali" target="_blank">Hisham Alshami</a></p>
                <h6 style="color: #fff;">Association:</h6>
                <p><a href="http://www.facebook.com/YemenRap" target="_blank">Alex Algomai</a> & <a
                            href="http://www.facebook.com/basheeradel009" target="_blank">Basheer Adel</a>
                <p>&copy; 2017 <a href="http://www.liuyemen.com" target="_blank">LIU</a></p>
            </div>
            <!-- End Disclaimer -->
            <div class="clearfix"></div>
        </div>
    </div>
</div>
<!-- === END FOOTER === -->