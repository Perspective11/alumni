{{--@TODO: remove this after deploying this in the chosen host--}}
<div style="position: fixed;background-color: white;padding: 10px;z-index: 999999">
    <a href="{{ route('aboutSys') }}"
       title="This version is a test version but all data assigned to it is real and must be">Alpha Version</a>
</div>
{{--here end--}}

{{--header space before the logo--}}
<div id="pre-header" class="container" style="height: {{ Route::currentRouteName() == 'home'?'2':'' }}40px">
</div>
{{--end header space before the logo--}}

<div data-spy="affix" data-offset-top="{{ Route::currentRouteName() == 'home'?'2':'' }}40">
    {{--header container for the logo--}}
    <div id="header">
        <div class="container">
            <div class="row">
                <!-- Logo -->
                <div class="logo col-sm-2 col-xs-6">
                    <a href="index.html" title="">
                        <img src="{{ asset('images/Logo.png') }}" alt="Logo"/>
                    </a>
                </div>
                <!-- End Logo -->

                <div class="col-sm-4 col-sm-offset-2 col-xs-6">
                    <h1 style="color: #fff !important;">{{ config('app.name') }}</h1>
                </div>
            </div>
        </div>
    </div>
{{--end header container for the logo--}}

<!-- Top Menu -->
    <nav class="navbar navbar-inverse">
        <div class="container-fluid">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
            </div>
            <div class="collapse navbar-collapse" id="myNavbar">
                <ul class="nav navbar-nav">
                    <li>
                        <a href="{{ route('home') }}">Home</a>
                    </li>
                    <li class="dropdown">
                        @if(Auth::check())
                            @if(Auth::user()->isGraduate())
                                <a class="dropdown-toggle" data-toggle="dropdown" href="#">Post<span
                                            class="caret"></span></a>
                                <ul class="dropdown-menu">
                                    <li><a href="{{ route('posts.index') }}">Posts</a></li>
                                    <li><a href="{{ route('posts.create') }}">Create</a></li>
                                </ul>
                            @else
                                <a href="{{ route('posts.index') }}">Posts</a>
                            @endif
                        @else
                            <a href="{{ route('posts.index') }}">Posts</a>
                        @endif
                    </li>
                    <li>
                        <a href="{{ route('news.index') }}">News</a>
                    </li>
                    <li class="dropdown">
                        <a class="dropdown-toggle" data-toggle="dropdown" href="#">Events<span class="caret"></span></a>
                        <ul class="dropdown-menu">
                            <li><a href="{{ route('events.index') }}">Events</a></li>
                            <li><a href="{{ route('calendar.show') }}">Calendar</a></li>
                        </ul>
                    </li>

                    <li>
                        <a href="{{ route('contact') }}">Contact</a>
                    </li>
                    @if(Auth::check())
                        @if(Auth::user()->isGraduate())
                            <li><a href="{{ route('companies.index') }}">Companies</a></li>
                            <li><a href="{{ route('graduates.index') }}">Graduates</a></li>
                        @elseif(Auth::user()->isCompany())
                            <li><a href="{{ route('graduates.index') }}">Graduates</a></li>
                        @endif
                        <li class="dropdown">
                            <a class="dropdown-toggle" data-toggle="dropdown" href="#">{{ Auth::user()->name }} <span
                                        class="caret"></span>
                                @if(auth()->user()->isGraduate())
                                    {!! auth()->user()->graduate->fullProfile() ?'':'&nbsp;&nbsp;<i title="You need to finish your profile data and upload the CV" class="fa fa-exclamation-circle text-danger"></i>' !!}
                                @endif
                            </a>
                            <ul class="dropdown-menu">
                                @if(Auth::user()->isAdministration())
                                    <li><a href="{{ url('admin') }}">Admin Panel</a></li>
                                @else
                                    <li>
                                        <a href="{{ route('profile',auth()->id()) }}">Profile
                                            @if(auth()->user()->isGraduate())
                                                {!! auth()->user()->graduate->fullProfile() ?'':'&nbsp;&nbsp;<i title="You need to finish your profile data and upload the CV" class="fa fa-exclamation-circle text-danger"></i>' !!}
                                            @endif
                                        </a>
                                    </li>
                                    <li><a href="" data-toggle="modal" data-target="#changeModal">Change Password</a>
                                    </li>
                                @endif
                                <li><a href="javascript:void(0)" onclick="$('#logout').submit();this.preventDefault();">Logout</a>
                                </li>
                                <form action="{{ url('logout') }}" method="post" id="logout">
                                    {{csrf_field()}}
                                </form>
                            </ul>
                        </li>
                    @else
                        <li>
                            <a href="{{ url('login') }}">Login</a>
                        </li>
                        <li>
                            <a href="{{ url('register') }}">Register</a>
                        </li>
                    @endif
                    @if(!Auth::check())
                        @if(config('app.debug'))
                            <li class="dropdown">
                                <a class="dropdown-toggle" data-toggle="dropdown">Login as <span
                                            class="caret"></span></a>
                                <ul class="dropdown-menu">
                                    <li>
                                        <form action="{{ url('login') }}" method="post" id="adminF">
                                            {{csrf_field()}}
                                            <input type="hidden" name="email" value="ahishamali10@gmail.com">
                                            <input type="hidden" name="password" value="199691">
                                            <button class="col-md-3 col-xs-12"
                                                    style="border: none;padding: 0;margin: 0;display: inline-block;width: 100%;background-color: transparent"
                                                    type="submit">Admin
                                            </button>
                                        </form>
                                    </li>
                                    <li>

                                        <form action="{{ url('login') }}" method="post" id="modF">
                                            {{csrf_field()}}
                                            <input type="hidden" name="email" value="aiman@gmail.com">
                                            <input type="hidden" name="password" value="123">
                                            <button class="col-md-3 col-xs-12"
                                                    style="border: none;padding: 0;margin: 0;display: inline-block;width: 100%;background-color: transparent"
                                                    type="submit">Moderator
                                            </button>
                                        </form>
                                    </li>
                                    <li>

                                        <form action="{{ url('login') }}" method="post" id="gradF">
                                            {{csrf_field()}}
                                            <input type="hidden" name="email" value="basheer@gmail.com">
                                            <input type="hidden" name="password" value="12345678">
                                            <button class="col-md-3 col-xs-12"
                                                    style="border: none;padding: 0;margin: 0;display: inline-block;width: 100%;background-color: transparent"
                                                    type="submit">Graduate
                                            </button>
                                        </form>
                                    </li>
                                    <li>

                                        <form action="{{ url('login') }}" method="post" id="comF">
                                            {{csrf_field()}}
                                            <input type="hidden" name="email" value="sobhee@gmail.com">
                                            <input type="hidden" name="password" value="12345678">
                                            <button class="col-md-3 col-xs-12"
                                                    style="border: none;padding: 0;margin: 0;display: inline-block;width: 100%;background-color: transparent"
                                                    type="submit">Company
                                            </button>
                                        </form>
                                    </li>
                                </ul>
                            </li>
                        @endif
                    @endif
                </ul>
            </div>
        </div>
    </nav>
    <!-- End Top Menu -->
</div>

{{--header space after the logo--}}
<div id="post_header" class="container" style="height:{{ Route::currentRouteName() == 'home'?'4':'' }}40px">
</div>
{{--end header space after the logo--}}
<div id="content-top-border" class="container">
</div>
<!-- === END HEADER === -->
