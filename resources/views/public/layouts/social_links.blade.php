@if($social_links)
    {{--social buttons--}}
    <ul class="social-icons pull-right hidden-xs">
        @foreach($social_links as $key => $value)
            @if(!empty($value))
                <li class="social-{{ $key }}">
                    <a href="{{ $value }}" target="_blank" title="{{ ucfirst($key) }}"></a>
                </li>
            @endif
        @endforeach
    </ul>
    {{--end social buttons--}}
@endif