<!-- === BEGIN HEADER === -->
<!DOCTYPE html>
<!--[if IE 8]>
<html lang="en" class="ie8"> <![endif]-->
<!--[if IE 9]>
<html lang="en" class="ie9"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
<!--<![endif]-->
<head>
    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-108875252-1"></script>
    <script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());

        gtag('config', 'UA-108875252-1');
    </script>

    <!-- Title -->
    <title>{{ config('app.name') }}</title>
    <!-- Meta -->
    <meta http-equiv="content-type" content="text/html; charset=utf-8"/>
    <meta name="description" content="">
    <meta name="author" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1"/>
    <!-- Favicon -->
    <link href="{{ asset('favicon.ico') }}" rel="shortcut icon">
    <!-- Bootstrap Core CSS -->
    <link rel="stylesheet" href="{{ asset('css/app.css') }}" rel="stylesheet">
    <!-- Template CSS -->
    <link rel="stylesheet" href="{{ asset('css/animate.css') }}" rel="stylesheet">
    <link rel="stylesheet" href="{{ asset('css/new/font-awesome.css') }}" rel="stylesheet">
    <link rel="stylesheet" href="{{ asset('css/new/nexus.css') }}" rel="stylesheet">
    <link rel="stylesheet" href="{{ asset('css/new/responsive.css') }}" rel="stylesheet">
    <link rel="stylesheet" href="{{ asset('css/new/custom.css') }}" rel="stylesheet">
    <script type="text/javascript" src="{{ asset('js/app.js') }}"></script>

@yield('links')
<!-- Google Fonts-->
    <link href="http://fonts.googleapis.com/css?family=Roboto+Condensed:400,300" rel="stylesheet" type="text/css">
    <link href="http://fonts.googleapis.com/css?family=PT+Sans" type="text/css" rel="stylesheet">
    <link href="http://fonts.googleapis.com/css?family=Roboto:400,300" rel="stylesheet" type="text/css">

    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/css/select2.min.css" rel="stylesheet"/>
    <style>
        .affix {
            top: 0;
            width: 100%;
            z-index: 100;
            position: fixed;
        }

        .affix + #post_header {
            margin-top: 184px;
        }

        .affix > #header {
            background-color: rgba(0, 42, 92, 1) !important;
        }

        .navbar-inverse {
            background-color: rgba(251, 176, 52, 0.65) !important;
            width: 70%;
            margin: 0 auto;
        }

        .affix .navbar-inverse {
            background-color: rgba(251, 176, 52, 1) !important;
        }

        .navbar .navbar-inverse {
            font-family: "Roboto Condensed";
        }

        .navbar .navbar-collapse {
            text-align: center;
            height: 360px;
        }

        .navbar-inverse .navbar-nav > li > a {
            color: white;
            font-size: 100%;
        }

        .navbar-inverse .navbar-nav > .open > a, .navbar-inverse .navbar-nav > .open > a:hover, .navbar-inverse .navbar-nav > .open > a:focus {
            background-color: rgba(251, 176, 52, 1) !important;
            color: #ffffff;
        }

        .navbar .navbar-nav {
            display: inline-block;
            float: none;
            vertical-align: top;
        }

        .navbar .navbar-collapse {
            text-align: center;
        }

        .navbar-inverse .navbar-toggle .icon-bar {
            background-color: white;
        }

        .dropdown-menu li {
            text-align: center;
        }

        @media (max-width: 767px) {
            .navbar-inverse .navbar-nav .open .dropdown-menu > li > a {
                color: #fff;
            }
        }

        @media (min-width: 768px) {
            .navbar-inverse {
                width: 100%;
            }
        }

        @media (min-width: 992px) {
            .navbar-inverse {
                width: 70%;
            }
        }
    </style>

    @yield('styles')

    @if(Session::has('toastr'))
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.css">
    @endif

</head>
<body>

<div id="body-bg">
    @include('public.layouts.social_links')
    @include('public.layouts.header')
    @yield('content')
    @include('public.layouts.footer')
</div>
{{--models--}}
<div class="modal fade" id="changeModal" role="dialog">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Change the Password</h4>
            </div>
            <div class="modal-body">
                <h4 class="text-success text-center" id="changePasswordS"></h4>
                <form action="{{ route('profile.change.password', auth()->id()) }}" method="post"
                      id="changePasswordForm">
                    {{ csrf_field() }}
                    <div class="form-group">
                        <input type="password" name="oldPassword" id="oldPas" class="form-control"
                               placeholder="Old Password">
                        <span class="help-block" style="color: #e51c23 !important" id="eOldPass"></span>
                    </div>
                    <div class="form-group">
                        <input type="password" name="newPassword" id="newPas" class="form-control"
                               placeholder="New Password">
                        <span class="help-block" style="color: #e51c23 !important" id="eNewPass"></span>
                    </div>
                    <div class="form-group">
                        <input type="password" name="newPassword_confirmation" id="repeatPas" class="form-control"
                               placeholder="Repeat Password">
                        <span class="help-block" style="color: #e51c23 !important" id="eCNewPass"></span>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" id="changePassword">Change</button>
                <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>

<!-- JS -->
{{--<script type="text/javascript" src="{{ asset('js/new/jquery.min.js') }}" type="text/javascript"></script>--}}
{{--<script type="text/javascript" src="{{ asset('js/new/bootstrap.min.js') }}" type="text/javascript"></script>--}}

<script type="text/javascript" src="{{ asset('js/new/scripts.js') }}"></script>
<!-- Isotope - Portfolio Sorting -->
<script type="text/javascript" src="{{ asset('js/new/jquery.isotope.js') }}" type="text/javascript"></script>
<!-- Mobile Menu - Slicknav -->
<script type="text/javascript" src="{{ asset('js/new/jquery.slicknav.js') }}" type="text/javascript"></script>
<!-- Animate on Scroll-->
<script type="text/javascript" src="{{ asset('js/new/jquery.visible.js') }}" charset="utf-8"></script>
<!-- Sticky Div -->
<script type="text/javascript" src="{{ asset('js/new/jquery.sticky.js') }}" charset="utf-8"></script>
<!-- Slimbox2-->
<script type="text/javascript" src="{{ asset('js/new/slimbox2.js') }}" charset="utf-8"></script>
{{--select2--}}
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js"></script>
{{--toastr--}}
@if(Session::has('toastr'))
    <script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.js"></script>
@endif
<!-- Modernizr -->
<script src="{{ asset('js/new/modernizr.custom.js') }}" type="text/javascript"></script>
<!-- End JS -->
@include('toastr.toastr')
<script>
    $('#changePassword').on('click', function (eve) {
        var data = $('#changePasswordForm').serialize();
        $.post('{{ route('profile.change.password', auth()->id()) }}', data, function (data, status) {
            $('#eOldPass').empty();
            $('#eNewPass').empty();
            $('#eCNewPass').empty();

            if (data.hasOwnProperty('errors')) {
                $('#eOldPass').text(data.errors.messages.oldPass);
            }

            if (data.hasOwnProperty('success')) {
                $('#changePasswordS').text(data.success.messages);

                setTimeout(function () {
                    $('#oldPas').val("");
                    $('#newPas').val("");
                    $('#repeatPas').val("");
                    $('#changePasswordS').empty();
                    $('#eOldPass').empty();
                    $('#eNewPass').empty();
                    $('#eCNewPass').empty();
                    $('#changeModal').modal('hide');
                },2000)
            }
        }).fail(function (data) {
            $('#eOldPass').empty();
            $('#eNewPass').empty();
            $('#eCNewPass').empty();

            if (data.responseJSON.hasOwnProperty('newPassword')) {
                $('#eNewPass').text(data.responseJSON.newPassword[0]);
            }
            if (data.responseJSON.hasOwnProperty('newPassword_confirmation')) {
                $('#eCNewPass').text(data.responseJSON.newPassword_confirmation[0]);
            }
        })
    });
</script>
@yield('scripts')
</body>
</html>