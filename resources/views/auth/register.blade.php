@extends('public.layouts.app')
@section('content')
    <!-- === BEGIN CONTENT === -->
    <div id="content">
        <div class="container background-white">
            <div class="row margin-vert-30">
                <!-- Register Box -->
                <div class="col-md-6 col-md-offset-3 col-xs-12">
                    <form class="signup-page" action="{{ url('register') }}" method="post">
                        {{ csrf_field() }}
                        <div class="signup-header">
                            <h2>Register a new account</h2>
                            <p>Already a member? Click
                                <a href="{{ url('login') }}">HERE </a>to login to your account.</p>
                        </div>
                        <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                            <label>Username</label>
                            <input class="form-control margin-bottom-20" name="name" type="text"
                                   value="{{ old('name') }}" autofocus>
                            @if ($errors->has('name'))
                                <span class="help-block">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                            @endif
                        </div>
                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                            <label>Email Address</label>
                            <input class="form-control margin-bottom-20" name="email" type="email"
                                   value="{{ old('email') }}">
                            @if ($errors->has('email'))
                                <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                            @endif
                        </div>
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                                    <label>Password</label>
                                    <input class="form-control margin-bottom-20" type="password" name="password">
                                    @if ($errors->has('password'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label>Confirm Password</label>
                                    <input class="form-control margin-bottom-20" type="password"
                                           name="password_confirmation">
                                </div>
                            </div>
                        </div>
                        <label>Type</label>
                        <br>
                        <label><input type="radio" id="graduate_check" name="type"
                                      value="graduate" {{ old('type') == 'graduate'?'checked':'' }}> Graduate</label>&nbsp;&nbsp;&nbsp;
                        <label><input type="radio" id="company_check" name="type"
                                      value="company" {{ old('type') == 'company'?'checked':'' }}> Company</label>
                        @if ($errors->has('type'))
                            <span class="help-block color-red">
                                    <strong>{{ $errors->first('type') }}</strong>
                                </span>
                        @endif
                        <div class="margin-bottom-20"></div>
                        {{--graduate--}}
                        <div id="graduate_section" style="display: none;">
                            <div class="form-group{{ $errors->has('liu_id') ? ' has-error' : '' }}">
                                <label>LIU ID</label>
                                <input class="form-control margin-bottom-20" type="text" name="liu_id"
                                       value="{{ old('liu_id') }}">
                                @if ($errors->has('liu_id'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('liu_id') }}</strong>
                                    </span>
                                @endif
                            </div>
                            <div class="form-group{{ $errors->has('full_name') ? ' has-error' : '' }}">
                                <label>Full Name</label>
                                <input class="form-control margin-bottom-20" type="text" name="full_name"
                                       value="{{ old('full_name') }}">
                                @if ($errors->has('full_name'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('full_name') }}</strong>
                                    </span>
                                @endif
                            </div>
                            <div class="form-group{{ $errors->has('graduation_year') ? ' has-error' : '' }}">
                                <label>Graduation Year</label>
                                <input class="form-control margin-bottom-20" type="text" name="graduation_year"
                                       value="{{ old('graduation_year') }}">
                                @if ($errors->has('graduation_year'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('graduation_year') }}</strong>
                                    </span>
                                @endif
                            </div>
                            <div class="form-group{{ $errors->has('gender') ? ' has-error' : '' }}">
                                <label>Gender</label><br>
                                <input type="radio" name="gender" value="1" {{ old('gender') == '1'?'checked':'' }}>
                                Male&nbsp;&nbsp;&nbsp;
                                <input type="radio" name="gender" value="0" {{ old('gender') == '0'?'checked':'' }}>
                                Female
                                @if ($errors->has('gender'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('gender') }}</strong>
                                    </span>
                                @endif
                            </div>
                            <div class="form-group{{ $errors->has('major') ? ' has-error' : '' }}">
                                <label>Major</label>
                                <select name="major" class="form-control" id="major">
                                    @foreach($majors as $major)
                                        <option value="{{ $major->id }}" {{ old('major') == $major->id?'selected':'' }}>{{ $major->name }}</option>
                                    @endforeach
                                </select>
                                @if ($errors->has('major'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('major') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        {{--company--}}
                        <div id="company_section" style="display: none">
                            <div class="form-group{{ $errors->has('company_name') ? ' has-error' : '' }}">
                                <label>Company Name</label>
                                <input class="form-control margin-bottom-20" type="text" name="company_name"
                                       value="{{ old('company_name') }}">
                                @if ($errors->has('company_name'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('company_name') }}</strong>
                                    </span>
                                @endif
                            </div>
                            <div class="form-group{{ $errors->has('industry') ? ' has-error' : '' }}">
                                <label>Industry</label>
                                <input class="form-control margin-bottom-20" type="text" name="industry"
                                       value="{{ old('industry') }}">
                                @if ($errors->has('industry'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('industry') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <hr>
                        <div class="row">
                            <div class="col-md-4 col-xs-12 text-center">
                                <button class="btn btn-primary" type="submit">Register</button>
                            </div>
                        </div>
                    </form>
                </div>
                <!-- End Register Box -->
            </div>
        </div>
    </div>
@endsection
@section('scripts')
    <script>
        $('#major').select2();

        if ($('#graduate_check').is(':checked')) {
            $('#graduate_section').show();
            $('#company_section').hide();
        } else if ($('#company_check').is(':checked')) {
            $('#graduate_section').hide();
            $('#company_section').show();
        }

        $('#graduate_check').click(function () {
            $('#graduate_section').slideDown();
            $('#company_section').slideUp();
        });

        $('#company_check').click(function () {
            $('#graduate_section').slideUp();
            $('#company_section').slideDown();
        });
    </script>
@endsection