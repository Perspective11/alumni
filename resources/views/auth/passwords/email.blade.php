@extends('public.layouts.app')
@section('content')
<!-- === BEGIN CONTENT === -->
<div id="content">
    <div class="container background-white">
        <div class="container">
            @if (session('status'))
                <div class="alert alert-success margin-vert-30">
                    {{ session('status') }}
                </div>
            @endif

            <div class="row margin-vert-30">
                <!-- Login Box -->
                <div class="col-md-6 col-md-offset-3 col-sm-offset-3">
                    <form class="login-page" action="{{ url('/password/email') }}" method="post">
                        {{ csrf_field() }}
                        <div class="login-header margin-bottom-30">
                            <h2>Reset your password</h2>
                        </div>
                        <div class="input-group{{ $errors->has('email') ? ' has-error' : '' }} margin-bottom-20">
                                        <span class="input-group-addon">
                                            <i class="fa fa-at"></i>
                                        </span>
                            <input placeholder="Email" class="form-control" name="email" type="email" value="{{ old('email') }}" required autofocus>
                            @if ($errors->has('email'))
                                <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                            @endif
                        </div>
                        <div class="row">
                            <div class="col-md-6 col-md-offset-6">
                                <button class="btn btn-primary pull-right" type="submit">Send reset link</button>
                            </div>
                        </div>
                    </form>
                </div>
                <!-- End Login Box -->
            </div>
        </div>
    </div>
</div>
<!-- === END CONTENT === -->
@endsection