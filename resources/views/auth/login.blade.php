@extends('public.layouts.app')
@section('content')
<!-- === BEGIN CONTENT === -->
<div id="content">
    <div class="container background-white">
            <div class="row margin-vert-30">
                <!-- Login Box -->
                <div class="col-sm-6 col-sm-offset-3 col-xs-12">
                    <form class="login-page" action="{{ url('login') }}" method="post">
                        {{ csrf_field() }}
                        <div class="login-header margin-bottom-30">
                            <h2>Login to your account</h2>
                        </div>
                        <div class="input-group{{ $errors->has('email') ? ' has-error' : '' }} margin-bottom-20">
                                        <span class="input-group-addon">
                                            <i class="fa fa-user"></i>
                                        </span>
                            <input placeholder="Email" class="form-control" name="email" type="email" value="{{ old('email') }}" autofocus>
                            @if ($errors->has('email'))
                                <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                            @endif
                        </div>
                        <div class="input-group{{ $errors->has('password') ? ' has-error' : '' }} margin-bottom-20">
                                        <span class="input-group-addon">
                                            <i class="fa fa-lock"></i>
                                        </span>
                            <input placeholder="Password" class="form-control" name="password" type="password">
                            @if ($errors->has('password'))
                                <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                            @endif
                        </div>
                        <div class="row">
                            <div class="col-md-5 col-xs-5 col-md-offset-1 col-xs-offset-1">
                                <label class="checkbox">
                                    <input type="checkbox" name="remember">
                                    Stay signed in
                                </label>
                            </div>
                            <div class="col-md-6 col-xs-6">
                                <button class="btn btn-primary pull-right" type="submit">Login</button>
                            </div>
                        </div>
                        <hr>
                        <h4>Forget your Password ?</h4>
                        <p>
                            <a href="{{ url('password/reset') }}">Click here </a>to reset your password.</p>
                    </form>
                </div>
                <!-- End Login Box -->
            </div>
    </div>
</div>
<!-- === END CONTENT === -->
@endsection