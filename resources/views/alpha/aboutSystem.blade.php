@extends('public.layouts.app')
@section('content')
    <!-- === BEGIN CONTENT === -->
    <div id="content">
        <div class="container background-white">
            <div class="row margin-vert-30">
                <div class="col-md-12">
                    <div class="error-404-page text-center">
                        <h3>About the System</h3>
                        <p style="line-height: 25px;text-align: left">
                            This web application has been built as a student project in the IT major, and therefore has been considered the final project for Senior 1 as well as Senior 2.
                            <br>
                            LIU Yemen didn’t have a way of communicating with graduates or bringing them together, and that’s the reason why we constructed the system, to give them a platform to share and reunite.
                            <br>
                            However, the web app, at the moment, is in the piloting phase, where it’s just had been put up for real life environment. You can say it’s still under intensive testing. Therefore, you might expect to find a few bugs here and there, and when you do, please report the bug in the form attached to the website.
                            <br>
                            Keep in mind, while the web app is being tested, the data kept here is real and should be real. So PLEASE, the data entered into the system should be real and complete. Otherwise, you won’t be verified.
                            <br>
                            We need your feedback. Tell us about your experience with using the website. Tell us what we could do to improve it. And tell us what problems you have faced during your inspection of the alumni web app.
                            <br>
                            <br>
                            Sincerely,<br>
                            The Developers,<br>
                            Hisham Al-Shami, Aiman Noman
                        </p>
                        <a href="https://goo.gl/forms/uZcdg33jSIEFeN7n1" target="_blank">Your Feedbacks <i class="fa fa-rss"></i></a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- === END CONTENT === -->
@endsection