<?php

/*
|--------------------------------------------------------------------------
| Frontend Routes
|--------------------------------------------------------------------------
|
| frontend UI routes for regular users and visitors
|
*/

/** @TODO: remove this after deploying this in the chosen host */
Route::get('/about-system', function(){
    return view('alpha.aboutSystem');
})->name('aboutSys');
/** here end */

Route::get('/test', function (Request $request) {
})->name('test');


Route::get('/calendar/getEvents', 'indexController@getEvents')->name('calendar.get');
Route::get('/calendar', 'indexController@showCalendar')->name('calendar.show');


Route::get('/','indexController@index')->name('home');
Route::get('/contact','indexController@contact')->name('contact');
Route::post('/contact','indexController@storeContact')->name('contact.store');
Route::get('/profile/{user}','indexController@profile')->name('profile')->middleware('regular');
Route::get('/profile/{user}/edit','indexController@profileEdit')->name('profile.edit')->middleware('regular');

Route::post('profile/{user}/change-password', 'indexController@changePassword')->name('profile.change.password')->middleware('regular');
Route::get('/profile/{user}/download/{graduate}','indexController@profileDownload')->name('profile.download.cv')->middleware('regular');
Route::get('/profile/{user}/view/{graduate}','indexController@profileView')->name('profile.view.cv')->middleware('regular');
Route::put('/profile/{user}/graduate','indexController@profileGUpdate')->name('profile.GUpdate')->middleware('regular');
Route::put('/profile/{user}/company','indexController@profileCUpdate')->name('profile.CUpdate')->middleware('regular');

Route::resource('/posts', 'PostsController');
Route::get('/posts/tag/{tag}', 'PostsController@tag')->name('posts.tag');
Route::get('/posts/category/{category}', 'PostsController@category')->name('posts.category');
Route::post('/posts/{post_id}/comments', 'CommentController@store')->name('postComments.store')->middleware('graduate');

Route::resource('/graduates', 'GraduatesController');
Route::get('/graduates/major/{major}', 'GraduatesController@major')->name('graduates.major')->middleware('regular');
Route::resource('/companies', 'CompaniesController');
Route::get('companies/industry/{industry}','CompaniesController@industry')->name('companies.industry')->middleware('regular');

Route::resource('/news', 'NewsController');
Route::get('/news/tag/{tag}', 'NewsController@tag')->name('news.tag');
Route::get('/news/category/{category}', 'NewsController@category')->name('news.category');
Route::post('/news/{news_id}/comments', 'CommentController@store')->name('newsComments.store')->middleware('graduate');

Route::resource('/events', 'EventsController');
Route::get('/events/tag/{tag}', 'EventsController@tag')->name('events.tag');
Route::get('/events/category/{category}', 'EventsController@category')->name('events.category');
Route::post('/events/{event_id}/comments', 'CommentController@store')->name('eventComments.store')->middleware('graduate');


//update a comment
Route::put('comments/{comment_id}', 'CommentController@update')->name('comments.update');
Route::delete('comments/{comment_id}', 'CommentController@delete')->name('comments.delete');

/*
|--------------------------------------------------------------------------
| Auth Routes
|--------------------------------------------------------------------------
|
|   login, register, reset, and forget password
|
*/

Auth::routes();

/*
|--------------------------------------------------------------------------
| Admin side, Routes
|--------------------------------------------------------------------------
|
| Here is where you can register admin routes. These
| routes are loaded when the user is authenticated within admin prefix group which
| contains the "web" middleware group.
|
*/

Route::group(['prefix' => 'admin','middleware' => 'administration', 'as' => 'admin.'], function(){


    Route::get('/', function () {
        return redirect('/admin/dashboard');
    });
    Route::get('/dashboard', 'DashboardController@index');

    Route::get('/profile', 'DashboardController@profile');

    // Calendar Routes
    Route::get('/calendar/getEvents', 'CalendarController@getEvents');
    Route::post('/calendar/updateEvent', 'CalendarController@updateEvent');
    Route::get('/calendar', 'CalendarController@index');

    // Settings Routes
    Route::get('/settings', 'SettingsController@show');
    Route::get('/settings/edit', 'SettingsController@edit');
    Route::post('/settings/storeImages', 'SettingsController@storeImages');
    Route::delete('/settings/deleteImage', 'SettingsController@deleteImage');
    Route::post('/settings/', 'SettingsController@update');



//    admin post routes
    Route::get('/posts/getData', 'PostController@getPostsData');
    Route::get('posts/{post}/activation', 'PostController@activation');
    Route::resource('posts','PostController');

//    admin news routes
    Route::get('/news/getData', 'NewController@getNewsData');
    Route::get('news/{news}/activation', 'NewController@activation');
    Route::resource('news', 'NewController');

//    admin event routes
    Route::get('/events/getData', 'EventController@getEventsData');
    Route::get('events/{event}/activation', 'EventController@activation');
    Route::resource('events','EventController');

//    admin category routes
    Route::get('/categories/getData', 'CategoryController@getCategoriesData');
    Route::resource('categories','CategoryController', ['except' => [
        'show'
    ]]);

//    admin major routes
    Route::get('/majors/getData', 'MajorController@getMajorsData');
    Route::resource('majors','MajorController', ['except' => [
        'show'
    ]]);

//    admin tags routes
    Route::get('/tags/getData', 'TagsController@getTagsData');
    Route::resource('tags','TagsController', ['except' => [
        'show'
    ]]);

//    admin user routes
    Route::get('/users/getData', 'UsersController@getUsersData');
    Route::get('users/{user}/activation','UsersController@activation');
    Route::get('users/{user}/changeRole','UsersController@changeRoleShow');
    Route::patch('users/{user}/changeRole','UsersController@changeRoleStore');
    Route::get('/changePassword','UsersController@changePassword');
    Route::post('/changePassword','UsersController@changePasswordStore');
    Route::get('users/{user}/uploadResumeShow','UsersController@uploadResumeShow');
    Route::post('users/{user}/uploadResumeStore','UsersController@uploadResumeStore');
    Route::post('users/{user}/downloadResume','UsersController@downloadResume');
    Route::delete('users/{user}/deleteResume','UsersController@deleteResume');
    Route::resource('users','UsersController');

//    admin graduate routes
    Route::get('graduates/{graduate}/changeRole','GraduateController@changeRoleShow');
    Route::get('/graduates/getData', 'GraduateController@getGraduatesData');
    Route::resource('graduates','GraduateController');

//    admin company routes
    Route::get('companies/{company}/changeRole','CompanyController@changeRoleShow');
    Route::get('/companies/getData', 'CompanyController@getCompaniesData');
    Route::resource('companies', 'CompanyController');

    Route::get('setting', 'SettingsController@imageStore')->name('admin.setting.imageStore');


    Route::get('/contacts/getData', 'ContactController@getContactsData');
    Route::delete('/contacts/{contact}', 'ContactController@destroy');
    Route::get('/contacts', 'ContactController@index');


});
