<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

/** @var \Illuminate\Database\Eloquent\Factory $factory */
use App\Major;

$factory->define(App\User::class, function (Faker\Generator $faker) {
    static $password;

    return [
        'name' => $faker->unique()->userName,
        'user_status' => $faker->numberBetween(0,1),
        'email' => $faker->unique()->email,
        'password' => $password ?: $password = bcrypt('secret'),
        'remember_token' => str_random(10),
    ];
});

$factory->define(App\Post::class, function(\Faker\Generator $faker){
    return [
        'user_id' => $faker->randomElement(\App\User::has('graduate')->pluck('id')->toArray()),
        'category_id' => $faker->randomElement(\App\Category::all()->pluck('id')->toArray()),
        'major_id' => $faker->randomElement(\App\Major::all()->pluck('id')->toArray()),
        'title' => $faker->sentence(),
        'body' => $faker->paragraph(20),
        'status' => $faker->boolean(70),
    ] ;
});

$factory->define(App\Event::class, function(\Faker\Generator $faker){
    return [
        'parent_id' => 0,
        'user_id' => '1',
        'category_id' => $faker->randomElement(\App\Category::all()->pluck('id')->toArray()),
        'major_id' => $faker->randomElement(\App\Major::all()->pluck('id')->toArray()),
        'title' => $faker->sentence(),
        'body' => $faker->paragraph(20),
        'type' => $faker->numberBetween(1,2),
        'location' => $faker->address,
        'date' => $faker->dateTimeBetween('+4 days', '+1 years'),
        'start' => $faker->time(),
        'end' => $faker->time(),
        'status' => $faker->boolean(70),
    ] ;
});

$factory->define(\App\News::class, function(\Faker\Generator $faker){
    return [
        'category_id' => $faker->randomElement(\App\Category::all()->pluck('id')->toArray()),
        'major_id' => $faker->randomElement(\App\Major::all()->pluck('id')->toArray()),
        'user_id' => '1',
        'title' => $faker->sentence,
        'body' => $faker->paragraph(20),
        'status' => $faker->boolean(70),
    ] ;
});

$factory->define(App\Comment::class, function(\Faker\Generator $faker){
    return [
        'user_id' => $faker->randomElement(\App\User::all()->pluck('id')->toArray()),
        'body' => $faker->paragraph(20),
    ];
});

$factory->define(App\Company::class, function (Faker\Generator $faker) {
    return [
        'company_name' => $faker->company,
        'industry' => $faker->words(3, true),
        'company_status' => $faker->randomElement(['active','blocked','inactive','verified','unverified']),
        'type' => $faker->optional()->words(3, true),
        'founded_at' => $faker->optional()->numberBetween(1850 , 2017),
        'short_description' => $faker->optional()->text(),
        'full_description' => $faker->optional()->text(),
        'size' => $faker->optional()->numberBetween(100, 1000) . ' - ' . $faker->numberBetween(100, 1000) ,
        'headquarters' => $faker->optional()->country,
        'address_head' => $faker->optional()->address,
        'address_1' => $faker->optional()->address,
        'address_2' => $faker->optional()->address,
        'address_3' => $faker->optional()->address,
        'address_4' => $faker->optional()->address,
        'website_link' => $faker->optional()->domainName,
        'facebook_link' => $faker->optional()->url,
        'twitter_link' => $faker->optional()->url,
        'linkedin_link' => $faker->optional()->url,

    ];
});
$factory->define(App\Graduate::class, function (Faker\Generator $faker) {
    return [
        'liu_id' => $faker->unique()->numberBetween(61000000, 61999999),
        'major_id' => $faker->randomElement(\App\Major::all()->pluck('id')->toArray()),
        //'user_id' => factory(App\User::class)->create()->id  ,
        'full_name' => $faker->name,
        'graduation_year' => $faker->numberBetween(2000 , 2017),
        'gender' => $faker->boolean(),
        'graduate_status' => $faker->randomElement(['active','blocked','inactive','verified','unverified']),
        'years_of_experience' => $faker->optional()->numberBetween(0,10),
        'phone' => $faker->optional()->numberBetween(700000000,770000000),
        'dob' => $faker->optional()->dateTimeBetween(),
        'perm_address' => $faker->optional()->address,
        'curr_address' => $faker->optional()->streetAddress,
        'origin' => $faker->optional()->country,
        'is_studying' => $faker->optional()->boolean(),
        'field_of_specialty' => $faker->optional()->words(3, true),
        'edu_level' => $faker->optional()->randomElement(['Masters', 'Bachelors', 'MBA', 'Assis. Prof.', 'Prof']),
        'gpa' => $faker->optional()->randomFloat(1,1,4),
        'english_pro' => $faker->optional()->randomElement(['Moderate', 'Perfect', 'Native', 'Beginner', 'Good']),
        'languages' => implode(", ", $faker->randomElements(['English' , 'Arabic', 'German', 'French' , 'Italian'], 2)) ,
        'is_employed' => $faker->optional()->boolean(),
        'job_title' => $faker->optional()->jobTitle,
        'emp_place' => $faker->optional()->company,
        'field_of_work' => $faker->optional()->words(4, true),
        'is_hireable' => $faker->optional()->boolean(),
        'description' => $faker->optional()->text(),
        'interests' => $faker->optional()->text(),
        'achievements' => $faker->optional()->text(),
        'volunteer' => $faker->optional()->text(),
        'goal' => $faker->optional()->text(),
        'website_link' => $faker->optional()->domainName,
        'facebook_link' => $faker->optional()->url,
        'twitter_link' => $faker->optional()->url,
        'instagram_link' => $faker->optional()->url,
        'github_link' => $faker->optional()->url,
        'linkedin_link' => $faker->optional()->url,
        'dribble_link' => $faker->optional()->url,

    ];
});

$factory->define(App\Contact::class, function(\Faker\Generator $faker){
    return [
        'name' => $faker->name,
        'email' => $faker->email,
        'message' => $faker->paragraphs(2, true)
    ];
});