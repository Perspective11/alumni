<?php

use App\Picture;
use App\Role;
use App\User;
use App\Tag;
use App\Category;
use App\Major;
use Faker\Factory as Faker;

use App\Graduate;
use Illuminate\Database\Seeder;
class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UsersTableSeeder::class);
        $faker = Faker::create();
        Eloquent::unguard();


        // to empty all the tables in the database
//        $tables = array_except(DB::select('SHOW TABLES'), ['migrations']);
//        foreach ($tables as $table) {
//            if ($table->Tables_in_alumni)
//                DB::table($table->Tables_in_alumni)->truncate();
//        }


        Category::create(["name" => "none" , "parent_id" => 0]);

        Category::create(["name" => "Business" , "parent_id" => 0]);
        Category::create(["name" => "Marketing" , "parent_id" => 0]);
        Category::create(["name" => "ECommerce" , "parent_id" => 0]);
        Category::create(["name" => "Job Search" , "parent_id" => 0]);
        Category::create(["name" => "Computer Science" , "parent_id" => 0]);
        Category::create(["name" => "Technology" , "parent_id" => 0]);
        Category::create(["name" => "Culture" , "parent_id" => 0]);
        Category::create(["name" => "Charity" , "parent_id" => 0]);
        Category::create(["name" => "Activities" , "parent_id" => 0]);
        Category::create(["name" => "Travel" , "parent_id" => 0]);
        Category::create(["name" => "Social" , "parent_id" => 0]);
        Category::create(["name" => "Community" , "parent_id" => 0]);
        Category::create(["name" => "Gathering" , "parent_id" => 0]);
        Category::create(["name" => "Discussion" , "parent_id" => 0]);
        Category::create(["name" => "IT" , "parent_id" => 0]);

        $this->command->info("Categories table seeded!");


        Major::create(["name" => "none"]);

        Major::create(["name" => "Information Technology"]);
        Major::create(["name" => "Management Information Systems"]);
        Major::create(["name" => "Business Administration"]);
        Major::create(["name" => "International Business"]);
        Major::create(["name" => "Banking and Finance"]);
        Major::create(["name" => "Graphics"]);
        Major::create(["name" => "Interior Design"]);
        Major::create(["name" => "Clinical Pharmacy"]);
        Major::create(["name" => "Architecture"]);
        Major::create(["name" => "Engineering"]);
        $this->command->info("Majors table seeded!");


        Role::create(["name" => "admin"]);
        Role::create(["name" => "moderator"]);
        Role::create(["name" => "graduate"]);
        Role::create(["name" => "company"]);
        $this->command->info("Roles table seeded!");


        Tag::create(["name" => "Javascript"]);
        Tag::create(["name" => "HTML"]);
        Tag::create(["name" => "Trade"]);
        Tag::create(["name" => "Economy"]);
        Tag::create(["name" => "Math"]);
        Tag::create(["name" => "Language"]);
        Tag::create(["name" => "Culture"]);
        Tag::create(["name" => "Art"]);
        Tag::create(["name" => "Science"]);
        Tag::create(["name" => "Psychology"]);
        Tag::create(["name" => "Architecture"]);
        Tag::create(["name" => "Accounting"]);
        Tag::create(["name" => "International"]);
        Tag::create(["name" => "National"]);
        Tag::create(["name" => "Ceremony"]);
        Tag::create(["name" => "Invite"]);
        Tag::create(["name" => "Message"]);
        Tag::create(["name" => "MIS"]);
        Tag::create(["name" => "Business Intelligence"]);
        Tag::create(["name" => "TEFL"]);
        Tag::create(["name" => "Student Center"]);
        Tag::create(["name" => "Islamic"]);
        Tag::create(["name" => "Literature"]);
        Tag::create(["name" => "Fitness"]);
        Tag::create(["name" => "Finance"]);
        Tag::create(["name" => "TQM"]);
        Tag::create(["name" => "Operations"]);
        Tag::create(["name" => "Pharmacy"]);
        Tag::create(["name" => "Medicine"]);
        Tag::create(["name" => "Physics"]);
        Tag::create(["name" => "Biology"]);
        Tag::create(["name" => "Chemistry"]);
        Tag::create(["name" => "Calculus"]);
        Tag::create(["name" => "Database"]);
        Tag::create(["name" => "AI"]);
        Tag::create(["name" => "Discussion"]);
        Tag::create(["name" => "Comedy"]);
        Tag::create(["name" => "Exchange"]);
        Tag::create(["name" => "Nutrition"]);
        Tag::create(["name" => "Data Structures"]);
        Tag::create(["name" => "Micro Economics"]);
        Tag::create(["name" => "Nanotech"]);
        Tag::create(["name" => "Web Design"]);
        Tag::create(["name" => "Motion Graphics"]);
        Tag::create(["name" => "Video Editing"]);
        Tag::create(["name" => "Poll"]);
        Tag::create(["name" => "Review"]);
        Tag::create(["name" => "Tag"]);
        Tag::create(["name" => "Post"]);
        $this->command->info("Tags table seeded!");




//
//        factory(User::class, 100)->create();
//        factory(Graduate::class, 100)->create();
//        factory(\App\Graduate::class, 100)->create();
        factory(App\User::class, 100)->create()->each(function ($u) {  //only works when the user id is nullable
            $u->graduate()->save(factory(App\Graduate::class)->make());
            $u->roles()->attach(3);
        });

        factory(App\User::class, 100)->create()->each(function ($u) {  //only works when the user id is nullable
            $u->company()->save(factory(App\Company::class)->make());
            $u->roles()->attach(4);

        });


        $this->command->info("Users table seeded!");




        factory(\App\Post::class, 100)->create();
        $this->command->info("Posts table seeded!");

        factory(\App\Event::class, 100)->create();
        $this->command->info("Events table seeded!");

        factory(\App\News::class, 69)->create();
        $this->command->info("News table seeded!");


        DB::table('users')->where('id',1)->update([
            'name' => 'hisham ali',
            'email' => 'ahishamali10@gmail.com',
            'user_status' => 1,
            'password' => bcrypt('199691'),
        ]);
        $this->command->info("admin user created");

        DB::table('users')->where('id',2)->update([
            'name' => 'aiman ali',
            'email' => 'aiman@gmail.com',
            'user_status' => 1,
            'password' => bcrypt('123'),
        ]);
        $this->command->info("moderator user created");

        DB::table('users')->where('id',3)->update([
            'name' => 'basheer',
            'email' => 'basheer@gmail.com',
            'user_status' => 1,
            'password' => bcrypt('123'),
        ]);
        $this->command->info("graduate user created");

        DB::table('users')->where('id',101)->update([
            'name' => 'sobhee',
            'email' => 'sobhee@gmail.com',
            'user_status' => 1,
            'password' => bcrypt('123'),
        ]);
        $this->command->info("company user created");

        DB::table('role_user')->where('user_id','1')->update([
            'role_id' => '1'
        ]);
        $this->command->info("admin role assigned");

        DB::table('role_user')->where('user_id','2')->update([
            'role_id' => '2'
        ]);
        $this->command->info("moderator role assigned");


        DB::table('graduates')->where('user_id','1')->delete();
        DB::table('graduates')->where('user_id','2')->delete();
        $this->command->info("fixing the users");

        $default_graduate = Picture::create([
            'name' => 'default.png',
            'path' => '/images/graduates/profile/default.png'
        ]);
        $default_company = Picture::create([
            'name' => 'default.jpg',
            'path' => '/images/companies/profile/default.jpg'
        ]);

        $default_post = Picture::create([
            'name' => 'default.png',
            'path' => '/images/posts/default.png'
        ]);
        $default_event = Picture::create([
            'name' => 'default.png',
            'path' => '/images/events/default.png'
        ]);
        $default_news = Picture::create([
            'name' => 'default.png',
            'path' => '/images/news/default.png'
        ]);

        foreach(Graduate::all() as $graduate){
            $graduate->pictures()->attach($default_graduate->id);
        }
        $this->command->info("graduates default image assigned");

        foreach(\App\Company::all() as $company){
            $company->pictures()->attach($default_company->id);
        }
        $this->command->info("companies default image assigned");


        foreach(\App\Post::all() as $post){
            $post->pictures()->attach($default_post->id);
            $post->tags()->attach($faker->randomElements(\App\Tag::all()->pluck('id')->toArray(), 4));
            factory('App\Comment', 10)->create(['commentable_id' => $post->id, 'commentable_type' => 'App\Post']);
        }
        $this->command->info("posts default image, random tags, and random comments assigned");

        foreach(\App\News::all() as $news){
            $news->pictures()->attach($default_news->id);
            $news->tags()->attach($faker->randomElements(\App\Tag::all()->pluck('id')->toArray(), 4));
            factory('App\Comment', 10)->create(['commentable_id' => $news->id, 'commentable_type' => 'App\News']);
        }

        $this->command->info("news default image, random tags, and random comments assigned");

        foreach(\App\Event::all() as $event){
            $event->pictures()->attach($default_event->id);
            $event->tags()->attach($faker->randomElements(\App\Tag::all()->pluck('id')->toArray(), 4));
            factory('App\Comment', 10)->create(['commentable_id' => $event->id, 'commentable_type' => 'App\Event']);
        }
        $this->command->info("events default image, random tags, and random comments assigned");

        \App\Setting::create(['name'=> 'social_links','properties'=>'{"facebook":"http://www.fb.com","twitter":"http://www.twitter.com","youtube":"http://www.youtube.com"}']);
        \App\Setting::create(['name'=> 'contact','properties'=>'{"header":"At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti quos dolores et quas feugiat. Et harum quidem rerum facilis est et expedita distinctio lorem ipsum dolor sit amet, consectetur adipiscing elit landitiis.","info":"<p>Lorem ipsum dolor sit amet, no cetero voluptatum est, audire sensibus maiestatis vis et. Vitae audire prodesset an his. Nulla ubique omnesque in sit.</p><ul class=\"list-unstyled\"><li><i class=\"fa fa-phone color-primary\"><\/i> +353-44-55-66<\/li><li><i class=\"fa fa-envelope color-primary\"><\/i> info@example.com<\/li><li><i class=\"fa fa-home color-primary\"><\/i> http:\/\/www.example.com<\/li><\/ul><ul class=\"list-unstyled\"><li><strong class=\"color-primary\">Monday-Friday:<\/strong>9am to 6pm<\/li><li><strong class=\"color-primary\">Saturday:<\/strong>10am to 3pm<\/li><li><strong class=\"color-primary\">Sunday:<\/strong>Closed<\/li><\/ul>"}']);
        \App\Setting::create(['name'=> 'home','properties'=>'{"slider":["images/slideshow/slide1.jpg","images/slideshow/slide2.jpg","images/slideshow/slide3.jpg","images/slideshow/slide4.jpg","images/slideshow/LIU_campus.jpg"]}']);
        $this->command->info("Setting data has been inserted");

        factory(\App\Contact::class, 50)->create();
        $this->command->info("Contacts table seeded!");


        $this->command->info("#--finish--#");

    }
}
