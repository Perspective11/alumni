<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEventsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('events', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('parent_id')->default(0)->unsigned()->nullable()->index();
            $table->integer('user_id')->unsigned()->index();
            $table->integer('category_id')->unsigned()->nullable()->index();
            $table->integer('major_id')->unsigned()->nullable()->index();
            $table->string('title');
            $table->text('body');
            $table->string('type')->nullable();
            $table->string('location')->nullable();
            $table->date('date');
            $table->time('start')->nullable();
            $table->time('end')->nullable();

            $table->string('status')->default('1');
            $table->timestamps();

            $table->foreign('user_id')
                ->references('id')->on('users')
                ->onDelete('cascade');

            $table->foreign('major_id')->references('id')->on('majors')->onDelete('set null');
            $table->foreign('category_id')->references('id')->on('categories')->onDelete('set null');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('events');
    }
}
