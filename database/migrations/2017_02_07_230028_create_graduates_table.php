<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGraduatesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('graduates', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->nullable()->unsigned()->index(); //TODO: fix this (nullable)
            $table->string('liu_id')->unique();
            $table->integer('major_id')->unsigned()->index();

            $table->string('full_name');
            $table->string('graduation_year', 4);
            $table->boolean('gender');
            $table->string('graduate_status')->default('unverified');
            $table->string('file_path')->nullable();
            $table->integer('years_of_experience')->nullable();
            $table->string('phone')->nullable();
            $table->date('dob')->nullable();
            $table->string('perm_address')->nullable();
            $table->string('curr_address')->nullable();
            $table->string('origin')->nullable();
            $table->boolean('is_studying')->nullable();
            $table->string('field_of_specialty')->nullable();
            $table->string('edu_level')->nullable();
            $table->decimal('gpa',2,1)->nullable();
            $table->string('english_pro')->nullable();
            $table->string('languages')->nullable();
            $table->boolean('is_employed')->nullable();
            $table->string('job_title')->nullable();
            $table->string('emp_place')->nullable();
            $table->string('field_of_work')->nullable();
            $table->boolean('is_hireable')->nullable();
            $table->text('description')->nullable();
            $table->text('interests')->nullable();
            $table->text('achievements')->nullable();
            $table->text('volunteer')->nullable();
            $table->text('goal')->nullable();
            $table->string('website_link')->nullable();
            $table->string('facebook_link')->nullable();
            $table->string('twitter_link')->nullable();
            $table->string('instagram_link')->nullable();
            $table->string('github_link')->nullable();
            $table->string('linkedin_link')->nullable();
            $table->string('dribble_link')->nullable();
            $table->string('pinterest_link')->nullable();
            $table->timestamps();

            $table->foreign('user_id')
                ->references('id')->on('users')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('graduates');
    }
}
