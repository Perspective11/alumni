const elixir = require('laravel-elixir');
var BrowserSync = require('laravel-elixir-browsersync2');



/*
 |--------------------------------------------------------------------------
 | Elixir Asset Management
 |--------------------------------------------------------------------------
 |
 | Elixir provides a clean, fluent API for defining some basic Gulp tasks
 | for your Laravel application. By default, we are compiling the Sass
 | file for your application as well as publishing vendor resources.
 |
 */

elixir((mix) => {
    mix.copy('node_modules/font-awesome/fonts','public/fonts');

    mix.sass('app.scss')
        .sass('admin.scss')
        .webpack('app.js');
});

elixir((mix) => {
    BrowserSync.init();
    mix.BrowserSync({
        proxy : "alumni.dev"
    });
});