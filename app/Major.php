<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Major extends Model
{
    //
    protected $fillable = [
        "name",
        "code",
        "color",
        "icon",
        "credits",
    ];

    public function posts()
    {
        return $this->hasMany('App\Post');
    }
    public function news()
    {
        return $this->hasMany('App\News');
    }
    public function events()
    {
        return $this->hasMany('App\Event');
    }
    public function graduates()
    {
        return $this->hasMany('App\Graduate');
    }
    public function path($admin = false){
        if($admin)
            return '/admin/majors/' . $this->id;

        return '/major/' . $this->id;
    }
}
