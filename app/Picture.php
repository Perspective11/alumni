<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Intervention\Image\Facades\Image;

class Picture extends Model
{
    //
    protected $fillable = [
        "name",
        "path",
        "description"
    ];

    public function posts()
    {
        return $this->morphedByMany('App\Post', 'imageable');
    }
    public function users()
    {
        return $this->morphedByMany('App\User', 'imageable');
    }
    public function news()
    {
        return $this->morphedByMany('App\News', 'imageable');
    }
    public function events()
    {
        return $this->morphedByMany('App\Event', 'imageable');
    }

    public static function modifyImage($image, $width = 1000){
        $picture = Image::make($image->getRealPath());
        $picture->resize(null, $width, function ($constraint) {
            $constraint->aspectRatio();
            $constraint->upsize();
        });
        // TODO:optimize image here
        return $picture;
    }
    public static function getSanitizedName($image){

        $extension = $image->guessExtension();
        $fileName = pathinfo($image->getClientOriginalName(), PATHINFO_FILENAME);
        $sanitizedName = str_limit( preg_replace( '/[^a-z0-9]+/', '-', strtolower( $fileName)), 50 , '');
        $imageName = Carbon::now()->timestamp . '_' . $sanitizedName . '.' . $extension;
        return $imageName;
    }
}
