<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

trait StatisticsFunctions
{
    public static function increasePercentage($durationDays = 30)
    {
        $count = static::count();
        $date = Carbon::now()->addDays(-$durationDays);
        $countNew = static::where('created_at', '>=', $date->toDateString())->count();
        if(! $count){
            return 0;
        }
        $value = ($countNew / $count) * 100;
        return round($value, 2);
    }

    public static function increaseDuringMonth($month)
    {
        $date = Carbon::create(Carbon::now()->year , $month, 1);
        $count = static::where('created_at', '<=', $date->addMonth()->toDateString())->count();

        return $count;
    }
}
