<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Setting extends Model
{
    protected $fillable = ['name', 'properties'];

    public static function links()
    {
        $linksArray = (array)json_decode(Setting::where('name','social_links')->first()->properties);
        $c = collect([]);
        if ($linksArray['facebook']) {
            $c->put('facebook', [
                'site'  => 'facebook',
                'icon'  => 'facebook-official',
                'color' => '#3b5998',
                'link'  => $linksArray['facebook'],
            ]);
        }
        if ($linksArray['twitter']) {
            $c->put('twitter',[
                'site'  => 'twitter',
                'icon'  => 'twitter',
                'color' => '#1da1f2',
                'link'  => $linksArray['twitter'],
            ]);
        }

        if ($linksArray['youtube']) {
            $c->put('youtube', [
                'site'  => 'youtube',
                'icon'  => 'youtube-play',
                'color' => '#cd201f',
                'link'  => $linksArray['youtube'],
            ]);
        }

        return $c;
    }
    public static function contact(){
        $contact = (array)json_decode(Setting::where('name','contact')->first()->properties);
        return $contact;
    }

    public static function sliderImages(){
        $home = (array)json_decode(Setting::where('name','home')->first()->properties);
        return $home['slider'];
    }
}
