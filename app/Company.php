<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Company extends Model
{
    use StatisticsFunctions;

    protected $fillable = [
        'company_name',
        'short_description',
        'full_description',
        'headquarters',
        'industry',
        'size',
        'founded_at',
        'type',
        'website_link',
        'facebook_link',
        'twitter_link',
        'linkedin_link',
        'address_head',
        'address_1',
        'address_2',
        'address_3',
        'address_4',
    ];

    protected $dates = [
    ];

    public function path(){
        return '/companies/' . $this->id;
    }

    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function pictures()
    {
        return $this->morphToMany('App\Picture', 'imageable');
    }

    public function noOfEmptyFields(){
        $emptyValues = 0;
        $arr = array_values( $this->toArray() );
        foreach ($arr as $value){
            if (empty($value)){
                $emptyValues ++;
            }
        }
        return $emptyValues;
    }

    public function links(){
        $c = collect([]);
        if ($this->website_link){
            $c->push([
                'site' => 'website',
                'icon' => 'edge',
                'color' => '#34bf49',
                'link' => $this->website_link,
            ]);
        }
        if ($this->facebook_link) {
            $c->push([
                'site' => 'facebook',
                'icon' => 'facebook-official',
                'color' => '#3b5998',
                'link' => $this->facebook_link,
            ]);
        }
        if ($this->twitter_link) {
            $c->push([
                'site' => 'twitter',
                'icon' => 'twitter',
                'color' => '#1da1f2',
                'link' => $this->twitter_link,
            ]);
        }

        if ($this->linkedin_link) {
            $c->push([
                'site' => 'linkedin',
                'icon' => 'linkedin',
                'color' => '#0077b5',
                'link' => $this->linkedin_link,
            ]);
        }

        return $c;
    }

    public static function activatedPercentage($days = 30)
    {
        $date = Carbon::now()->addDays(-$days);

        $count = static::where('created_at', '>=', $date->toDateString())->count();
        $activatedCount = static::where('created_at', '>=', $date->toDateString())->whereHas('user', function ($query) {
            $query->where('user_status', '=', 1);
        })->count();

        return compact('activatedCount', 'count');
    }
}
