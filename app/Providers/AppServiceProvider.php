<?php

namespace App\Providers;

use App\Setting;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider {

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        try {
            if (Setting::where('name', 'social_links')->count()){

            view()->share('social_links', (array)json_decode(Setting::where('name', 'social_links')->first()->properties));
            
        } else
            view()->share('social_links', null);
            
        } catch (Exception $e) {
            
        }
        

    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
        if ($this->app->isLocal())
        {
            $this->app->register(\Barryvdh\Debugbar\ServiceProvider::class);

        }
    }
}
