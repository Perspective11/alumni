<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

//TODO: Change name to Alumni
class Graduate extends Model {

    use StatisticsFunctions;
    protected $fillable = [
        'liu_id',
        'user_id',
        'major_id',
        'full_name',
        'graduation_year',
        'years_of_experience',
        'phone',
        'gender',
        'file_path',
        'dob',
        'perm_address',
        'curr_address',
        'origin',
        'is_studying',
        'field_of_specialty',
        'edu_level',
        'gpa',
        'english_pro',
        'languages',
        'is_employed',
        'job_title',
        'emp_place',
        'field_of_work',
        'is_hireable',
        'description',
        'interests',
        'achievements',
        'volunteer',
        'goal',
        'website_link',
        'facebook_link',
        'twitter_link',
        'instagram_link',
        'github_link',
        'linkedin_link',
        'dribble_link',
    ];

    protected $dates = [
        'dob',
    ];


    public function major()
    {
        return $this->belongsTo('App\Major');
    }


    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function pictures()
    {
        return $this->morphToMany('App\Picture', 'imageable');
    }

    public function path()
    {
        return '/graduates/' . $this->id;
    }

    public function noOfEmptyFields()
    {
        $emptyValues = 0;
        $arr = array_values($this->toArray());
        foreach ($arr as $value)
        {
            if (empty($value))
            {
                $emptyValues++;
            }
        }

        return $emptyValues;
    }

    public function age()
    {
        if ($this->dob)
        {
            return $this->dob->age;
        }
    }

    public function links()
    {
        $c = collect([]);
        if ($this->website_link)
        {
            $c->push([
                'site'  => 'website',
                'icon'  => 'edge',
                'color' => '#34bf49',
                'link'  => $this->website_link,
            ]);
        }
        if ($this->facebook_link)
        {
            $c->push([
                'site'  => 'facebook',
                'icon'  => 'facebook-official',
                'color' => '#3b5998',
                'link'  => $this->facebook_link,
            ]);
        }
        if ($this->twitter_link)
        {
            $c->push([
                'site'  => 'twitter',
                'icon'  => 'twitter',
                'color' => '#1da1f2',
                'link'  => $this->twitter_link,
            ]);
        }
        if ($this->instagram_link)
        {
            $c->push([
                'site'  => 'instagram',
                'icon'  => 'instagram',
                'color' => '#c13584',
                'link'  => $this->instagram_link,
            ]);
        }
        if ($this->github_link)
        {
            $c->push([
                'site'  => 'github',
                'icon'  => 'github',
                'color' => '#333',
                'link'  => $this->github_link,
            ]);
        }
        if ($this->linkedin_link)
        {
            $c->push([
                'site'  => 'linkedin',
                'icon'  => 'linkedin',
                'color' => '#0077b5',
                'link'  => $this->linkedin_link,
            ]);
        }
        if ($this->dribble_link)
        {
            $c->push([
                'site'  => 'dribble',
                'icon'  => 'dribble',
                'color' => '#ea4c89',
                'link'  => $this->dribble_link,
            ]);
        }
        if ($this->pinterest_link)
        {
            $c->push([
                'site'  => 'pinterest',
                'icon'  => 'pinterest',
                'color' => '#bd081c',
                'link'  => $this->pinterest_link,
            ]);
        }

        return $c;
    }

    public static function activatedPercentage($days = 30)
    {
        $date = Carbon::now()->addDays(-$days);

        $count = static::where('created_at', '>=', $date->toDateString())->count();
        $activatedCount = static::where('created_at', '>=', $date->toDateString())->whereHas('user', function ($query) {
            $query->where('user_status', '=', 1);
        })->count();

        return compact('activatedCount', 'count');
    }

    //TODO: ability to attach PDF files

    public function fullProfile()
    {
        if (!empty($this->file_path)){
            return true;
        }

        return false;
    }

}
