<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Session;

class News extends Model
{
    use StatisticsFunctions;
    
    protected $fillable = [
        "category_id",
        "major_id",
        "user_id",
        "title",
        "body",
        "status"
    ];

    public function category()
    {
        return $this->belongsTo('App\Category');
    }

    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function major()
    {
        return $this->belongsTo('App\Major');
    }
    public function tags()
    {
        return $this->morphToMany('App\Tag', 'taggable');
    }
    public function tagsString()
    {
        return implode(', ', $this->tags->pluck('name')->toArray());
    }
    public function tagsWithLinks()
    {
        $tags = $this->tags->map(function ($tag) {
            $url = '/news/tag/' . $tag->id;
            return  "<a href='{$url}'>{$tag->name}</a>";
        })->toArray();
        return implode(', ', $tags);
    }
    public function pictures()
    {
        return $this->morphToMany('App\Picture', 'imageable');
    }
    public function comments()
    {
        return $this->morphMany('App\Comment', 'commentable');
    }
    public function path($admin = null){
        if($admin){
            return '/admin/news/' . $this->id;
        }
        return '/news/' . $this->id;
    }

    public function toggleActivation(){
        if ($this->status){
            $this->status = 0;
            Session::flash('toastr','News is unpublished');
        }
        else{
            $this->status = 1;
            Session::flash('toastr','News is published');
        }
        $this->save();
    }

    public function getPicture(){
        if ($this->pictures()->count()){
            return $this->pictures()->first()->path;
        }
        else return '/images/news/default.png';
    }

    public static function publishedPercentage($days = 30)
    {
        $date = Carbon::now()->addDays(-$days);

        $count = static::where('created_at', '>=', $date->toDateString())->count();
        $publishedCount = static::where('created_at', '>=', $date->toDateString())->where('status', 1)->count();

        return compact('publishedCount', 'count');
    }
}

