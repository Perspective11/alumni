<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class editGraduateRequest extends FormRequest {

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */

    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'resume'              => 'mimes:pdf|max:2000',
            'name'                => 'required|max:30',
            'gender'              => 'required|boolean',
            'full_name'           => 'required|max:100',
            'years_of_experience' => 'nullable|integer|max:10',
            'phone'               => 'nullable|integer|digits_between:9,20',
            'dob'                 => 'required|date|before: 20 years ago',
            'perm_address'        => 'nullable|max:255',
            'curr_address'        => 'nullable|max:255',
            'origin'              => 'nullable|max:255',
            'is_studying'         => 'nullable|boolean',
            'field_of_specialty'  => 'nullable|max:50',
            'edu_level'           => 'nullable|max:50',
            'gpa'                 => 'nullable|numeric|max:4',
            'english_pro'         => 'max:50',
            'languages'           => 'nullable|max:50',
            'is_employed'         => 'nullable|boolean',
            'job_title'           => 'nullable|max:100',
            'emp_place'           => 'nullable|max:100',
            'field_of_work'       => 'nullable|max:100',
            'is_hireable'         => 'nullable|nullable|boolean',
            'description'         => 'nullable|max:255',
            'interests'           => 'nullable|max:255',
            'achievements'        => 'nullable|max:255',
            'volunteer'           => 'nullable|max:255',
            'goal'                => 'nullable|max:255',
            'website_link'        => 'nullable|url|max:255',
            'facebook_link'       => 'nullable|url|max:255',
            'twitter_link'        => 'nullable|url|max:255',
            'instagram_link'      => 'nullable|url|max:255',
            'github_link'         => 'nullable|url|max:255',
            'linkedin_link'       => 'nullable|url|max:255',
            'dribble_link'        => 'nullable|url|max:255',
            'date'                => 'nullable|date',
            'picture'             => 'nullable|image|dimensions:min_width=100,min_height=200|mimes:jpeg,png|max:2000',

        ];

    }

}
