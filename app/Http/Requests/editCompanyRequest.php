<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class editCompanyRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|max:20',
            'company_name' => 'required|max:100',
            "industry" => "required|max:50",
            "short_description" => "nullable|max:255",
            "full_description" => "nullable|max:1000",
            "headquarters" => "nullable|max:50",
            "size" => "nullable|max:10",
            "founded_at" => "nullable|integer|max:2017",
            "type" => "nullable|max:50",
            "website_link" => "nullable|url|max:255",
            "facebook_link" => "nullable|url|max:255",
            "twitter_link" => "nullable|url|max:255",
            "linkedin_link" => "nullable|url|max:255",
            "address_head" => "nullable|max:255",
            "address_1" => "nullable|max:255",
            "address_2" => "nullable|max:255",
            "address_3" => "nullable|max:255",
            "address_4" => "nullable|max:255",
            'picture' => 'nullable|image|dimensions:min_width=100,min_height=200|mimes:jpeg,png|max:2000',
        ];
    }
}
