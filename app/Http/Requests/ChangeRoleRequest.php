<?php

namespace App\Http\Requests;

use Carbon\Carbon;
use Illuminate\Foundation\Http\FormRequest;

class ChangeRoleRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $thisYear = Carbon::now()->year;
        $userType = [
            'user_type' => 'required|exists:roles,id',
        ];
        $graduateRules = [
            'liu_id' => 'required|integer|digits:8',
            'full_name' => 'required|max:255',
            'graduation_year' => 'required|integer|between:2000,' . $thisYear,
            'gender' => 'required|boolean',
            'major_id' => 'required|exists:majors,id',
        ];
        $companyRules = [
            'company_name' => 'required|max:100',
            "industry" => "required|max:50",
        ];
        switch (request('user_type')){
            case '1':
            case '2':
                return $userType;
                break;
            case '3':
                return array_merge($userType, $graduateRules);
                break;
            case '4':
                return array_merge($userType, $companyRules);
                break;
            default:
                return [
                    'user_type' => 'required|exists:roles,id',
                ];
                break;
        }

    }
}
