<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;

class MajorRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        if (Auth::user()->roles[0]->id == 1)
            return true;
        return false;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'between:3,50|required|unique:majors,name,' . $this->get('id'),
            'code' => 'between:3,8|nullable|unique:majors,code,' . $this->get('id'),
            'icon' => 'max:20',
            'color' => 'max:20',
        ];
    }
}
