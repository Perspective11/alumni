<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CreateEventRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
            'title' => 'required|max:100',
            'body' => 'required',
            'category' => 'integer|exists:categories,id',
            'major' => 'integer|exists:majors,id',
            'date' => 'date|required|after:today',
            'start' => 'date_format:H:i A|required',
            'end' => 'date_format:H:i A|after:start|required',
            'location' => 'max:255',
            'picture' => 'image|dimensions:min_width=100,min_height=200|mimes:jpeg,png|max:2000',
            'tags' => 'array|max:5',
        ];
    }
}
