<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class administration
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (Auth::check()){
            if (Auth::user()->isAdmin() || Auth::user()->isModerator()){
//                Auth::logout();
//                Session::flash('toastr', ['You dont have permission to access', 'error']);
                return $next($request);
            }else{
                return redirect('/');
            }
        }else{
            return redirect('login');
        }
    }
}
