<?php

namespace App\Http\Middleware;

use App\User;
use Closure;
use Illuminate\Support\Facades\Auth;
use Session;

class admin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (Auth::check()){
            if (Auth::user()->roles()->first()->id != 1){
//                Auth::logout();
//                Session::flash('toastr', ['You dont have permission to access', 'error']);
                return redirect('/');
            }else{
                return $next($request);
            }
        }else{
            return redirect('login');
        }
    }
}
