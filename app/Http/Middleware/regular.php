<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class regular
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (Auth::check()){
            if (Auth::user()->isGraduate() || Auth::user()->isCompany()){
                return $next($request);
            }else{
                return redirect()->route('home');
            }
        }else{
            return redirect('login');
        }
    }
}
