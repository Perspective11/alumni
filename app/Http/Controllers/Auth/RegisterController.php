<?php

namespace App\Http\Controllers\Auth;

use App\Mail\welcome;
use App\Major;
use App\User;
use App\Http\Controllers\Controller;
use Carbon\Carbon;
use Exception;
use Illuminate\Auth\Events\Registered;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
use Session;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        $thisYear = Carbon::now()->year;
        return Validator::make($data, [
            'name' => 'required|max:15|alpha_dash|unique:users,name',
            'email' => 'required|email|max:255|unique:users,email',
            'password' => 'required|min:6|confirmed',
            'type' => 'required|in:company,graduate',
            'liu_id' => 'required_if:type,graduate|integer|digits:8|unique:graduates,liu_id',
            'full_name' => 'required_if:type,graduate|max:255',
            'graduation_year' => 'required_if:type,graduate|integer|between:2000,' . $thisYear,
            'gender' => 'required_if:type,graduate|boolean',
            'major' => 'required_if:type,graduate|exists:majors,id',
            'company_name' => 'required_if:type,company|max:50',
            'industry' => 'required_if:type,company|max:50',
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return User
     */
    protected function create(array $data)
    {
        $user = User::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'password' => bcrypt($data['password']),
        ]);
        if ($data['type'] == 'graduate')
        {
            $user->graduate()->create([
                'liu_id' => $data['liu_id'],
                'full_name' => $data['full_name'],
                'graduation_year' => $data['graduation_year'],
                'gender' => $data['gender'],
                'major_id' => $data['major'],
            ]);
            $user->roles()->attach(3);
            $user->graduate->pictures()->attach(1);

        }
        else if ($data['type'] == 'company')
        {
            $user->company()->create([
                'company_name' => $data['company_name'],
                'industry' => $data['industry'],
            ]);
            $user->roles()->attach(4);
            $user->company->pictures()->attach(2);
        }
        try{
            \Mail::to($user)->send(new welcome($user));
            Session::flash('toastr','An email has been sent to you');
        }
        catch(Exception $e){

        }
        return $user;
    }

    public function register(Request $request){
        $this->validator($request->all())->validate();

        event(new Registered($user = $this->create($request->all())));

        return $this->registered($request, $user)
            ?: redirect($this->redirectPath());
    }

    public function showRegistrationForm()
    {
        $majors = Major::all();
        return view('auth.register', compact('majors'));
    }

}
