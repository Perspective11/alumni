<?php

namespace App\Http\Controllers;

use App\Company;
use App\Graduate;
use App\Http\Requests\ChangeRoleRequest;
use App\Http\Requests\UserAdminRequest;
use App\Mail\confirm;
use App\Major;
use App\Picture;
use App\Role;
use App\User;
use Auth;
use Carbon\Carbon;
use DB;
use ErrorException;
use File;
use Hash;
use Illuminate\Support\Facades\Input;
use Mockery\Exception;
use Response;
use Session;
use Illuminate\Http\Request;
use Storage;
use Swift_TransportException;
use Validator;
use Yajra\Datatables\Datatables;

class UsersController extends Controller {

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.user.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $majors = Major::all();
        $roles = Role::all();
        $user = new User;

        return view('admin.user.create', compact('majors', 'roles', 'user'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function store(UserAdminRequest $request)
    {
        $user = new User;
        DB::transaction(function () use ($request, $user) {

            $user = User::create([
                'name'        => $request->name,
                'email'       => $request->email,
                'password'    => bcrypt($request->password),
                'user_status' => $request->user_status,
            ]);
            $user->roles()->attach(request('user_type'));

            if ($request->user_type == '3')
            {
                if (!$request['gpa']) // because nullable rule doesnt fucking work
                {
                    $request['gpa'] = null;
                }
                if (!$request['years_of_experience']) // because nullable rule doesnt fucking work
                {
                    $request['years_of_experience'] = null;
                }
                if (!$request['dob']) // because nullable rule doesnt fucking work
                {
                    $request['dob'] = null;
                }


                $user->graduate()->create(
                    $request->all()
                );
                if ($image = $request->file('picture'))
                {
                    $image_name = Picture::getSanitizedName($image);
                    $image_path = 'images/graduates/profile/' . $image_name;
                    $picture = Picture::modifyImage($image);
                    $picture->save($image_path);

                    $picture = $user->graduate->pictures()->create(
                        [
                            'name' => $image_name,
                            'path' => '/' . $image_path,
                        ]
                    );
                }
            } else if ($request->user_type == '4')
            {
                $user->company()->create(
                    request()->all()
                );
                if ($image = $request->file('picture'))
                {
                    $image_name = Picture::getSanitizedName($image);
                    $image_path = 'images/companies/profile/' . $image_name;
                    $picture = Picture::modifyImage($image);
                    $picture->save($image_path);

                    $picture = $user->company->pictures()->create(
                        [
                            'name' => $image_name,
                            'path' => '/' . $image_path,
                        ]
                    );
                }
            }
        });
        Session::flash('toastr', 'User Created Successfully');

        return redirect($user->path(true));
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function show(User $user)
    {
        $graduate = $user->graduate;
        $company = $user->company;

        return view('admin.user.show', compact('user', 'graduate', 'company'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function edit(User $user)
    {
        $majors = Major::all();
        $roles = Role::all();
        $graduate = $user->graduate;
        $company = $user->company;

        return view('admin.user.edit', compact('user', 'majors', 'roles', 'graduate', 'company'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int                      $id
     *
     * @return \Illuminate\Http\Response
     */
    public function update(UserAdminRequest $request, User $user)
    {
        $graduate = $user->graduate;
        $company = $user->company;
        $userArray = [
            'name'        => $request->name,
            'email'       => $request->email,
            'user_status' => $request->user_status,
        ];
        if ($request['password']) // because nullable rule doesnt fucking work
        {
            $userArray['password'] = bcrypt($request->password);
        }
        DB::transaction(function () use ($request, $user, $graduate, $company, $userArray) {

            $user->update($userArray);

            if ($request->user_type == '3')
            { //graduate
                if (!$request['gpa']) // because nullable rule doesnt fucking work
                {
                    $request['gpa'] = null;
                }
                if (!$request['years_of_experience']) // because nullable rule doesnt fucking work
                {
                    $request['years_of_experience'] = null;
                }
                if (!$request['dob']) // because nullable rule doesnt fucking work
                {
                    $request['dob'] = null;
                }
                $user->graduate->update(
                    $request->all()
                );
                if ($image = $request->file('picture'))
                {
                    if ($graduate->pictures->count())
                    {
                        foreach ($graduate->pictures as $picture)
                        {
                            if ($picture->name === 'default.png' || $picture->name === 'default.jpg')
                                continue;
                            $str = public_path() . $picture->path;
                            try
                            {
                                unlink($str);
                            } catch (Exception $e)
                            {

                            }
                        }
                    }
                    $graduate->pictures()->detach();
                    $graduate->pictures()->delete();
                    $image_name = Picture::getSanitizedName($image);
                    $image_path = 'images/graduates/profile/' . $image_name;
                    $picture = Picture::modifyImage($image);
                    $picture->save($image_path);

                    $picture = $user->graduate->pictures()->create(
                        [
                            'name' => $image_name,
                            'path' => '/' . $image_path,
                        ]
                    );
                }
            } else if ($request->user_type == '4')
            { // company
                $user->company->update(request()->all());
                if ($image = $request->file('picture'))
                {
                    if ($company->pictures->count())
                    {
                        foreach ($company->pictures as $picture)
                        {
                            if ($picture->name === 'default.png' || $picture->name === 'default.jpg')
                                continue;
                            $str = public_path() . $picture->path;
                            try
                            {
                                unlink($str);
                            } catch (ErrorException $e)
                            {

                            }
                        }
                    }
                    $company->pictures()->detach();
                    $company->pictures()->delete();
                    $image_name = Picture::getSanitizedName($image);
                    $image_path = 'images/companies/profile/' . $image_name;
                    $picture = Picture::modifyImage($image);
                    $picture->save($image_path);

                    $picture = $user->company->pictures()->create(
                        [
                            'name' => $image_name,
                            'path' => '/' . $image_path,
                        ]
                    );
                }
            }
        });
        Session::flash('toastr', 'User Updated Successfully');

        return redirect($user->path(true));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy(User $user)
    {
        if (!$user->isAdmin())
        {
            DB::transaction(function () use ($user) {
                if ($user->child() && $user->child()->pictures->count())
                {
                    foreach ($user->child()->pictures as $picture)
                    {
                        if ($picture->name === 'default.png' || $picture->name === 'default.jpg')
                            continue;
                        $str = public_path() . $picture->path;
                        unlink($str);
                        $user->child()->pictures()->detach();
                        $user->child()->pictures()->delete();
                    }
                }

                $user->child()->pictures()->detach();


                /** @TODO: fi graduate need to also delete all post related */
                if ($user->isGraduate())
                    $user->graduate->delete();
                else
                    $user->company->delete();

                foreach ($user->posts as $post){
                    foreach ($post->pictures as $picture){
                        $str = public_path() . $picture->path;
                        unlink($str);
                    }
                    $post->pictures()->delete();
                    $post->pictures()->detach();
                }
                $user->posts()->delete();

                $user->comments()->delete();

                $user->delete();

            });

            Session::flash('toastr', 'User Deleted successfully');

            return redirect('admin/users');
        }
        Session::flash('toastr', 'You cannot delete an ADMIN user!');

        return redirect('admin/users');
    }


    public function activation(User $user)
    {
        if (!$user->user_status){
            try{
                \Mail::to($user)->send(new confirm($user));
            }
            catch(Swift_TransportException $e){

            }
            catch(Exception $e){

            }
        }
        $user->toggleActive();


        return redirect('admin/users');
    }

    public function changeRoleShow(User $user)
    {
        $graduate = new Graduate;
        $company = new Company;
        if ($user->isCompany())
        {
            $company = $user->company;
        } elseif ($user->isGraduate())
        {
            $graduate = $user->graduate;
        }
        $majors = Major::all();

        return view('admin.user.change-role', compact('user', 'company', 'graduate', 'majors'));
    }

    public function changeRoleStore(ChangeRoleRequest $request, User $user)
    {
        $newRole = request('user_type');
        $oldRole = $user->roles()->first()->id;
        if ($newRole == $oldRole)
        {
            return redirect()->back()->withErrors(['user-type' => 'Please choose another role.']);
        }
        DB::transaction(function () use ($user, $request, $newRole, $oldRole) {
            if ($oldRole == 3)
            { // from graduate to
                $user->graduate()->delete(); // delete graduate record
            }

            if ($oldRole == 4)
            { // from company to
                $user->company()->delete(); // delete company record
            }

            if ($newRole == 3)
            { // change to graduate
                $user->graduate()->create([
                    'liu_id'          => request('liu_id'),
                    'full_name'       => request('full_name'),
                    'graduation_year' => request('graduation_year'),
                    'gender'          => request('gender'),
                    'major_id'        => request('major_id'),
                ]);
            }

            if ($newRole == 4)
            { // change to company
                $user->company()->create([
                    'company_name' => request('company_name'),
                    'industry'     => request('industry'),
                ]);
            }
            $user->roles()->detach(); // remove previous roles
            $user->roles()->attach($newRole);
        }); // end transaction
        Session::flash('toastr', 'Role Changed Successfully');

        return redirect($user->path('true') . '/edit');
    }

    public function changePassword()
    {
        $user = Auth::user();

        return view('admin.user.change-password', compact('user'));
    }

    public function changePasswordStore(Request $request)
    {
        $user = Auth::user();
        Validator::make($request->all(), [
            'old_password' => 'required',
            'password'     => 'min:6|confirmed|required',
        ])->validate();

        if (!Hash::check($request->old_password, Auth::user()->password))
        {
            return redirect()->back()->withErrors(['old_password' => 'Old password is invalid.']);
        }

        $user->update([
            'password' => bcrypt($request->password),
        ]);
        Session::flash('toastr', 'Password Changed Successfully!');

        return redirect($user->path(true));
    }

    public function uploadResumeShow(User $user)
    {
        if (!$user || !$user->isGraduate())
        {
            Session::flash('Graduate does not exits', 'error');

            return redirect()->back();
        }
        $graduate = $user->graduate;

        return view('admin.user.upload-resume', compact('user', 'graduate'));
    }

    public function uploadResumeStore(Request $request, User $user)
    {
        $graduate = $user->graduate;
        $validator = Validator::make($request->all(), [
            "resume" => 'required|mimes:pdf|max:2000',
        ]);
        if ($validator->fails())
        {
            return response()->json($validator->messages(), 500);
        }
        if ($graduate->file_path)
        {
            $str = storage_path('app/' . $graduate->file_path);
            File::delete($str);
        }
        $guessExtension = $request->file('resume')->guessExtension();

        $fileName = Carbon::now()->timestamp . '_' . $user->name . '.' . $guessExtension;

        $path = $request->file('resume')->storeAs('resumes', $fileName, null);

        $user->graduate()->update([
            'file_path' => $path,
        ]);

        return Response::json(["success" => true], 201);
    }

    public function downloadResume(Request $request, User $user)
    {
        $graduate = $user->graduate;

        return response()->download(storage_path('app/' . $graduate->file_path), null, [], null);
    }

    public function DeleteResume(Request $request, User $user)
    {
        $graduate = $user->graduate;
        if ($graduate->file_path)
        {
            $graduate->update([
                'file_path' => null,
            ]);
            $str = storage_path('app/' . $graduate->file_path);
            File::delete($str);
            Session::flash('toastr', 'CV File Successfully Deleted');
        }

        return redirect()->back();
    }

    public function getUsersData(Request $request)
    {
        $users = User::select(['id', 'name', 'email', 'user_status', 'created_at']);

        if ($request->has('role'))
        {
            $users->whereHas('roles', function ($query) use ($request) {
                $query->where('name', 'like', $request->role);
            });
        }

        if ($request->has('status'))
        {
            if ($request->status == 'active')
                $users->where('user_status', 1);
            else if ($request->status == 'inactive')
                $users->where('user_status', 0);
        }

        if ($request->has('user'))
        {
            $userIds = User::searchUser(trim($request->user), 'id');
            $users->whereIn('id', $userIds);
        }

        return Datatables::of($users)
            ->addColumn('role', function ($user) {
                return $user->roles()->first()->name;
            })
            ->addColumn('child_name', function ($user) {
                return $user->childName();
            })
            ->editColumn('user_status', function ($user) {
                $class = $user->user_status ? 'success' : 'danger';
                $status = $user->user_status ? 'Deactivate' : 'Activate';

                return '<a href="' . $user->path(true) . '/activation" class="btn btn-xs user-status btn-' . $class . '">' . $status . '</a>';

            })->editColumn('created_at', function ($user) {
                return $user->created_at->toFormattedDateString();
            })
            ->make(true);
    }
}
