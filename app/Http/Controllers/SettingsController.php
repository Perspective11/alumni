<?php

namespace App\Http\Controllers;

use App\Picture;
use App\Setting;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use Intervention\Image\Facades\Image;
use Psy\Util\Json;
use Session;
use Validator;

class SettingsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function show()
    {
        $settings = Setting::all();
        $socialLinks = Setting::links();
        $contact = Setting::contact();
        $sliderImages = Setting::sliderImages();
        return view('admin.setting.show' , compact('settings', 'socialLinks', 'contact', 'sliderImages'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit()
    {
        $settings = Setting::all();
        $socialLinks = Setting::links();
        $contact = Setting::contact();
        $sliderImages = Setting::sliderImages();
        return view('admin.setting.edit' , compact('settings', 'socialLinks', 'contact', 'sliderImages'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        Validator::make($request->all(), [
            'facebook_link' => 'nullable|url|max:255',
            'twitter_link' => 'nullable|url|max:255',
            'youtube_link' => 'nullable|url|max:255',
            'ckHeader' => 'max:500',
            'ckInfo' => 'max:2000',
        ])->validate();

        $linksArray = json_decode(Setting::where('name','social_links')->first()->properties);
        $contactArray = json_decode(Setting::where('name','contact')->first()->properties);
        $linksArray->facebook = $request->facebook_link;
        $linksArray->twitter = $request->twitter_link;
        $linksArray->youtube = $request->youtube_link;
        $contactArray->header = $request->ckHeader;
        $contactArray->info = $request->ckInfo;


        Setting::where('name', '=', 'social_links')->update(['properties' => Json::encode($linksArray) ]);
        Setting::where('name', '=', 'contact')->update(['properties' => Json::encode($contactArray) ]);
        return back();
    }

    public function storeImages(Request $request){

        $validator = Validator::make($request->all(), [
            "images.*" => 'image|dimensions:min_width=1200',
        ]);
        if ($validator->fails())
        {
            return response()->json(implode( '
            ' ,array_flatten( $validator->messages()->toArray() )) , 500);
        }

        $images = $request->file('images');
        $imagesArray = (array)json_decode(Setting::where('name','home')->first()->properties);

        foreach($images as $image)
        {
            $imageArray = array('image' => $image);

            $imageName = Picture::getSanitizedName($image);
            $imagePath = public_path('images/slideshow/').$imageName;
            $picture = Image::make($image->getRealPath());
            $picture->fit(1300, 400, function ($constraint) {
                $constraint->upsize();
            });
            $picture->save($imagePath);
            array_push($imagesArray['slider'],'images/slideshow/'.$imageName);


        }

        Setting::where('name','home')->update(['properties' => Json::encode($imagesArray)]);
        //\Session::flash('toastr', 'Slider Image Uploaded Successfully');
        return response('ok', 200);

    }

    public function deleteImage(Request $request){
        $images = (array)json_decode(Setting::where('name','home')->first()->properties);
        $imagePath = 'images/slideshow/'.$request->name;
        foreach ($images['slider'] as $key => $image){
            if ($imagePath == $image){
                File::delete(public_path($imagePath));
                array_pull($images, 'slider.'.$key);
            }
        }
        $images['slider'] = array_values($images['slider']);
        Setting::where('name','home')->update(['properties' => Json::encode($images)]);
        //Session::flash('toastr', 'Slider Image Deleted Successfully');
        return response('ok',200);
    }
}
