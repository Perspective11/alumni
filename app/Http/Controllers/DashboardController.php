<?php

namespace App\Http\Controllers;

use App\Category;
use App\Company;
use App\Graduate;
use App\Event;
use App\Major;
use App\News;
use App\Post;
use App\User;
use Auth;
use Carbon\Carbon;
use function GuzzleHttp\Psr7\str;
use Illuminate\Http\Request;
use Psy\Util\Json;
use Response;

class DashboardController extends Controller
{
    public function index()
    {
        $recentGraduates = Graduate::latest()->take(8)->get();
        $recentCompanies = Company::latest()->take(8)->get();
        $recentPosts = Post::latest()->take(6)->get();
        $recentNews = News::latest()->take(6)->get();
        $recentEvents = Event::where('date', '>', Carbon::now()->toDateString())->latest()->take(6)->get();

        return view('admin.index', compact('recentGraduates', 'recentCompanies', 'recentPosts', 'recentEvents', 'recentNews'));
    }

    public function profile()
    {
        return redirect('/admin/users/'.Auth::user()->id);
    }


}
