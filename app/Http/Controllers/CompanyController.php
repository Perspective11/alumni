<?php

namespace App\Http\Controllers;

use App\Company;
use App\User;
use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;

class CompanyController extends Controller
{
    /** @TODO: you cannot change the company user role it will always stay as a company
     * @TODO: the company cannot create posts or events
     * @TODO: the company cannot comment for a post
     */

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        return view('admin.company.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Company $company)
    {
        return redirect($company->user->path(true));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Company $company)
    {
        return redirect($company->user->path(true) . '/edit');

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Company $company)
    {
    }
    public function changeRoleShow(Company $company){
        return redirect($company->user->path(true) . '/changeRole');
    }

    public function getCompaniesData(Request $request){
        $companies = Company::with('user');

//        - status
//        - user name
//        - industry
//        - type
//        - size

        if($request->has('status')){
            $companies->where('company_status', 'like', $request->status);
        }

        if ($request->has('user')) {
            $userIds = User::searchUser(trim($request->user) , 'id');
            $companies->whereIn('user_id', $userIds);
        }

        if ($request->has('industry')) {
            $companies->where('industry', $request->industry);
        }
        if ($request->has('type')) {
            $companies->where('type', $request->type);
        }
        if ($request->has('size')) {
            $companies->where('size', $request->size);
        }



        return Datatables::of($companies)
            ->addColumn('email', function ($company) {
                return $company->user->email;
            })
            ->addColumn('links',function ($company)
            {
                $htmlSting = "<div class='text-center link-icons'>";
                foreach ($company->links() as $link){
                    $htmlSting .= "<a class='link-icon' href='".
                        $link['link'] ."'><i style='color:".
                        $link['color'] ."' class='fa fa-".
                        $link['icon'] ."'></i></a>";
                }
                $htmlSting .= "</div>";

                return $htmlSting;

            })
            ->editColumn('company_status', function ($company) {
                if ($status = $company->company_status) {
                    return "<div class='text-center'><span style='background-color:lightgrey !important;' class='label label-primary'>{$status}</span></div>";
                } else return '';
            })
            ->editColumn('size', function ($company) {
                if ($size = $company->size) {
                    return "<div class='text-center'><span class='label label-info'>{$size}</span></div>";
                } else return '';
            })


            ->make(true);
    }
}
