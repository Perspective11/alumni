<?php

namespace App\Http\Controllers;

use App\Category;
use App\Comment;
use App\Http\Requests\CreatePostRequest;
use App\Major;
use App\Picture;
use App\Post;
use App\Tag;
use Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Redirect;
use Session;

class PostsController extends Controller {

    public function __construct()
    {
        $this->middleware('graduate', ['only' => ['create', 'store']]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $recentPosts = Post::where('status', '1')->with('pictures')->latest()->take(5)->get();
        if (isset($request->search) && !empty($request->search))
        {
            $posts = Post::where('title', 'like', '%' . $request->search . '%')->where('status', '1')->withCount(['tags', 'category', 'comments'])->with(['category', 'tags', 'pictures', 'user', 'major'])->latest()->paginate(10);
            $categories = Category::has('posts')->get();
            $tags = Tag::has('posts')->get();

            return view('public.posts.index', compact('posts', 'categories', 'tags', 'recentPosts'));
        }
        $posts = Post::where('status', '1')->withCount(['tags', 'category', 'comments'])->with(['category', 'tags', 'pictures', 'user', 'major'])->latest()->paginate(10);
        $categories = Category::has('posts')->get();
        $tags = Tag::has('posts')->get();

        return view('public.posts.index', compact('posts', 'categories', 'tags', 'recentPosts'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categories = Category::all();
        $tags = Tag::all();
        $majors = Major::all();

        return view('public.posts.create', compact('categories', 'tags', 'majors'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function store(CreatePostRequest $request)
    {
        $post = Auth::user()->posts()->create(
            [
                'title'       => request('title'),
                'body'        => request('body'),
                'category_id' => request('category'),
                'major_id'    => request('major'),
            ]
        );

        $post->tags()->attach(request('tags'));

        if ($image = $request->file('picture'))
        {
            $image_name = Picture::getSanitizedName($image);
            $image_path = 'images/posts/' . $image_name;
            $picture = Picture::modifyImage($image);
            $picture->save($image_path);
            $picture = $post->pictures()->create(
                [
                    'name' => $image_name,
                    'path' => '/' . $image_path,
                ]
            );


        }


        Session::flash('toastr', 'Post Successfully Created, Waiting to be confirmed');

        return \redirect('posts');
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function show(Post $post)
    {
        $recentPosts = Post::where('status', '1')->with('pictures')->latest()->take(5)->get();

        return view('public.posts.show', [
            'post'        => $post->load(['tags', 'user', 'comments.user.graduate.pictures', 'pictures', 'category', 'major']),
            'recentPosts' => $recentPosts,
        ]);

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $post = Post::find($id);
        $categories = Category::all();
        $tags = Tag::all();
        $majors = Major::all();

        return view('public.posts.edit', compact('post', 'categories', 'tags', 'majors'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \App\Http\Requests\CreatePostRequest|\Illuminate\Http\Request $request
     * @param  int                                                          $id
     *
     * @return \Illuminate\Http\Response
     */
    public function update(CreatePostRequest $request, $id)
    {
        //
        $post = Post::find($id);
        $post->update([
            'title'       => request('title'),
            'body'        => request('body'),
            'category_id' => request('category'),
            'major_id'    => request('major'),
        ]);
        if ($request->has('tags'))
        {
            $post->tags()->sync($request->input('tags'));
        } else
        {
            $post->tags()->detach();
        }
        if ($image = $request->file('picture'))
        {
            if ($post->pictures->count())
            {
                foreach ($post->pictures as $picture)
                {
                    if ($picture->name === 'default.png' || $picture->name === 'default.jpg')
                        continue;
                    $str = public_path() . $picture->path;
                    unlink($str);
                }
            }

            $post->pictures()->detach();
            $post->pictures()->delete();
            $image_name = Picture::getSanitizedName($image);
            $image_path = 'images/posts/' . $image_name;
            $picture = Picture::modifyImage($image);
            $picture->save($image_path);

            $picture = $post->pictures()->create(
                [
                    'name' => $image_name,
                    'path' => '/' . $image_path,
                ]
            );
        }

        Session::flash('toastr', 'Post Successfully Updated');

        return redirect()->route('posts.show', $id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $post = Post::findOrFail($id);
        $post->pictures()->detach();
        $post->pictures()->delete();

        if ($post->pictures->count())
        {
            foreach ($post->pictures as $picture)
            {
                if ($picture->name === 'default.png' || $picture->name === 'default.jpg')
                    continue;
                $str = public_path() . $picture->path;
                unlink($str);
            }
        }

        $post->delete();
        Session::flash('toastr', 'Post Successfully Deleted');

        return Redirect::to('posts');
    }

    public function tag(Tag $tag, Request $request)
    {
        $recentPosts = Post::where('status', '1')->latest()->take(5)->get();
        if (isset($request->search) && !empty($request->search))
        {
            $posts = $tag->posts()->where('title', 'like', '%' . $request->search . '%')->where('status', '1')->latest()->paginate(10);

            return view('public.posts.tag', compact('posts', 'recentPosts', 'tag'));
        }
        $posts = $tag->posts()->where('status', '1')->paginate(10);

        return view('public.posts.tag', compact('posts', 'recentPosts', 'tag'));
    }

    public function category(Category $category, Request $request)
    {
        $recentPosts = Post::where('status', '1')->latest()->take(5)->get();
        if (isset($request->search) && !empty($request->search))
        {
            $posts = $category->posts()->where('title', 'like', '%' . $request->search . '%')->where('status', '1')->latest()->paginate(10);

            return view('public.posts.category', compact('posts', 'recentPosts', 'category'));
        }
        $posts = $category->posts()->where('status', '1')->paginate(10);

        return view('public.posts.category', compact('posts', 'recentPosts', 'category'));
    }
}
