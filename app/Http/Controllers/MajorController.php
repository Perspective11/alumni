<?php

namespace App\Http\Controllers;

use App\Http\Requests\MajorRequest;
use Session;
use App\Major;
use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;

class MajorController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.major.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.major.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(MajorRequest $request)
    {
        if (!request('code')) // Because nullable rule doesn't work
        {
            $request->code = null;
        }
        $major = Major::create([
            'name' => $request->name,
            'code' => $request->code,
            'icon' => $request->icon,
            'color' => $request->color
        ]);

        Session::flash('toastr', 'Major Successfully Created');

        return redirect('admin/majors/create');
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show(Major $major)
    {
        return view('admin.major.show', compact('major'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Major $major)
    {
        return view('admin.major.edit', compact('major'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(MajorRequest $request, Major $major)
    {
        if (! request('code')) // Because nullable rule doesn't work
        {
            $request->code = null;
        }
        $major->update([
            'name' => $request->name,
            'code' => $request->code,
            'icon' => $request->icon,
            'color' => $request->color
        ]);

        Session::flash('toastr', 'Major Successfully Updated');

        return redirect('/admin/majors');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Major $major)
    {
        $major->delete();
        Session::flash('toastr', 'Major Deleted Successfully');
        return redirect('/admin/majors');
    }

    public function getMajorsData(Request $request)
    {
        $majors = Major::orderBy('name');


        return Datatables::of($majors)
            ->editColumn('icon', function ($major) {
                return '<div class=""><i style="color: ' . $major->color . '" class="fa ' . $major->icon . '"></i></div>';

            })
            ->addColumn('posts', function ($major) {
                $posts = $major->posts()->count();
                return "<div class=''><a class='model-link' href=''><span class='label label-success label-lg'>{$posts}</span></div></a>";
            })
            ->addColumn('events', function ($major) {
                $events = $major->events()->count();
                return "<div class=''><a class='model-link' href=''><span class='label label-info label-lg'>{$events}</span></div></a>";
            })
            ->addColumn('news', function ($major) {
                $news = $major->news()->count();
                return "<div class=''><a class='model-link' href=''><span class='label label-warning label-lg'>{$news}</span></div></a>";
            })
            ->addColumn('graduates', function ($major) {
                $news = $major->graduates()->count();
                return "<div class=''><a class='model-link' href=''><span class='label label-primary label-lg'>{$news}</span></div></a>";
            })
            //TODO: fill href attributes with proper link
            ->make(true);
    }
}
