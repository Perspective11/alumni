<?php

namespace App\Http\Controllers;

use App\Category;
use App\Event;
use App\Http\Requests\CreateEventRequest;
use App\Major;
use App\Picture;
use App\Tag;
use App\User;
use Auth;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Redirect;
use Session;
use Yajra\Datatables\Datatables;

class EventController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.event.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categories = Category::all();
        $majors = Major::all();
        $tags = Tag::all();
        return view('admin.event.create', compact('categories', 'majors', 'tags'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(CreateEventRequest $request)
    {
        $start = Carbon::parse(request('start'))->toTimeString();
        $end = Carbon::parse(request('end'))->toTimeString();

        $event = Auth::user()->events()->create([
                'category_id' => empty($request->category) ? null : request('category'),
                'major_id'    => empty($request->major) ? null : request('major'),
                "title" => request('title'),
                "body" => request('body'),
                "date" => request('date'),
                "start" => $start,
                "end" => $end,
                "location" => request('location'),
            ]

        );

        $event->tags()->attach(request('tags'));

        if ($image = $request->file('picture')) {
            $image_name = Picture::getSanitizedName($image);
            $image_path = 'images/events/' . $image_name;
            $picture = Picture::modifyImage($image);
            $picture->save($image_path);
            $picture = $event->pictures()->create(
                [
                    'name' => $image_name,
                    'path' => '/' . $image_path,
                ]
            );
        }

        Session::flash('toastr', 'Event Successfully Created');

        return Redirect::to('/admin/events');
    }

    /**
     * Display the specified resource.
     *
     * @param Event $event
     * @return \Illuminate\Http\Response
     * @internal param int $id
     */
    public function show(Event $event)
    {
        $commentsCount = $event->comments()->count();
        $comments = $event->comments()->paginate(20);
        return view('admin.event.show', compact('event', 'comments', 'commentsCount'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Event $event)
    {
        $categories = Category::all();
        $majors = Major::all();
        $tags = Tag::all();
        return view('admin.event.edit', compact('categories', 'majors', 'event', 'tags'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(CreateEventRequest $request, Event $event)
    {
        $start = Carbon::parse(request('start'))->toTimeString();
        $end = Carbon::parse(request('end'))->toTimeString();
        $event->update([
            'category_id' => empty($request->category) ? null : request('category'),
            'major_id'    => empty($request->major) ? null : request('major'),
            "title" => request('title'),
            "body" => request('body'),
            "date" => request('date'),
            "start" => $start,
            "end" => $end,
            "location" => request('location'),
        ]);
        if ($request->has('tags')) {
            $event->tags()->sync($request->input('tags'));
        } else {
            $event->tags()->detach();
        }
        if ($image = $request->file('picture')) {
            if ($event->pictures->count()) {
                foreach ($event->pictures as $picture) {
                    if ($picture->name === 'default.png' || $picture->name === 'default.jpg')
                        continue;
                    $str = public_path() . $picture->path;
                    unlink($str);
                }
            }

            $event->pictures()->detach();
            $event->pictures()->delete();
            $image_name = Picture::getSanitizedName($image);
            $image_path = 'images/events/' . $image_name;
            $picture = Picture::modifyImage($image);
            $picture->save($image_path);

            $picture = $event->pictures()->create(
                [
                    'name' => $image_name,
                    'path' => '/' . $image_path,
                ]
            );
        }

        Session::flash('toastr', 'Event Successfully Updated');
        return redirect('/admin/events');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Event $event)
    {
        if ($event->pictures->count()) {
            foreach ($event->pictures as $picture) {
                if ($picture->name === 'default.png' || $picture->name === 'default.jpg')
                    continue;
                $str = public_path() . $picture->path;
                unlink($str);
            }
        }

        $event->pictures()->detach();
        $event->pictures()->delete();

        $event->comments()->delete();

        $event->delete();
        Session::flash('toastr', 'Event Successfully Deleted');
        return redirect('/admin/events');
    }

    public function activation($id)
    {
        $event = Event::find($id);
        $event->togglePublished();
        return back();
    }

    public function getEventsData(Request $request)
    {
        $events = Event::with(['major', 'category', 'user', 'tags']);
        if ($request->has('status')) {
            if ($request->status == 'published')
                $events->where('status', 1);
            else if ($request->status == 'unpublished')
                $events->where('status', 0);
        }
        if ($request->has('major')) {
            $events->whereHas('major', function ($query) use ($request) {
                $query->where('name', 'like', $request->major);
            });
        }
        if ($request->has('category')) {
            $events->whereHas('category', function ($query) use ($request) {
                $query->where('name', 'like', $request->category);
            });
        }
        if ($request->has('tag')) {
            $events->whereHas('tags', function ($query) use ($request) {
                $query->where('name', 'like', $request->tag);
            });
        }
        if ($request->has('user')) {
            $userIds = User::searchUser(trim($request->user), 'id');
            $events->whereIn('user_id', $userIds);
        }
        if ($request->has('daterange')) {
            $arr = explode(' - ', request('daterange'));
            $startDate = Carbon::parse($arr[0])->toDateString();
            $endDate = Carbon::parse($arr[1])->addDay()->toDateString();

            $events->whereBetween('date', [$startDate, $endDate]);

        }

        return Datatables::of($events)
            ->addColumn('category', function ($event) {
                return $event->category ? $event->category->name : '';
            })
            ->addColumn('major', function ($event) {
                return $event->major ? $event->major->name : '';
            })
            ->addColumn('user', function ($event) {
                return $event->user ? $event->user->name : '';
            })
            ->addColumn('datetime', function ($event) {
                $date = $event->date->toFormattedDateString();
                $start = date("g:i a", strtotime($event->start));
                $end = date("g:i a", strtotime($event->end));
                return '<div class="text-center"><span class="label label-sm label-info">' . $date . '</span>  <span class="label label-sm label-warning">' . $start . ' - ' . $end . '</span></div>';
            })
            ->addColumn('tags', function ($event) {

                return implode(', ', $event->tags->pluck('name')->toArray());
            })
            ->addColumn('comments', function ($event) {
                return '<div class="text-center"><a href="' . $event->path(true) . '" class="btn btn-xs event-comments btn-warning">' . $event->comments()->count() . '</a></div>'; // TODO: Optimize
            })
            ->editColumn('status', function ($event) {
                $class = $event->status ? 'success' : 'danger';
                $status = $event->status ? 'Unpublish' : 'Publish';
                return '<div class="text-center"><a href="' . $event->path(true) . '/activation" class="btn btn-xs event-status btn-' . $class . '">' . $status . '</a></div>';

            })->editColumn('created_at', function ($event) {
                return $event->created_at->toFormattedDateString();
            })
            ->editColumn('body', function ($event) {
                return str_limit($event->body, 200);
            })
            ->editColumn('location', function ($event) {
                return str_limit($event->location, 100);
            })
            ->make(true);
    }
}
