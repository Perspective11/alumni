<?php

namespace App\Http\Controllers;

use App\Event;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Response;

/**
 * Class CalendarController
 *
 * @package App\Http\Controllers
 */
class CalendarController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $events = Event::all();
        return view('admin.calendar.index', compact('events'));
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    public function getEvents(Request $request)
    {
        $arr = [];
        $events = Event::whereBetween('date', [$request->start, $request->end])->each(function ($event) use (&$arr) {
            array_push($arr, [
                'id'    => $event->id,
                'title' => str_limit($event->title, 100),
                'start' => $event->date->toDateString().' '.$event->start,
                'end'   => $event->date->toDateString().' '.$event->end,
                'url'   => $event->path(true),
            ]);
        });

        return $arr;
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @return mixed
     */
    public function updateEvent(Request $request)
    {
        $event = Event::findOrFail($request->id);
        $dateTimeStart = Carbon::parse($request->start);
        $dateTimeEnd = '';
        $date = $dateTimeStart->toDateString();
        $start = $dateTimeStart->toTimeString();
        $end = '';
        $updateArray = [
            'date'  => $date,
            'start' => $start,
        ];

        if ($request->end) {
            $dateTimeEnd = Carbon::parse($request->end);
            $end = $dateTimeEnd->toTimeString();
            $updateArray ['end'] = $end;
        }
        if ($event->update($updateArray)){
            return Response::json(compact('date', 'start', 'end'), 201);
        }
        else return Response::json(['msg' => 'unable to update'], 500);

    }



}
