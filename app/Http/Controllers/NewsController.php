<?php

namespace App\Http\Controllers;

use App\News;
use App\Category;
use App\Http\Requests\CreateNewsRequest;
use App\Major;
use App\Picture;
use App\Tag;
use App\User;
use Auth;
use Illuminate\Http\Request;
use Redirect;
use Session;
use Yajra\Datatables\Datatables;

class NewsController extends Controller {

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $recentNews = News::where('status', '1')->with('pictures')->latest()->take(5)->get();
        if (isset($request->search) && !empty($request->search))
        {
            $news = News::where('title', 'like', '%' . $request->search . '%')->where('status', '1')->with(['tags', 'category', 'major', 'comments', 'pictures'])->latest()->paginate(10);
            $categories = Category::has('news')->get();
            $tags = Tag::has('news')->get();

            return view('public.news.index', compact('news', 'categories', 'tags', 'recentNews'));
        }
        $news = News::where('status', '1')->with(['tags', 'category', 'major', 'comments', 'pictures'])->latest()->paginate(10);
        $categories = Category::has('news')->get();
        $tags = Tag::has('news')->get();

        return view('public.news.index', compact('news', 'categories', 'tags', 'recentNews'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $categories = Category::all();
        $tags = Tag::all();
        $majors = Major::all();

        return view('news.create', compact('categories', 'tags', 'majors'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function store(CreateNewsRequest $request)
    {
        $new = Auth::user()->news()->create(
            [
                'title'       => request('title'),
                'body'        => request('body'),
                'category_id' => empty($request->category) ? null : request('category'),
                'major_id'    => empty($request->major) ? null : request('major'),
            ]
        );
        $new->tags()->attach(request('tags'));

        if ($image = $request->file('picture'))
        {
            $image_name = Picture::getSanitizedName($image);
            $image_path = 'images/news/' . $image_name;
            $picture = Picture::modifyImage($image);
            $picture->save($image_path);
            $picture = $new->pictures()->create(
                [
                    'name' => $image_name,
                    'path' => '/' . $image_path,
                ]
            );


        }

        Session::flash('toastr', 'News Post Successfully Created');

        return Redirect::to('news');
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function show(News $news)
    {
        $recentNews = News::where('status', '1')->with('pictures')->latest()->take(5)->get();
        $new = $news->load(['tags', 'category', 'comments.user.graduate.pictures', 'major', 'pictures']);

        return view('public.news.show', compact('new', 'recentNews'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $new = News::find($id);
        $tags = Tag::all();
        $categories = Category::all();
        $majors = Major::all();

        return view('news.edit', compact('new', 'tags', 'categories', 'majors'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int                      $id
     *
     * @return \Illuminate\Http\Response
     */
    public function update(CreateNewsRequest $request, $id)
    {
        //
        $new = News::find($id);
        $new->update([
            'title'       => request('title'),
            'body'        => request('body'),
            'category_id' => empty($request->category) ? null : request('category'),
            'major_id'    => empty($request->major) ? null : request('major'),
        ]);
        if ($request->has('tags'))
        {
            $new->tags()->sync($request->input('tags'));
        } else
        {
            $new->tags()->detach();
        }
        if ($image = $request->file('picture'))
        {
            if ($new->pictures->count())
            {
                foreach ($new->pictures as $picture)
                {
                    if ($picture->name === 'default.png' || $picture->name === 'default.jpg')
                        continue;
                    $str = public_path() . $picture->path;
                    unlink($str);
                }
            }

            $new->pictures()->detach();
            $new->pictures()->delete();
            $image_name = Picture::getSanitizedName($image);
            $image_path = 'images/news/' . $image_name;
            $picture = Picture::modifyImage($image);
            $picture->save($image_path);

            $picture = $new->pictures()->create(
                [
                    'name' => $image_name,
                    'path' => '/' . $image_path,
                ]
            );
        }

        Session::flash('toastr', 'News Post Successfully Updated');

        return redirect('');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy(News $news)
    {
        if ($news->pictures->count())
        {
            foreach ($news->pictures as $picture)
            {
                if ($picture->name === 'default.png' || $picture->name === 'default.jpg')
                    continue;
                $str = public_path() . $picture->path;
                unlink($str);
            }
        }

        $news->pictures()->detach();
        $news->pictures()->delete();

        $news->comments()->delete();

        $news->delete();
        Session::flash('toastr', 'News Post Successfully Deleted');

        return Redirect::to('/admin/news');
    }

    public function tag(Tag $tag, Request $request)
    {
        $recentNews = News::where('status', '1')->latest()->take(5)->get();
        if (isset($request->search) && !empty($request->search))
        {
            $news = $tag->news()->where('title', 'like', '%' . $request->search . '%')->where('status', '1')->latest()->paginate(10);

            return view('public.news.tag', compact('news', 'recentNews', 'tag'));
        }
        $news = $tag->news()->where('status', '1')->paginate(10);

        return view('public.news.tag', compact('news', 'recentNews', 'tag'));
    }

    public function category(Category $category, Request $request)
    {
        $recentNews = News::where('status', '1')->latest()->take(5)->get();
        if (isset($request->search) && !empty($request->search))
        {
            $news = $category->news()->where('title', 'like', '%' . $request->search . '%')->where('status', '1')->latest()->paginate(10);

            return view('public.news.category', compact('news', 'recentNews', 'category'));
        }
        $news = $category->news()->where('status', '1')->paginate(10);

        return view('public.news.category', compact('news', 'recentNews', 'category'));
    }

}
