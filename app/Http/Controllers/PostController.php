<?php

namespace App\Http\Controllers;

use App\Category;
use App\Http\Requests\CreatePostRequest;
use App\Major;
use App\Picture;
use App\Post;
use App\Tag;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Redirect;
use Session;
use Yajra\Datatables\Datatables;

class PostController extends Controller {

    public function __constructor()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.post.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categories = Category::all();
        $majors = Major::all();
        $tags = Tag::all();

        return view('admin.post.create', compact('categories', 'majors', 'tags'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function store(CreatePostRequest $request)
    {
        $post = Auth::user()->posts()->create(
            [
                'title'       => request('title'),
                'body'        => request('body'),
                'category_id' => empty($request->category) ? null : request('category'),
                'major_id'    => empty($request->major) ? null : request('major'),
            ]
        );

        $post->tags()->attach(request('tags'));

        if ($image = $request->file('picture'))
        {
            $image_name = Picture::getSanitizedName($image);
            $image_path = 'images/posts/' . $image_name;
            $picture = Picture::modifyImage($image);
            $picture->save($image_path);
            $picture = $post->pictures()->create(
                [
                    'name' => $image_name,
                    'path' => '/' . $image_path,
                ]
            );
        }

        Session::flash('toastr', 'Post Successfully Created');

        return Redirect::to('admin/posts');
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function show(Post $post)
    {
        $commentsCount = $post->comments()->count();
        $comments = $post->comments()->paginate(20);

        return view('admin.post.show', compact('post', 'comments', 'commentsCount'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function edit(Post $post)
    {
        $categories = Category::all();
        $majors = Major::all();
        $tags = Tag::all();

        return view('admin.post.edit', compact('categories', 'majors', 'post', 'tags'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int                      $id
     *
     * @return \Illuminate\Http\Response
     */
    public function update(CreatePostRequest $request, Post $post)
    {
        $post->update([
            'title'       => request('title'),
            'body'        => request('body'),
            'category_id' => empty($request->category) ? null : request('category'),
            'major_id'    => empty($request->major) ? null : request('major'),
        ]);
        if ($request->has('tags'))
        {
            $post->tags()->sync($request->input('tags'));
        } else
        {
            $post->tags()->detach();
        }
        if ($image = $request->file('picture'))
        {
            if ($post->pictures->count())
            {
                foreach ($post->pictures as $picture)
                {
                    if ($picture->name === 'default.png' || $picture->name === 'default.jpg')
                        continue;
                    $str = public_path() . $picture->path;
                    unlink($str);
                }
            }

            $post->pictures()->detach();
            $post->pictures()->delete();
            $image_name = Picture::getSanitizedName($image);
            $image_path = 'images/posts/' . $image_name;
            $picture = Picture::modifyImage($image);
            $picture->save($image_path);

            $picture = $post->pictures()->create(
                [
                    'name' => $image_name,
                    'path' => '/' . $image_path,
                ]
            );
        }

        Session::flash('toastr', 'Post Successfully Updated');

        return redirect('admin/posts');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy(Post $post)
    {
        if ($post->pictures->count())
        {
            foreach ($post->pictures as $picture)
            {
                if ($picture->name === 'default.png' || $picture->name === 'default.jpg')
                    continue;
                $str = public_path() . $picture->path;
                unlink($str);
            }
        }

        $post->pictures()->detach();
        $post->pictures()->delete();

        $post->comments()->delete();

        $post->delete();
        Session::flash('toastr', 'Post Successfully Deleted');

        return Redirect::to('/admin/posts');
    }

    /**
     * Change the activation mode.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function activation(Post $post)
    {
        $post->togglePublished();

        return back();
    }


    public function getPostsData(Request $request)
    {
        $posts = Post::with(['major', 'category', 'user', 'tags']);// TODO: Optimize
        if ($request->has('status'))
        {
            if ($request->status == 'published')
                $posts->where('status', 1);
            else if ($request->status == 'unpublished')
                $posts->where('status', 0);
        }
        if ($request->has('major'))
        {
            $posts->whereHas('major', function ($query) use ($request) {
                $query->where('name', 'like', $request->major);
            });
        }
        if ($request->has('category'))
        {
            $posts->whereHas('category', function ($query) use ($request) {
                $query->where('name', 'like', $request->category);
            });
        }
        if ($request->has('tag'))
        {
            $posts->whereHas('tags', function ($query) use ($request) {
                $query->where('name', 'like', $request->tag);
            });
        }
        if ($request->has('user'))
        {
            $userIds = User::searchUser(trim($request->user), 'id');
            $posts->whereIn('user_id', $userIds);
        }

        return Datatables::of($posts)
            ->addColumn('category', function ($post) {
                return $post->category ? $post->category->name : '';
            })
            ->addColumn('major', function ($post) {
                return $post->major ? $post->major->name : '';
            })
            ->addColumn('user', function ($post) {
                return $post->user ? $post->user->name : '';
            })
            ->addColumn('tags', function ($post) {
                return implode(', ', $post->tags->pluck('name')->toArray());
            })
            ->addColumn('comments', function ($post) {
                return '<div class="text-center"><a href="' . $post->path(true) . '" class="btn btn-xs event-comments btn-warning">' . $post->comments()->count() . '</a></div>'; // TODO: Optimize
            })
            ->editColumn('status', function ($post) {
                $class = $post->status ? 'success' : 'danger';
                $status = $post->status ? 'Unpublish' : 'Publish';

                return '<div class="text-center"><a href="' . $post->path(true) . '/activation" class="btn btn-xs post-status btn-' . $class . '">' . $status . '</a></div>';

            })->editColumn('created_at', function ($post) {
                return $post->created_at->toFormattedDateString();
            })
            ->editColumn('body', function ($post) {
                return str_limit($post->body, 200);
            })
            ->make(true);
    }
}
