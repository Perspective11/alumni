<?php

namespace App\Http\Controllers;

use App\Category;
use App\Http\Requests\CreateNewsRequest;
use App\Major;
use App\News;
use App\Picture;
use App\Tag;
use App\User;
use Auth;
use Illuminate\Http\Request;
use Redirect;
use Session;
use Yajra\Datatables\Datatables;

class NewController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.news.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $categories = Category::all();
        $majors = Major::all();
        $tags = Tag::all();
        return view('admin.news.create', compact('categories', 'majors', 'tags'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CreateNewsRequest $request)
    {
        $news = Auth::user()->news()->create(
            [
                'title' => request('title'),
                'body' => request('body'),
                'category_id' => empty($request->category) ? null : request('category'),
                'major_id'    => empty($request->major) ? null : request('major'),
            ]
        );

        $news->tags()->attach(request('tags'));

        if ($image  = $request->file('picture'))
        {
            $image_name = Picture::getSanitizedName($image);
            $image_path = 'images/news/' . $image_name;
            $picture = Picture::modifyImage($image);
            $picture->save($image_path);
            $picture = $news->pictures()->create(
                [
                    'name' => $image_name,
                    'path' => '/' . $image_path,
                ]
            );
        }

        Session::flash('toastr', 'News Post Successfully Created');

        return Redirect::to('admin/news');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(News $news)
    {
        $commentsCount = $news->comments()->count();
        $comments = $news->comments()->paginate(20);
        return view('admin.news.show', compact('news', 'comments', 'commentsCount'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(News $news)
    {
        $categories = Category::all();
        $majors = Major::all();
        $tags = Tag::all();
        return view('admin.news.edit', compact('categories', 'majors', 'news', 'tags'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(CreateNewsRequest $request, News $news)
    {
        $news->update([
            'title' => request('title'),
            'body' => request('body'),
            'category_id' => empty($request->category) ? null : request('category'),
            'major_id'    => empty($request->major) ? null : request('major'),
        ]);
        if($request->has('tags')) {
            $news->tags()->sync($request->input('tags'));
        }
        else{
            $news->tags()->detach();
        }
        if($image = $request->file('picture'))
        {
            if ($news->pictures->count()){
                foreach($news->pictures as $picture)
                {
                    if ($picture->name === 'default.png' || $picture->name === 'default.jpg')
                        continue;
                    $str = public_path() . $picture->path;
                    unlink($str);
                }
            }

            $news->pictures()->detach();
            $news->pictures()->delete();
            $image_name = Picture::getSanitizedName($image);
            $image_path = 'images/news/' . $image_name;
            $picture = Picture::modifyImage($image);
            $picture->save($image_path);

            $picture = $news->pictures()->create(
                [
                    'name' => $image_name,
                    'path' => '/' . $image_path,
                ]
            );
        }

        Session::flash('toastr', 'News Post Successfully Updated');
        return redirect('admin/news');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(News $news)
    {
        if ($news->pictures->count()) {
            foreach ($news->pictures as $picture) {
                if ($picture->name === 'default.png' || $picture->name === 'default.jpg')
                    continue;
                $str = public_path() . $picture->path;
                unlink($str);
            }
        }

        $news->pictures()->detach();
        $news->pictures()->delete();

        $news->delete();
        Session::flash('toastr', 'News Post Successfully Deleted');

        return Redirect::to('/admin/news');
    }

    public function activation(News $news)
    {
        $news->toggleActivation();
        return back();
    }

    public function getNewsData(Request $request)
    {
        $news = News::with(['major', 'category', 'user', 'tags']);
        if ($request->has('status')) {
            if ($request->status == 'published')
                $news->where('status', 1);
            else if ($request->status == 'unpublished')
                $news->where('status', 0);
        }
        if ($request->has('major')) {
            $news->whereHas('major', function ($query) use ($request) {
                $query->where('name', 'like', $request->major);
            });
        }
        if ($request->has('category')) {
            $news->whereHas('category', function ($query) use ($request) {
                $query->where('name', 'like', $request->category);
            });
        }
        if ($request->has('tag')) {
            $news->whereHas('tags', function ($query) use ($request) {
                $query->where('name', 'like', $request->tag);
            });
        }
        if ($request->has('user')) {
            $userIds = User::searchUser(trim($request->user), 'id');
            $news->whereIn('user_id', $userIds);
        }

        return Datatables::of($news)
            ->addColumn('category', function ($new) {
                return $new->category ? $new->category->name : '';
            })
            ->addColumn('major', function ($new) {
                return $new->major ? $new->major->name : '';
            })
            ->addColumn('user', function ($new) {
                return $new->user ? $new->user->name : '';
            })
            ->addColumn('tags', function ($new) {

                return implode(', ', $new->tags->pluck('name')->toArray());
            })
            ->addColumn('comments', function ($post) {
                return '<div class="text-center"><a href="' . $post->path(true) . '" class="btn btn-xs news-comments btn-warning">' . $post->comments()->count() . '</a></div>'; // TODO: Optimize
            })
            ->editColumn('status', function ($new) {
                $class = $new->status ? 'success' : 'danger';
                $status = $new->status ? 'Unpublish' : 'Publish';
                return '<a href="' . $new->path(true) . '/activation" class="btn btn-xs news-status btn-' . $class . '">' . $status . '</a>';

            })->editColumn('created_at', function ($new) {
                return $new->created_at->toFormattedDateString();
            })
            ->editColumn('body', function ($new) {
                return str_limit($new->body, 200);
            })
            ->make(true);
    }
}
