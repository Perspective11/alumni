<?php

namespace App\Http\Controllers;

use App\Comment;
use App\Event;
use App\News;
use App\Post;
use Auth;
use Illuminate\Http\Request;
use Redirect;
use Session;

class CommentController extends Controller
{

    public function __construct()
    {
        $this->middleware('graduate');
    }

    public function store($id, Request $request)
    {
        $this->validate($request, [
            'body' => 'required|max:255',
        ]);


        if ( str_contains($request->getRequestUri() , 'posts/')){
            $commentable = Post::find($id);
        }
        else if ( str_contains($request->getRequestUri() , 'news/')){
            $commentable = News::find($id);
        }
        else if ( str_contains($request->getRequestUri() , 'events/')){
            $commentable = Event::find($id);
        }

        $comment = new Comment(
            [
                'body' => request('body'),
            ]
        );
        $comment->commentable()->associate($commentable);
        $comment->user()->associate(Auth::user());
        $comment->save();

        Session::flash('toastr', 'Comment Successfully Created');
        return redirect()->back();
    }

    public function update($comment,Request $request)
    {
        Comment::find($comment)->update(['body' => $request->body]);

        Session::flash('toastr','comment has been updated successfully');
        return back();
    }

    public function delete($comment){
        Comment::find($comment)->delete();
        Session::flash('toastr','deleted successfully');
        return back();
    }
}
