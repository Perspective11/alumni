<?php

namespace App\Http\Controllers;

use App\Http\Middleware\regular;
use App\Tag;
use Illuminate\Http\Request;
use Session;
use Validator;
use Yajra\Datatables\Datatables;

class TagsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        return view('admin.tag.index');

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view ('admin.tag.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        Validator::make($request->all(), [
            "tags" => 'required|array|min:1',
            "tags.*" => 'required|string|distinct|between:2,10|unique:tags,name',
        ], $this->messages())->validate();
        $createdCount = 0;
        foreach(request('tags') as $tag){
            Tag::create([
               'name' => $tag
            ]);
            $createdCount ++;
        }
        Session::flash('toastr', $createdCount . ' Tags Added Successfully');
        return redirect('/admin/tags');
    }
    public function messages()
    {
        $messages = [];
        foreach(request('tags') as $key => $val)
        {
            $messages['tags.'.$key.'.between'] = 'The field "Tag '.$key.'" must be between :min - :max characters.';
            $messages['tags.'.$key.'.distinct'] = 'The field "Tag '.$key.'" must be distinct.';
            $messages['tags.'.$key.'.string'] = 'The field "Tag '.$key.'" must be a string.';
            $messages['tags.'.$key.'.required'] = 'The field "Tag '.$key.'" is required.';
            $messages['tags.'.$key.'.unique'] = 'The field "Tag '.$key.'" already exists in the database.';
        }
        return $messages;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Tag $tag)
    {
        return view('admin.tag.edit', compact('tag'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Tag $tag)
    {
        Validator::make($request->all(), [
            "name" => 'required|string|between:2,10|unique:tags,name,' . $tag->id
        ])->validate();
        $tag->update([
            'name' => $tag->name
        ]);
        Session::flash('toastr', 'Tag Updated Successfully');
        return redirect('/admin/tags');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Tag $tag)
    {
        $tag->posts()->detach();
        $tag->events()->detach();
        $tag->news()->detach();
        $tag->delete();
        Session::flash('toastr', 'Tag Deleted Successfully');
        return redirect('/admin/tags');
    }

    public function getTagsData(Request $request)
    {
        $tags = Tag::orderBy('name');
        return Datatables::of($tags)
            ->addColumn('posts', function ($tag) {
                $posts = $tag->posts()->count();
                return "<div class=''><a class='model-link' href=''><span class='label label-success label-lg'>{$posts}</span></div></a>";
            })
            ->addColumn('events', function ($tag) {
                $events = $tag->events()->count();
                return "<div class=''><a class='model-link' href=''><span class='label label-info label-lg'>{$events}</span></div></a>";
            })
            ->addColumn('news', function ($tag) {
                $news = $tag->news()->count();
                return "<div class=''><a class='model-link' href=''><span class='label label-warning label-lg'>{$news}</span></div></a>";
            })
            //TODO: fill href attributes with proper link
            ->make(true);
    }
}
