<?php

namespace App\Http\Controllers;

use App\Category;
use App\Event;
use App\Http\Requests\CreateEventRequest;
use App\Major;
use App\Picture;
use App\Tag;
use App\User;
use Auth;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Redirect;
use Session;
use Yajra\Datatables\Datatables;

class EventsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $recentEvents = Event::where('status','1')->latest()->take(5)->get();
        $eventsWithinDate = null;
        $events = Event::where('status','1')->latest()->paginate(10);
        $categories = Category::has('events')->get();
        $tags = Tag::has('events')->get();
        if (isset($request->date) && !empty($request->date)){
            $date = new Carbon($request->date);
            $date = $date->format('Y-m-d');
            $events = Event::where('date', $date)->where('status', '1')->latest()->paginate(10);
        }
        if (isset($request->search) && !empty($request->search)){
            $events = Event::where('title','like','%'.$request->search.'%')->where('status','1')->latest()->paginate(10);
            $categories = Category::has('events')->get();
            $tags = Tag::has('events')->get();
        }
        return view('public.events.index',compact('events','categories', 'tags','recentEvents'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $categories = Category::all();
        $tags = Tag::all();
        $majors = Major::all();

        return view('events.create', compact('categories', 'tags', 'majors'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \App\Http\Requests\CreateEventRequest|\Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function store(CreateEventRequest $request)
    {
        $event = Auth::user()->events()->create([
                "category_id" => request('category'),
                "major_id"    => request('major'),
                "title"       => request('title'),
                "body"        => request('body'),
                "date"        => request('date'),
                "start"       => request('start'),
                "end"         => request('end'),
                "location"    => request('location'),
            ]

        );

        $event->tags()->attach(request('tags'));

        if ($image = $request->file('picture'))
        {
            $image_name = Picture::getSanitizedName($image);
            $image_path = 'images/events/' . $image_name;
            $picture = Picture::modifyImage($image);
            $picture->save($image_path);
            $picture = $event->pictures()->create(
                [
                    'name' => $image_name,
                    'path' => '/' . $image_path,
                ]
            );


        }

        Session::flash('toastr', 'Event Successfully Created');

        return Redirect::to('events');
    }

    /**
     * Display the specified resource.
     *
     * @param \App\Event $event
     *
     * @return \Illuminate\Http\Response
     * @internal param int $id
     *
     */
    public function show(Event $event)
    {
        $recentEvents = Event::where('status', '1')->latest()->take(5)->get();

        return view('public.events.show', [
            'event'        => $event->load(['tags', 'comments.user.graduate.pictures', 'category', 'major', 'pictures']),
            'recentEvents' => $recentEvents,
        ]);

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $event = Event::find($id);
        $categories = Category::all();
        $tags = Tag::all();
        $majors = Major::all();

        return view('events.edit', compact('event', 'categories', 'tags', 'majors'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \App\Http\Requests\CreateEventRequest|\Illuminate\Http\Request $request
     * @param  int                                                           $id
     *
     * @return \Illuminate\Http\Response
     */
    public function update(CreateEventRequest $request, $id)
    {
        //

        $event = Event::find($id);
        $event->update([
            "category_id" => request('category'),
            "major_id"    => request('major'),
            "title"       => request('title'),
            "body"        => request('body'),
            "date"        => request('date'),
            "start"       => request('start'),
            "end"         => request('end'),
            "location"    => request('location'),
        ]);
        if ($request->has('tags'))
        {
            $event->tags()->sync($request->input('tags'));
        } else
        {
            $event->tags()->detach();
        }
        if ($image = $request->file('picture'))
        {
            if ($event->pictures->count())
            {
                foreach ($event->pictures as $picture)
                {
                    if ($picture->name === 'default.png' || $picture->name === 'default.jpg')
                        continue;
                    $str = public_path() . $picture->path;
                    unlink($str);
                }
            }

            $event->pictures()->detach();
            $event->pictures()->delete();
            $image_name = Picture::getSanitizedName($image);
            $image_path = 'images/events/' . $image_name;
            $picture = Picture::modifyImage($image);
            $picture->save($image_path);

            $picture = $event->pictures()->create(
                [
                    'name' => $image_name,
                    'path' => '/' . $image_path,
                ]
            );
        }

        Session::flash('toastr', 'Event Successfully Updated');

        return redirect('events');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $event = Event::findOrFail($id);


        if ($event->pictures->count())
        {
            foreach ($event->pictures as $picture)
            {
                if ($picture->name === 'default.png' || $picture->name === 'default.jpg')
                    continue;
                $str = public_path() . $picture->path;
                unlink($str);
            }
        }

        $event->pictures()->detach();
        $event->pictures()->delete();

        $event->delete();
        Session::flash('toastr', 'Event Successfully Deleted');

        return Redirect::to('events');
    }

    public function tag(Tag $tag, Request $request)
    {
        $recentEvents = Event::where('status', '1')->latest()->take(5)->get();
        if (isset($request->search) && !empty($request->search))
        {
            $events = $tag->events()->where('title', 'like', '%' . $request->search . '%')->where('status', '1')->latest()->paginate(10);

            return view('public.events.tag', compact('events', 'recentEvents', 'tag'));
        }
        $events = $tag->events()->where('status', '1')->paginate(10);

        return view('public.events.tag', compact('events', 'recentEvents', 'tag'));
    }

    public function category(Category $category, Request $request)
    {
        $recentEvents = Event::where('status', '1')->latest()->take(5)->get();
        if (isset($request->search) && !empty($request->search))
        {
            $events = $category->events()->where('title', 'like', '%' . $request->search . '%')->where('status', '1')->latest()->paginate(10);

            return view('public.events.category', compact('events', 'recentEvents', 'category'));
        }
        $events = $category->events()->where('status', '1')->paginate(10);

        return view('public.events.category', compact('events', 'recentEvents', 'category'));
    }
}
