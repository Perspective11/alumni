<?php

namespace App\Http\Controllers;

use App\Graduate;
use App\User;
use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;

class GraduateController extends Controller
{
    /** @TODO: the graduate cannot create news or events */

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        return view('admin.graduate.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Graduate $graduate)
    {
        return redirect($graduate->user->path(true));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Graduate $graduate)
    {
        return redirect($graduate->user->path(true) . '/edit');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Graduate $graduate)
    {
        
    }
    public function changeRoleShow(Graduate $graduate){
        return redirect($graduate->user->path(true) . '/changeRole');
    }

    public function getGraduatesData(Request $request){
        $graduates = Graduate::with('user', 'major');

//        - status
//        - user name
//        - major
//        - year
//        - gender
//        - hirable
        if($request->has('status')){
            $graduates->where('graduate_status', 'like', $request->status);
        }

        if ($request->has('user')) {
            $userIds = User::searchUser(trim($request->user) , 'id');
            $graduates->whereIn('user_id', $userIds);
        }
        if ($request->has('major')) {
            $graduates->whereHas('major', function ($query) use ($request) {
                $query->where('name', 'like', $request->major);
            });
        }
        if ($request->has('year')) {
            $graduates->where('graduation_year', $request->year);
        }
        if ($request->has('gender')) {
            $gender = $request->gender;
            if ($gender == 'male')
                $graduates->where('gender', '=', 1);
            elseif ($gender == 'female')
                $graduates->where('gender', '=', 0);
        }
        if ($request->has('hireable')) {
            $hireable = $request->hireable;
            if ($hireable == 'yes')
                $graduates->where('is_hireable', '=', 1);
            elseif ($hireable == 'no')
                $graduates->where('is_hireable', '=', 0);
        }


        return Datatables::of($graduates)
            ->addColumn('age', function ($graduate) {
                if($age = $graduate->age()){
                    return "<div class='text-center'><span class='label label-warning label-lg'>{$age}</span></div>";
                }
                else return '';
            })
            ->addColumn('major', function ($graduate) {
                return $graduate->major ? $graduate->major->name : '';
            })
            ->addColumn('email', function ($graduate) {
                return $graduate->user->email;
            })
            ->addColumn('links',function ($graduate)
            {
                $htmlSting = "<div class='text-center link-icons'>";
                foreach ($graduate->links() as $link){
                    $htmlSting .= "<a class='link-icon' href='".
                        $link['link'] ."'><i style='color:".
                        $link['color'] ."' class='fa fa-".
                        $link['icon'] ."'></i></a>";
                }
                $htmlSting .= "</div>";

                return $htmlSting;

            })->editColumn('gender',function ($graduate)
            {
                $gender = $graduate->gender ? 'male': 'female';
                $color = $graduate->gender ? 'lightblue': 'pink';
                return '<div class="text-center"><span style="color:'. $color .'" class="fa-2x fa fa-'. $gender .'"></span></div>';

            })
            ->editColumn('is_hireable',function ($graduate)
            {
                $check = '';
                $hireable = $graduate->is_hireable;
                if($hireable === 0)
                    $check = 'times';
                elseif($hireable === 1)
                    $check = 'check';
                else return '';
                $color = $graduate->is_hireable ? 'lightgreen': 'lightsalmon';
                return '<div class="text-center"><span style="color:'. $color .'" class="fa-2x fa fa-'. $check .'"></span></div>';

            })
            ->editColumn('graduation_year',function ($graduate)
            {
                return '<div class="text-center"><span class="label label-info">'. $graduate->graduation_year .'</span></div>';

            })
            ->editColumn('gpa', function ($graduate) {
                if($gpa = $graduate->gpa){
                    return "<div class='text-center'><span style='background-color:lightsteelblue !important;' class='label label-success'>{$gpa}</span></div>";
                }
                else return '';
            })
            ->editColumn('graduate_status', function ($graduate) {
                if ($status = $graduate->graduate_status) {
                    return "<div class='text-center'><span style='background-color:lightgrey !important;' class='label label-primary'>{$status}</span></div>";
                } else return '';
            })


            ->make(true);
    }
}
