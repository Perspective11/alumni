<?php

namespace App\Http\Controllers;

use App\Category;
use App\Http\Requests\CategoryRequest;
use Session;
use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;

class CategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.category.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.category.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CategoryRequest $request)
    {
//        return $request->all();
        $category = Category::create([
            'name' => $request->name,
            'color' => $request->color,
            'icon' => $request->icon
        ]);

        Session::flash('toastr', 'Category Successfully Created');

        return redirect('/admin/categories/create');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Category $category)
    {
        return view('admin.category.show',compact('category'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Category $category)
    {
        return view('admin.category.edit',compact('category'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(CategoryRequest $request, Category $category)
    {
        $category->update([
            'name' => $request->name,
            'color' => $request->color,
            'icon' => $request->icon
        ]);

        Session::flash('toastr','Category has been updated');

        return redirect('/admin/categories');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Category $category)
    {
        $category->delete();
        Session::flash('toastr', 'Category Deleted Successfully');
        return redirect('/admin/categories');
    }


    public function getCategoriesData(Request $request)
    {
        $categories = Category::orderBy('name');


        return Datatables::of($categories)
            ->editColumn('icon', function ($category) {
                return '<div class=""><i style="color: '. $category->color .'" class="fa '. $category->icon .'"></i></div>';
            })
            ->addColumn('posts', function ($category) {
                $posts = $category->posts()->count();
                return "<div class=''><a class='model-link' href=''><span class='label label-success label-lg'>{$posts}</span></div></a>";
            })
            ->addColumn('events', function ($category) {
                $events = $category->events()->count();
                return "<div class=''><a class='model-link' href=''><span class='label label-info label-lg'>{$events}</span></div></a>";
            })
            ->addColumn('news', function ($category) {
                $news = $category->news()->count();
                return "<div class=''><a class='model-link' href=''><span class='label label-warning label-lg'>{$news}</span></div></a>";
            })
            //TODO: fill href attributes with proper link
            ->make(true);
    }
}
