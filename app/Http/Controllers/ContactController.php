<?php

namespace App\Http\Controllers;

use App\Contact;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Session;
use Yajra\Datatables\Datatables;

class ContactController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.contact.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Contact $contact)
    {
        $contact->delete();
        Session::flash('toastr', 'Contact Message Successfully Deleted');
        return redirect('/admin/contacts');
    }

    public function getContactsData(Request $request){
        $contacts = Contact::query();

        if ($request->has('daterange')) {
            $arr = explode(' - ', request('daterange'));
            $startDate = Carbon::parse($arr[0])->toDateString();
            $endDate = Carbon::parse($arr[1])->addDay()->toDateString();

            $contacts->whereBetween('created_at', [$startDate, $endDate]);
        }
        return Datatables::of($contacts)
            ->editColumn('created_at', function ($post) {
                return $post->created_at->toFormattedDateString();
            })->make(true);
    }
}
