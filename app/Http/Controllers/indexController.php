<?php

namespace App\Http\Controllers;

use App\Company;
use App\Contact;
use App\Event;
use App\Graduate;
use App\Http\Requests\editCompanyRequest;
use App\Http\Requests\editGraduateRequest;
use App\Http\Requests\profileRequest;
use App\Picture;
use App\Post;
use App\Setting;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Hash;
use Session;
use App\User;
use Illuminate\Http\Request;

class indexController extends Controller {

    public function __construct()
    {
        $this->middleware('regular', ['only' => 'profile']);
    }

    //
    public function index()
    {
        if (Setting::where('name', 'home')->count())
        {
            $property = (array)json_decode(Setting::where('name', 'home')->first()->properties);
            $photosSlider = $property['slider'];
        } else
            $photosSlider = null;
        $posts = Post::where('status', '1')->count();
        $trendingPosts = Post::where('status', '1')->with('pictures')->withCount(['comments', 'pictures'])->orderBy('comments_count', 'desc')->take(6)->get();
        $companies = Company::whereHas('user', function ($user) {
            $user->where('user_status', '1');
        })->get();
        $graduates = Graduate::whereHas('user', function ($user) {
            $user->where('user_status', '1');
        })->get();;

        return view('public.index', compact('posts', 'companies', 'graduates', 'trendingPosts', 'photosSlider'));
    }

    public function contact(Request $request)
    {
        if (Setting::where('name', 'contact')->count())
        {
            $contact = (array)json_decode(Setting::where('name', 'contact')->first()->properties);
            $contact_header = $contact['header'];
            $contact_info = $contact['info'];
        } else
        {
            $contact_header = null;
            $contact_info = null;
        }

        return view('public.contact', compact('contact_header', 'contact_info'));
    }

    public function profile(User $user)
    {
        if (Auth::user()->isGraduate())
        {
            if (!$user->user_status)
            {
                Session::flash('toastr', ['This user does not exist', 'warning']);

                return redirect()->route('profile', Auth::id());
            }

            return view('public.profile.index', [
                'user' => $user->load('graduate')
            ]);
        } elseif (Auth::user()->isCompany())
        {
            if ($user->id == Auth::id())
            {
                return view('public.profile.index', [
                    'user' => $user->load('company')
                ]);
            }
            if ($user->isGraduate())
            {
                return view('public.profile.index', [
                    'user' => $user->load('graduate')
                ]);
            } else
            {
                return redirect()->route('home');
            }
        }
    }

    public function profileEdit(User $user)
    {
        if ($user->id == auth()->id())
        {
//            return auth()->user()->graduate->dob->year;
            return view('public.profile.edit', compact('user'));
        }

        return redirect()->route('profile', auth()->id());
    }

    public function profileGUpdate(User $user, editGraduateRequest $request)
    {
        $graduate = $user->graduate;

        $user->name = $request->name;
        $user->save();

        if ($request->file('resume'))
        {
            if ($graduate->file_path)
            {
                $str = storage_path('app/public/' . $graduate->file_path);
                File::delete($str);
            }
            $guessExtension = $request->file('resume')->guessExtension();

            $fileName = Carbon::now()->timestamp . '_' . $user->name . '.' . $guessExtension;

            $path = $request->file('resume')->storeAs('public/resumes', $fileName, null);

            $user->graduate()->update([
                'file_path' => $path,
            ]);
        }


        if (!$request['gpa']) // because nullable rule doesnt fucking work
        {
            $request['gpa'] = null;
        }
        if (!$request['years_of_experience']) // because nullable rule doesnt fucking work
        {
            $request['years_of_experience'] = null;
        }

        $graduate->update($request->all());

        if ($image = $request->file('picture'))
        {
            if ($graduate->pictures->count())
            {
                foreach ($graduate->pictures as $picture)
                {
                    if ($picture->name === 'default.png' || $picture->name === 'default.jpg')
                        continue;
                    $str = public_path() . $picture->path;
                    unlink($str);
                }
            }

            $graduate->pictures()->detach();
            $graduate->pictures()->delete();
            $image_name = Picture::getSanitizedName($image);
            $image_path = 'images/graduates/profile/' . $image_name;

            $picture = Picture::modifyImage($image);
            $picture->save($image_path);

            $picture = $graduate->pictures()->create(
                [
                    'name' => $image_name,
                    'path' => '/' . $image_path,
                ]
            );
        }


        Session::flash("toastr", "User Profile Updated Successfully!");

        return redirect()->route('profile', auth()->id());
    }

    public function profileCUpdate(User $user, editCompanyRequest $request)
    {
        $company = $user->company;

        $user->name = $request->name;
        $user->save();

        $company->update($request->all());


        if ($image = $request->file('picture'))
        {
            if ($company->pictures->count())
            {
                foreach ($company->pictures as $picture)
                {
                    if ($picture->name === 'default.png' || $picture->name === 'default.jpg')
                        continue;
                    $str = public_path() . $picture->path;
                    unlink($str);
                }
            }

            $company->pictures()->detach();
            $company->pictures()->delete();
            $image_name = Picture::getSanitizedName($image);
            $image_path = 'images/companies/profile/' . $image_name;
            $picture = Picture::modifyImage($image);
            $picture->save($image_path);

            $picture = $company->pictures()->create(
                [
                    'name' => $image_name,
                    'path' => '/' . $image_path,
                ]
            );
        }


        Session::flash("toastr", "Company Profile Updated Successfully!");

        return redirect()->route('profile', auth()->id());
    }

    public function profileDownload(User $user, Graduate $graduate)
    {
        return response()->download(storage_path('app/' . $graduate->file_path), $graduate->full_name . '.pdf', ['Content-Type: application/pdf',]);
    }

    public function profileView(User $user, $graduate)
    {
        $fullName = str_replace('-', ' ', $graduate);
        $graduate = Graduate::where('full_name', 'like', '%' . $fullName . '%')->firstOrFail();

        return response(File::get(storage_path('app/' . $graduate->file_path)), 200)->header('Content-Type', 'application/pdf');
    }

    public function showCalendar()
    {
        return view('public.calendar.index');
    }

    public function getEvents(Request $request)
    {
        $arr = [];
        $events = Event::whereBetween('date', [$request->start, $request->end])->each(function ($event) use (&$arr) {
            array_push($arr, [
                'id'    => $event->id,
                'title' => str_limit($event->title, 100),
                'start' => $event->date->toDateString() . ' ' . $event->start,
                'end'   => $event->date->toDateString() . ' ' . $event->end,
                'url'   => $event->path(),
            ]);
        });

        return $arr;
    }

    public function changePassword(Request $request, User $user)
    {
        if (Hash::check($request->oldPassword, $user->password))
        {
            $this->validate($request, [
                'oldPassword'              => 'required',
                'newPassword'              => 'required|min:6|confirmed',
                'newPassword_confirmation' => 'required',
            ]);
            $user->password = bcrypt($request->newPassword);

            $user->save();

            return response()->json([
                'success' => [
                    'status'   => 200,
                    'messages' => 'Password has changed successfully',
                ],
            ]);
        }

        return response()->json([
            'errors' => [
                'status'   => 401,
                'messages' => [
                    'oldPass' => 'the password is not correct',
                ],
            ],
        ]);
    }

    /**
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function storeContact(Request $request)
    {
        $this->validate($request, [
            'name'    => 'required|max:100|string',
            'email'   => 'required|email',
            'message' => 'required|max:400|min:5',
        ]);
        Contact::create(['name' => $request->name, 'email' => $request->email, 'message' => $request->message]);
        Session::flash('toastr', 'The message has been sent');

        return back();
    }
}
