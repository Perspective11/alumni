<?php

namespace App\Http\Controllers;

use App\Graduate;
use App\Http\Requests\editGraduateRequest;
use App\Major;
use App\Picture;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Redirect;
use Session;

class GraduatesController extends Controller
{

    /**
     * GraduatesController constructor.
     */
    public function __construct()
    {
        $this->middleware('regular');
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $recentGraduates = Graduate::whereHas('user', function($user){
            $user->where('user_status','1');
        })->take(5)->get();
        $majors = Major::has('graduates')->get();
        if (isset($request->name,$request->id)){
            $graduates = Graduate::whereHas('user', function($user){
                $user->where('user_status','1');
            })->where('full_name','like','%'.$request->name.'%')->where('liu_id','like','%'.$request->id.'%')->latest()->paginate(10);
            return view('public.graduates.index', compact('graduates','recentGraduates','majors'));
        }
        $graduates = Graduate::with('major')->whereHas('user', function ($user){$user->where('user_status','1');})->latest()->paginate(10);
        return view('public.graduates.index', compact('graduates','recentGraduates','majors'));
//        TODO: or make it like the tags in the posts page
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Graduate $graduate)
    {
//        return $graduate;
        $user = $graduate->user;
        return view('graduates.show', compact('user'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $graduate = Graduate::find($id);
        $user = $graduate->user;
        return view('graduates.edit', compact('user'));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(editGraduateRequest $request, $id)
    {
        //
        //dd($request['picture']);
        $graduate = Graduate::find($id);
        $user = $graduate->user;

        $date = $request['dobDay'] . '-' . $request['dobMonth'] . '-' . $request['dobYear'];
        if (! $date = Carbon::parse($date))
        {
            $messages = "The date you entered is invalid.";
            return Redirect::to('/graduates/' . $id . '/edit')->withErrors($messages);
        }

        if(!$request['gpa']) // because nullable rule doesnt fucking work
        {
            $request['gpa'] = null;
        }
        if(!$request['years_of_experience']) // because nullable rule doesnt fucking work
        {
            $request['years_of_experience'] = null;
        }

        $request->request->add(['dob' => $date]);

        $user->update($request->all());
        $graduate->update($request->all());

        if($image = $request->file('picture'))
        {
            if ($graduate->pictures->count()){
                foreach($graduate->pictures as $picture)
                {
                    if ($picture->name === 'default.png' || $picture->name === 'default.jpg')
                        continue;
                    $str = public_path() . $picture->path;
                    unlink($str);
                }
            }

            $graduate->pictures()->detach();
            $graduate->pictures()->delete();
            $image_name = Picture::getSanitizedName($image);
            $image_path = 'images/graduates/profile/' . $image_name;
            $picture = Picture::modifyImage($image);
            $picture->save($image_path);

            $picture = $graduate->pictures()->create(
                [
                    'name' => $image_name,
                    'path' => '/' . $image_path,
                ]
            );
        }


        Session::flash("toastr", "User Profile Updated Successfully!");
        return redirect($graduate->path());
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function major(Major $major,Request $request){
        if (isset($request->name,$request->id)) {
            $graduates = $major->graduates()->whereHas('user', function($user){
                $user->where('user_status','1');
            })->where('full_name','like','%'.$request->name.'%')->where('liu_id','like','%'.$request->id.'%')->latest()->paginate(10);
            return view('public.graduates.major', compact('graduates'));
        }
        $graduates = $major->graduates()->whereHas('user', function($user){
            $user->where('user_status','1');
        })->latest()->paginate(10);
        return view('public.graduates.major', compact('graduates'));
    }
}
