<?php

namespace App\Http\Controllers;

use App\Company;
use App\Http\Requests\editCompanyRequest;
use App\Picture;
use Illuminate\Http\Request;
use Session;

class CompaniesController extends Controller
{
    public function __construct()
    {
        $this->middleware('graduate');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $recentCompanies = Company::whereHas('user', function($user){
            $user->where('user_status','1');
        })->take(5)->get();
        if (isset($request->search) && !empty($request->search)){
            $companies = Company::whereHas('user', function ($user){$user->where('user_status','1');})->where('company_name','like','%'.$request->search.'%')->latest()->paginate(10);
//            return view('public.companies.index', compact('companies','recentCompanies'));
        }else{
            $companies = Company::whereHas('user', function ($user){$user->where('user_status','1');})->latest()->paginate(10);
        }
        $industries = Company::distinct('industry')->get(['industry']);
        return view('public.companies.index', compact('companies','industries','recentCompanies'));
//        TODO: or make it like the tags in the posts page
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        $company = Company::find($id);

        return view('companies.show', compact('company'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $company = Company::find($id);
        return view('companies.edit', compact('company'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(editCompanyRequest $request, $id)
    {
        //
        //dd($request['picture']);
        $company = Company::find($id);


        //dd($request->all());

        $company->update($request->all());


        if ($image = $request->file('picture')) {
            if ($company->pictures->count()) {
                foreach ($company->pictures as $picture) {
                    if ($picture->name === 'default.png' || $picture->name === 'default.jpg')
                        continue;
                    $str = public_path() . $picture->path;
                    unlink($str);
                }
            }

            $company->pictures()->detach();
            $company->pictures()->delete();
            $image_name = Picture::getSanitizedName($image);
            $image_path = 'images/companies/profile/' . $image_name;
            $picture = Picture::modifyImage($image);
            $picture->save($image_path);

            $picture = $company->pictures()->create(
                [
                    'name' => $image_name,
                    'path' => '/' . $image_path,
                ]
            );
        }


        Session::flash("toastr", "Company Profile Updated Successfully!");
        return redirect($company->path());
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function industry(Request $request,$industry){
        if (isset($request->search) && !empty($request->search)){
            $companies = Company::whereHas('user', function ($user){$user->where('user_status','1');})->where('company_name','like','%'.$request->search.'%')->latest()->paginate(10);
            return view('public.companies.industry', compact('companies','industry'));
        }
        $companies = Company::whereHas('user', function ($user){$user->where('user_status','1');})->where('industry',$industry)->latest()->paginate(10);
        return view('public.companies.industry',compact('companies','industry'));
    }
}
