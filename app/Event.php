<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Session;

class Event extends Model
{
    use StatisticsFunctions;

    protected $fillable = [
        "parent_id",
        "user_id",
        "category_id",
        "major_id",
        "title",
        "body",
        "type",
        "date",
        "start",
        "end",
        "location",
        "status"
    ];
    protected $dates = [
      'date'
    ];

    public function parent()
    {
        return $this->belongsTo('App\Event', 'parent_id');
    }

    public function children()
    {
        return $this->hasMany('App\Event', 'parent_id');
    }

    public function category()
    {
        return $this->belongsTo('App\Category');
    }
    public function major()
    {
        return $this->belongsTo('App\Major');
    }

    public function user()
    {
        return $this->belongsTo('App\User');
    }
    public function tags()
    {
        return $this->morphToMany('App\Tag', 'taggable');
    }
    public function tagsString()
    {
        return implode(', ', $this->tags->pluck('name')->toArray());
    }
    public function tagsWithLinks()
    {
        $tags = $this->tags->map(function ($tag) {
            $url = '/events/tag/' . $tag->id;
            return  "<a href='{$url}'>{$tag->name}</a>";
        })->toArray();
        return implode(', ', $tags);
    }
    public function pictures()
    {
        return $this->morphToMany('App\Picture', 'imageable');
    }
    public function comments()
    {
        return $this->morphMany('App\Comment', 'commentable');
    }

    public function path($admin = null){
        if($admin){
            return '/admin/events/' . $this->id;
        }
        return '/events/' . $this->id;
    }
    public function togglePublished(){
        if ($this->status){
            $this->status = 0;
            Session::flash('toastr','Event is unpublished');
        }
        else{
            $this->status = 1;
            Session::flash('toastr','Event is published');
        }

        $this->save();
    }

    public function getPicture(){
        if ($this->pictures()->count()){
            return $this->pictures()->first()->path;
        }
        else return '/images/events/default.png';
    }

    public static function publishedPercentage($days = 30)
    {
        $date = Carbon::now()->addDays(-$days);

        $count = static::where('created_at', '>=', $date->toDateString())->count();
        $publishedCount = static::where('created_at', '>=', $date->toDateString())->where('status', 1)->count();

        return compact('publishedCount', 'count');
    }
}
