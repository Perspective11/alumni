<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Session;

class User extends Authenticatable {

    use Notifiable;
    use StatisticsFunctions;


    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'full_name',
        'user_status',
        'email',
        'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function roles()
    {
        return $this->belongsToMany('App\Role');
    }

    public function posts()
    {
        return $this->hasMany('App\Post');
    }

    public function events()
    {
        return $this->hasMany('App\Event');
    }

    public function news()
    {
        return $this->hasMany('App\News');
    }

    public function graduate()
    {
        return $this->hasOne('App\Graduate');
    }

    public function company()
    {
        return $this->hasOne('App\Company');
    }

    public function comments()
    {
        return $this->hasMany('App\Comment');

        //TODO: replace relationship with "hasManyThrough" Posts
    }

    public function path($admin = null)
    {
        if ($admin)
        {
            return '/admin/users/' . $this->id;
        }

        return '/users/' . $this->id;
    }

    public function isCompany()
    {
        return (bool)count($this->company);
    }

    public function isGraduate()
    {
        return (bool)count($this->graduate);
    }

    public function child()
    {
        if ((bool)count($this->company))
        {
            return $this->company;
        }
        if ((bool)count($this->graduate))
        {
            return $this->graduate;
        }
    }

    public function childName()
    {
        if ((bool)count($this->company))
        {
            return $this->company->company_name;
        }
        if ((bool)count($this->graduate))
        {
            return $this->graduate->full_name;
        }

        return '';
    }

    public function childOrUserName()
    {
        if ((bool)count($this->company))
        {
            return $this->company->company_name;
        }
        if ((bool)count($this->graduate))
        {
            return $this->graduate->full_name;
        }

        return $this->name;
    }


    public function hasRoles()
    {
        return (bool)count($this->roles);
    }

    public function isAdmin()
    {
        if ($this->hasRoles())
        {
            if ($this->roles()->first()->id != 1)
                return false;

            return true;
        }

        return false;
    }

    public function isModerator()
    {
        if ($this->hasRoles())
        {
            if ($this->roles()->first()->id != 2)
                return false;

            return true;
        }

        return false;
    }

    public function isRegular()
    {
        if ($this->hasRoles())
        {
            if ($this->roles()->first()->id == 3 || $this->roles()->first()->id == 4)
                return true;

            return false;
        }

        return false;
    }

    public function isAdministration()
    {
        if ($this->hasRoles())
        {
            if ($this->roles()->first()->id == 1 || $this->roles()->first()->id == 2)
                return true;

            return false;
        }

        return false;
    }

    public function getPicture()
    {
        if ($this->child() && $this->child()->pictures()->count())
        {
            return $this->child()->pictures()->first()->path;
        } elseif ($this->isGraduate())
            return '/images/graduates/profile/default.png';
        elseif ($this->isCompany())
            return '/images/companies/profile/default.jpg';
        else return '/images/graduates/profile/default.png';
    }

    public function toggleActive()
    {
        if ($this->user_status)
        {
            $this->user_status = 0;

            Session::flash('toastr', 'User successfully deactivated');
        } else
        {
            $this->user_status = 1;
            Session::flash('toastr', 'User successfully activated');
        }
        $this->save();
    }

    public static function searchUser($string, $returnType = null)
    {
        $c = collect(new User);
        $users = User::where('name', 'like', '%' . $string . '%')->with('company', 'graduate')->get();
        foreach ($users as $user)
        {
            $c->push($user);
        }
        $graduates = Graduate::where('liu_id', 'like', '%' . $string . '%')->orWhere('full_name', 'like', '%' . $string . '%')->with('user')->get();
        foreach ($graduates as $graduate)
        {
            $c->push($graduate->user->load('graduate'));
        }
        $companies = Company::where('company_name', 'like', '%' . $string . '%')->with('user')->get();
        foreach ($companies as $company)
        {
            $c->push($company->user->load('company'));
        }
        if ($returnType == 'id')
        {
            return $c->pluck('id')->toArray();
        }

        return $c;
    }

    public static function activatedPercentage($days = 30)
    {
        $date = Carbon::now()->addDays(-$days);

        $count = static::where('created_at', '>=', $date->toDateString())->count();
        $activatedCount = static::where('created_at', '>=', $date->toDateString())->where('user_status', 1)->count();

        return compact('activatedCount', 'count');
    }

}
