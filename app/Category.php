<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    //
    protected $fillable = [
        "parent_id",
        "name",
        "color",
        "icon",
    ];

    public function parent()
    {
        return $this->belongsTo('App\Category', 'parent_id');
    }

    public function children()
    {
        return $this->hasMany('App\Category', 'parent_id');
    }

    public function posts()
    {
        return $this->hasMany('App\Post');
    }
    public function news()
    {
        return $this->hasMany('App\News');
    }
    public function events()
    {
        return $this->hasMany('App\Event');
    }
    public function path($admin = false){
        if($admin)
            return '/admin/categories/' . $this->id;
        return '/category/'.$this->id;
    }
}
