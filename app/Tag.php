<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tag extends Model
{
    //
    protected $fillable = [
        "name",
    ];
    public function posts()
    {
        return $this->morphedByMany('App\Post', 'taggable');
    }

    public function news()
    {
        return $this->morphedByMany('App\News', 'taggable');
    }

    public function events()
    {
        return $this->morphedByMany('App\Event', 'taggable');
    }

    public function path($admin = false){
        if($admin)
            return '/admin/tags/' . $this->id;
        return '/tag/'.$this->id;
    }
}
